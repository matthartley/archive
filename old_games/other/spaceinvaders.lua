test = "no"
function test(text, x, y)
	love.graphics.print(text, x, y)
end

count = 0

function collisionCheck(onex, oney, onew, oneh, twox, twoy, twow, twoh)
	if onex > twox + twow or 
	onex + onew < twox or
	oney > twoy + twoh or
	oney + oneh < twoy then
		return false
	else
		return true
	end
end

function love.load()
	--HERO
	hero = {}
	hero.x = 300
	hero.y = 450
	hero.speed = 300
	hero.shots = {}

	--ENEMIES
	enemies = {}

	--1ST ROW
	for i=1, 14 do
		enemy = {}
		enemy.width = 40
		enemy.height = 20
		enemy.x = i * (enemy.width + 10) + 10
		enemy.y = enemy.height + 200
		table.insert(enemies, enemy)
	end

	--2ND ROW
	for i=1, 14 do
		enemy = {}
		enemy.width = 40
		enemy.height = 20
		enemy.x = i * (enemy.width + 10) + 10
		enemy.y = enemy.height + 150
		table.insert(enemies, enemy)
	end

	--3RD ROW
	for i=1, 14 do
		enemy = {}
		enemy.width = 40
		enemy.height = 20
		enemy.x = i * (enemy.width + 10) + 10
		enemy.y = enemy.height + 100
		table.insert(enemies, enemy)
	end

	--4TH ROW
	for i=1, 14 do
		enemy = {}
		enemy.width = 40
		enemy.height = 20
		enemy.x = i * (enemy.width + 10) + 10
		enemy.y = enemy.height + 50
		table.insert(enemies, enemy)
	end
end

function shoot()
	local shot = {}
	shot.x = hero.x + 15
	shot.y = hero.y
	table.insert(hero.shots, shot)
end

function love.keyreleased(key)
	if(key == " ") then
		shoot()
	end
end

function love.update(dt)
	--CONTROLS
	if love.keyboard.isDown("left") then
		hero.x = hero.x - hero.speed*dt
	elseif love.keyboard.isDown("right") then
		hero.x = hero.x + hero.speed*dt
	end

	--ENEMIES
	for i, v in ipairs(enemies) do
		v.y = v.y + dt*10
	end

	--SHOTPHISICS
	local remEnemy = {}
	local remShot = {}

	for i,v in ipairs(hero.shots) do
		v.y = v.y - dt * 500

		if v.y < 1 then
			table.insert(remShot, i)
		end

		for ii,vv in ipairs(enemies) do
			if collisionCheck(v.x, v.y, 2, 5, vv.x, vv.y, vv.width, vv.height) then
				table.insert(remEnemy, ii)
				table.insert(remShot, i)
				count = count + 1
			end
		end
	end

	for i,v in ipairs(remEnemy) do
		table.remove(enemies, v)
	end

	for i,v in ipairs(remShot) do
		table.remove(hero.shots, v)
	end
end--function end

function love.draw()
	--DRAW GROUND
	love.graphics.setColor(0, 255, 0, 255)
	love.graphics.rectangle("fill", 0, 465, 800, 150)

	--DRAW ENEMIES
	for i,v in ipairs(enemies) do

		if v.y > 445 then
			love.graphics.print("Game Over", 100, 100)
		else
			--DRAW HERO
			love.graphics.setColor(255, 0, 0, 255)
			love.graphics.rectangle("fill", hero.x, hero.y, 30, 15)
			love.graphics.rectangle("fill", hero.x + 14, hero.y - 5, 2, 5)

			--DRAW ENEMIES
			love.graphics.setColor(0, 255, 255, 255)
			love.graphics.rectangle("fill", v.x, v.y, v.width, v.height)
		end
	end

	--DRAW SHOTS
	love.graphics.setColor(255, 255, 255, 255)
	for i,v in ipairs(hero.shots) do
		love.graphics.rectangle("fill", v.x, v.y, 2, 5)
	end

	--DRAW TESTS
	if(test == "yes") then
		test(10, 10)
	end

	--WIN
	if count == 56 then
		love.graphics.print("YOU WIN!!!", 100, 100)
	end
end