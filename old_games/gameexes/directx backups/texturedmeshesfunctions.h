#include <windows.h>
#include <windowsx.h>
#include <d3d9.h>
#include <d3dx9.h>

struct CUSTOMVERTEX{FLOAT x, y, z; D3DVECTOR NORMAL; FLOAT U, V;};
#define CUSTOMFVF (D3DFVF_XYZ/*RHW*/ | D3DFVF_NORMAL | D3DFVF_TEX1)
/*typedef struct D3DVECTOR{
	float x, y, z;
}D3DVECTOR, *LPD3DVECTOR;*/

LPDIRECT3DVERTEXBUFFER9 v_buffer;

#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080
//800 x 600
//1024 x 768
//1152 x 864
//1280 x 1024
//1600 x 1200
//1440 x 900
//1680 x 1050
//1920 x 1080
//1920 x 1200
//2560 x 1600

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

LPDIRECT3D9 d3d;
LPDIRECT3DDEVICE9 d3ddev;
LPDIRECT3DTEXTURE9 blocktexture;
LPD3DXMESH meshSpaceship;
DWORD numMaterials;
D3DMATERIAL9* material;
LPDIRECT3DTEXTURE9* texture;

///////////////////////////////
void init_graphics(void);
void init_light(void);

void initD3D(HWND hWnd){
	d3d = Direct3DCreate9(D3D_SDK_VERSION);

	D3DPRESENT_PARAMETERS d3dpp;

	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;
	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
	d3dpp.BackBufferWidth = SCREEN_WIDTH;
	d3dpp.BackBufferHeight = SCREEN_HEIGHT;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

	d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &d3ddev);

	init_graphics();
	init_light();

	d3ddev->SetRenderState(D3DRS_LIGHTING, TRUE);
	d3ddev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);
	d3ddev->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(100, 100, 100));
	d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
	d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	d3ddev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);

	d3ddev->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, 8);
	d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);
	d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);
	d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
}



void render_frame(void){
	d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 40, 100), 1.0f, 0);
	d3ddev->Clear(0 ,NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

	d3ddev->BeginScene();
	d3ddev->SetFVF(CUSTOMFVF);

		static float index = 0.0f; index+=0.03f;

		//view matrix
		D3DXMATRIX matView;
		D3DXMatrixLookAtLH(&matView,
		&D3DXVECTOR3(/*(float)sin(index) * 4*/0.0f, 0.0f, 20.0f),
		&D3DXVECTOR3(0.0f, 0.0f, 0.0f),
		&D3DXVECTOR3(0.0f, 1.0f, 0.0f));
		d3ddev->SetTransform(D3DTS_VIEW, &matView);

		//projection matrix
		D3DXMATRIX matProjection;
		D3DXMatrixPerspectiveFovLH(&matProjection,
		D3DXToRadian(45),
		(float)SCREEN_WIDTH / (float)SCREEN_HEIGHT,
		1.0f,
		100.0f);
		d3ddev->SetTransform(D3DTS_PROJECTION, &matProjection);

		d3ddev->SetStreamSource(0, v_buffer, 0, sizeof(CUSTOMVERTEX));

		//world transforms and draws
		//D3DXMATRIX matRotate;
		//D3DXMatrixRotationY(&matRotate, index);

		//D3DXMATRIX matTranslate;
		//D3DXMatrixTranslation(&matTranslate, 0.0f, 0.0f, 0.0f);
		//d3ddev->SetTransform(D3DTS_WORLD, &(matTranslate));

		////////////////CUBE///////////////////
		//d3ddev->SetTexture(0, blocktexture);

		D3DXMATRIX matTranslate;
		D3DXMatrixTranslation(&matTranslate, 3.0f, -3.0f, -10.0f);
		D3DXMATRIX matRotate;
		D3DXMatrixRotationY(&matRotate, index);
		D3DXMATRIX matRotateX;
		D3DXMatrixRotationX(&matRotateX, index);
		d3ddev->SetTransform(D3DTS_WORLD, &(matTranslate));

		/*d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 4, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 8, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 12, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 16, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 20, 2);

		//////////////CUBE 2///////////////////

		D3DXMatrixTranslation(&matTranslate, 3.0f, 3.0f, -10.0f);
		d3ddev->SetTransform(D3DTS_WORLD, &(matTranslate));

		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 4, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 8, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 12, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 16, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 20, 2);

		//////////////CUBE 3////////////////
		D3DXMatrixTranslation(&matTranslate, -3.0f, -3.0f, -10.0f);
		d3ddev->SetTransform(D3DTS_WORLD, &(matTranslate));

		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 4, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 8, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 12, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 16, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 20, 2);

		/////////////////////CUBE 4/////////////////////
		D3DXMatrixTranslation(&matTranslate, -3.0f, 3.0f, -10.0f);
		d3ddev->SetTransform(D3DTS_WORLD, &(matTranslate));

		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 4, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 8, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 12, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 16, 2);
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 20, 2);*/

		////////SPACESHIP////////////
		D3DXMatrixTranslation(&matTranslate, 0.0f, 0.0f, 0.0f);
		d3ddev->SetTransform(D3DTS_WORLD, &(matRotate * matTranslate));

		for(DWORD i = 0; i < numMaterials; i++){
			d3ddev->SetMaterial(&material[i]);

			if(texture[i] != NULL){
				d3ddev->SetTexture(0, texture[i]);
			}
			meshSpaceship->DrawSubset(i);
		}

	d3ddev->EndScene();
	d3ddev->Present(NULL, NULL, NULL, NULL);
}

void cleanD3D(void){
	v_buffer->Release();
	blocktexture->Release();
	meshSpaceship->Release();
	d3ddev->Release();
	d3d->Release();
}

void init_graphics(void){
	D3DXCreateTextureFromFile(d3ddev, L"textures/brick.jpg", &blocktexture);

	LPD3DXBUFFER bufShipMaterial;
	D3DXLoadMeshFromX(L"airplane.x",
	D3DXMESH_SYSTEMMEM,
	d3ddev,
	NULL,
	&bufShipMaterial,
	NULL,
	&numMaterials,
	&meshSpaceship);

	D3DXMATERIAL* tempMaterials = (D3DXMATERIAL*)bufShipMaterial->GetBufferPointer();
	material = new D3DMATERIAL9[numMaterials];
	texture = new LPDIRECT3DTEXTURE9[numMaterials];

	for(DWORD i = 0; i < numMaterials; i++){
		material[i] = tempMaterials[i].MatD3D;
		material[i].Ambient = material[i].Diffuse;
		if(FAILED(D3DXCreateTextureFromFileA(d3ddev, tempMaterials[i].pTextureFilename, &texture[i]))){
			texture[i] = NULL;
		}
	}

	int alpha = 255;
	CUSTOMVERTEX vertices[] = {
		//front face 0
		{3.0f, 3.0f, 3.0f, 0.0f, 0.0f, 1.0f, /*D3DCOLOR_ARGB(alpha, 0, 255, 255)*/ 0.0f, 0.0f},
		{-3.0f, 3.0f, 3.0f, 0.0f, 0.0f, 1.0f, /*D3DCOLOR_ARGB(alpha, 0, 255, 255)*/ 1.0f, 0.0f},
		{3.0f, -3.0f, 3.0f, 0.0f, 0.0f, 1.0f, /*D3DCOLOR_ARGB(alpha, 0, 255, 255)*/ 0.0f, 1.0f},
		{-3.0f, -3.0f, 3.0f, 0.0f, 0.0f, 1.0f,/*D3DCOLOR_ARGB(alpha, 0, 255, 255)*/ 1.0f, 1.0f},

		//back face 4
		{-3.0f, 3.0f, -3.0f, 0.0f, 0.0f, -1.0f /*D3DCOLOR_ARGB(alpha, 255, 255, 0),*/ , 0.0f, 0.0f},
		{3.0f, 3.0f, -3.0f, 0.0f, 0.0f, -1.0f /*D3DCOLOR_ARGB(alpha, 255, 255, 0),*/ , 1.0f, 0.0f},
		{-3.0f, -3.0f, -3.0f, 0.0f, 0.0f, -1.0f/* D3DCOLOR_ARGB(alpha, 255, 255, 0),*/ , 0.0f, 1.0f},
		{3.0f, -3.0f, -3.0f, 0.0f, 0.0f, -1.0f/* D3DCOLOR_ARGB(alpha, 255, 255, 0),*/ , 1.0f, 1.0f},

		//right face 8
		{-3.0f, 3.0f, 3.0f, -1.0f, 0.0f, 0.0f/* D3DCOLOR_ARGB(alpha, 255, 0, 255),*/ , 0.0f, 0.0f},
		{-3.0f, 3.0f, -3.0f, -1.0f, 0.0f, 0.0f/* D3DCOLOR_ARGB(alpha, 255, 0, 255),*/ , 1.0f, 0.0f},
		{-3.0f, -3.0f, 3.0f, -1.0f, 0.0f, 0.0f/* D3DCOLOR_ARGB(alpha, 255, 0, 255),*/ , 0.0f, 1.0f},
		{-3.0f, -3.0f, -3.0f, -1.0f, 0.0f, 0.0f/* D3DCOLOR_ARGB(alpha, 255, 0, 255),*/ , 1.0f, 1.0f},

		//left face 12
		{3.0f, 3.0f, -3.0f, 1.0f, 0.0f, 0.0f/*D3DCOLOR_ARGB(alpha, 255, 255, 255),*/ , 0.0f, 0.0f},
		{3.0f, 3.0f, 3.0f, 1.0f, 0.0f, 0.0f /*D3DCOLOR_ARGB(alpha, 255, 255, 255), */, 1.0f, 0.0f},
		{3.0f, -3.0f, -3.0f, 1.0f, 0.0f, 0.0f /*D3DCOLOR_ARGB(alpha, 255, 255, 255),*/ , 0.0f, 1.0f},
		{3.0f, -3.0f, 3.0f, 1.0f, 0.0f, 0.0f /*D3DCOLOR_ARGB(alpha, 255, 255, 255),*/ , 1.0f, 1.0f},
		
		//top face 16
		{3.0f, 3.0f, -3.0f, 0.0f, 1.0f, 0.0f /*D3DCOLOR_ARGB(alpha, 0, 255, 0),*/ , 0.0f, 0.0f},
		{-3.0f, 3.0f, -3.0f, 0.0f, 1.0f, 0.0f/* D3DCOLOR_ARGB(alpha, 0, 255, 0),*/ , 1.0f, 0.0f},
		{3.0f, 3.0f, 3.0f, 0.0f, 1.0f, 0.0f /*D3DCOLOR_ARGB(alpha, 0, 255, 0),*/ , 0.0f, 1.0f},
		{-3.0f, 3.0f, 3.0f, 0.0f, 1.0f, 0.0f /*D3DCOLOR_ARGB(alpha, 0, 255, 0), */, 1.0f, 1.0f},

		//bottom face 20
		{-3.0f, -3.0f, -3.0f, 0.0f, -1.0f, 0.0f/*D3DCOLOR_ARGB(alpha, 0, 255, 0), */, 0.0f, 0.0f},
		{3.0f, -3.0f, -3.0f, 0.0f, -1.0f, 0.0f /*D3DCOLOR_ARGB(alpha, 0, 255, 0), */, 1.0f, 0.0f},
		{-3.0f, -3.0f, 3.0f, 0.0f, -1.0f, 0.0f/* D3DCOLOR_ARGB(alpha, 0, 255, 0),*/ , 0.0f, 1.0f},
		{3.0f, -3.0f, 3.0f, 0.0f, -1.0f, 0.0f /*D3DCOLOR_ARGB(alpha, 0, 255, 0),*/ , 1.0f, 1.0f},

	};

	//create vertex buffer
	d3ddev->CreateVertexBuffer(24 * sizeof(CUSTOMVERTEX),
	0,
	CUSTOMFVF,
	D3DPOOL_MANAGED,
	&v_buffer,
	NULL);

	VOID* pVoid;

	//lock buffer and load vertices into it
	v_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	v_buffer->Unlock();
}

void init_light(void){
	D3DLIGHT9 light;
	D3DMATERIAL9 material;

	ZeroMemory(&light, sizeof(light));
	light.Type = D3DLIGHT_POINT/*DIRECTIONAL*//*SPOT*/;
	light.Diffuse = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	light./*Direction*/Position = D3DXVECTOR3(0.0f, 0.0f, 30.0f);
	light.Range = 100.0f;
	light.Attenuation0 = 0.0f;
	light.Attenuation1 = 0.125f;
	light.Attenuation2 = 0.0f;
	light.Phi = D3DXToRadian(40.0f);
	light.Theta = D3DXToRadian(20.0f);
	light.Falloff = 1.0f;

	d3ddev->SetLight(0, &light);
	d3ddev->LightEnable(0, TRUE);

	ZeroMemory(&material, sizeof(D3DMATERIAL9));
	material.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	material.Ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	d3ddev->SetMaterial(&material);
} 