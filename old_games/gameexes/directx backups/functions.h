#include <windows.h>
#include <windowsx.h>
#include <d3d9.h>
#include <d3dx9.h>

#define CUSTOMFVF (D3DFVF_XYZ/*RHW*/ | D3DFVF_NORMAL)
struct CUSTOMVERTEX{
	FLOAT x, y, z, rhw;
	D3DVECTOR NORMAL;
};
/*typedef struct D3DVECTOR{
	float x, y, z;
}D3DVECTOR, *LPD3DVECTOR;*/

LPDIRECT3DVERTEXBUFFER9 v_buffer;
LPDIRECT3DINDEXBUFFER9 i_buffer;


#define SCREEN_WIDTH 1440
#define SCREEN_HEIGHT 900
//800 x 600
//1024 x 768
//1152 x 864
//1280 x 1024
//1600 x 1200
//1440 x 900
//1680 x 1050
//1920 x 1080
//1920 x 1200
//2560 x 1600

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

LPDIRECT3D9 d3d;
LPDIRECT3DDEVICE9 d3ddev;

///////////////////////////////
void init_graphics(void);
void init_light(void);

void initD3D(HWND hWnd){
	d3d = Direct3DCreate9(D3D_SDK_VERSION);

	D3DPRESENT_PARAMETERS d3dpp;

	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;
	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
	d3dpp.BackBufferWidth = SCREEN_WIDTH;
	d3dpp.BackBufferHeight = SCREEN_HEIGHT;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

	d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &d3ddev);

	init_graphics();
	init_light();

	d3ddev->SetRenderState(D3DRS_LIGHTING, TRUE);
	d3ddev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);
	d3ddev->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(50, 50, 50));
	d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
	d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	d3ddev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
}



void render_frame(void){
	d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 40, 100), 1.0f, 0);
	d3ddev->Clear(0 ,NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

	d3ddev->BeginScene();
		d3ddev->SetFVF(CUSTOMFVF);

		//view transform
			D3DXMATRIX matView;
			D3DXMatrixLookAtLH(&matView,
				&D3DXVECTOR3 (0.0f, 0.0f, 50.0f),
				&D3DXVECTOR3 (0.0f, 0.0f, 0.0f),
				&D3DXVECTOR3 (0.0f, 1.0f, 0.0f));
			d3ddev->SetTransform(D3DTS_VIEW, &matView);

			//projection transform
			D3DXMATRIX matProjection;
			D3DXMatrixPerspectiveFovLH(&matProjection,
				D3DXToRadian(45),
				(FLOAT)SCREEN_WIDTH / (FLOAT)SCREEN_HEIGHT,
				1.0f,
				100.0f);
			d3ddev->SetTransform(D3DTS_PROJECTION, &matProjection);

		d3ddev->SetStreamSource(0, v_buffer, 0, sizeof(CUSTOMVERTEX));

			static float index = 0.0f; index+=0.04f;
			static float other = 0.0f; other-=0.04f;

			//world transform
			//translate
			//D3DXMATRIX matTranslate;
			//D3DXMatrixTranslation(&matTranslate, 120.f, 4.0f, 0.0f);
			//rotate
			/*D3DXMATRIX matRotateY;
			D3DXMatrixRotationY(&matRotateY, index);
			//scale
			D3DXMATRIX matScale;
			D3DXMatrixScaling(&matScale, 1.5f, 1.5f, 1.5f);

			d3ddev->SetTransform(D3DTS_WORLD, &(matRotateY));
		d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);*/

			//square one
			D3DXMATRIX matRotateY;
			D3DXMatrixRotationY(&matRotateY, index);
			D3DXMATRIX matRotate180x;
			D3DXMatrixRotationX(&matRotate180x, D3DXToRadian(180));
			D3DXMATRIX matRotate180y;
			D3DXMatrixRotationY(&matRotate180y, D3DXToRadian(180));
			D3DXMATRIX TranslateBack;
			D3DXMatrixTranslation(&TranslateBack, 5.0f, 0.0f, 10.0f);
			D3DXMATRIX Translate;
			D3DXMatrixTranslation(&Translate, 0.0f, 0.0f, -3.0f);

			d3ddev->SetTransform(D3DTS_WORLD, &(/*matRotate180x * matRotate180y **/Translate * matRotateY * TranslateBack));
			d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

		//square two
			D3DXMatrixRotationY(&matRotateY, other);
			D3DXMatrixRotationX(&matRotate180x, D3DXToRadian(180));
			D3DXMatrixRotationY(&matRotate180y, D3DXToRadian(180));
			D3DXMatrixTranslation(&TranslateBack, 5.0f, 0.0f, 20.0f);
			D3DXMatrixTranslation(&Translate, 0.0f, 0.0f, -3.0f);

			d3ddev->SetTransform(D3DTS_WORLD, &(/*matRotate180x * matRotate180y **/Translate * matRotateY * TranslateBack));
			d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

		//cube
		static float rote = 0.0f; rote+=0.01f;
		D3DXMATRIX translateSort;
		D3DXMatrixTranslation(&translateSort, 0.0f, 0.0f, 3.0f );
		D3DXMATRIX rotate;
		D3DXMatrixRotationY(&rotate, index);
		D3DXMATRIX rota;
		D3DXMatrixRotationX(&rota, rote);
		D3DXMATRIX translate;
		D3DXMatrixTranslation(&translate, -8.0f, 0.0f, 20.0f );
		d3ddev->SetTransform(D3DTS_WORLD, &(/*translateSort */ rotate * rota * translate));
		d3ddev->SetIndices(i_buffer);
		d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 24, 0, 12);

	d3ddev->EndScene();
	d3ddev->Present(NULL, NULL, NULL, NULL);
}

void cleanD3D(void){
	v_buffer->Release();
	d3ddev->Release();
	d3d->Release();
}

void init_graphics(void){
	int alpha = 255;
	CUSTOMVERTEX vertices[] = {
		//side 1
		{-3.0f, -3.0f, 3.0f, 0.0f, 0.0f, 1.0f, D3DCOLOR_ARGB(alpha, 0, 255, 255)},
		{3.0f, -3.0f, 3.0f, 0.0f, 0.0f, 1.0f, D3DCOLOR_ARGB(alpha, 255, 0, 255)},
		{-3.0f, 3.0f, 3.0f, 0.0f, 0.0f, 1.0f, D3DCOLOR_ARGB(alpha, 255, 255, 0)},
		{3.0f, 3.0f, 3.0f, 0.0f, 0.0f, 1.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},

		//side 2
		{-3.0f, -3.0f, -3.0f, 0.0f, 0.0f, -1.0f, D3DCOLOR_ARGB(alpha, 255, 0, 255)},
		{-3.0f, 3.0f, -3.0f, 0.0f, 0.0f, -1.0f, D3DCOLOR_ARGB(alpha, 0, 255, 255)},
		{3.0f, -3.0f, -3.0f, 0.0f, 0.0f, -1.0f, D3DCOLOR_ARGB(alpha, 255, 255, 0)},
		{3.0f, 3.0f, -3.0f, 0.0f, 0.0f, -1.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},

		//side 3
		{-3.0f, 3.0f, -3.0f, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{-3.0f, 3.0f, 3.0f, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{3.0f, 3.0f, -3.0f, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{3.0f, 3.0f, 3.0f, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},

		//side 4
		{-3.0f, -3.0f, -3.0f, 0.0f, -1.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{3.0f, -3.0f, -3.0f, 0.0f, -1.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{-3.0f, -3.0f, 3.0f, 0.0f, -1.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{3.0f, -3.0f, 3.0f, 0.0f, -1.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},

		//side 5
		{3.0f, -3.0f, -3.0f, 1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{3.0f, 3.0f, -3.0f, 1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{3.0f, -3.0f, 3.0f, 1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{3.0f, 3.0f, 3.0f, 1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},

		//side 6
		{-3.0f, -3.0f, -3.0f, -1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{-3.0f, -3.0f, 3.0f, -1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{-3.0f, 3.0f, -3.0f, -1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
		{-3.0f, 3.0f, 3.0f, -1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(alpha, 255, 255, 255)},
	};

	d3ddev->CreateVertexBuffer(24*sizeof(CUSTOMVERTEX), 0,
    CUSTOMFVF, D3DPOOL_MANAGED, &v_buffer, NULL);
	VOID* pVoid;
	v_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	v_buffer->Unlock();

	short indices[] = {
		0, 1, 2,//side 1
		2, 1, 3,
		4, 5, 6,//side 2
		6, 5, 7,
		8, 9, 10,//side 3
		10, 9, 11,
		12, 13, 14,//side 4
		14, 13, 15,
		16, 17, 18,//side 5
		18, 17, 19,
		20, 21, 22,//side 6
		22, 21, 23,
	};

	d3ddev->CreateIndexBuffer(36*sizeof(short), 0, D3DFMT_INDEX16, D3DPOOL_MANAGED, &i_buffer, NULL);
	i_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, indices, sizeof(indices));
	i_buffer->Unlock();
}

void init_light(void){
	D3DLIGHT9 light;
	D3DMATERIAL9 material;

	ZeroMemory(&light, sizeof(light));
	light.Type = D3DLIGHT_POINT/*DIRECTIONAL*//*SPOT*/;
	light.Diffuse = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	light./*Direction*/Position = D3DXVECTOR3(0.0f, 0.0f, 30.0f);
	light.Range = 100.0f;
	light.Attenuation0 = 0.0f;
	light.Attenuation1 = 0.125f;
	light.Attenuation2 = 0.0f;
	light.Phi = D3DXToRadian(40.0f);
	light.Theta = D3DXToRadian(20.0f);
	light.Falloff = 1.0f;

	d3ddev->SetLight(0, &light);
	d3ddev->LightEnable(0, TRUE);

	ZeroMemory(&material, sizeof(D3DMATERIAL9));
	material.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	material.Ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

	d3ddev->SetMaterial(&material);
}
