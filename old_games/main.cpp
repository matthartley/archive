#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <cmath>
#include <ctime>
using namespace std;

int damage();
int damage2();
void enemy();

void fight(){
    ofstream healthFileOutput("health.txt");
    healthFileOutput << "100";
    healthFileOutput.close();

    int health;
    ifstream healthFileInput("health.txt");
    healthFileInput >> health;
    healthFileInput.close();

    enemy();
    int enemyHealth = 100;

    while(health > 0){
        if(enemyHealth < 1){
            break;
        }
        cout << "///////////////////////////////////////////////////////////////////////////////" << endl;
        cout << "Type (attack) to attack the enemy or type (fallback) to return to the main menu" << endl;
        string choice;
        cin >> choice;
        cout << "" << endl;
        if(choice == "attack"){
            //attack
            int yourDamage = damage();
            enemyHealth = enemyHealth - yourDamage;
            cout << "You deal " << yourDamage << " damage points to your enemy" << endl;
            cout << "Your enemy now has " << enemyHealth << " health" << endl;
            cout << "" << endl;
            if(enemyHealth < 1){
                break;
            }
            cout << "Enemy's turn..." << endl;
            int enemyDamage = damage2();
            health = health - enemyDamage;
            cout << "Your enemy deals " << enemyDamage << " damage points to you" << endl;
            cout << "You now have " << health << " health" << endl;
            cout << "" << endl;
        }else if(choice == "fallback"){
            //return to menu
            break;
        }
    }

    if(health < 1){
        cout << "You are dead!!!" << endl;
        cout << "Returning to menu..." << endl;
        cout << "" << endl;
        //cout << "Type (fight) to start a new fight or type (menu) to return to the menu" << endl;
        //string choice;
        //cin >> choice;
    }else if(health > 0 && enemyHealth > 0){
        cout << "You have retreated" << endl;
        cout << "Returning to menu..." << endl;
        cout << "" << endl;
    }else{
        cout << "You have won the battle!!!" << endl;
        cout << "Returning to menu..." << endl;
        cout << "" << endl;
    }
}

int damage(){
    srand(time(0));
    int damage = 1+(rand()%20);
    return damage;
}

int damage2(){
    srand(53+(time(0)));
    int damage2 = 1+(rand()%20);
    return damage2;
}

void enemy(){
    srand(time(0));
    int random = 1+(rand()%8);
    switch(random){
        case 1:
            //call enemy type NINJA
            cout << "A Ninja jumps out of a near by bush to confront you on your path!" << endl;
        break;
        case 2:
            //call enemy type ZOMBIE
            cout << "A Zombie walks out of the mist in front of you!" << endl;
        break;
        case 3:
            //call enemy type SKAG
            cout << "A hungry Skag approaches looking for a meal!" << endl;
        break;
        case 4:
            //call enemy type skeleton
            cout << "A skeleton raises out the ground to confront you!" << endl;
        break;
        case 5:
            //call enemy type wizard
            cout << "An evil wizard creates a magical sheild to block your path!" << endl;
        break;
        case 6:
            //call enemy type wolf
            cout << "A hungry Wolf approaches looking for a meal!" << endl;
        break;
        case 7:
            //call enemy type ent
            cout << "A giant Ent walks out of the forest to stomp on you!" << endl;
        break;
        case 8:
            //call enemy type mac
            cout << "A macbookpro approaches and says 'you can pass if you right click with me'!" << endl;
        break;
    }
}

int num = 53;
int main(){
while(num == 53){
    string menuChoice;
    cout << "MAIN MENU" << endl;
    cout << "Type (fight) to go into battle or type (exit) to quit the programme" << endl;
    cin >> menuChoice;
    cout << "" << endl;
    if(menuChoice == "fight"){
        fight();
    }else if(menuChoice == "exit"){
        break;
    }
}
return 0;}
