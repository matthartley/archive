package giantjelly;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.JsePlatform;

import java.io.File;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.LWJGLException;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;
import java.io.IOException;

public class Game {

	//public static String PATH = "";
	//public static String SRCPATH = "";
	//public static String RESPATH = "";

	//public static Texture font;

	public static void main(String[] args) {
		//String path = new File(".").getAbsolutePath()+"/";
		//path += "lua/";
		//System.out.println(path);

		//PATH = path;

		LuaValue lv = JsePlatform.standardGlobals();

		lv.get("dofile").call(LuaValue.valueOf( "config.lua" ));
		//SRCPATH = ""+lv.get("srcpath");
		//RESPATH = ""+lv.get("respath");
		//LuaValue test = lv.get("src");
		//System.out.println(SRCPATH + " - " + RESPATH);

		String src = ""+lv.get("srcpath");
		System.out.println("Src: " + src);

		String lua = src + "/Game.lua";
		lv.get("dofile").call(LuaValue.valueOf( lua ));
	}

}