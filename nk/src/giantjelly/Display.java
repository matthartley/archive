package giantjelly;

import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.LWJGLException;
import giantjelly.gl;

public class Display {

	public static boolean running = true;

	public static void createdisplay(DisplayMode dm, boolean fullscreen) {
		try{
			org.lwjgl.opengl.Display.setDisplayMode(dm);
			if(fullscreen){
				org.lwjgl.opengl.Display.setDisplayMode(org.lwjgl.opengl.Display.getDesktopDisplayMode());
				org.lwjgl.opengl.Display.setFullscreen(true);
			}
			org.lwjgl.opengl.Display.create();
		}catch(LWJGLException e){}

		gl.setcanvas(org.lwjgl.opengl.Display.getWidth(), org.lwjgl.opengl.Display.getHeight());
	}

	public static boolean iscloserequested() {
		return (org.lwjgl.opengl.Display.isCloseRequested()) ? true : false;
	}

	public static void update() {
		org.lwjgl.opengl.Display.update();
	}

	public static int getwidth() {
		return org.lwjgl.opengl.Display.getWidth();
	}

	public static int getheight() {
		return org.lwjgl.opengl.Display.getHeight();
	}

	public static void sync(int frames) {
		org.lwjgl.opengl.Display.sync(frames);
	}

}