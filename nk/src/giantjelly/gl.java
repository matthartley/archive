package giantjelly;

import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;
import java.io.IOException;

public class gl {

	public gl() {
	}

	public static void setcanvas(int width, int height) {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, width, height, 0, 10, -10);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_DEPTH_TEST);
		
		glAlphaFunc(GL_GREATER, 0.1f);
		glEnable(GL_ALPHA_TEST);
	}

	public static void enabletextures(boolean tex) {
		if(tex)
			glEnable(GL_TEXTURE_2D);
		else
			glDisable(GL_TEXTURE_2D);
	}

	public static void color(double r, double g, double b, double a) {
		glColor4d(r, g, b, a);
	}

	public static void vertex(double x, double y, double z) {
		glVertex3d(x, y, z);
	}

	public static void texcoord(double x, double y) {
		glTexCoord2d(x, y);
	}

	public static void BEGIN() {
		glBegin(GL_QUADS);
	}

	public static void END() {
		glEnd();
	}

	//public static String workspacepath = "";

	public static Texture loadtexture(String file) {
		try{
			return TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(file), GL_NEAREST);
		}catch(IOException e){
		}

		return null;
	}

	public static void bindtexture(Texture texture) {
		//Game.font.bind();
		glBindTexture(GL_TEXTURE_2D, texture.getTextureID());
	}

	public static void clear() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	public static void push() {
		glPushMatrix();
	}

	public static void pop() {
		glPopMatrix();
	}

	public static void translate(int x, int y) {
		glTranslated(x, y, 0);
	}

}