Terrain = {}

Terrain.coords = {
	--{ x1=0, y1=0, x2=32, y2=0, x3=32, y3=64, x4=0, y4=64 }
	{ yleft = 0, yright = 0 }
}

Terrain.points = 128
Terrain.pointwidth = 16

function Terrain.load()
	--table.insert(Terrain.coords, { x1=0, y1=0, x2=32, y2=0, x3=32, y3=64, x4=0, y4=64 })

	for i=2,Terrain.points do
		table.insert(Terrain.coords, {})
	end

	print(#Terrain.coords)

	last = 0

	for i=2,Terrain.points do
		rand = last + (math.random()-0.5)*16
		if rand < 0 then
			rand = rand * -1
		end
		--Terrain.coords[i] = { x1=32*i, y1=-rand, x2=32+32*i, y2=0, x3=32+32*i, y3=64, x4=32*i, y4=64 }
		--Terrain.coords[i-1].y2 = -rand
		Terrain.coords[i] = { yleft = -rand, yright = 0 }
		Terrain.coords[i-1].yright = -rand
		last = rand
	end
end

function Terrain.render()
	for i=1,Terrain.points do
		local size = Terrain.pointwidth*(i-1)

		Graphics.draw({
			x1 = size,
			y1 = (Display:getheight()-64)+Terrain.coords[i].yleft,
			x2 = Terrain.pointwidth+size,
			y2 = (Display:getheight()-64)+Terrain.coords[i].yright,
			x3 = Terrain.pointwidth+size,
			y3 = Display:getheight(),
			x4 = size,
			y4 = Display:getheight(),
			}, { r=131/255,  g=181/255, b=49/255, a=1 }) --91/255 36/255 0
	end
end