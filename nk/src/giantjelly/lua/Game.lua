print("Hello World from LUA!");


dofile(srcpath .. "lib/Main.lua")


dofile(srcpath .. "Terrain.lua");

Terrain.load()

function render()
	Graphics.color({1, 1, 1, 1})

	Font.draw(Font.LARGE, "hello world", 10, 10)
	Font.draw(Font.MEDIUM, "hello world", 10, 10+32)
	Font.draw(Font.SMALL, "hello world", 10, 10+48)

	Terrain.render()
end

Main.start(render)