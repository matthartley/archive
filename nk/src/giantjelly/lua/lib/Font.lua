Font = {}

Font.SMALL = 1;
Font.MEDIUM = 2;
Font.LARGE = 4;

Font.characters = "abcdefghijklmnopqrstuvwxyz "

function Font.draw(size, text, xx, yy)
	for i=0,Text:stringlength(text)-1 do
		Graphics.drawtile(8, Text:characterindex(Font.characters, text, i), { x=(xx+(8*size)*i), y=yy, z=0, size=size })
		--print(Text:characterindex(Font.characters, text, i))
	end
end