Graphics = {
	
	

}

Graphics.TEXTURE = gl:loadtexture(respath .. "Font.png")
--Graphics.font = gl:loadtexture(respath .. "Font.png")

function Graphics.loadtexture(file)
	return gl:loadtexture(respath .. file)
end

function Graphics.draw(coords, color)
	gl:enabletextures(false);
	gl:color(color.r, color.g, color.b, color.a);

	gl:BEGIN();
		gl:vertex(coords.x1, coords.y1, 0);
		gl:vertex(coords.x2, coords.y2, 0);
		gl:color(50/255, 20/255, 0)
		gl:vertex(coords.x3, coords.y3, 0);
		gl:vertex(coords.x4, coords.y4, 0);
	gl:END();
end

function Graphics.drawsprite(coords, pos)
	gl:enabletextures(true);
	--texture:bind();
	gl:bindtexture(Graphics.TEXTURE);
	--gl:color(1, 0, 1, 1);

	width = Graphics.TEXTURE:getImageWidth();
	height = Graphics.TEXTURE:getImageHeight();

	gl:BEGIN()

		gl:texcoord( coords.x/width, coords.y/height )
		gl:vertex( pos.x, pos.y, pos.z )

		gl:texcoord( (coords.x+coords.width)/width, coords.y/height )
		gl:vertex( pos.x+coords.width*pos.size, pos.y, pos.z )

		gl:texcoord( (coords.x+coords.width)/width, (coords.y+coords.height)/height )
		gl:vertex( pos.x+coords.width*pos.size, pos.y+coords.height*pos.size, pos.z )

		gl:texcoord( coords.x/width, (coords.y+coords.height)/height )
		gl:vertex( pos.x, pos.y+coords.height*pos.size, pos.z )

	gl:END()

	--gl:BEGIN();
	--gl:END();
end

function Graphics.drawtile(size, tile, pos)
	local tiley = math.floor((tile*size)/Graphics.TEXTURE:getImageWidth())*size
	local tilex = (tile*size)%Graphics.TEXTURE:getImageWidth()

	Graphics.drawsprite({ x = tilex, y = tiley, width = size, height = size }, pos)
end

function Graphics.color(color)
	gl:color(color[1], color[2], color[3], color[4])
end