Display = luajava.newInstance("giantjelly.Display");
dm = luajava.newInstance("org.lwjgl.opengl.DisplayMode", 1280, 720);
gl = luajava.newInstance("giantjelly.gl");
Text = luajava.newInstance("giantjelly.text");

dofile("config.lua")

Display:createdisplay(dm);

dofile(libpath .. "Graphics.lua");
dofile(libpath .. "Font.lua");

function clear()
	gl:clear()
end

function update()
	Display:update()
end

function running()
	if Display:iscloserequested() then
		return false
	else
		return true
	end
end

Main = {
	
}

function Main.start(func)
	while running()==true do
		clear()

		func()

		update()
	end
end