package giantjelly;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class Input {

	public static boolean W() {
		return Keyboard.isKeyDown(Keyboard.KEY_W);
	}

	public static boolean A() {
		return Keyboard.isKeyDown(Keyboard.KEY_A);
	}
	public static boolean S() {
		return Keyboard.isKeyDown(Keyboard.KEY_S);
	}

	public static boolean D() {
		return Keyboard.isKeyDown(Keyboard.KEY_D);
	}

	public static boolean UP() {
		return Keyboard.isKeyDown(Keyboard.KEY_UP);
	}

	public static boolean LEFT() {
		return Keyboard.isKeyDown(Keyboard.KEY_LEFT);
	}
	public static boolean DOWN() {
		return Keyboard.isKeyDown(Keyboard.KEY_DOWN);
	}

	public static boolean RIGHT() {
		return Keyboard.isKeyDown(Keyboard.KEY_RIGHT);
	}


	public static boolean CLICK() {
		return Mouse.isButtonDown(0);
	}

	public static boolean RIGHTCLICK() {
		return Mouse.isButtonDown(1);
	}
	
	public static int getmousex() {
		return Mouse.getX();
	}
	
	public static int getmousey() {
		return Display.getHeight() - Mouse.getY();
	}

}