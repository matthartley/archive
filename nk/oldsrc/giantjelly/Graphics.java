package giantjelly;

import org.newdawn.slick.opengl.Texture;
import static org.lwjgl.opengl.GL11.*;

public class Graphics {

	public static Texture font;
	
	public static void draw(Texture texture, double[] coords, double[] pos) {
		glEnable(GL_TEXTURE_2D);
		texture.bind();
		glColor4d(1, 1, 1, 1);

		double width = texture.getImageWidth();
		double height = texture.getImageHeight();

		glBegin(GL_QUADS);

			glTexCoord2d(coords[0]/width, coords[1]/height);
			glVertex3d(pos[0], pos[1], pos[2]);

			glTexCoord2d((coords[0]+coords[2])/width, coords[1]/height);
			glVertex3d(pos[0]+coords[2]*pos[3], pos[1], pos[2]);

			glTexCoord2d((coords[0]+coords[3])/width, (coords[1]+coords[3])/height);
			glVertex3d(pos[0]+coords[2]*pos[3], pos[1]+coords[3]*pos[3], pos[2]);

			glTexCoord2d(coords[0]/width, (coords[1]+coords[3])/height);
			glVertex3d(pos[0], pos[1]+coords[3]*pos[3], pos[2]);

		glEnd();
	}

	public static void draw(int[] coords, double[] color) {
		glDisable(GL_TEXTURE_2D);
		glColor4d(color[0], color[1], color[2], color[3]);

		glBegin(GL_QUADS);
			glVertex3d(coords[0], coords[1], 0);
			glVertex3d(coords[2], coords[3], 0);
			glVertex3d(coords[4], coords[5], 0);
			glVertex3d(coords[6], coords[7], 0);
		glEnd();
	}

	public static void drawtile(Texture texture, int size, int tile, double[] pos) {
		int y = ((tile*size)/texture.getImageWidth())*size;
		int x = (tile*size)%texture.getImageWidth();

		draw(texture, new double[]{ x, y, size, size }, /*new double[]{  }*/ pos);
	}
	
}