package giantjelly;

import org.luaj.vm2.*;
import org.luaj.vm2.lib.jse.*;
import org.luaj.vm2.lib.ZeroArgFunction;

import org.lwjgl.opengl.Display;

public class Test extends ZeroArgFunction {

	public Test() {
	}

	public LuaValue call() {
		return valueOf(Display.getWidth());
	}

}