package giantjelly.gui;

import giantjelly.Graphics;

public class Font {

	public static int SMALL = 1;
	public static int MEDIUM = 2;
	public static int LARGE = 4;

	public static String characters = "abcdefghijklmnopqrstuvwxyz ";

	public static void draw(int size, String text, int x, int y) {
		/*Graphics.drawtile();*/
		for(int i = 0; i < text.length(); i++){
			Graphics.drawtile(Graphics.font, 8, characters.indexOf(text.charAt(i)), new double[]{ x+((8*size)*i), y, 0, size });
		}
	}

}