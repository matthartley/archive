package giantjelly;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;
import java.io.IOException;
import java.util.Random;

import giantjelly.gui.Font;
import giantjelly.Graphics;
import giantjelly.lua;
//import giantjelly.Test;

public class Game {

	public static int WIDTH = 1280;
	public static int HEIGHT = 720;
	public static boolean running = true;
	public static String workspacepath = "../";

	public static void main(String[] args) {
		//Terrain.load();

		System.out.println(Display.getTitle());

		lua.run();

		System.out.println("Lua has been run?!");

		try{
			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
			Display.setTitle("nk");
			Display.setResizable(false);
			Display.create();
		}catch(LWJGLException e){
		}

		try{
			Graphics.font = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(workspacepath+"res/Font.png"), GL_NEAREST);
		}catch(IOException e){
		}

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 10, -10);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		while(running && !Display.isCloseRequested()){
			run();
			Display.update();
			Display.sync(60);
		}
	}

	public static void run() {
		//Graphics.draw(new int[]{ 100, 100, 300, 100, 300, 300, 100, 300 }, new double[]{ 0, 1, 0.5, 1 });

		Terrain.draw();

		//Graphics.draw(Graphics.font, new double[]{ 0, 0, 16, 16 }, new double[]{ 10, 10, 0, 8 });
		Graphics.drawtile(Graphics.font, 8, 8, new double[]{ 10, 10, 0, 2 });
		Font.draw(Font.LARGE, "hello world", 100, 100);
	}

	public static void size() {
		System.out.println(WIDTH+" - "+HEIGHT);
	}

}