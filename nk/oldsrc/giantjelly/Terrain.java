package giantjelly;

import java.util.Random;

import giantjelly.Graphics;

public class Terrain {

	public static int[] coords;

	public static void load() {
		coords = new int[128];
		Random r = new Random();
		for(int i = 0; i < 128; i++){
			int last = (i==0) ? 0 : coords[i-1];
			coords[i] = last + (r.nextInt(32)-16);
			if(coords[i] < 0) coords[i] = last - (r.nextInt(16)-8);
		}
	}

	public static void draw() {
		for(int i = 0; i < coords.length-1; i++){
			int width = 32;
			int x = i*width;

			int height = coords[i+1];
			//height = (i==7) ? coords[i] : coords[i+1];
			int offset = 32;

			Graphics.draw(new int[]{ x, Game.HEIGHT-offset-(width+coords[i]), x+width, Game.HEIGHT-offset-(width+height), x+width, Game.HEIGHT, x, Game.HEIGHT },
				new double[]{ 0, 1, 0.5, 1 });
		}
	}

}