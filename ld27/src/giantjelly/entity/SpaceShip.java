package giantjelly.entity;

import giantjelly.Game;

public class SpaceShip extends Entity {
	
	public static int WIDTH = 26;
	public static int HEIGHT = 50;
	
	public double xs = 0;
	public double ys = 0;
	public double rotation = 0.0;
	
	public SpaceShip(double x, double y) {
		super(x, y);
	}
	
	public void render(Game game) {
		game.spriteSheet.render(game, (int)x, (int)y, 32, 0, 32, 64, 2, (int)rotation);
		game.spriteSheet.render(game, (int)x, (int)y, 0, 0, 32, 64, 2, (int)rotation);
	}
	
	public void tick(Game game) {
		x += xs;
		y += ys;
		
		if(game.up) ys += -0.05;
		
		if(game.right) rotation += 4;
		if(game.left) rotation -= 4;
		if(rotation >= 360) rotation = 0;
		if(rotation <= -360) rotation = 0;
	}
	
}
