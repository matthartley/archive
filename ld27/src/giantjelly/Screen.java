package giantjelly;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;

public class Screen {
	
	public static int HEIGHT = 480;
	public static int WIDTH = (int)(HEIGHT*1.5);
	
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	public int[] pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
	
	public Screen(Game game) {
		game.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		JFrame frame = new JFrame("LD27");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(game);
		frame.pack();
		frame.setResizable(false);
		frame.setLocation(new Point(-1820, 100));
		frame.setVisible(true);
		
		game.createBufferStrategy(3);
	}
	
	public void render(Game game) {
		BufferStrategy bs = game.getBufferStrategy();
		game.render();
		Graphics gfx = bs.getDrawGraphics();
		gfx.drawImage(image, 0, 0, game.getWidth(), game.getHeight(), null);
		gfx.dispose();
		bs.show();
		clear();
	}
	
	public void clear() {
		for(int i = 0; i < pixels.length; i++) {
			pixels[i] = 0;
		}
	}
	
}
