package giantjelly.gfx;

import giantjelly.Game;

public class Font {
	
	private static String chars = "abcdefghijklmnopqrstuvwxyz1234567890. ";
	
	public static void render(Game game, int x, int y, int scale, String text) {
		for(int i = 0; i < text.length(); i++) {
			int index = chars.indexOf(text.charAt(i));
			if(index>=0 && index<chars.length())game.font.render(game, x+(i*(8*scale)), y, (index%8)*8, (index/8)*8, 8, 8, scale);
		}
	}
	
}
