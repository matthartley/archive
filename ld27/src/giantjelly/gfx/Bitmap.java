package giantjelly.gfx;

import giantjelly.Game;
import giantjelly.Screen;

import java.awt.image.BufferedImage;

import java.io.IOException;

import javax.imageio.ImageIO;

public class Bitmap {
	
	public int[] pixels;
	public int height;
	public int width;
	
	public Bitmap(String file) {
		BufferedImage res = null;
		
		try {
			res = ImageIO.read(Bitmap.class.getResourceAsStream(file));
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		pixels = res.getRGB(0, 0, res.getWidth(), res.getHeight(), null, 0, res.getWidth());
		height = res.getHeight();
		width = res.getWidth();
	}
	
	public void render(Game game, int x, int y) {
		for(int yy = 0; yy < height; yy++) {
			for(int xx = 0; xx < width; xx++) {
				game.screen.pixels[(x+xx)+(y*Screen.WIDTH+yy*Screen.WIDTH)] = pixels[xx+yy*width];
			}
		}
	}
	
	public void render(Game game, int xx, int yy, int xSheet, int ySheet, int w, int h, int scale) {
		for(int y = 0; y < h*scale; y++) {
			for(int x = 0; x < w*scale; x++) {
				if(xx+x>0&&xx+x<Screen.WIDTH && yy+y>0&&yy+y<Screen.HEIGHT) {
					int screen = (xx+x)+(yy*Screen.WIDTH+y*Screen.WIDTH);
					int sheet = (xSheet+(x/scale))+(ySheet*width+(y/scale)*width);
					if(pixels[sheet] != 16777215) game.screen.pixels[screen] = pixels[sheet];
				}
			}
		}
	}
	
	public void render(Game game, int xx, int yy, int xSheet, int ySheet, int w, int h, int scale, int r) {
		for(int y = 0; y < h*scale; y++) {
			for(int x = 0; x < w*scale; x++) {
				int rx = -((w*scale)/2) + x;
				int ry = -((h*scale)/2) + y;
				//System.out.println(rx + " - " + ry);
				
				double sin = Math.sin(Math.toRadians(r));
				double cos = Math.cos(Math.toRadians(r));
				
				rx = (int)( (rx * sin) - (ry * cos) );
				ry = (int)( (ry * sin) - (rx * cos) );
				
				if(xx+rx>0&&xx+rx<Screen.WIDTH && yy+ry>0&&yy+ry<Screen.HEIGHT) {
					int screen = (xx+rx)+(yy*Screen.WIDTH+ry*Screen.WIDTH);
					int sheet = (xSheet+(x/scale))+(ySheet*width+(y/scale)*width);
					if(pixels[sheet] != 16777215) game.screen.pixels[screen] = pixels[sheet];
				}
			}
		}
	}
	
}
