package giantjelly;

import giantjelly.entity.SpaceShip;
import giantjelly.gfx.Bitmap;

import java.awt.Canvas;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import giantjelly.gfx.Font;

public class Game extends Canvas implements Runnable, KeyListener {
	
	private boolean running = true;
	public Screen screen;
	
	private Bitmap test = new Bitmap("/Test.png");
	public Bitmap font = new Bitmap("/Font.png");
	public Bitmap spriteSheet = new Bitmap("/SpriteSheet.png");
	
	public SpaceShip ship = new SpaceShip(100, 100);
	
	public void run() {
		requestFocus();
		addKeyListener(this);
		System.out.println(spriteSheet.pixels[0]);
		
		long lastFrame = System.currentTimeMillis();
		long lastTick = System.nanoTime();
		double quedTicks = 0.0;
		double nanoPerTick = 1000000000.0/60.0;
		int frames = 0;
		int ticks = 0;
		
		while(running) {
			quedTicks += (System.nanoTime() - lastTick)/nanoPerTick;
			lastTick = System.nanoTime();
			
			while(quedTicks > 1.0) {
				tick();
				ticks++;
				quedTicks -= 1.0;
			}
			
			screen.render(this);
			frames++;
			if(System.currentTimeMillis() - lastFrame > 1000) {
				System.out.println("frames " + frames + ", ticks " + ticks);
				lastFrame = System.currentTimeMillis();
				frames = 0;
				ticks = 0;
			}
		}
	}
	
	public void render() {
		/*for(int i = 0; i < screen.pixels.length; i++) {
			screen.pixels[i] = (int)(System.currentTimeMillis() + i);
		}*/
		Font.render(this, 10, 10, 4, ""+ship.rotation);
		ship.render(this);
	}
	
	public void tick() {
		ship.tick(this);
	}
	
	public static void main(String[] args) {
		Game game = new Game();
		game.screen = new Screen(game);
		new Thread(game).start();
	}
	
	public boolean right = false;
	public boolean left = false;
	public boolean up = false;
	public boolean down = false;
	
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_RIGHT) right = true;
		if(e.getKeyCode()==KeyEvent.VK_LEFT) left = true;
		if(e.getKeyCode()==KeyEvent.VK_UP) up = true;
		if(e.getKeyCode()==KeyEvent.VK_DOWN) down = true;
	}

	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_RIGHT) right = false;
		if(e.getKeyCode()==KeyEvent.VK_LEFT) left = false;
		if(e.getKeyCode()==KeyEvent.VK_UP) up = false;
		if(e.getKeyCode()==KeyEvent.VK_DOWN) down = false;
	}

	public void keyTyped(KeyEvent e) {
	}

}
