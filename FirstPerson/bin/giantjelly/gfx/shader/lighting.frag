varying vec3 color;
uniform sampler2D texture;
uniform int enableTextures;

void main() {
	if(enableTextures==1)
		gl_FragColor = vec4(color, 1) * texture2D(texture, gl_TexCoord[0].st);
	else
		gl_FragColor = vec4(color, 1);
}