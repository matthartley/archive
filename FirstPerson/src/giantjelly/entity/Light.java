package giantjelly.entity;

public class Light extends Entity {
	
	public Light(double xx, double yy, double zz) {
		x = xx;
		y = yy;
		z = zz;
	}
	
	public void setPosition(Entity entity) {
		x = entity.x;
		y = entity.y;
		z = entity.z;
	}
	
}
