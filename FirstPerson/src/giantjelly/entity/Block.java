package giantjelly.entity;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import java.util.ArrayList;
import java.util.List;

import giantjelly.Game;
import giantjelly.gfx.Lighting;
import giantjelly.gfx.Sprite;

public class Block extends Entity {
	
	public Block(double xx, double yy, double zz) {
		x = xx;
		y = yy;
		z = zz;
	}
	
	public void render() {
		/*Light[] lights = Lighting.nearLights(this);
		Lighting.render(0, lights[0]);
		Lighting.render(1, lights[1]);
		Lighting.render(2, lights[2]);
		Lighting.render(3, lights[3]);*/
		
		glMaterialf(GL_FRONT, GL_SHININESS, 128);
		Sprite.metal.bind();
		glColor4d(1, 1, 1, 1);
		
		glPushMatrix();
		
		Game.player.transformations();
		
		//glTranslated(x, y, z);
			Game.lightingShader.enableTextures(true);
			
			glDisable(GL_LIGHTING);
			glBegin(GL_QUADS);
				glTexCoord2d(0, 0); glNormal3d(0, 0, 1); glVertex3d(x+0, y+1, z+0);
				glTexCoord2d(1, 0); glNormal3d(0, 0, 1); glVertex3d(x+1, y+1, z+0);
				glTexCoord2d(1, 1); glNormal3d(0, 0, 1); glVertex3d(x+1, y+0, z+0);
				glTexCoord2d(0, 1); glNormal3d(0, 0, 1); glVertex3d(x+0, y+0, z+0);
			glEnd();
			
			
			Game.lightingShader.enableTextures(true);
			glBegin(GL_QUADS);
				glTexCoord2d(0, 0); glNormal3d(0, 0, -1); glVertex3d(x+1, y+1, z+-1);
				glTexCoord2d(1, 0); glNormal3d(0, 0, -1); glVertex3d(x+0, y+1, z+-1);
				glTexCoord2d(1, 1); glNormal3d(0, 0, -1); glVertex3d(x+0, y+0, z+-1);
				glTexCoord2d(0, 1); glNormal3d(0, 0, -1); glVertex3d(x+1, y+0, z+-1);
				
				glTexCoord2d(0, 0); glNormal3d(1, 0, 0); glVertex3d(x+1, y+1, z+0);
				glTexCoord2d(1, 0); glNormal3d(1, 0, 0); glVertex3d(x+1, y+1, z+-1);
				glTexCoord2d(1, 1); glNormal3d(1, 0, 0); glVertex3d(x+1, y+0, z+-1);
				glTexCoord2d(0, 1); glNormal3d(1, 0, 0); glVertex3d(x+1, y+0, z+0);
				
				glTexCoord2d(0, 0); glNormal3d(-1, 0, 0); glVertex3d(x+0, y+1, z+-1);
				glTexCoord2d(1, 0); glNormal3d(-1, 0, 0); glVertex3d(x+0, y+1, z+0);
				glTexCoord2d(1, 1); glNormal3d(-1, 0, 0); glVertex3d(x+0, y+0, z+0);
				glTexCoord2d(0, 1); glNormal3d(-1, 0, 0); glVertex3d(x+0, y+0, z+-1);
				
				glTexCoord2d(0, 0); glNormal3d(0, 1, 0); glVertex3d(x+0, y+1, z+-1);
				glTexCoord2d(1, 0); glNormal3d(0, 1, 0); glVertex3d(x+1, y+1, z+-1);
				glTexCoord2d(1, 1); glNormal3d(0, 1, 0); glVertex3d(x+1, y+1, z+0);
				glTexCoord2d(0, 1); glNormal3d(0, 1, 0); glVertex3d(x+0, y+1, z+0);
			glEnd();
			
		glPopMatrix();
	}
	
}
