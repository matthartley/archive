package giantjelly.entity;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import org.lwjgl.util.vector.Vector4f;

import giantjelly.Game;
import giantjelly.Math;

public class Bullet extends Entity {
	
	public Bullet(double xx, double yy, double zz, double xr, double yr) {
		x = xx;
		y = yy;
		z = zz;
		xRot = xr;
		yRot = yr;
		
		life = coolDown;
	}
	
	public static double length = 0.25;
	public static double halfLength = length/2.0;
	public static double width = 0.1;
	public static double halfWidth = width/2;
	public static int coolDown = 120;
	
	public double xRot = 0;
	public double yRot = 0;
	//public int life = coolDown;
	
	public void render() {
		glUseProgram(0);
		glDisable(GL_LIGHTING);
		glDisable(GL_TEXTURE_2D);
		
		glPushMatrix();
			
			Game.player.rotations();
			Game.player.translations();
			
			glTranslated(x, y, z);
			glRotated(-xRot, 0, 1, 0);
			glRotated(yRot, 1, 0, 0);
			glTranslated(-x, -y, -z);
			
			glColor4d(1, 0.9, 0, 1);
			glBegin(GL_QUADS);
				//front
				glVertex3d(x-halfWidth, y+halfWidth, z-halfLength);
				glVertex3d(x+halfWidth, y+halfWidth, z-halfLength);
				glVertex3d(x+halfWidth, y-halfWidth, z-halfLength);
				glVertex3d(x-halfWidth, y-halfWidth, z-halfLength);
				//back
				glVertex3d(x-halfWidth, y+halfWidth, z+halfLength);
				glVertex3d(x+halfWidth, y+halfWidth, z+halfLength);
				glVertex3d(x+halfWidth, y-halfWidth, z+halfLength);
				glVertex3d(x-halfWidth, y-halfWidth, z+halfLength);
				//top
				glVertex3d(x-halfWidth, y+halfWidth, z-halfLength);
				glVertex3d(x+halfWidth, y+halfWidth, z-halfLength);
				glVertex3d(x+halfWidth, y+halfWidth, z+halfLength);
				glVertex3d(x-halfWidth, y+halfWidth, z+halfLength);
				//bottom
				glVertex3d(x-halfWidth, y-halfWidth, z-halfLength);
				glVertex3d(x+halfWidth, y-halfWidth, z-halfLength);
				glVertex3d(x+halfWidth, y-halfWidth, z+halfLength);
				glVertex3d(x-halfWidth, y-halfWidth, z+halfLength);
				//right
				glVertex3d(x+halfWidth, y+halfWidth, z-halfLength);
				glVertex3d(x+halfWidth, y-halfWidth, z-halfLength);
				glVertex3d(x+halfWidth, y-halfWidth, z+halfLength);
				glVertex3d(x+halfWidth, y+halfWidth, z+halfLength);
				//left
				glVertex3d(x-halfWidth, y-halfWidth, z-halfLength);
				glVertex3d(x-halfWidth, y+halfWidth, z-halfLength);
				glVertex3d(x-halfWidth, y+halfWidth, z+halfLength);
				glVertex3d(x-halfWidth, y-halfWidth, z+halfLength);
			glEnd();
			
			/*glBegin(GL_LINES);
				glVertex3d(x+(Math.sin(xRot)*half), y+(Math.tan(yRot)*half), z-(Math.cos(xRot)*half));
				glVertex3d(x-(Math.sin(xRot)*half), y-(Math.tan(yRot)*half), z+(Math.cos(xRot)*half));
			glEnd();*/
			/*glBegin(GL_POINTS);
				glVertex3d(x, y, z);
			glEnd();*/
		glPopMatrix();
		glUseProgram(Game.lightingShader.program);
	}
	
	public void tick() {
		if(life > 0) life--;
		
		double speed = 1.0;
		double yy = Math.sin(yRot);
		double xx = Math.sin(xRot)*Math.cos(yRot);
		double zz = Math.cos(xRot)*Math.cos(yRot);
		
		for(int i = 0; i < 10; i++){
			z -= (zz*speed)/10.0;
			x += (xx*speed)/10.0;
			y += (yy*speed)/10.0;
			
			if(collisions()) break;
		}
		
		//collisions();
	}
	
	private boolean collisions() {
		for(int i = 0; i < Game.entities.size(); i++){
			if(Game.entities.get(i) instanceof Block){
				double xBlock = Game.entities.get(i).x;
				double yBlock = Game.entities.get(i).y;
				double zBlock = Game.entities.get(i).z;
				
				if(
						x < xBlock+1 && x > xBlock &&
						y < yBlock+1 && y > yBlock &&
						z < zBlock && z > zBlock-1
				){
					life = 0;
					spark();
					return true;
				}
			}
		}
		
		return false;
	}
	
	public void spark() {
		for(int i = 0; i < 16; i++){
			Game.particles.add(new Particle( x, y, z));
		}
	}
	
}
