package giantjelly.entity;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import giantjelly.Game;
import giantjelly.Math;

public class Particle extends Entity {
	
	public static int coolDown = 60;
	
	public Particle(double xx, double yy, double zz){
		x = xx;
		y = yy;
		z = zz;
		xs = (Math.rand.nextDouble()-0.5)/5;
		ys = (Math.rand.nextDouble()-0.5)/5;
		zs = (Math.rand.nextDouble()-0.5)/5;
		
		life = coolDown;
	}
	
	public double xs;
	public double ys;
	public double zs;
	
	public void render() {
		glDisable(GL_TEXTURE_2D);
		glUseProgram(0);
		
		glPushMatrix();
		{
			glColor4d(1, 0.9, 0, 1);
			
			Game.player.transformations();
			
			glBegin(GL_POINTS);
			{
				glVertex3d(x, y, z);
			}
			glEnd();
		}
		glPopMatrix();
		
		glUseProgram(Game.lightingShader.program);
	}
	
	public void tick() {
		if(life > 0) life--;
		ys -= 0.005;
		
		//for(int i = 0; i < 10; i++){
			x += xs;
			y += ys;
			z += zs;
			
			collisions();
		//}
	}
	
	public boolean collided = false;
	
	public void collisions() {
		for(int i = 0; i < Game.entities.size(); i++){
			if(Game.entities.get(i) instanceof Block){
				double xBlock = Game.entities.get(i).x;
				double yBlock = Game.entities.get(i).y;
				double zBlock = Game.entities.get(i).z;
				
				if(x < xBlock+1 && x > xBlock &&
				y <= yBlock+1 && y > yBlock &&
				z < zBlock && z > zBlock-1){
					
					double xAdd = (xBlock+1)-x;
					double xMin = x-xBlock;
					double yAdd = (yBlock+1)-y;
					double yMin = y-yBlock;
					double zAdd = zBlock-z;
					double zMin = z-(zBlock-1);
					
					double xx = Math.min(xAdd, xMin);
					double yy = Math.min(yAdd, yMin);
					double zz = Math.min(zAdd, zMin);
					
					double smallest = Math.min(Math.min(xx, yy), zz);
					
					if(smallest==yy){
						if(yy==yAdd){
							//if(ys <= 0){
								y = yBlock+1;
							//}
						}else{
							y = yBlock;
						}
						
						ys = 0;
						xs = 0;
						zs = 0;
					}
					else
					if(smallest==xx){
						if(xx==xAdd){
							x = xBlock+1;
						}else{
							x = xBlock;
						}
						
						xs = 0;
					}
					else
					if(smallest==zz){
						if(zz==zAdd){
							z = zBlock;
						}else{
							z = zBlock-1;
						}
						
						zs = 0;
					}
					
					collided = true;
				}
			}
		}
	}
	
}
