package giantjelly.entity;

import giantjelly.Game;

import static org.lwjgl.opengl.GL11.*;

import giantjelly.Math;

public class Player extends Entity {
	
	public double xRot = 0;
	public double yRot = 0;
	public double xRotGun = 0;
	public double yRotGun = 0;
	public double bobMultiplier = 0;
	public boolean bobing = false;
	public double bobDir = 1;
	private double bobOffsetAmmount = 0;
	private double bobOffsetMax = 0.03;
	
	public static double top = 0.2;
	public static double bottom = 1.5;
	public static double side = 0.4;
	
	public double xGun = 0.3;
	public double yGun = -0.3;
	public double zGun = -0.5;
	public double xGunEnd = xGun;
	public double yGunEnd = yGun;
	public double zGunEnd = zGun - 1;
	
	public double xs = 0;
	public double zs = 0;
	public double ys = 0;
	
	public Player() {
		x = 0;
		y = 1.5;
		z = 0;
	}
	
	public double bobOffset() {
		double offset = Math.sin(bobMultiplier) * bobOffsetMax;
		return (offset != 0) ? offset : 0;
	}
	
	private double horiBobOffset() {
		return bobOffsetAmmount*bobDir;
	}
	
	public void transformations() {
		glRotated(-yRot, 1, 0, 0);
		glRotated(xRot, 0, 1, 0);
		glTranslated( -(x+horiBobOffset()*Math.cos(xRot)), -(y+bobOffsetAmmount), -(z+horiBobOffset()*Math.sin(xRot)) );
	}
	
	public void rotations() {
		glRotated(-yRot, 1, 0, 0);
		glRotated(xRot, 0, 1, 0);
	}
	
	public void translations() {
		glTranslated( -(x+horiBobOffset()*Math.cos(xRot)), -(y+bobOffsetAmmount), -(z+horiBobOffset()*Math.sin(xRot)) );
	}
	public void reverseTranslations() {
		glTranslated( (x+horiBobOffset()*Math.cos(xRot)), (y+bobOffsetAmmount), (z+horiBobOffset()*Math.sin(xRot)) );
	}
	
	public void render() {
		Game.lightingShader.enableTextures(false);
		glPushMatrix();
			glTranslated(xGun -(horiBobOffset()*Math.cos(xRot)), yGun -bobOffsetAmmount, zGun -horiBobOffset()*Math.sin(xRot));
			glRotated(90 - (xRotGun-xRot), 0, 1, 0);
			glRotated(yRotGun-yRot, 0, 0, 1);
		
			Game.AssaultRifle.render(0, 0, 0);
		glPopMatrix();
	}
	
	public void bob() {
		bobMultiplier += (bobing) ? 10 : -10;
		if(bobMultiplier >= 180){
			bobMultiplier = 0;
			bobDir = (bobDir==1) ? -1 : 1;
		}
	}
	
	public void tick() {
		if(bobing) bobOffsetAmmount = bobOffset();
		else if(bobOffsetAmmount > 0) bobOffsetAmmount -= 0.01;
		
		if(bobing || bobMultiplier != 0) bob();
		
		int maxAngle = 15;
		int moveAmmount = 4;
		
		xRotGun += (xRot-xRotGun)/moveAmmount;
		if(xRot-xRotGun > maxAngle) xRotGun = xRot - maxAngle;
		if(xRot-xRotGun < -maxAngle) xRotGun = xRot + maxAngle;
		
		yRotGun += (yRot-yRotGun)/moveAmmount;
		if(yRot-yRotGun > maxAngle) yRotGun = yRot - maxAngle;
		if(yRot-yRotGun < -maxAngle) yRotGun = yRot + maxAngle;
		
		
		x += xs;
		z += zs;
		y += ys;
		ys -= 0.01;
		
		collisions();
	}
	
	public void collisions() {
		for(int i = 0; i < Game.entities.size(); i++){
			if(Game.entities.get(i) instanceof Block){
				double xBlock = Game.entities.get(i).x;
				double yBlock = Game.entities.get(i).y;
				double zBlock = Game.entities.get(i).z;
				
				if(x-side < xBlock+1 && x+side > xBlock &&
				y-bottom <= yBlock+1 && y+top > yBlock &&
				z-side < zBlock && z+side > zBlock-1){
					
					double xAdd = (xBlock+1)-(x-side);
					double xMin = (x+side)-(xBlock);
					double yAdd = (yBlock+1)-(y-bottom);
					double yMin = (y+top)-(yBlock);
					double zAdd = (zBlock)-(z-side);
					double zMin = (z+side)-(zBlock-1);
					
					double xx = Math.min(xAdd, xMin);
					double yy = Math.min(yAdd, yMin);
					double zz = Math.min(zAdd, zMin);
					
					double smallest = Math.min(Math.min(xx, yy), zz);
					
					if(smallest==xx){
						if(xx==xAdd){
							x = (xBlock+1)+side;
						}else{
							x = (xBlock)-side;
						}
					}
					if(smallest==yy){
						if(yy==yAdd){
							if(ys <= 0){
								y = (yBlock+1)+bottom;
								ys = 0;
							}
						}else{
							y = (yBlock)-top;
							ys = 0;
						}
					}
					if(smallest==zz){
						if(zz==zAdd){
							z = (zBlock)+side;
						}else{
							z = (zBlock-1)-side;
						}
					}
					
				}
			}
		}
	}
	
}
