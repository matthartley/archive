package giantjelly;

import java.nio.FloatBuffer;
import java.util.Random;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

//import java.lang.Math;

public class Math {
	
	public static double zTransformationRotation(double x, double y, double z, double xRot, double yRot) {
		return ((z * cos(xRot)) + (x * sin(xRot))) * cos(yRot); 
	}
	public static double xTransformationRotation(double x, double y, double z, double xRot, double yRot) {
		return ((x * cos(xRot)) - (z * sin(xRot))) * cos(yRot);
	}
	public static double yTransformationRotation(double x, double y, double z, double xRot, double yRot) {
		return (-(z * cos(xRot)) + (x * sin(xRot))) * sin(yRot); //+ (x * sin(xRot)) * sin(yRot);
	}
	
	public static Vector4f rotatePoint(double x, double y, double z, double xRot, double yRot) {
		Matrix4f m = new Matrix4f()
		.rotate((float)-Math.toRadians(xRot), new Vector3f(0, 1, 0))
		.rotate((float)Math.toRadians(yRot), new Vector3f(1, 0, 0))
		.translate(new Vector3f((float)x, (float)y, (float)z));
		Vector4f v = Matrix4f.transform(m, new Vector4f(0, 0, 0, 1), null);
		return v;
	}
	
	public static FloatBuffer floatBuffer(float[] values) {
		/*float[] floats = new float[doubles.length];
		for(int i = 0; i < doubles.length; i++) floats[i] = (float)doubles[i];*/
		
		FloatBuffer buffer = BufferUtils.createFloatBuffer(values.length);
		buffer.put(values);
		buffer.flip();
		return buffer;
	}
	
	public static FloatBuffer floatBuffer(double[] values) {
		float[] floats = new float[values.length];
		for(int i = 0; i < values.length; i++) floats[i] = (float)values[i];
		
		FloatBuffer buffer = BufferUtils.createFloatBuffer(values.length);
		buffer.put(floats);
		buffer.flip();
		return buffer;
	}
	
	public static double sin(double num) {
		return java.lang.Math.sin(java.lang.Math.toRadians(num));
	}
	
	public static double cos(double num) {
		return java.lang.Math.cos(java.lang.Math.toRadians(num));
	}
	
	public static double tan(double num) {
		return java.lang.Math.tan(java.lang.Math.toRadians(num));
	}
	
	public static double sqrt(double num){
		return java.lang.Math.sqrt(num);
	}
	
	public static double toRadians(double num) {
		return java.lang.Math.toRadians(num);
	}
	
	public static double toDegrees(double num) {
		return java.lang.Math.toDegrees(num);
	}
	
	public static double normalToAngle(double xNormal, double yNormal) {
		return toDegrees(java.lang.Math.atan2(xNormal, yNormal));
	}
	
	/*public static double tan(double num) {
		return java.lang.Math.tan(num);
	}*/
	
	public static double atan(double num) {
		return java.lang.Math.atan(num);
	}
	
	public static double atan2(double num1, double num2) {
		return java.lang.Math.atan2(num1, num2);
	}
	
	public static double min(double one, double two) {
		return (one <= two) ? one : two;
	}
	
	public static Random rand = newRandom();
	public static Random newRandom() {
		return new Random();
	}
	
}
