package giantjelly.gfx;

import static org.lwjgl.opengl.GL11.*;

import java.io.*;
import java.util.ArrayList;

import giantjelly.Game;
import giantjelly.gfx.Vertex;

public class Model {
	
	public ArrayList<Vertex> vertices = new ArrayList<Vertex>();
	public ArrayList<Vertex> normals = new ArrayList<Vertex>();
	public ArrayList<int[]> vertexIndices = new ArrayList<int[]>();
	public ArrayList<int[]> normalIndices = new ArrayList<int[]>();
	
	public Model(String file) {
		try{
			
			BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
			
			String line;
			while((line = reader.readLine()) != null){
				if(line.startsWith("v ")){
					float x = Float.valueOf(line.split(" ")[1]);
					float y = Float.valueOf(line.split(" ")[2]);
					float z = Float.valueOf(line.split(" ")[3]);
					vertices.add(new Vertex(x, y, z));
				}else
				if(line.startsWith("vn ")){
					float x = Float.valueOf(line.split(" ")[1]);
					float y = Float.valueOf(line.split(" ")[2]);
					float z = Float.valueOf(line.split(" ")[3]);
					normals.add(new Vertex(x, y, z));
				}else
				if(line.startsWith("f ")){
					float a = Float.valueOf(line.split(" ")[1].split("/")[0]);
					float b = Float.valueOf(line.split(" ")[2].split("/")[0]);
					float c = Float.valueOf(line.split(" ")[3].split("/")[0]);
					int[] vi = new int[]{ (int)a, (int)b, (int)c };
					
					//System.out.println(line);
					
					float d = Float.valueOf(line.split(" ")[1].split("/")[2]);
					float e = Float.valueOf(line.split(" ")[2].split("/")[2]);
					float f = Float.valueOf(line.split(" ")[3].split("/")[2]);
					int[] ni = new int[]{ (int)d, (int)e, (int)f };
					
					vertexIndices.add(vi);
					normalIndices.add(ni);
				}
			}
			
			reader.close();
			
		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	public void render(double x, double y, double z) {
		glColor4f(0.2f, 0.2f, 0.2f, 1.0f);
		Game.lightingShader.enableTextures(false);
		
		glBegin(GL_TRIANGLES);
			for(int i = 0; i < vertexIndices.size(); i++){
				Vertex n1 = normals.get(normalIndices.get(i)[0] - 1);
				Vertex v1 = vertices.get(vertexIndices.get(i)[0] - 1);
				glNormal3f(n1.x, n1.y, n1.z);
				glVertex3d(x+v1.x, y+v1.y, z+v1.z);
				
				Vertex n2 = normals.get(normalIndices.get(i)[1] - 1);
				Vertex v2 = vertices.get(vertexIndices.get(i)[1] - 1);
				glNormal3f(n2.x, n2.y, n2.z);
				glVertex3d(x+v2.x, y+v2.y, z+v2.z);
				
				Vertex n3 = normals.get(normalIndices.get(i)[2] - 1);
				Vertex v3 = vertices.get(vertexIndices.get(i)[2] - 1);
				glNormal3f(n3.x, n3.y, n3.z);
				glVertex3d(x+v3.x, y+v3.y, z+v3.z);
			}
		glEnd();
	}
	
}