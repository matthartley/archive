package giantjelly.gfx;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.util.glu.GLU.*;
import static org.lwjgl.opengl.ARBShadowAmbient.GL_TEXTURE_COMPARE_FAIL_VALUE_ARB;
import static org.lwjgl.opengl.ARBFramebufferObject.*;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import giantjelly.Game;
import giantjelly.Math;
import giantjelly.entity.Entity;
import giantjelly.entity.Light;

public class Lighting {
	
	public static int shadowMapWidth;
	public static int shadowMapHeight;
	public static int frameBuffer;
	public static int renderBuffer;
	
	public static final Matrix4f depthModelViewProjection = new Matrix4f();
	public static final FloatBuffer textureBuffer = BufferUtils.createFloatBuffer(16);
	
	public static final FloatBuffer lightPosition = Math.floatBuffer(new double[]{ 0, 2, -10, 1 });
	
	public static void init() {
		glShadeModel(GL_SMOOTH);
		glEnable(GL_LIGHTING);
		glLightModel(GL_LIGHT_MODEL_AMBIENT, Math.floatBuffer(new float[]{ 0.05f, 0.05f, 0.05f, 1 }));
		
		glEnable(GL_LIGHT0);
		glEnable(GL_LIGHT1);
		glEnable(GL_LIGHT2);
		glEnable(GL_LIGHT3);
		double intensity = 2.0;
		double intensity2 = 2.0;
		glLight(GL_LIGHT0, GL_DIFFUSE, Math.floatBuffer(new double[]{ intensity, intensity, intensity, intensity2 }));
		glLight(GL_LIGHT1, GL_DIFFUSE, Math.floatBuffer(new double[]{ intensity, intensity, intensity, intensity2 }));
		glLight(GL_LIGHT2, GL_DIFFUSE, Math.floatBuffer(new double[]{ intensity, intensity, intensity, intensity2 }));
		glLight(GL_LIGHT3, GL_DIFFUSE, Math.floatBuffer(new double[]{ intensity, intensity, intensity, intensity2 }));
		
		glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1);
		glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.1f);
		glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.1f);
		
		glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1.0f);
		glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.2f);
		glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.08f);
		glLightf(GL_LIGHT2, GL_CONSTANT_ATTENUATION, 1.0f);
		glLightf(GL_LIGHT2, GL_LINEAR_ATTENUATION, 0.2f);
		glLightf(GL_LIGHT2, GL_QUADRATIC_ATTENUATION, 0.08f);
		glLightf(GL_LIGHT3, GL_CONSTANT_ATTENUATION, 1.0f);
		glLightf(GL_LIGHT3, GL_LINEAR_ATTENUATION, 0.2f);
		glLightf(GL_LIGHT3, GL_QUADRATIC_ATTENUATION, 0.08f);
		
		glEnable(GL_COLOR_MATERIAL);
		glColorMaterial(GL_FRONT, GL_DIFFUSE);
		//glEnable(GL_NORMALIZE);
	}
	
	public static void render(int glLight, Light light) {
		double x = light.x;
		double y = light.y;
		double z = light.z;
		
		glPushMatrix();
			Game.player.transformations();
			
			if(glLight == 0) glLight(GL_LIGHT0, GL_POSITION, Math.floatBuffer(new float[]{ (float)x, (float)y, (float)z, 1 }));
			if(glLight == 1) glLight(GL_LIGHT1, GL_POSITION, Math.floatBuffer(new float[]{ (float)x, (float)y, (float)z, 1 }));
			if(glLight == 2) glLight(GL_LIGHT2, GL_POSITION, Math.floatBuffer(new float[]{ (float)x, (float)y, (float)z, 1 }));
			if(glLight == 3) glLight(GL_LIGHT3, GL_POSITION, Math.floatBuffer(new float[]{ (float)x, (float)y, (float)z, 1 }));
		glPopMatrix();
	}
	
	private static double distance(Entity one, Entity two) {
		double xx = (one.x >= two.x) ? one.x-two.x : two.x-one.x;
		double yy = (one.y >= two.y) ? one.y-two.y : two.y-one.y;
		double zz = (one.z >= two.z) ? one.z-two.z : two.z-one.z;
		return Math.sqrt((xx*xx)+(yy*yy)+(zz*zz));
	}
	
	public static Light[] nearLights(Entity entity) {
		List<Light> tempLights = new ArrayList<Light>();
		for(int i = 0; i < Game.lights.size(); i++){
			tempLights.add(Game.lights.get(i));
		}
		Light[] lights = new Light[4];
		
		//one
		int tempLight = 0;
		for(int i = 0; i < tempLights.size(); i++){
			double dis = distance(tempLights.get(i), entity);
			if(dis < distance(tempLights.get(tempLight), entity)){
				tempLight = i;
			}
		}
		//System.out.println("tempLight: " + tempLight);
		lights[0] = tempLights.get(tempLight);
		tempLights.remove(tempLight);
		
		//two
		tempLight = 0;
		for(int i = 0; i < tempLights.size(); i++){
			double dis = distance(tempLights.get(i), entity);
			if(dis < distance(tempLights.get(tempLight), entity)){
				tempLight = i;
			}
		}
		lights[1] = tempLights.get(tempLight);
		tempLights.remove(tempLight);
		
		//three
		tempLight = 0;
		for(int i = 0; i < tempLights.size(); i++){
			double dis = distance(tempLights.get(i), entity);
			if(dis < distance(tempLights.get(tempLight), entity)){
				tempLight = i;
			}
		}
		lights[2] = tempLights.get(tempLight);
		tempLights.remove(tempLight);
		
		//four
		tempLight = 0;
		for(int i = 0; i < tempLights.size(); i++){
			double dis = distance(tempLights.get(i), entity);
			if(dis < distance(tempLights.get(tempLight), entity)){
				tempLight = i;
			}
		}
		lights[3] = tempLights.get(tempLight);
		tempLights.remove(tempLight);
		
		return lights;
	}
	
	/*public static void initShadowStuff() {
		//glEnable(GL_POLYGON_OFFSET);
		glPolygonOffset(2.5f, 0);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FAIL_VALUE_ARB, 0.5f);
		
		glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
		glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
		glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
		glTexGeni(GL_Q, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
		
		
		
		final int RENDERBUFFER_SIZE = glGetInteger(GL_MAX_RENDERBUFFER_SIZE);
		final int TEXTURE_SIZE = glGetInteger(GL_MAX_TEXTURE_SIZE);
		
		if(TEXTURE_SIZE > 1024){
			if(RENDERBUFFER_SIZE < TEXTURE_SIZE)
				shadowMapWidth = shadowMapHeight = RENDERBUFFER_SIZE;
			else
				shadowMapWidth = shadowMapHeight = 1024;
		}else
			shadowMapWidth = shadowMapHeight = TEXTURE_SIZE;
		
		frameBuffer = glGenFramebuffers();
		renderBuffer = glGenRenderbuffers();
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
		
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, shadowMapWidth, shadowMapHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderBuffer);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		
		int status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if(status != GL_FRAMEBUFFER_COMPLETE) System.err.println("frame buffer error: " + gluErrorString(glGetError()));
		
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}*/
	
	/*public static void genTextureCoords() {
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		
		glEnable(GL_TEXTURE_GEN_S);
		glEnable(GL_TEXTURE_GEN_T);
		glEnable(GL_TEXTURE_GEN_R);
		glEnable(GL_TEXTURE_GEN_Q);
		
		textureBuffer.clear();
		
		textureBuffer.put(0, depthModelViewProjection.m00);
		textureBuffer.put(1, depthModelViewProjection.m01);
		textureBuffer.put(2, depthModelViewProjection.m02);
		textureBuffer.put(3, depthModelViewProjection.m03);
		glTexGen(GL_S, GL_EYE_PLANE, textureBuffer);
		
		textureBuffer.put(0, depthModelViewProjection.m10);
		textureBuffer.put(1, depthModelViewProjection.m11);
		textureBuffer.put(2, depthModelViewProjection.m12);
		textureBuffer.put(3, depthModelViewProjection.m13);
		glTexGen(GL_T, GL_EYE_PLANE, textureBuffer);
		
		textureBuffer.put(0, depthModelViewProjection.m20);
		textureBuffer.put(1, depthModelViewProjection.m21);
		textureBuffer.put(2, depthModelViewProjection.m22);
		textureBuffer.put(3, depthModelViewProjection.m23);
		glTexGen(GL_R, GL_EYE_PLANE, textureBuffer);
		
		textureBuffer.put(0, depthModelViewProjection.m30);
		textureBuffer.put(1, depthModelViewProjection.m31);
		textureBuffer.put(2, depthModelViewProjection.m32);
		textureBuffer.put(3, depthModelViewProjection.m33);
		glTexGen(GL_Q, GL_EYE_PLANE, textureBuffer);
	}*/
	
	/*public static void renderShadowMap() {
		FloatBuffer lightModelView = BufferUtils.createFloatBuffer(16);
		FloatBuffer lightProjection = BufferUtils.createFloatBuffer(16);
		Matrix4f lightProjectionTemp = new Matrix4f();
		Matrix4f lightModelViewTemp = new Matrix4f();
		
		float sceneBoundingRadius = 10;
		float lightToSceneDistance = (float) Math.sqrt(
					lightPosition.get(0) * lightPosition.get(0) +
					lightPosition.get(1) * lightPosition.get(1) +
					lightPosition.get(2) * lightPosition.get(2)
				);
		
		float nearPlane = lightToSceneDistance - sceneBoundingRadius;
		if(nearPlane < 0) System.err.println("camera too close to scene");
		
		float fov = (float) java.lang.Math.toDegrees(2.0f * java.lang.Math.atan(sceneBoundingRadius / lightToSceneDistance));
		
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
			glLoadIdentity();
			gluPerspective(fov, 1, 0.2f, 100);
			glGetFloat(GL_PROJECTION_MATRIX, lightProjection);
			glMatrixMode(GL_MODELVIEW);
			
			glPushMatrix();
				glLoadIdentity();
				gluLookAt(lightPosition.get(0), lightPosition.get(1), lightPosition.get(2), 0, 0, 0, 0, 1, 0);
				glGetFloat(GL_MODELVIEW_MATRIX, lightModelView);
				glViewport(0, 0, shadowMapWidth, shadowMapHeight);
				glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
				glClear(GL_DEPTH_BUFFER_BIT);
				
				glPushAttrib(GL_ALL_ATTRIB_BITS);
				{
					glShadeModel(GL_FLAT);
					glDisable(GL_LIGHTING);
					glDisable(GL_COLOR_MATERIAL);
					glDisable(GL_NORMALIZE);
					glColorMask(false, false, false, false);
					glEnable(GL_POLYGON_OFFSET_FILL);
					Game.renderStuff();
					glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 0, 0, shadowMapWidth, shadowMapHeight, 0);
					glPopMatrix();
					glMatrixMode(GL_PROJECTION);
					glPopMatrix();
					glMatrixMode(GL_MODELVIEW);
					glBindFramebuffer(GL_FRAMEBUFFER, 0);
				}
				glPopAttrib();
		glViewport(0, 0, Display.getWidth(), Display.getHeight());
		lightProjectionTemp.load(lightProjection);
		lightModelViewTemp.load(lightModelView);
		lightProjection.flip();
		lightModelView.flip();
		depthModelViewProjection.setIdentity();
		
		depthModelViewProjection.translate(new Vector3f(0.5f, 0.5f, 0.5f));
		depthModelViewProjection.scale(new Vector3f(0.5f, 0.5f, 0.5f));
		Matrix4f.mul(depthModelViewProjection, lightProjectionTemp, depthModelViewProjection);
		Matrix4f.mul(depthModelViewProjection, lightModelViewTemp, depthModelViewProjection);
		Matrix4f.transpose(depthModelViewProjection, depthModelViewProjection);
	}*/
	
}
