package giantjelly.gfx;

public class Vertex {
	
	public float x;
	public float y;
	public float z;
	
	public Vertex(float xx, float yy, float zz) {
		x = xx;
		y = yy;
		z = zz;
	}
	
}
