package giantjelly.gfx;

import static org.lwjgl.opengl.GL11.*;

import java.io.IOException;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Sprite {
	
	public static Texture metal = load("res/metal.png");
	public static Texture Font = load("res/BitmapFont.png");
	
	/*public static void init() {
		
	}*/
	
	private static Texture load(String file) {
		try{
			return TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(file), GL_NEAREST);
		}catch(IOException e){
			System.out.println("Sprite load error - " + e);
		}
		
		return null;
	}
	
}
