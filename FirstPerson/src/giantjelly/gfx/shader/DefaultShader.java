package giantjelly.gfx.shader;

import static org.lwjgl.opengl.GL20.*;

public class DefaultShader extends Shader {
	
	public DefaultShader() {
		super("lighting.vert", "lighting.frag");
	}
	
	
	private int enableTextures = glGetUniformLocation(program, "enableTextures");
	
	public void enableTextures(boolean enable) {
		int i = (enable) ? 1 : 0;
		glUniform1i(enableTextures, i);
	}
	
}
