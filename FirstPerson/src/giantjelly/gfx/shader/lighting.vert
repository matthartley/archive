varying vec3 color;

uniform vec3 lights[8];

vec3 vertexPosition = (gl_ModelViewMatrix * gl_Vertex).xyz;

uniform vec3 translation;

float constant = gl_LightSource[0].constantAttenuation;
float linear = gl_LightSource[0].linearAttenuation;
float quadratic = gl_LightSource[0].quadraticAttenuation;
float attenuation(vec3 lightPos) {
	float d = length(vec3(lightPos.xyz - gl_Vertex.xyz));
	float att = 1.0 / (
		constant +
		(linear*d) +
		(quadratic*d*d)
	);
	return att;
}

float lightDiffuseIntensity(vec3 lightPos) {
	vec3 lightDirection = normalize(lightPos.xyz - gl_Vertex.xyz);
	vec3 surfaceNormal = (gl_Normal).xyz;
	float diffuseLightIntensity = 3.0 * max(0.0, dot(surfaceNormal, lightDirection));
	return diffuseLightIntensity;
}

void main() {
	//vec3 lightPosition = staticLightPosition; //gl_LightSource[0].position;
	
	
	
	//float diffuseLightIntensity = (lightDiffuseIntensity(lights[0]) * attenuation(lights[0]));
	//for(int i = 1; i < 4; i++){
		//if(lights[i] != 0){
			//float intensity = (lightDiffuseIntensity(lights[i]) * attenuation(lights[i]));
			//if(intensity > diffuseLightIntensity) diffuseLightIntensity = intensity;
		//}
	//}
	
	float diffuseLightIntensity = 0.0;
	for(int i = 0; i < 4; i++){
		diffuseLightIntensity += (lightDiffuseIntensity(lights[i]) * attenuation(lights[i]));
	}
	
	
	//diffuseLightIntensity = lightDiffuseIntensity(gl_LightSource[0].position) * attenuation(gl_LightSource[0].position);
	//diffuseLightIntensity = lightDiffuseIntensity(lights[0]) * attenuation(lights[0]);
	
	if(diffuseLightIntensity > 1.0) diffuseLightIntensity = 1.0;
	color.rgb = (diffuseLightIntensity * gl_Color.rgb);
	if(color.r < gl_LightModel.ambient.r) color.r = gl_LightModel.ambient.r;
	if(color.g < gl_LightModel.ambient.g) color.g = gl_LightModel.ambient.g;
	if(color.b < gl_LightModel.ambient.b) color.b = gl_LightModel.ambient.b;
	
	//vec3 reflectionDirection = normalize(reflect(-lightDirection, surfaceNormal));
	//float specular = max(0.0, dot(surfaceNormal, reflectionDirection));
	//if(diffuseLightIntensity != 0){
	//	float fspecular = pow(specular, gl_FrontMaterial.shininess);
	//	color.rgb += vec3(fspecular, fspecular, fspecular);
	//}
	
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	
}