package giantjelly.gfx.shader;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import giantjelly.Game;

import java.io.*;

public class Shader {
	
	public int program;
	
	int vertShader;
	int fragShader;
	
	String VertShaderSource = new String();
	String FragShaderSource = new String();
	
	public Shader(String VertFile, String FragFile){
		program = glCreateProgram();
		vertShader = glCreateShader(GL_VERTEX_SHADER);
		fragShader = glCreateShader(GL_FRAGMENT_SHADER);
		
		try{
			BufferedReader VReader = new BufferedReader(new FileReader("src/giantjelly/gfx/shader/" + VertFile));
			BufferedReader FReader = new BufferedReader(new FileReader("src/giantjelly/gfx/shader/" + FragFile));
			
			String line;
			while((line = VReader.readLine()) != null){
				VertShaderSource += line + '\n';
			}
			while((line = FReader.readLine()) != null){
				FragShaderSource += line + '\n';
			}
			
			VReader.close();
			FReader.close();
		}catch(IOException e){
			System.err.println("Shader read error! - " + e);
		}
		
		glShaderSource(vertShader, VertShaderSource);
		glCompileShader(vertShader);
		glShaderSource(fragShader, FragShaderSource);
		glCompileShader(fragShader);
		
		if(glGetShader(vertShader, GL_COMPILE_STATUS) == GL_FALSE){
			System.out.println("VertShader compile error!" + vertShader);
			System.out.println(glGetShaderInfoLog(vertShader, glGetShader(vertShader, GL_INFO_LOG_LENGTH)));
		}
		if(glGetShader(fragShader, GL_COMPILE_STATUS) == GL_FALSE){
			System.out.println("FragShader compile error!" + fragShader);
			System.out.println(glGetShaderInfoLog(fragShader, glGetShader(fragShader, GL_INFO_LOG_LENGTH)));
		}
		
		glAttachShader(program, vertShader);
		glAttachShader(program, fragShader);
		glLinkProgram(program);
		glValidateProgram(program);
	}
	
}
