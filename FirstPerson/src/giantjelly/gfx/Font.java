package giantjelly.gfx;

import static org.lwjgl.opengl.GL11.*;

public class Font {
	
	private static String characters =
			"abcdefghijklmnopqrstuvwxyz      " +
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ      " +
			"0123456789      " +
			"-_()�$%.";
	
	private static int charWidth = 8;
	private static double sheetWidth = 16;
	private static double sheetHeight = 16;
	private static double sheetCharWidth = 1/sheetWidth;
	private static double sheetCharHeight = 1/sheetHeight;
	
	public static void write(int x, int y, int size, String text) {
		charWidth = 8*size;
		for(int i = 0 ; i < text.length(); i++){
			int index = characters.indexOf(text.charAt(i));
			//int index = (int) text.charAt(i);
			render(x+i*charWidth, y, index%sheetWidth, index/(int)sheetWidth);
		}
	}
	
	private static void render(int x, int y, double tx, double ty) {
		Sprite.Font.bind();
		glColor4d(1, 1, 1, 1);
		glBegin(GL_QUADS);
			glTexCoord2d(tx/sheetWidth, ty/sheetHeight); //glTexCoord2d(0, 0);
			glVertex2d(x, y);
			
			glTexCoord2d(tx/sheetWidth+sheetCharWidth, ty/sheetHeight); //glTexCoord2d(1, 0);
			glVertex2d(x+charWidth, y);
			
			glTexCoord2d(tx/sheetWidth+sheetCharWidth, ty/sheetHeight+sheetCharHeight); //glTexCoord2d(1, 1);
			glVertex2d(x+charWidth, y+charWidth);
			
			glTexCoord2d(tx/sheetWidth, ty/sheetHeight+sheetCharHeight); //glTexCoord2d(0, 1);
			glVertex2d(x, y+charWidth);
		glEnd();
	}
	
}
