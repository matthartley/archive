package giantjelly;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.util.glu.GLU.gluPerspective;

import java.util.ArrayList;
import java.util.List;

import net.java.games.input.Component;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.util.vector.*;

import giantjelly.entity.Block;
import giantjelly.entity.Bullet;
import giantjelly.entity.Entity;
import giantjelly.entity.Light;
import giantjelly.entity.Particle;
import giantjelly.entity.Player;
import giantjelly.gfx.Font;
import giantjelly.gfx.Lighting;
import giantjelly.gfx.Model;
import giantjelly.gfx.Sprite;
import giantjelly.gfx.gfx;
import giantjelly.gfx.shader.DefaultShader;
import giantjelly.gfx.shader.Shader;
import giantjelly.Math;

public class Game {
	
	public static int HEIGHT = 900;
	public static int WIDTH = (int)(HEIGHT*1.5);
	public static boolean running = true;
	public static int fps = 0;
	public static int tps = 0;
	
	public static Player player = new Player();
	public static boolean paused = true;
	public static DefaultShader lightingShader;
	public static int currentShader = 0;
	public static String text = "";
	
	public static List<Entity> entities = new ArrayList<Entity>();
	public static List<Bullet> bullets = new ArrayList<Bullet>();
	public static List<Particle> particles = new ArrayList<Particle>();
	public static List<Light> lights = new ArrayList<Light>();
	public static Model AssaultRifle = new Model("res/AssaultRifle.obj");
	
	public static void main(String[] args) {
		try{
			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
			//Display.setResizable(true);
			Display.setTitle("First Person");
			Display.create();
			/*Mouse.setCursorPosition(Display.getWidth()/2, Display.getHeight()/2);
			Mouse.setGrabbed(true);*/
			Display.setVSyncEnabled(true);
		}catch(LWJGLException e){
			System.out.println(e);
		}
		
		lightingShader = new DefaultShader(); //new Shader("lighting.vert", "lighting.frag");
		
		InputHandler.init();
		Lighting.init();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glActiveTexture(GL_TEXTURE0);
		glLineWidth(1);
		glDisable(GL_TEXTURE_2D);
		
		lights.add(new Light(4, 3f, -4));
		lights.add(new Light(4, 3f, 4));
		lights.add(new Light(-4, 3f, -4));
		lights.add(new Light(-4, 3f, 4));
		
		lights.add(new Light(4, 2f, -4+16));
		lights.add(new Light(4, 2f, 4+16));
		lights.add(new Light(-4, 2f, -4+16));
		lights.add(new Light(-4, 2f, 4+16));
		
		entities.add(new Block(4, 0, -6));
		entities.add(new Block(-1, 0, -6));
		entities.add(new Block(1, 0, -6));
		entities.add(new Block(0, 1, -6));
		for(int x = 0; x < 16; x++){
			for(int z = 0; z < 32; z++){
				entities.add(new Block(-8+x, -1, -8+z));
			}
		}
		for(int x = 0; x < 16; x++){
			for(int z = 0; z < 16; z++){
				entities.add(new Block(-8+x, -1+z, -8));
			}
		}
		
		
		int frames = 0;
		int ticks = 0;
		long time = System.currentTimeMillis();
		
		long nanoTime = System.nanoTime();
		double quedTicks = 0;
		double nsPerTick = 1000000000/60;
		while(running){
			frames++;
			if(System.currentTimeMillis()-time > 1000){
				fps = frames;
				frames = 0;
				tps = ticks;
				ticks = 0;
				time = System.currentTimeMillis();
				//particles.add(new Particle(0, 3, 0));
			}
			
			quedTicks += (double)(System.nanoTime() - nanoTime)/nsPerTick;
			//System.out.println(quedTicks);
			nanoTime = System.nanoTime();
			while(quedTicks>1.0){
				quedTicks-=1;
				tick();
				ticks++;
				//System.out.println("tick!");
			}
			
			if(Display.isCloseRequested()) running = false;
			render();
			Display.update();
		}
		
		System.exit(0);
	}
	
	public static void render() {
		
		glLineWidth(3);
		glPointSize(3);
		
		glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1);
		glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.18f);
		glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.18f);
		
		glUseProgram(currentShader);
		int loc = glGetUniformLocation(lightingShader.program, "texture");
		glUniform1i(loc, 0);
		glUniform3f( glGetUniformLocation(lightingShader.program, "staticLightPosition"), 0, 1, -5 );
		
		//int lightsVariable = glGetUniformLocation(lightingShader.program, "lights[8]");
		for(int i = 0; i < lights.size(); i++){
			glUniform3f( glGetUniformLocation(lightingShader.program, "lights["+i+"]"), (float)lights.get(i).x, (float)lights.get(i).y, (float)lights.get(i).z );
		}
		
		//glUniform3f( glGetUniformLocation(lightingShader.program, "translation"), (float)-Game.player.x, (float)-Game.player.y, (float)-Game.player.z );
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		gfx.perspective3D();
		
		/*Lighting.render(0, lights.get(0));
		Lighting.render(1, lights.get(1));
		Lighting.render(2, lights.get(2));
		Lighting.render(3, lights.get(3));*/
		
		Light[] lights = Lighting.nearLights(player);
		Lighting.render(0, lights[0]);
		Lighting.render(1, lights[1]);
		Lighting.render(2, lights[2]);
		Lighting.render(3, lights[3]);
		
		renderStuff();
		
		glUseProgram(0);
		gfx.perspective2D();
		int start = 8;
		int height = 24;
		Font.write(8, start+(height*0), 2, "First Person Game");
		Font.write(8, start+(height*1), 2, "frames " + fps);
		Font.write(8, start+(height*2), 2, "ticks " + tps);
		Font.write(8, start+(height*3), 2, "shader " + currentShader);
		Font.write(8, start+(height*4), 2, "particles " + particles.size());
		
		/*if(bullets.size()>0){
			Font.write(8, start+(height*5), 2, "z " + Math.cos(bullets.get(0).xRot) );
			Font.write(8, start+(height*6), 2, "x " + Math.sin(bullets.get(0).xRot)	);
			Font.write(8, start+(height*7), 2, "y " + Math.sin(bullets.get(0).yRot)	);
		}*/
		
		//Font.write(8, start+(height*4), 2, "rotationX " + player.xRot + ", rotationY " + player.yRot);
		//Font.write(8, start+(height*5), 2, "bobMultiplier " + player.bobMultiplier);
		//Font.write(8, start+(height*6), 2, "bobOffset " + player.bobOffset());
		//Font.write(8, start+(height*7), 2, "x " + InputHandler.xNormal + ", z " + InputHandler.zNormal);
		//Font.write(8, start+(height*8), 2, "move angle " + Math.normalToAngle(InputHandler.xNormal, InputHandler.zNormal));
		//Font.write(8, start+(height*9), 2, "bob direction " + player.bobDir);
		
		//Font.write(8, start+(height*11), 2, "sin " + Math.sin(player.xRot));
		//Font.write(8, start+(height*12), 2, "cos " + Math.cos(player.xRot));
		//Font.write(8, start+(height*13), 2, "tan " + Math.tan(player.yRot));
		
		//Font.write(8, start+(height*15), 2, "text " + text);
		
		/*for(int i = 0; i < InputHandler.gamePadComponents.length; i++){
			Font.write(8, start+(height*(6+i)), 2, InputHandler.gamePadComponents[i].getName() + " - " + InputHandler.gamePadComponents[i].getPollData());
		}*/
		
		/*for(int i = 0; i < particles.size(); i++){
			Font.write(8, start+(height*(6+i)), 2, ""+particles.get(i).collided);
		}*/
		
	}
	
	public static double xRotPoint = 0;
	public static double yRotPoint = 0;
	
	public static void renderStuff() {
		player.render();
		
		//new Bullet(player.x, player.y, player.z, player.xRot, player.yRot).render();
		
		for(Entity e : entities){
			e.render();
		}
		for(Bullet b : bullets){
			b.render();
		}
		for(Particle p : particles){
			p.render();
		}
		
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		
		glPushMatrix();
		player.transformations();
		AssaultRifle.render(0, 1, 0);
		
		glUseProgram(0);
		glColor4d(1, 1, 0, 1);
		glDisable(GL_TEXTURE_2D);
		glBegin(GL_LINES);
			glVertex3d(0, 0, 0);
			glVertex3d(0, 2, 0);
			
			glVertex3d(0, 1, -1);
			glVertex3d(0, 1, 1);
			
			glVertex3d(1, 0, 0);
			glVertex3d(1, 2, 0);
			
			glVertex3d(1, 1, -1);
			glVertex3d(1, 1, 1);
		glEnd();
		
		
		glColor4d(0.5, 1, 0, 1);
		/*Matrix4f m = new Matrix4f()
		.rotate((float)Math.toRadians(yRotPoint), new Vector3f(1, 0, 0))
		.rotate((float)-Math.toRadians(xRotPoint), new Vector3f(0, 1, 0));
		Vector4f v = Matrix4f.transform(m, new Vector4f(0, 0, -1, 1), null);*/
		Vector4f vec = Math.rotatePoint(0, 0, -1, xRotPoint, yRotPoint);
		text = vec.x + " " + vec.y + " " + vec.z + " " + vec.w;
		glBegin(GL_POINTS);
			glVertex3d( vec.x, vec.y+1, vec.z
					//Math.xTransformationRotation(0, 0, -1, xRotPoint, yRotPoint),
					//Math.yTransformationRotation(0, 0, -1, xRotPoint, yRotPoint) + 2,
					//Math.zTransformationRotation(0, 0, -1, xRotPoint, yRotPoint)
					);
		glEnd();
		
		glUseProgram(lightingShader.program);
		glPopMatrix();
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	
	public static void tick() {
		InputHandler.tick();
		player.tick();
		
		for(Entity e : entities){
			e.tick();
		}
		for(Bullet b : bullets){
			b.tick();
		}
		for(int i = 0; i < particles.size(); i++){
			particles.get(i).tick();
		}
		for(int i = 0; i < bullets.size(); i++){
			if(i < bullets.size()){
				if(bullets.get(i).life <= 0){
					bullets.remove(i);
					i--;
				}
			}
		}
		if(particles.size()>0) if(particles.get(0).life<=0) particles.remove(0);
		if(particles.size()>0) if(particles.get(0).life<=0) particles.remove(0);
		if(particles.size()>0) if(particles.get(0).life<=0) particles.remove(0);
	}
	
}
