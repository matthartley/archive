package giantjelly;

import static org.lwjgl.opengl.GL11.glViewport;

import giantjelly.entity.Bullet;
import giantjelly.Math;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.util.vector.Vector4f;

import net.java.games.input.*;

public class InputHandler {
	
	public static Controller gamePad = null;
	public static Component[] gamePadComponents;
	
	public static boolean trigger = false;
	public static boolean lastTrigger = false;
	
	public static void connectGamePad() {
		for(Controller c : ControllerEnvironment.getDefaultEnvironment().getControllers()){
			//System.out.println(c.getName() + " - " + c.getType());
			if(c.getType() == Controller.Type.GAMEPAD){
				gamePad = c;
			}
		}
	}
	
	public static void init() {
		connectGamePad();
		gamePadComponents = gamePad.getComponents();
	}
	
	private static double xRotation = 0;
	private static double yRotation = 0;
	public static double xNormal = 0;
	public static double zNormal = 0;
	
	public static void shoot() {
		Vector4f vec = Math.rotatePoint(
				Game.player.xGunEnd, Game.player.yGunEnd, Game.player.zGunEnd, Game.player.xRot, Game.player.yRot
		);
		Game.bullets.add(
				new Bullet(
						Game.player.x+vec.x, Game.player.y+vec.y, Game.player.z+vec.z, Game.player.xRot, Game.player.yRot
				)
		);
	}
	
	public static void tick() {
		
		gamePad.poll();
		gamePadComponents = gamePad.getComponents();
		
		if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) Game.xRotPoint+=3;
		if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)) Game.xRotPoint-=3;
		if(Keyboard.isKeyDown(Keyboard.KEY_UP)) Game.yRotPoint+=3;
		if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) Game.yRotPoint-=3;
		
		if(gamePad != null){
			for(Component c : gamePadComponents){
				float data = c.getPollData();
				
				if(c.getName().equals("Z Axis")){
					lastTrigger = trigger;
					trigger = (data < -0.2 && data > -1) ? true : false;
				}
				
				if(c.getName().equals("Button 0") && data==1.0 && Game.player.ys==0){
					Game.player.ys = 0.15;
				}
			}
		}
		
		if(trigger && !lastTrigger) shoot();
		
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE) && Game.player.ys==0){
			Game.player.ys = 0.15;
		}
		
		while(Mouse.next()){
			if(Mouse.getEventButton()==0 && Mouse.getEventButtonState()){
				shoot();
			}
		}
		
		while(Keyboard.next()){
			/*if(Keyboard.getEventKey()==Keyboard.KEY_SPACE && Keyboard.getEventKeyState()){
				Game.player.ys = 0.1;
			}*/
			
			if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState()){
				if(Game.paused){
					Game.paused = false;
					Mouse.setGrabbed(true);
					Mouse.setCursorPosition(Display.getWidth()/2, Display.getHeight()/2);
				}else{
					Game.paused = true;
					Mouse.setGrabbed(false);
				}
			}
			
			if(Keyboard.getEventKey() == Keyboard.KEY_F11 && Keyboard.getEventKeyState()){
				if(Display.isFullscreen()){
					try {
						Display.setFullscreen(false);
						Display.setDisplayMode(new DisplayMode(Game.WIDTH, Game.HEIGHT));
					} catch (LWJGLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					try {
						Display.setDisplayMode(Display.getDesktopDisplayMode());
						Display.setFullscreen(true);
					} catch (LWJGLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				glViewport(0, 0, Display.getWidth(), Display.getHeight());
			}
			
			if(Keyboard.getEventKey() == Keyboard.KEY_F1 && Keyboard.getEventKeyState()){
				if(Game.currentShader == 0)
					Game.currentShader = Game.lightingShader.program;
				else
					Game.currentShader = 0;
			}
		}
		
		xRotation = 0;
		yRotation = 0;
		
		if(!Game.paused){
			xRotation = (double)(Mouse.getX() - (Display.getWidth()/2)) / 5;
			yRotation = (double)(Mouse.getY() - (Display.getHeight()/2)) / 5;
			Mouse.setCursorPosition(Display.getWidth()/2, Display.getHeight()/2);
		}
		
		boolean w = (Keyboard.isKeyDown(Keyboard.KEY_W)) ? true : false;
		boolean s = (Keyboard.isKeyDown(Keyboard.KEY_S)) ? true : false;
		boolean d = (Keyboard.isKeyDown(Keyboard.KEY_D)) ? true : false;
		boolean a = (Keyboard.isKeyDown(Keyboard.KEY_A)) ? true : false;
		
		//Game.player.bobing = (w||a||s||d) ? true : false;
		
		xNormal = 0;
		zNormal = 0;
		if(w) zNormal++;
		if(s) zNormal--;
		if(a) xNormal--;
		if(d) xNormal++;
		playerMovement();
		
		Game.player.xRot += xRotation;
		Game.player.yRot += yRotation;
		
		if(Game.player.yRot > 90) Game.player.yRot = 90;
		if(Game.player.yRot < -90) Game.player.yRot = -90;
	}
	
	private static void playerMovement() {
		double xGamepad = 0;
		double yGamepad = 0;
		
		if(gamePad != null){
			/*Y Axis
			X Axis
			Y Rotation
			X Rotation*/
			
			
			for(Component c : gamePadComponents){
				String name = c.getName();
				float pollData = c.getPollData();
				if(name.equals("X Rotation") && (pollData<-0.2 || pollData>0.2)) xRotation = 3 * pollData;
				if(name.equals("Y Rotation") && (pollData<-0.2 || pollData>0.2)) yRotation = -(3 * pollData);
				
				if(name.equals("X Axis") && (pollData<-0.2 || pollData>0.2))
					xGamepad = pollData;
				if(name.equals("Y Axis") && (pollData<-0.2 || pollData>0.2))
					yGamepad = pollData;
			}
		}
		
		if(xGamepad!=0||yGamepad!=0){
			Game.player.zs = (yGamepad * Math.cos(Game.player.xRot)) / 15;
			Game.player.xs = -(yGamepad * Math.sin(Game.player.xRot)) / 15;
			
			Game.player.zs += (xGamepad * Math.sin(Game.player.xRot)) / 15;
			Game.player.xs += (xGamepad * Math.cos(Game.player.xRot)) / 15;
			
			Game.player.bobing = true;
		}
		else
		if(xNormal!=0||zNormal!=0){
			double normalAngle = Math.normalToAngle(xNormal, zNormal);
			//System.out.println(Game.player.xRot + normalAngle);
			Game.player.zs = -(Math.cos(Game.player.xRot + normalAngle) / 15);
			Game.player.xs = (Math.sin(Game.player.xRot + normalAngle) / 15);
			
			Game.player.bobing = true;
		}else{
			Game.player.bobing = false;
			Game.player.xs = 0;
			Game.player.zs = 0;
		}
	}
	
}
