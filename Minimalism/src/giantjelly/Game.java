package giantjelly;

import giantjelly.entity.Entity;
import giantjelly.entity.Player;
import giantjelly.entity.TerrainSection;
import giantjelly.gfx.Bitmap;
import giantjelly.gfx.Screen;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;

import javax.swing.JFrame;

public class Game extends Canvas implements Runnable, KeyListener {
	
	public static int HEIGHT = 240;
	public static int WIDTH = 360;
	public static int SCALE = 2;
	public static int entityScale = 1;
	
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
	
	public static Screen screen = new Screen();
	public static Bitmap font = new Bitmap("/Font.png");
	public static Bitmap playerSheet = new Bitmap("/PlayerSheet.png");
	public static Bitmap tester = new Bitmap("/tester.png");
	public static Bitmap terrain = new Bitmap("/Terrain.png");
	public static Bitmap sheet = new Bitmap("/Sheet.png");
	public static Bitmap background = new Bitmap("/Background.png");
	
	public static Player player; //= new Player();
	public static ArrayList<Entity> entities; //= new ArrayList<Entity>();
	public static TerrainSection ts; //= new TerrainSection(0, 4096);
	public static int[] ints = new int[terrain.width];
	
	public static int fps = 0;
	
	public static boolean ending = false;
	public static int endingStart = 3930;
	
	public static boolean inGame = false;
	public static boolean doReset = false;
	//public static boolean win = false;
	
	public static boolean flash = false;
	
	public void stop() {
		System.exit(0);
	}
	
	public void sleepWait(long ns) {
		long time = System.nanoTime();
		while(System.nanoTime()-time < ns);
	}
	
	public void run() {
		int frames = 0;
		long timeMillis = System.currentTimeMillis();
		long timeNano = System.nanoTime();
		double unproccessed = 0.0;
		double nsPerTick = 1000000000.0/30.0;
		
		long flashMillis = System.currentTimeMillis();
		
		while(true){
			unproccessed = (double)(System.nanoTime() - timeNano);
			timeNano = System.nanoTime();
			
			try {
				Thread.sleep((int)(nsPerTick-unproccessed)/1000000 );
			} catch (InterruptedException e) {
				e.printStackTrace();
			}catch(Exception e){}
			
			//sleepWait((int)(nsPerTick-unproccessed));
			
			//while(unproccessed>=1){
				
				//unproccessed -= 1;
				frames++;
				if(System.currentTimeMillis() - timeMillis > 1000){
					timeMillis = System.currentTimeMillis(); 
					System.out.println("frames " + frames );
					fps = frames;
					frames = 0;
				}
				
				if(System.currentTimeMillis() - flashMillis > 400){
					flashMillis = System.currentTimeMillis(); 
					flash = (flash) ? false : true;
				}
				
				BufferStrategy bs = getBufferStrategy();
				if(doReset) reset();
				if(inGame) screen.render();
				else screen.renderMenu();
				if(!hasFocus()&&flash) screen.draw("Click to focus!", WIDTH/2-(int)(7.5d*16.0d), 1, 2);
				//unproccessed -= 1;
				for(int i = 0; i < pixels.length; i++){
					pixels[i] = screen.pixels[i];
				}
				
				Graphics gfx = bs.getDrawGraphics();
				gfx.drawImage(image, 0, 0, getWidth(), getHeight(), null);
				gfx.dispose();
				bs.show();
				
				for(int i = 0; i < screen.pixels.length; i++){
					if(inGame) screen.pixels[i] = 0xeeeeee;
					else screen.pixels[i] = 0x000000;
				}
				
			//}
				
		}
	}
	
	public static void reset() {
		doReset = false;
		player = new Player();
		entities = new ArrayList<Entity>();
		ts = new TerrainSection(0, 4096);
		ending = false;
	}
	
	public static void main(String[] args) {
		Game game = new Game();
		game.setPreferredSize(new Dimension(WIDTH*SCALE, HEIGHT*SCALE));
		
		JFrame frame = new JFrame("Limbo Dare");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(game);
		frame.pack();
		
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
		
		game.start();
	}
	
	public void start() {
		addKeyListener(this);
		
		System.out.println(tester.pixels[0] + " - " + tester.pixels[1]);
		
		reset();
		
		//entities.add(new TerrainSection(0, 4096));
		
		createBufferStrategy(3);
		requestFocus();
		new Thread(this).start();
	}
	
	public static boolean right = false;
	public static boolean left = false;
	public static boolean up = false;
	public static boolean down = false;
	//public static boolean upLast = false;
	//public static boolean downLast = false;
	
	public static boolean enter = false;
	public static boolean esc = false;
	//public static boolean enterLast = false;
	
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_RIGHT) right = true;
		if(e.getKeyCode()==KeyEvent.VK_LEFT) left = true;
		if(e.getKeyCode()==KeyEvent.VK_UP) up = true;
		if(e.getKeyCode()==KeyEvent.VK_DOWN) down = true;
		if(e.getKeyCode()==KeyEvent.VK_ENTER) enter = true;
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE) esc = true;
	}

	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_RIGHT) right = false;
		if(e.getKeyCode()==KeyEvent.VK_LEFT) left = false;
		if(e.getKeyCode()==KeyEvent.VK_UP) up = false;
		if(e.getKeyCode()==KeyEvent.VK_DOWN) down = false;
		if(e.getKeyCode()==KeyEvent.VK_ENTER) enter = false;
		if(e.getKeyCode()==KeyEvent.VK_ESCAPE) esc = false;
	}

	public void keyTyped(KeyEvent e) {
		/*System.out.println("key " + e.getKeyChar());
		int key = e.getKeyCode();
		if(e.getKeyCode()==KeyEvent.VK_UP){
			screen.menuIndex--;
			System.out.println("key");
		}
		if(e.getKeyCode()==KeyEvent.VK_DOWN){
			screen.menuIndex++;
			System.out.println("key");
		}
		if(screen.menuIndex<0) screen.menuIndex = 1;
		if(screen.menuIndex>1) screen.menuIndex = 0;*/
	}

}
