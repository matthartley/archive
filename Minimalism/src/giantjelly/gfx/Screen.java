package giantjelly.gfx;

import giantjelly.Game;

public class Screen extends Bitmap {
	
	public Screen() {
		super(Game.WIDTH, Game.HEIGHT);
	}
	
	public static double xScroll = 0;
	public static double yScroll = 0;
	public int menuIndex = 0;
	
	public void render() {
		double xPlayer = Game.player.x;
		double yPlayer = Game.player.y;
		double xx = (width/2-32)-xPlayer;
		double yy = (height/2-32)-yPlayer;
		xScroll += (xx-xScroll)/10;
		if(xScroll>0) xScroll = 0;
		if(xScroll<-Game.terrain.width+width) xScroll = -Game.terrain.width+width;
		yScroll += (yy-yScroll)/10;
		
		
		draw(Game.background, (int)(xScroll/10.0), 0, 1, false);
		for(int i = 0; i < Game.entities.size(); i++) Game.entities.get(i).render();
		Game.ts.render();
		Game.player.render();
		
		lightingShader();
		
		//draw("fps " + Game.fps, 1, 1, 1);
		//draw("The quick brown fox", 0, 8, 1);
		//draw("Limbo Dare", (width/2)-(5*8), 40, 1);
		
		if(Game.player.dead){
			if(Game.ending){
				draw("Le Fin", width/2-(int)(3d*16.0d), 32, 2);
				
				draw("Sadly this story can not end", width/2-(int)(14d*8.0d), 32+24, 1);
				draw("with anything but death", width/2-(int)(11.5d*8.0d), 32+32, 1);
				
				draw("press escape to return to the menu", width/2-(int)(17d*8.0d), 32+48, 1);
				
				if(Game.esc){
					Game.doReset = true;
					Game.inGame = false;
				}
			}else{
				draw("You are dead!", width/2-(int)(6.5d*16.0d), 32, 2);
				//draw("press enter to restart", width/2-(int)(11d*8.0d), 48, 1);
				draw("press escape to return to the menu", width/2-(int)(17d*8.0d), 48, 1);
				
				if(Game.esc){
					Game.doReset = true;
					Game.inGame = false;
				}
			}
		}
	}
	
	public void renderMenu() {
		if(Game.up){
			menuIndex=0;
		}
		if(Game.down){
			menuIndex=1;
		}
		if(menuIndex<0) menuIndex = 1;
		if(menuIndex>1) menuIndex = 0;
		
		if(Game.enter){
			if(menuIndex==0) Game.inGame = true;
			if(menuIndex==1) System.exit(0);
		}
		
		draw("Limbo Dare", (width/2)-(5*16), 32, 2);
		
		boolean flash = Game.flash;
		String play = (flash&&menuIndex==0) ? "Play -" : "Play";
		String exit = (flash&&menuIndex==1) ? "Exit -" : "Exit";
		
		draw(play, (width/2)-(2*8), 64, 1);
		draw(exit, (width/2)-(2*8), 64+16, 1);
		
		draw("Use arrow keys and", 0, 128, 1);
		draw("enter for the menu", 0, 128+8, 1);
		
		draw("Use arrow keys to", 0, 160, 1);
		draw("move and jump in game", 0, 160+8, 1);
	}
	
	public void lightingShader() {
		double xPlayer = Game.player.x;
		double yPlayer = Game.player.y;
		
		for(int y = 0; y < height; y++){
			for(int x = 0; x < width; x++){
				int color = pixels[x+y*width];
				
				int r = (color >> 16) & 0xff;
				int g = (color >> 8) & 0xff;
				int b = (color) & 0xff;
				
				double xc =xPlayer+xScroll+32;
				double yc = yPlayer+yScroll+32;
				
				double xDis = (xc < x) ? x-xc : xc-x;
				double yDis = (yc < y) ? y-yc : yc-y;
				double dis = Math.sqrt((xDis*xDis)+(yDis*yDis));
				if(dis<20) dis = 20;
				double brightness = 20/(dis);
				if(brightness>0.3) brightness=0.3;
				
				r = (int)(r*brightness);
				g = (int)(g*brightness);
				b = (int)(b*brightness);
				
				r *= 1.5;
				g *= 1.5;
				b *= 1.5;
				
				pixels[x+y*width] = r << 16 | g << 8 | b;
			}
		}
	}
	
}
