package giantjelly.gfx;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import giantjelly.Game;

public class Bitmap {
	
	public int[] pixels;
	public int height;
	public int width;
	
	public Bitmap(String file) {
		BufferedImage res = null;
		
		try{
			res = ImageIO.read(Bitmap.class.getResourceAsStream(file));
		}catch(IOException e){
			System.out.println(e);
		}
		
		pixels = res.getRGB(0, 0, res.getWidth(), res.getHeight(), null, 0, res.getWidth());
		height = res.getHeight();
		width = res.getWidth();
	}
	
	public Bitmap(int w, int h) {
		pixels = new int[w*h];
		height = h;
		width = w;
	}
	
	/*public void draw(Bitmap bitmap) {
		
	}*/
	
	public void draw(int x, int y, int w, int h) {
		for(int yy = y; yy < y+h; yy++){
			if(yy < 0 || yy >= height) continue;
			for(int xx = x; xx < x+w; xx++){
				if(xx < 0 || xx >= width) continue;
				pixels[xx+yy*width] = 0x73a000;
			}
		}
	}
	
	public void draw(Bitmap bitmap, int x, int y, int size, boolean flip) {
		
		for(int h = 0; h < bitmap.height*size; h++){
			if(y+h < 0 || y+h > height-1) continue;
			for(int w = 0; w < bitmap.width*size; w++){
				if(x+w < 0 || x+w > width-1) continue;
				int thisPixel = (x+w)+(y+h)*width;
				int bitmapPixel = (w/size)+(h/size)*bitmap.width;
				if(flip) bitmapPixel = (bitmap.width-1-(w/size))+(h/size)*bitmap.width;
				if(bitmap.pixels[bitmapPixel] != -11075457 && bitmap.pixels[bitmapPixel] != -4784384) pixels[thisPixel] = bitmap.pixels[bitmapPixel];
			}
		}
		
	}
	
	String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz!- ";
	public void draw(String text, int x, int y, int scale) {
		for(int i = 0; i < text.length(); i++){
			int index = chars.indexOf(text.charAt(i));
			if(index>=0) draw(Game.font.cut( (index%16)*8, (index/16)*8, 8, 8 ), x+(i*(8*scale)), y, scale, false);
		}
	}
	
	public Bitmap cut(int x, int y, int w, int h) {
		Bitmap bitmap = new Bitmap(w, h);
		
		for(int yy = 0; yy < h; yy++){
			for(int xx = 0; xx < w; xx++){
				bitmap.pixels[ xx+yy*w ] = 
						pixels[ (x+xx)+(y+yy)*width ];
			}
		}
		
		return bitmap;
	}
	
}
