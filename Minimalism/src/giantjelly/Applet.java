package giantjelly;

import java.awt.BorderLayout;

public class Applet extends java.applet.Applet {
	
	private Game game = new Game();
	
	public void init() {
		setLayout(new BorderLayout());
		add(game, BorderLayout.CENTER);
	}
	
	public void start() {
		game.start();
	}
	
	public void stop() {
		game.stop();
	}
	
}
