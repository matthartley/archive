package giantjelly.entity;

import giantjelly.Game;
import giantjelly.Sound;
import giantjelly.gfx.Screen;

public class Plank extends Entity {
	
	public int aniFrame, aniCount = 0;
	public boolean down = false;
	
	public Plank(int xx, int yy) {
		x = xx;
		y = yy;
	}
	
	public void render() {
		Game.screen.draw(Game.playerSheet.cut(64*aniFrame, 0, 64, 64), (int)(x+Screen.xScroll), (int)(y+Screen.yScroll), 1, false);
		if(down) animate();
		
		/*int left = 22*Game.entityScale;
		int right = 41*Game.entityScale;
		int top = 13*Game.entityScale;
		int bottom = 54*Game.entityScale;
		
		if(Game.player.x+right > x) down = true;
		
		if(Game.player.x+right > x && Game.player.x+left < x+60){
			if(Game.player.y+bottom > y+61){
				Game.player.y = (y+61)-bottom;
				Game.player.collision = true;
				Game.player.ySpeed = 0;
			}
		}*/
	}
	
	public void animate() {
		if(aniFrame < 6){
			aniCount++;
			if(aniCount>2){
				aniCount=0;
				aniFrame++;
				if(aniFrame==6) Sound.hit.play();
			}
		}
		/*if(aniFrame>6){
			//aniFrame=0;
		}*/
	}
	
}
