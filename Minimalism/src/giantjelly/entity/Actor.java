package giantjelly.entity;

import giantjelly.Game;
import giantjelly.Sound;

public class Actor extends Entity {
	
	public Actor(int xx, int yy){
		x = xx;
		y = yy;
	}
	
	public int aniFrame = 0;
	public int aniCount = 0;
	public boolean flip = false;
	public double ySpeed = 0.0;
	public boolean collision = false;
	public boolean collisionLast = false;
	public boolean dead = false;
	
	public void render() {
		int left = 22*Game.entityScale;
		int right = 41*Game.entityScale;
		int top = 13*Game.entityScale;
		int bottom = 54*Game.entityScale;
		
		if(x+left <= 0) x = -left;
		if(x+right >= Game.terrain.width) x = Game.terrain.width - right;
		
		if(!collision) ySpeed += 0.1;
		moveY(ySpeed);
		
		if(!dead && collision && Game.up) jump();
		
		collisionLast = collision;
		collision = false;
		
		collisions();
	}
	
	public void jump() {
	}
	
	public void die() {
		if(!dead){
			dead = true;
			Sound.dead.play();
		}
	}
	
	public void collisions() {
		int left = 22*Game.entityScale;
		int right = 41*Game.entityScale;
		int top = 13*Game.entityScale;
		int bottom = 54*Game.entityScale;
		int tileSize = 8;
		
		
		//TerrainSection
		int index = (int)((x+32)-Game.ts.x);
		if(index>0 && index<Game.ts.ints.length && y+bottom > Game.ts.ints[index]+64){
			y = (Game.ts.ints[index]+64) - bottom;
			collision = true;
			ySpeed = 0;
		}
		
		
		if(!dead){
			//Spikes
			for(int i = 0; i < Game.ts.spikes.size(); i++){
				if(x+right >= Game.ts.spikes.get(i).x && x+left <= Game.ts.spikes.get(i).x+tileSize &&
				y+bottom >= Game.ts.spikes.get(i).y && y+top <= Game.ts.spikes.get(i).y+tileSize){
					die();
				}
			}
			
			
			//Planks
			for(int i = 0; i < Game.ts.planks.size(); i++){
				//if(x+right > Game.ts.planks.get(i).x) Game.ts.planks.get(i).down = true;
				if(Game.ts.planks.get(i).down && x+right > Game.ts.planks.get(i).x && x+left < Game.ts.planks.get(i).x+60){
					if(y+bottom > Game.ts.planks.get(i).y+61){
						y = (Game.ts.planks.get(i).y+61)-bottom;
						collision = true;
						ySpeed = 0;
					}
				}
			}
		}
	}
	
}
