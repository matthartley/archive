package giantjelly.entity;

import java.util.Random;

import giantjelly.Game;
import giantjelly.gfx.Screen;

public class Spike extends Entity {
	
	public int xSprite = 0;
	
	public Spike(int xx, int yy) {
		x = xx;
		y = yy+64;
		Random random = new Random();
		xSprite = random.nextInt(4);
	}
	
	public void render() {
		Game.screen.draw(Game.sheet.cut(16+(8*xSprite), 0, 8, 8), (int)x+(int)Screen.xScroll, (int)y+(int)Screen.yScroll, 1, false);
		
		/*int left = 22*Game.entityScale;
		int right = 41*Game.entityScale;
		int top = 13*Game.entityScale;
		int bottom = 54*Game.entityScale;
		int tileSize = 8;
		
		if(
				Game.player.x+right >= x && Game.player.x+left <= x+tileSize &&
				Game.player.y+bottom >= y && Game.player.y+top <= y+tileSize
		){
			Game.player.die();
		}*/
	}
	
}
