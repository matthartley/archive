package giantjelly.entity;

import java.util.ArrayList;

import giantjelly.Game;
import giantjelly.gfx.Screen;

public class TerrainSection extends Entity {
	
	public int[] ints;
	public ArrayList<Spike> spikes = new ArrayList<Spike>();
	public ArrayList<Plank> planks = new ArrayList<Plank>();
	
	public TerrainSection(int xStart, int xEnd) {
		ints = new int[xEnd-xStart];
		
		for(int x = xStart; x < xEnd; x++){
			for(int y = 0; y < Game.terrain.height; y++){
				if(Game.terrain.pixels[x+y*Game.terrain.width] == -16777216){
					ints[x-xStart] = y;
					break;
				}
				if(Game.terrain.pixels[x+y*Game.terrain.width] == -38400){
					spikes.add(new Spike(x, y-7));
				}
				if(Game.terrain.pixels[x+y*Game.terrain.width] == -10240){
					planks.add(new Plank(x-2, (y+64)-60));
				}
				if(Game.terrain.pixels[x+y*Game.terrain.width] == -65536){
					Game.entities.add(new Enemy(x-32, y-54));
				}
				if(Game.terrain.pixels[x+y*Game.terrain.width] == -16739073){
					Game.entities.add(new Squasher(x, y+63));
				}
			}
		}
		
		x = xStart;
	}
	
	public void render() {
		for(int i = 0; i < ints.length; i++){
			Game.screen.draw((int)x+i+(int)Screen.xScroll, ints[i]+64+(int)Screen.yScroll, 1, Game.HEIGHT-(ints[i]+64+(int)Screen.yScroll));
		}
		for(int i = 0; i < spikes.size(); i++){
			spikes.get(i).render();
		}
		for(int i = 0; i < planks.size(); i++){
			planks.get(i).render();
		}
		
		/*int left = 22*Game.entityScale;
		int right = 41*Game.entityScale;
		int top = 13*Game.entityScale;
		int bottom = 54*Game.entityScale;
		
		
		
		int index = (int)((Game.player.x+32)-x);
		if(index>0 && index<ints.length && Game.player.y+bottom > ints[index]+64){
			Game.player.y = (ints[index]+64) - bottom;
			Game.player.collision = true;
			Game.player.ySpeed = 0;
		}*/
	}
	
}
