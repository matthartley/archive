package giantjelly.entity;

import giantjelly.Game;
import giantjelly.Sound;
import giantjelly.gfx.Screen;

public class Squasher extends Entity {
	
	public Squasher(int xx, int yy) {
		x = xx;
		y = yy;
	}
	
	public double ySlab = 200;
	public double ySpeed = 0;
	public boolean active = false;
	
	public void render() {
		Game.screen.draw(Game.sheet.cut(0, 8, 32, 2), (int)(x+Screen.xScroll), (int)(y+Screen.yScroll), 1, false);
		Game.screen.draw(Game.sheet.cut(64, 0, 64, 64), (int)(x+Screen.xScroll)-16, (int)((y+Screen.yScroll)-ySlab), 1, false);
		
		int left = 22*Game.entityScale;
		int right = 41*Game.entityScale;
		int top = 13*Game.entityScale;
		int bottom = 54*Game.entityScale;
		
		if(!active && ySlab>=200 && Game.player.x+32<x+32 && Game.player.x+32>x && Game.player.y+bottom>y){
			active = true;
			Sound.click.play();
		}
		
		if(active){
			if(ySlab > 64){
				ySpeed++;
			}
			if(ySlab < 64){
				ySlab = 64;
				ySpeed = 0;
				active = false;
				Sound.explosion.play();
			}
			
			ySlab -= ySpeed;
		}else{
			if(ySlab < 200) ySlab++;
		}
		
		if(Game.player.x+left<x+48 && Game.player.x+right>x-16 && Game.player.y+top<(y-ySlab)+64 && Game.player.y+bottom>(y-ySlab)){
			Game.player.die();
		}
	}
	
}
