package giantjelly.entity;

import giantjelly.Game;
import giantjelly.gfx.Screen;

public class Enemy extends Actor {
	
	public Enemy(int xx, int yy) {
		super(xx, yy);
	}

	public void render() {
		if(!dead) Game.screen.draw(Game.playerSheet.cut(64*aniFrame, 256, 64, 64),  (int)(x+Screen.xScroll), (int)(y+Screen.yScroll), Game.entityScale, flip);
		else Game.screen.draw(Game.playerSheet.cut(64, 192, 64, 64),  (int)(x+Screen.xScroll), (int)(y+Screen.yScroll), Game.entityScale, false);
		
		if(!dead && !Game.player.dead){
			int left = 22*Game.entityScale;
			int right = 41*Game.entityScale;
			int top = 13*Game.entityScale;
			int bottom = 54*Game.entityScale;
			
			if(
					Game.player.x+left < x+right &&
					Game.player.x+right > x+left &&
					Game.player.y+top < y+bottom &&
					Game.player.y+bottom > y+top
			)
			{
				Game.player.die();
			}
			
			double distance = (x < Game.player.x) ? Game.player.x-x : x-Game.player.x;
			
			if(distance < Game.WIDTH/2){
				if(x < Game.player.x){
					moveX(1.5);
					flip = false;
				}else{
					moveX(-1.5);
					flip = true;
				}
				
				animate();
				//System.out.println("im coming");
			}
		}
		
		super.render();
	}
	
	public void animate() {
		aniCount++;
		if(aniCount>6){
			aniCount=0;
			aniFrame++;
		}
		if(aniFrame>7){
			aniFrame=0;
		}
	}
	
}
