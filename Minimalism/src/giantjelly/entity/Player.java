package giantjelly.entity;

import giantjelly.Game;
import giantjelly.Sound;
import giantjelly.gfx.Screen;

public class Player extends Actor {
	
	/*public int aniFrame = 0;
	public int aniCount = 0;
	public boolean flip = false;*/
	
	public boolean jumping = false;
	
	public Player() {
		super(100, 0);
	}
	
	public void render() {
		//dead = false;
		
		if(!dead){
			if(Game.right){
				moveX(1.5);
				flip = false;
			}
			if(Game.left){
				moveX(-1.5);
				flip = true;
			}
			
			if((Game.right||Game.left)) animate();
			else{
				aniFrame = 0;
				aniCount = 0;
			}
		}
		
		//if(!collision) aniFrame = 1;
		if(collision && jumping){
			jumping = false;
			Sound.land.play();
		}
		
		if(!dead) Game.screen.draw(Game.playerSheet.cut(64*aniFrame, 128, 64, 64),  (int)(x+Screen.xScroll), (int)(y+Screen.yScroll), Game.entityScale, flip);
		else Game.screen.draw(Game.playerSheet.cut(64, 192, 64, 64),  (int)(x+Screen.xScroll), (int)(y+Screen.yScroll), Game.entityScale, false);
		//Game.screen.draw((int)(x+Screen.xScroll)+32, (int)(y+Screen.yScroll)+32, 1, 1);
		
		for(int i = 0; i < Game.ts.planks.size(); i++){
			if(!Game.ts.planks.get(i).down && x+41 > Game.ts.planks.get(i).x){
				Game.ts.planks.get(i).down = true;
				Sound.click.play();
			}
		}
		
		if(!Game.ending && x+48 > Game.endingStart){
			Game.ending = true;
			Game.entities.add(new Enemy(Game.endingStart+130, 0));
			Game.entities.add(new Enemy(Game.endingStart-100, 0));
			Game.entities.add(new Enemy(Game.endingStart-200, 0));
		}
		
		super.render();
	}
	
	public void animate() {
		aniCount++;
		if(aniCount>6){
			aniCount=0;
			aniFrame++;
			//if(aniFrame==3||aniFrame==7) Sound.land.play();
		}
		if(aniFrame>7){
			aniFrame=0;
		}
	}
	
	public void jump() {
		ySpeed = -3;
		//aniFrame = 1;
		jumping = true;
	}
	
}
