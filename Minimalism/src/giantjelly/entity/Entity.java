package giantjelly.entity;

public class Entity {
	
	public double x = 0;
	public double y = 0;
	
	public void moveX(double xx) {
		x += xx;
	}
	
	public void moveY(double yy) {
		y += yy;
	}
	
	public void render() {
	}
	
}
