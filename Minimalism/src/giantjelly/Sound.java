package giantjelly;

import java.applet.Applet;
import java.applet.AudioClip;

public class Sound {
	
	public static Sound step = new Sound("/step.wav");
	public static Sound hit = new Sound("/hit.wav");
	public static Sound click = new Sound("/click.wav");
	public static Sound explosion = new Sound("/explosion.wav");
	public static Sound dead = new Sound("/dead.wav");
	public static Sound land = new Sound("/land.wav");
	
	private AudioClip clip;
	
	public Sound(String file){
		
		clip = Applet.newAudioClip(Sound.class.getResource(file));
	}
	
	public void play() {
		new Thread() {
			public void run() {
				clip.play();
			}
		}.start();
	}
	
}
