package com.giantjelly.lime;

import static org.lwjgl.opengl.GL11.*;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;

public class GLLight {
	static public void Ambient(float r, float g, float b){
		FloatBuffer pos;
		pos = BufferUtils.createFloatBuffer(4).put(new float[] { r, g, b, 1.0f});
        pos.flip();
        glLightModel(GL_LIGHT_MODEL_AMBIENT, pos);
	}
	
	static public void Pos(float x, float y, float z){
		FloatBuffer pos;
		pos = BufferUtils.createFloatBuffer(4).put(new float[] { x, y, z, 1.0f});
        pos.flip();
		glLight(GL_LIGHT0, GL_POSITION, pos);
	}
}
