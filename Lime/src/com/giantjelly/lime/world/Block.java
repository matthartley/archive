package com.giantjelly.lime.world;

import static org.lwjgl.opengl.GL11.*;

//import static com.giantjelly.lime.Light.Pos;
import com.giantjelly.lime.Lime;
import com.giantjelly.lime.LimeMath;
import com.giantjelly.lime.entity.Torch;
import com.giantjelly.lime.entity.Vertex;
import com.giantjelly.lime.entity.Light;

public class Block {
	public int x;
	public int y;
	public int z;
	public float Light = 1f;
	public float Color = (LimeMath.Rand.nextFloat() * 0.01f) + 0.0f;
	
	public Block(int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public int ClosestTorch(float x, float y, float z){
		int t = 0;
		
		float Dis = 0;
		
		for(int i = 0; i < Lime.Lights.size(); i++){
			if(LimeMath.Positive(x - (Lime.Lights.get(i).x+0.5f)) + LimeMath.Positive(z - (Lime.Lights.get(i).z+0.5f)) < Dis){
				Dis = LimeMath.Positive(x - (Lime.Lights.get(i).x+0.5f)) + LimeMath.Positive(z - (Lime.Lights.get(i).z+0.5f));
				t = i;
			}
			
			if(i == 0){
				Dis = LimeMath.Positive(x - (Lime.Lights.get(i).x+0.5f)) + LimeMath.Positive(z - (Lime.Lights.get(i).z+0.5f));
			}
		}
		
		return t;
	}
	
	private void VertexLight(float x, float y, float z){
		float Light = 0.2f;
		
		/*Light = 1 /
				( LimeMath.Positive(Lime.Lights.get(0this.ClosestTorch()).x - x)
				+ LimeMath.Positive(Lime.Lights.get(0this.ClosestTorch()).z - z)
				+ LimeMath.Positive(Lime.Lights.get(0this.ClosestTorch()).y - y)
				);*/
		
		//for(Light l : Lime.Lights){
			float xx = LimeMath.Positive((Lime.Lights.get(this.ClosestTorch(x, y, z)).x+0.5f) - x)*LimeMath.Positive((Lime.Lights.get(this.ClosestTorch(x, y, z)).x+0.5f) - x);
			float zz = LimeMath.Positive((Lime.Lights.get(this.ClosestTorch(x, y, z)).z+0.5f) - z)*LimeMath.Positive((Lime.Lights.get(this.ClosestTorch(x, y, z)).z+0.5f) - z);
			float yy = LimeMath.Positive((Lime.Lights.get(this.ClosestTorch(x, y, z)).y+0.5f) - y)*LimeMath.Positive((Lime.Lights.get(this.ClosestTorch(x, y, z)).y+0.5f) - y);
			
			if(Math.sqrt(xx+zz+yy) < 10){
				
				Light = 1 / ((float)Math.sqrt(xx+zz+yy)/2);
			}
		//}
		
		if(Light > 1.0f)
			Light = 1;
		
		//glColor4f(0.5f * (Light * 1.6f), 0.8f * (Light * 1.6f), 0.2f * Light, 1);
		glColor4f(Light, Light, Light, 1);
	}
	
	private void SideLight(float x, float t, float z){
		
	}
	
	public void OldDraw(){
		glDisable(GL_TEXTURE_2D);
		
		if(this.Light == 0)
			glColor4f(0.5f , 0.8f , 0.2f, 1);
		else
			glColor4f((0.5f * (this.Light * 1.6f)) - this.Color, (0.8f * (this.Light * 1.6f)) - this.Color, (0.2f * this.Light) - this.Color, 1);
		
		glBegin(GL_QUADS);
			glNormal3f(0, 0, 1);
			glVertex3f(this.x, 		this.y, 	this.z + 1);//front
			glVertex3f(this.x + 1, 	this.y, 	this.z + 1);
			glVertex3f(this.x + 1, 	this.y - 1, this.z + 1);
			glVertex3f(this.x, 		this.y - 1, this.z + 1);
			
			glNormal3f(0, 0, -1);
			glVertex3f(this.x, 		this.y, 	this.z);//back
			glVertex3f(this.x + 1, 	this.y, 	this.z);
			glVertex3f(this.x + 1, 	this.y - 1, this.z);
			glVertex3f(this.x, 		this.y - 1, this.z);
			
			glNormal3f(1, 0, 0);
			glVertex3f(this.x + 1, 	this.y, 	this.z + 1);//right
			glVertex3f(this.x + 1, 	this.y, 	this.z);
			glVertex3f(this.x + 1, 	this.y - 1, this.z);
			glVertex3f(this.x + 1, 	this.y - 1, this.z + 1);
			
			glNormal3f(-1, 0, 0);
			glVertex3f(this.x, 		this.y, 	this.z);//left
			glVertex3f(this.x, 		this.y, 	this.z + 1);
			glVertex3f(this.x, 		this.y - 1, this.z + 1);
			glVertex3f(this.x, 		this.y - 1, this.z);
			
			glNormal3f(0, 1, 0);
			glVertex3f(this.x, 		this.y, 	this.z);//top
			glVertex3f(this.x + 1, 	this.y, 	this.z);
			glVertex3f(this.x + 1, 	this.y, 	this.z + 1);
			glVertex3f(this.x, 		this.y, 	this.z + 1);
			
			glNormal3f(0, -1, 0);
			glVertex3f(this.x, 		this.y - 1, this.z + 1);//bottom
			glVertex3f(this.x + 1, 	this.y - 1, this.z + 1);
			glVertex3f(this.x + 1, 	this.y - 1, this.z);
			glVertex3f(this.x, 		this.y - 1, this.z);
		glEnd();
	}
	
	public void Draw(){
		glDisable(GL_TEXTURE_2D);
		
		glColor4f((0.5f * (this.Light * 1.6f)) - this.Color, (0.8f * (this.Light * 1.6f)) - this.Color, (0.2f * this.Light) - this.Color, 1);
		
		//glBegin(GL_QUADS);
		
		boolean left = true;
		boolean right = true;
		boolean front = true;
		boolean back = true;
		boolean top = true;
		boolean bottom = true;
		
		for(Block b : Lime.Blocks){
			if(b.z == z && b.y == y && b.x == x-1)
				left = false;
			
			if(b.z == z && b.y == y && b.x == x+1)
				right = false;
			
			if(b.z == z-1 && b.y == y && b.x == x)
				back = false;
			
			if(b.z == z+1 && b.y == y && b.x == x)
				front = false;
			
			if(b.z == z && b.y == y-1 && b.x == x)
				bottom = false;
			
			if(b.z == z && b.y == y+1 && b.x == x)
				top = false;
		}
		
			glEnable(GL_TEXTURE_2D);
			Lime.Stone.bind();
		
			if(front){
				//glBegin(GL_QUADS);
				this.DrawFront();
				//glEnd();
			}
			
			if(back){
				//glBegin(GL_QUADS);
				this.DrawBack();
				//glEnd();
			}
			
			if(right){
				//glBegin(GL_QUADS);
				this.DrawRight();
				//glEnd();
			}
			
			if(left){
				//glBegin(GL_QUADS);
				this.DrawLeft();
				//glEnd();
			}
			
			if(top){
				this.DrawTop();
			}
			
			if(bottom){
				glBegin(GL_QUADS);
				this.DrawBottom();
				glEnd();
			}
			
			
			
		//glEnd();
	}
	
	private void DrawFront(){
		glBegin(GL_QUADS);
		
		Vertex.draw(this.x, 		this.y, 	this.z + 1, 0, 0);//front
		Vertex.draw(this.x + 1, 	this.y, 	this.z + 1, 64, 0);
		Vertex.draw(this.x + 1, 	this.y - 1, this.z + 1, 64, 64);
		Vertex.draw(this.x, 		this.y - 1, this.z + 1, 0, 64);
		
		glEnd();
	}
	
	private void DrawBack(){
		glBegin(GL_QUADS);
		
		Vertex.draw(this.x, 		this.y, 	this.z, 0, 0);//back
		Vertex.draw(this.x + 1, 	this.y, 	this.z, 64, 0);
		Vertex.draw(this.x + 1, 	this.y - 1, this.z, 64, 64);
		Vertex.draw(this.x, 		this.y - 1, this.z, 0, 64);
		
		glEnd();
	}
	
	private void DrawRight(){
		glBegin(GL_QUADS);
		
		Vertex.draw(this.x + 1, 	this.y, 	this.z + 1, 0, 0);//right
		Vertex.draw(this.x + 1, 	this.y, 	this.z, 64, 0);
		Vertex.draw(this.x + 1, 	this.y - 1, this.z, 64, 64);
		Vertex.draw(this.x + 1, 	this.y - 1, this.z + 1, 0, 64);
		
		glEnd();
	}

	private void DrawLeft(){
		glBegin(GL_QUADS);
		
		Vertex.draw(this.x, 		this.y, 	this.z, 	0, 0);//left
		Vertex.draw(this.x, 		this.y, 	this.z + 1, 64, 0);
		Vertex.draw(this.x, 		this.y - 1, this.z + 1, 64, 64);
		Vertex.draw(this.x, 		this.y - 1, this.z, 	0, 64);
		
		glEnd();
	}

	private void DrawTop(){
		glBegin(GL_QUADS);
		
			Vertex.draw(this.x, this.y,	this.z, 		64, 0);//top
			Vertex.draw(this.x + 1, this.y, this.z, 	128, 0);
			Vertex.draw(this.x + 1, this.y, this.z + 1, 128, 64);
			Vertex.draw(this.x, this.y, this.z + 1, 	64, 64);
		
		glEnd();
	}

	private void DrawBottom(){
		/*glColor4f(0.5f, 0.8f, 0.2f, 1);
		glVertex3f(this.x, 		this.y - 1, this.z + 1);//bottom
		glVertex3f(this.x + 1, 	this.y - 1, this.z + 1);
		glVertex3f(this.x + 1, 	this.y - 1, this.z);
		glVertex3f(this.x, 		this.y - 1, this.z);*/
		
		glBegin(GL_QUADS);
		
			Vertex.draw(this.x, this.y-1, this.z+1, 	0, 0);//top
			Vertex.draw(this.x+1, this.y-1, this.z+1, 	64, 0);
			Vertex.draw(this.x+1, this.y-1, this.z, 	64, 64);
			Vertex.draw(this.x, this.y-1, this.z, 		0, 64);
		
		glEnd();
	}
	
	public void DrawFrame(){
		glDisable(GL_TEXTURE_2D);
		glColor4f(0, 0, 0, 1);
		
		glBegin(GL_LINE_LOOP);
			glVertex3f(this.x, 		this.y, 	this.z + 1);//front
			glVertex3f(this.x + 1, 	this.y, 	this.z + 1);
			glVertex3f(this.x + 1, 	this.y - 1, this.z + 1);
			glVertex3f(this.x, 		this.y - 1, this.z + 1);
		glEnd();
		
		glBegin(GL_LINE_LOOP);
			glVertex3f(this.x, 		this.y, 	this.z);//back
			glVertex3f(this.x + 1, 	this.y, 	this.z);
			glVertex3f(this.x + 1, 	this.y - 1, this.z);
			glVertex3f(this.x, 		this.y - 1, this.z);
		glEnd();
	
		glBegin(GL_LINE_LOOP);
			glVertex3f(this.x + 1, 	this.y, 	this.z + 1);//right
			glVertex3f(this.x + 1, 	this.y, 	this.z);
			glVertex3f(this.x + 1, 	this.y - 1, this.z);
			glVertex3f(this.x + 1, 	this.y - 1, this.z + 1);
		glEnd();
		
		glBegin(GL_LINE_LOOP);
			glVertex3f(this.x, 		this.y, 	this.z);//left
			glVertex3f(this.x, 		this.y, 	this.z + 1);
			glVertex3f(this.x, 		this.y - 1, this.z + 1);
			glVertex3f(this.x, 		this.y - 1, this.z);
		glEnd();
		
		glBegin(GL_LINE_LOOP);
			glVertex3f(this.x, 		this.y, 	this.z);//top
			glVertex3f(this.x + 1, 	this.y, 	this.z);
			glVertex3f(this.x + 1, 	this.y, 	this.z + 1);
			glVertex3f(this.x, 		this.y, 	this.z + 1);
		glEnd();
		
		glBegin(GL_LINE_LOOP);
			glVertex3f(this.x, 		this.y - 1, this.z + 1);//bottom
			glVertex3f(this.x + 1, 	this.y - 1, this.z + 1);
			glVertex3f(this.x + 1, 	this.y - 1, this.z);
			glVertex3f(this.x, 		this.y - 1, this.z);
		glEnd();
	}
	
	public void DrawPurpleFront(){
		glColor4f(1, 0, 1, 1);
		glBegin(GL_QUADS);
			glVertex3f(this.x, 		this.y, 	this.z + 1.001f);//front
			glVertex3f(this.x + 1, 	this.y, 	this.z + 1.001f);
			glVertex3f(this.x + 1, 	this.y - 1, this.z + 1.001f);
			glVertex3f(this.x, 		this.y - 1, this.z + 1.001f);
		glEnd();
	}
	
	public void DrawPurpleBack(){
		glColor4f(1, 0, 1, 1);
		glBegin(GL_QUADS);
		glVertex3f(this.x, 		this.y, 	this.z - 0.001f);//back
		glVertex3f(this.x + 1, 	this.y, 	this.z - 0.001f);
		glVertex3f(this.x + 1, 	this.y - 1, this.z - 0.001f);
		glVertex3f(this.x, 		this.y - 1, this.z - 0.001f);
		glEnd();
	}
	
	public void DrawPurpleRight(){
		glColor4f(1, 0, 1, 1);
		glBegin(GL_QUADS);
		glVertex3f(this.x + 1.001f, 	this.y, 	this.z + 1);//right
		glVertex3f(this.x + 1.001f, 	this.y, 	this.z);
		glVertex3f(this.x + 1.001f, 	this.y - 1, this.z);
		glVertex3f(this.x + 1.001f, 	this.y - 1, this.z + 1);
		glEnd();
	}
	
	public void DrawPurpleLeft(){
		glColor4f(1, 0, 1, 1);
		glBegin(GL_QUADS);
		glVertex3f(this.x - 0.001f, 		this.y, 	this.z);//left
		glVertex3f(this.x - 0.001f, 		this.y, 	this.z + 1);
		glVertex3f(this.x - 0.001f, 		this.y - 1, this.z + 1);
		glVertex3f(this.x - 0.001f, 		this.y - 1, this.z);
		glEnd();
	}
	
	public void DrawPurpleTop(){
		glColor4f(1, 0, 1, 1);
		glBegin(GL_QUADS);
		glVertex3f(this.x, 		this.y + 0.001f, 	this.z);//top
		glVertex3f(this.x + 1, 	this.y + 0.001f, 	this.z);
		glVertex3f(this.x + 1, 	this.y + 0.001f, 	this.z + 1);
		glVertex3f(this.x, 		this.y + 0.001f, 	this.z + 1);
		glEnd();
	}
	
	public void DrawPurpleBottom(){
		glColor4f(1, 0, 1, 1);
		glBegin(GL_QUADS);
		glVertex3f(this.x, 		this.y - 1.001f, this.z + 1);//bottom
		glVertex3f(this.x + 1, 	this.y - 1.001f, this.z + 1);
		glVertex3f(this.x + 1, 	this.y - 1.001f, this.z);
		glVertex3f(this.x, 		this.y - 1.001f, this.z);
		glEnd();
	}
	
	public void DrawPurpleBlock(){
		this.DrawPurpleFront();
		this.DrawPurpleBack();
		this.DrawPurpleRight();
		this.DrawPurpleLeft();
		this.DrawPurpleTop();
		this.DrawPurpleBottom();
	}
}
