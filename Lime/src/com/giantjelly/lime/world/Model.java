package com.giantjelly.lime.world;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.util.vector.Vector3f;

import static org.lwjgl.opengl.GL11.*;

import com.giantjelly.lime.entity.Vertex;

public class Model {
	
	public ArrayList<Vector3f> vertices = new ArrayList<Vector3f>();
	public ArrayList<Vector3f> normals = new ArrayList<Vector3f>();
	public ArrayList<Face> faces = new ArrayList<Face>();
	
	public Model(String file) {
		try{
			
			BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
			
			String line;
			while((line = reader.readLine()) != null){
				
				if(line.startsWith("v ")){
					float x = Float.valueOf(line.split(" ")[1]);
					float y = Float.valueOf(line.split(" ")[2]);
					float z = Float.valueOf(line.split(" ")[3]);
					vertices.add(new Vector3f(x, y, z));
				}else
				if(line.startsWith("vn ")){
					float x = Float.valueOf(line.split(" ")[1]);
					float y = Float.valueOf(line.split(" ")[2]);
					float z = Float.valueOf(line.split(" ")[3]);
					normals.add(new Vector3f(x, y, z));
				}else
				if(line.startsWith("f ")){
					
					Vector3f vi = new Vector3f(
						Float.valueOf(line.split(" ")[1].split("/")[0]),
						Float.valueOf(line.split(" ")[2].split("/")[0]),
						Float.valueOf(line.split(" ")[3].split("/")[0])
					);
					
					System.out.println(line);
					
					Vector3f ni = new Vector3f(
						Float.valueOf(line.split(" ")[1].split("/")[2]),
						Float.valueOf(line.split(" ")[2].split("/")[2]),
						Float.valueOf(line.split(" ")[3].split("/")[2])
					);
					
					faces.add(new Face(vi, ni));
					
				}
				
			}
			
		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	public void draw() {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		//glPushMatrix();
		///glTranslatef(-17, 1, 0);
		
		glColor4f(0.5f, 0.5f, 0.5f, 1);
		glDisable(GL_TEXTURE_2D);
		glBegin(GL_TRIANGLES);
			for(Face face : faces){
				Vector3f n1 = normals.get((int) face.normal.x-1);
				glNormal3f(n1.x, n1.y, n1.z);
				Vector3f v1 = vertices.get((int) face.vertex.x-1);
				Vertex.draw(v1.x, v1.y, v1.z);
				
				Vector3f n2 = normals.get((int) face.normal.y-1);
				glNormal3f(n2.x, n2.y, n2.z);
				Vector3f v2 = vertices.get((int) face.vertex.y-1);
				Vertex.draw(v2.x, v2.y, v2.z);
				
				Vector3f n3 = normals.get((int) face.normal.z-1);
				glNormal3f(n3.x, n3.y, n3.z);
				Vector3f v3 = vertices.get((int) face.vertex.z-1);
				Vertex.draw(v3.x, v3.y, v3.z);
			}
		glEnd();
		
		//glPopMatrix();
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	
}
