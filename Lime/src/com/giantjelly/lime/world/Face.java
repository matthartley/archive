package com.giantjelly.lime.world;

import org.lwjgl.util.vector.Vector3f;

public class Face {
	
	public Vector3f vertex = new Vector3f();
	public Vector3f normal = new Vector3f();
	
	public Face(Vector3f v, Vector3f n) {
		vertex = v;
		normal = n;
	}
	
}
