package com.giantjelly.lime.world;

import java.io.File;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import com.giantjelly.lime.Error;
import com.giantjelly.lime.Lime;
import com.giantjelly.lime.world.Block;
import com.giantjelly.lime.entity.Bulb;
import com.giantjelly.lime.entity.Light;
import com.giantjelly.lime.entity.Torch;

public class Data {
	public static void Save(){
		try{
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(new File("res/World.jelly")));
			
			for(Block Block : Lime.Blocks){
				dos.writeByte(0);
				dos.writeInt(Block.x);
				dos.writeInt(Block.y);
				dos.writeInt(Block.z);
			}
			
			for(Light Torch : Lime.Lights){
				dos.writeByte(1);
				dos.writeFloat(Torch.x);
				dos.writeFloat(Torch.y);
				dos.writeFloat(Torch.z);
			}
			
			dos.close();
		}catch(Exception e){
			Lime.ErrorLog.add(new Error("Saving error!", e));
		}
	}
	
	public static void Load(){
		try{
			DataInputStream dis = new DataInputStream(new FileInputStream(new File("res/World.jelly")));
			
			//while(Lime.Blocks.add(new Block(dis.readInt(), dis.readInt(), dis.readInt())));
			//dis.readUTF();
			//while(Lime.Torches.add(new Torch(dis.readFloat(), dis.readFloat(), dis.readFloat())));
			
			while(dis.available() != 0){
				int a = dis.readByte();
				
				if(a == 0)
					Lime.Blocks.add(new Block(dis.readInt(), dis.readInt(), dis.readInt()));
				if(a == 1)
					Lime.Lights.add(new Bulb((int)dis.readFloat(), (int)dis.readFloat(), (int)dis.readFloat()));
			}
			
			dis.close();
		}catch(Exception e){
			Lime.ErrorLog.add(new Error("Loading error!", e));
		}
	}
}
