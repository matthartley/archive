package com.giantjelly.lime;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;
import org.lwjgl.opengl.Display;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.awt.Font;
import java.io.IOException;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import java.util.ArrayList;
import java.util.Random;

import com.giantjelly.lime.entity.Player;
import com.giantjelly.lime.entity.Light;
import com.giantjelly.lime.world.Block;
import com.giantjelly.lime.world.Model;
import com.giantjelly.lime.entity.Torch;
import com.giantjelly.lime.LimeMath;
import com.giantjelly.lime.phys.Picking;
import com.giantjelly.lime.Error;
import com.giantjelly.lime.world.Data;
import com.giantjelly.lime.shaders.Shader;

public class Lime {
	public static Player Plyr = new Player();
	public static int Mousex;
	public static int Mousey;
	public static Texture Terrain;
	public static Texture Stone;
	public static TrueTypeFont Gui = new TrueTypeFont(new Font("monospaced", Font.PLAIN, 24), false);
	public static TrueTypeFont GuiError = new TrueTypeFont(new Font("monospaced", Font.PLAIN, 12), false);
	public static boolean GameMenu = true;
	public static boolean EscapeDown = false;
	
	static boolean ShaderLighting = false;
	
	public static ArrayList<Error> ErrorLog = new ArrayList<Error>();
	
	public static ArrayList<Block> Blocks = new ArrayList<Block>();
	public static ArrayList<Light> Lights = new ArrayList<Light>();
	
	public static Shader Default = new Shader("Default.vert", "Default.frag");
	public static Shader Lighting = new Shader("Lighting.vert", "Lighting.frag");
	
	public static float asd = Plyr.y-1.5f;
	
	public static Model gun = new Model("res/models/rifleex.obj");
	
	public static void Init(){
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_DEPTH_TEST);
		
		/*glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glEnable(GL_COLOR_MATERIAL);
		//glColorMaterial(GL_FRONT, GL_DIFFUSE);
		glEnable(GL_NORMALIZE);*/
		
		//Mouse.setCursorPosition(1440 / 2, 900 / 2);
		Mouse.setGrabbed(false);
		
		try{
			Terrain = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Terrain.png"), GL_NEAREST);
			Stone = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/stone.png"), GL_NEAREST);
		}catch(IOException e){
			e.printStackTrace();
			System.out.println(e);
		}
		
		/*try{
			Torch t = Lights.get(0);
		}catch(Exception e){
			ErrorLog.add(new Error("There are no Torches!", e));
		}*/
		
		Data.Load();
		
		//GenerateLighting();
	}
	
	public static void Update(){
		/*if(Lights.size() > 0)
			Lights.remove(0);*/
		
		Time.Update();
		Projection.Enable3d();
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		
		Plyr.Update();
		
		gun.draw();
		
		if(!GameMenu){
			Mouse.setCursorPosition(1440 / 2, 900 / 2);
		}
		
		if(!Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) EscapeDown = false;
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) && !EscapeDown){
			EscapeDown = true;
			
			if(GameMenu){
				GameMenu = false;
				Mouse.setGrabbed(true);
				Mouse.setCursorPosition(1440 / 2, 900 / 2);
			}else{
				GameMenu = true;
				Mouse.setGrabbed(false);
			}
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_1)) ShaderLighting = false;
		if(Keyboard.isKeyDown(Keyboard.KEY_2)) ShaderLighting = true;
		
		if(Keyboard.isKeyDown(Keyboard.KEY_R)){
			Default = new Shader("Default.vert", "Default.frag");
			Lighting = new Shader("Lighting.vert", "Lighting.frag");
		}
		
		while(Keyboard.next()){
			if(Keyboard.getEventKey() == Keyboard.KEY_F5 && Keyboard.getEventKeyState()){
				Data.Save();
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_F6 && Keyboard.getEventKeyState()){
				while(Blocks.size()>0)
					Blocks.remove(0);
				while(Lights.size()>0)
					Lights.remove(0);
				
				Data.Load();
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_F7 && Keyboard.getEventKeyState()){
				Lights.remove(0);
			}
		}
		
		for(int i = 0; i < Blocks.size(); i++){
			Blocks.get(i).Draw();
		}
		
		Picking.Update();
		
		for(int i = 0; i < Lights.size(); i++){
			Lights.get(i).draw();
		}
		
		
		//glLoadIdentity();
		//GLLight.Pos(0, 1, 2);
		//GLLight.Pos(-4, 1,-4);
		//GLLight.Ambient(0.2f, 0.2f, 0.2f);
		
		
		Projection.Enable2d();
		
		Gui.drawString(10, 10, "Lime DevBuild", Color.white);
		Gui.drawString(10, 40, "fps: " + Time.fps, Color.white);
		
		Gui.drawString(10, 100, "Number of Blocks: " + Blocks.size(), Color.white);
		Gui.drawString(10, 130, "Number of Torches: " + Lights.size(), Color.white);
		Gui.drawString(10, 160, "Shader Lighting: " + ShaderLighting, Color.white);
		Gui.drawString(10, 190,  "Delta: " + Time.Delta, Color.white);
		
		//Gui.drawString(10, 250,  "ScreenSize: " + ScreenSize.width + " * " + ScreenSize.height, Color.white);
		Gui.drawString(10, 280,  "Resized: " + Display.wasResized() + ", Width: " + Display.getWidth() + ", Height: " + Display.getHeight(), Color.white);
		Gui.drawString(10, 310, "Player.y: " + (asd));
		if(Plyr.y-1.5f > asd)
			asd = Plyr.y-1.5f;
		
		Gui.drawString(10, 340, "Player.x: " + Plyr.x);
		Gui.drawString(10, 370, "Player.z: " + Plyr.z);
		
		//Gui.drawString(10, 40, Picking.BlockSide, Color.white);
		//Gui.drawString(10, 70, ""+Picking.Id, Color.white);
		
		//Gui.drawString(10, 40, "Display Driver Adapter: "+Display.getAdapter(), Color.white);
		//Gui.drawString(10, 70, "Display Driver Version: "+Display.getVersion(), Color.white);
		//Gui.drawString(10, 100, "Display Mode: "+Display.getDisplayMode(), Color.white);
		
		//GuiError.drawString(10, (Display.getDisplayMode().getHeight() - 40) - (15 * ErrorLog.size()), "Error Log:", Color.white);
		for(int i = 0; i < ErrorLog.size(); i++){
			GuiError.drawString(10, ((Display.getDisplayMode().getHeight() - 5) - (15 * ErrorLog.size())) + (i * 15), ErrorLog.get(i).e + " - " + ErrorLog.get(i).Message, Color.white);
		}
		
		glEnable(GL_TEXTURE_2D);
		Terrain.bind();
		glColor4f(1, 1, 1, 1);
		glBegin(GL_QUADS);
			glTexCoord2f(0.5f, 0);			glVertex2f((Display.getWidth() / 2) - 16, (Display.getHeight() / 2) - 16);
			glTexCoord2f(1, 0);				glVertex2f((Display.getWidth() / 2) + 16, (Display.getHeight() / 2) - 16);
			glTexCoord2f(1, 0.5f);			glVertex2f((Display.getWidth() / 2) + 16, (Display.getHeight() / 2) + 16);
			glTexCoord2f(0.5f, 0.5f);		glVertex2f((Display.getWidth() / 2) - 16, (Display.getHeight() / 2) + 16);
		glEnd();
	}
}