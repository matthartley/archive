package com.giantjelly.lime.phys;

import org.lwjgl.input.Mouse;
import org.lwjgl.input.Keyboard;

import com.giantjelly.lime.Lime;
import com.giantjelly.lime.LimeMath;
import com.giantjelly.lime.world.Block;
import com.giantjelly.lime.entity.Bulb;
import com.giantjelly.lime.entity.Torch;
import com.giantjelly.lime.Error;

public class Picking {
	public static int Id = 0;
	public static String BlockSide = "";
	public static boolean Picked = false;
	public static boolean ClickDown = false;
	public static boolean RightDown = false;
	public static boolean DeleteDown = false;
	
	public static void Update(){
		if(!Mouse.isButtonDown(0))
			ClickDown = false;
		if(!Mouse.isButtonDown(1))
			RightDown = false;
		if(!Keyboard.isKeyDown(Keyboard.KEY_E))
			DeleteDown = false;
		
		float Pickx = Lime.Plyr.x;
		float Picky = Lime.Plyr.y;
		float Pickz = Lime.Plyr.z;
		
		for(int i = 0; i < 300; i++){
			Pickx -= (float)Math.sin(Math.toRadians(Lime.Plyr.Yaw)) / 100;
			Picky += (float)Math.tan(Math.toRadians(Lime.Plyr.Pitch)) / 100;
			Pickz -= (float)Math.cos(Math.toRadians(Lime.Plyr.Yaw)) / 100;
			
			boolean p = false;
			
			for(int j = 0; j < Lime.Blocks.size(); j++){
				if(
					Pickx > Lime.Blocks.get(j).x && Pickx < Lime.Blocks.get(j).x + 1 &&
					Picky < Lime.Blocks.get(j).y && Picky > Lime.Blocks.get(j).y - 1 &&
					Pickz > Lime.Blocks.get(j).z && Pickz < Lime.Blocks.get(j).z + 1
				)
				{
					
					float Front = (Lime.Blocks.get(j).z + 1) - Pickz;
					float Back = Pickz - (Lime.Blocks.get(j).z);
					
					float Right = (Lime.Blocks.get(j).x + 1) - Pickx;
					float Left = Pickx - (Lime.Blocks.get(j).x);
					
					float Top = (Lime.Blocks.get(j).y) - Picky;
					float Bottom = Picky - (Lime.Blocks.get(j).y - 1);
					
					float xDis = LimeMath.Smallest2(Right, Left);
					float yDis = LimeMath.Smallest2(Top, Bottom);
					float zDis = LimeMath.Smallest2(Front, Back);
					
					float Side = LimeMath.Smallest3(xDis, yDis, zDis);
					
					if(Side == xDis){
						if(xDis == Right){
							BlockSide = "Right";
							//Lime.Blocks.get(j).DrawPurpleRight();
						}
						else{
							BlockSide = "Left";
							//Lime.Blocks.get(j).DrawPurpleLeft();
						}
					}
					if(Side == yDis){
						if(yDis == Top){
							BlockSide = "Top";
							//Lime.Blocks.get(j).DrawPurpleTop();
						}
						else{
							BlockSide = "Bottom";
							//Lime.Blocks.get(j).DrawPurpleBottom();
						}
					}
					if(Side == zDis){
						if(zDis == Front){
							BlockSide = "Front";
							//Lime.Blocks.get(j).DrawPurpleFront();
						}
						else{
							BlockSide = "Back";
							//Lime.Blocks.get(j).DrawPurpleBack();
						}
					}
					
					if(Keyboard.isKeyDown(Keyboard.KEY_E) && !DeleteDown){
						DeleteDown = true;
						
						Lime.Blocks.remove(j);
					}
					
					if(Mouse.isButtonDown(0) && !ClickDown){
						ClickDown = true;
						
						if(Side == xDis){
							if(xDis == Right){
								Lime.Blocks.add(new Block(Lime.Blocks.get(j).x + 1, Lime.Blocks.get(j).y, Lime.Blocks.get(j).z));
							}
							else{
								Lime.Blocks.add(new Block(Lime.Blocks.get(j).x - 1, Lime.Blocks.get(j).y, Lime.Blocks.get(j).z));
							}
						}
						if(Side == yDis){
							if(yDis == Top){
								Lime.Blocks.add(new Block(Lime.Blocks.get(j).x, Lime.Blocks.get(j).y + 1, Lime.Blocks.get(j).z));
							}
							else{
								Lime.Blocks.add(new Block(Lime.Blocks.get(j).x, Lime.Blocks.get(j).y - 1, Lime.Blocks.get(j).z));
							}
						}
						if(Side == zDis){
							if(zDis == Front){
								Lime.Blocks.add(new Block(Lime.Blocks.get(j).x, Lime.Blocks.get(j).y, Lime.Blocks.get(j).z + 1));
							}
							else{
								Lime.Blocks.add(new Block(Lime.Blocks.get(j).x, Lime.Blocks.get(j).y, Lime.Blocks.get(j).z - 1));
							}
						}
					}
					
					if(Mouse.isButtonDown(1) && !RightDown){
						RightDown = true;
						
						if(Side == xDis){
							if(xDis == Right){
								Lime.Lights.add(new Bulb(Lime.Blocks.get(j).x + 1, Lime.Blocks.get(j).y, Lime.Blocks.get(j).z));
							}
							else{
								Lime.Lights.add(new Bulb(Lime.Blocks.get(j).x - 1, Lime.Blocks.get(j).y, Lime.Blocks.get(j).z));
							}
						}
						if(Side == yDis){
							if(yDis == Top){
								Lime.Lights.add(new Bulb(Lime.Blocks.get(j).x, Lime.Blocks.get(j).y + 1, Lime.Blocks.get(j).z));
							}
							else{
								Lime.Lights.add(new Bulb(Lime.Blocks.get(j).x, Lime.Blocks.get(j).y - 1, Lime.Blocks.get(j).z));
							}
						}
						if(Side == zDis){
							if(zDis == Front){
								Lime.Lights.add(new Bulb(Lime.Blocks.get(j).x, Lime.Blocks.get(j).y, Lime.Blocks.get(j).z + 1));
							}
							else{
								Lime.Lights.add(new Bulb(Lime.Blocks.get(j).x, Lime.Blocks.get(j).y, Lime.Blocks.get(j).z - 1));
							}
						}
					}
					
					
					try{
						Lime.Blocks.get(j).DrawFrame();
					}catch(Exception e){
						Lime.ErrorLog.add(new Error("Block doesn't exist!", e));
					}
					
					
					p = true;
					Id = j;
					j = Lime.Blocks.size();
					i = 300;
				}
			}
			
			Picked = p;
		}
	}
}
