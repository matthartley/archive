package com.giantjelly.lime.entity;

import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex3f;

import org.newdawn.slick.opengl.Texture;

import com.giantjelly.lime.Lime;
import com.giantjelly.lime.LimeMath;

public class Vertex {
	private static int closestlight(float x, float y, float z){
		int t = 0;
		
		float Dis = 0;
		
		for(int i = 0; i < Lime.Lights.size(); i++){
			if(LimeMath.Positive(x - (Lime.Lights.get(i).x+0.5f)) + LimeMath.Positive(z - (Lime.Lights.get(i).z+0.5f)) < Dis){
				Dis = LimeMath.Positive(x - (Lime.Lights.get(i).x+0.5f)) + LimeMath.Positive(z - (Lime.Lights.get(i).z+0.5f));
				t = i;
			}
			
			if(i == 0){
				Dis = LimeMath.Positive(x - (Lime.Lights.get(i).x+0.5f)) + LimeMath.Positive(z - (Lime.Lights.get(i).z+0.5f));
			}
		}
		
		return t;
	}
	
	private static void vertexlight(float x, float y, float z){
		float min = 0.1f;
		float Light = min;
	
		float xx = LimeMath.Positive((Lime.Lights.get(closestlight(x, y, z)).x+0.5f) - x)*LimeMath.Positive((Lime.Lights.get(closestlight(x, y, z)).x+0.5f) - x);
		float zz = LimeMath.Positive((Lime.Lights.get(closestlight(x, y, z)).z+0.5f) - z)*LimeMath.Positive((Lime.Lights.get(closestlight(x, y, z)).z+0.5f) - z);
		float yy = LimeMath.Positive((Lime.Lights.get(closestlight(x, y, z)).y-1) - y)*LimeMath.Positive((Lime.Lights.get(closestlight(x, y, z)).y-1) - y);
		
		if(Math.sqrt(xx+zz+yy) < 50){
			Light = 1  / ((float)Math.sqrt(xx+zz+yy) / Lime.Lights.get(closestlight(x, y, z)).intensity);
		}
	
		if(Light > 1.0f)
			Light = 1;
		if(Light < min)
			Light = min;
	
		glColor4f(Light, Light, Light, 1);
	}
	
	public static void draw(float x, float y, float z){
		vertexlight(x, y, z);
		glVertex3f(x, y, z);
	}
	
	public static void draw(float x, float y, float z, float tx, float ty){
		vertexlight(x, y, z);
		
		glTexCoord2f(tx/Lime.Stone.getImageWidth(), ty/Lime.Stone.getImageHeight());
		glVertex3f(x, y, z);
	}
}
