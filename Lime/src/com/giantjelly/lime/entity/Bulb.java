package com.giantjelly.lime.entity;

import static org.lwjgl.opengl.GL11.*;

public class Bulb extends Light {
	
	public Bulb(int xx, int yy, int zz) {
		super(xx, yy, zz, 2);
	}
	
	public static float SIZE = 0.25f;
	public static float HALFSIZE = SIZE/2;
	public static float LEFT = 0.5f-HALFSIZE;
	public static float RIGHT = 0.5f+HALFSIZE;
	
	public void draw() {
		glDisable(GL_TEXTURE_2D);
		glColor4f(1, 1, 1 ,1);
		
		glBegin(GL_QUADS);
		
			glVertex3f(x+LEFT, (y-1)+SIZE, z+LEFT);
			glVertex3f(x+RIGHT, (y-1)+SIZE, z+LEFT);
			glVertex3f(x+RIGHT, (y-1)+SIZE, z+RIGHT);
			glVertex3f(x+LEFT, (y-1)+SIZE, z+RIGHT);
			
			glVertex3f(x+LEFT, (y-1)+SIZE, z+RIGHT);
			glVertex3f(x+RIGHT, (y-1)+SIZE, z+RIGHT);
			glVertex3f(x+RIGHT, (y-1), z+RIGHT);
			glVertex3f(x+LEFT, (y-1), z+RIGHT);
			
			glVertex3f(x+RIGHT, (y-1)+SIZE, z+LEFT);
			glVertex3f(x+LEFT, (y-1)+SIZE, z+LEFT);
			glVertex3f(x+LEFT, (y-1), z+LEFT);
			glVertex3f(x+RIGHT, (y-1), z+LEFT);
			
			glVertex3f(x+RIGHT, (y-1)+SIZE, z+RIGHT);
			glVertex3f(x+RIGHT, (y-1)+SIZE, z+LEFT);
			glVertex3f(x+RIGHT, (y-1), z+LEFT);
			glVertex3f(x+RIGHT, (y-1), z+RIGHT);
			
			glVertex3f(x+LEFT, (y-1)+SIZE, z+LEFT);
			glVertex3f(x+LEFT, (y-1)+SIZE, z+RIGHT);
			glVertex3f(x+LEFT, (y-1), z+RIGHT);
			glVertex3f(x+LEFT, (y-1), z+LEFT);
		
		glEnd();
	}
	
}
