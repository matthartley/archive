package com.giantjelly.lime.entity;

import static org.lwjgl.opengl.GL11.*;

import com.giantjelly.lime.entity.Light;
import com.giantjelly.lime.Lime;

public class Torch extends Light {
	/*public float x;
	public float y;
	public float z;*/
	public static float xSize = 1.0f / 8.0f;
	public static float zSize = 1.0f / 8.0f;
	public static float BlockPos = 0.5f - ((1.0f / 8.0f) / 2);
	
	public Torch(float x, float y, float z){
		super(x, y, z, 1.5f);
	}
	
	public void draw(){
		glEnable(GL_TEXTURE_2D);
		Lime.Terrain.bind();
		glColor4f(1, 1, 1, 1);
		
		glBegin(GL_QUADS);
			glNormal3f(0, 0, 1);
			glTexCoord2f(0, 0.25f);						glVertex3f(this.x + BlockPos, 			this.y - 0.5f, 	this.z + BlockPos + xSize);//front
			glTexCoord2f(0.03125f, 0.25f);				glVertex3f(this.x + BlockPos + xSize, 	this.y - 0.5f, 	this.z + BlockPos + xSize);
			glTexCoord2f(0.03125f, 0.25f + 0.125f);		glVertex3f(this.x + BlockPos + xSize, 	this.y - 1, 	this.z + BlockPos + xSize);
			glTexCoord2f(0, 0.25f + 0.125f);			glVertex3f(this.x + BlockPos, 			this.y - 1, 	this.z + BlockPos + xSize);
			
			glNormal3f(0, 0, -1);
			glTexCoord2f(0, 0.25f);						glVertex3f(this.x + BlockPos + xSize, 			this.y - 0.5f, 	this.z + BlockPos);//back
			glTexCoord2f(0.03125f, 0.25f);				glVertex3f(this.x + BlockPos, 	this.y - 0.5f, 	this.z + BlockPos);
			glTexCoord2f(0.03125f, 0.25f + 0.125f);		glVertex3f(this.x + BlockPos, 	this.y - 1, 	this.z + BlockPos);
			glTexCoord2f(0, 0.25f + 0.125f);			glVertex3f(this.x + BlockPos + xSize, 			this.y - 1, 	this.z + BlockPos);
			
			glNormal3f(1, 0, 0);
			glTexCoord2f(0, 0.25f);						glVertex3f(this.x + BlockPos + xSize, 	this.y - 0.5f, 	this.z + BlockPos + xSize);//right
			glTexCoord2f(0.03125f, 0.25f);				glVertex3f(this.x + BlockPos + xSize, 	this.y - 0.5f, 	this.z + BlockPos);
			glTexCoord2f(0.03125f, 0.25f + 0.125f);		glVertex3f(this.x + BlockPos + xSize, 	this.y - 1, 	this.z + BlockPos);
			glTexCoord2f(0, 0.25f + 0.125f);			glVertex3f(this.x + BlockPos + xSize, 	this.y - 1, 	this.z + BlockPos + xSize);
			
			glNormal3f(-1, 0, 0);
			glTexCoord2f(0, 0.25f);						glVertex3f(this.x + BlockPos, 			this.y - 0.5f, 	this.z + BlockPos);//left
			glTexCoord2f(0.03125f, 0.25f);				glVertex3f(this.x + BlockPos, 			this.y - 0.5f, 	this.z + BlockPos + xSize);
			glTexCoord2f(0.03125f, 0.25f + 0.125f);		glVertex3f(this.x + BlockPos, 			this.y - 1, 	this.z + BlockPos + xSize);
			glTexCoord2f(0, 0.25f + 0.125f);			glVertex3f(this.x + BlockPos, 			this.y - 1, 	this.z + BlockPos);
			
			glNormal3f(0, 1, 0);
			glVertex3f(this.x + BlockPos, 					this.y - 0.5f, 	this.z + BlockPos);//top
			glVertex3f(this.x + BlockPos + (1.0f / 8.0f), 	this.y - 0.5f, 	this.z + BlockPos);
			glVertex3f(this.x + BlockPos + (1.0f / 8.0f), 	this.y - 0.5f, 	this.z + BlockPos + (1.0f / 8.0f));
			glVertex3f(this.x + BlockPos, 					this.y - 0.5f, 	this.z + BlockPos + (1.0f / 8.0f));
			
			glNormal3f(0, -1, 0);
			glVertex3f(this.x + BlockPos, 					this.y - 1, 	this.z + BlockPos + (1.0f / 8.0f));//bottom
			glVertex3f(this.x + BlockPos + (1.0f / 8.0f), 	this.y - 1, 	this.z + BlockPos + (1.0f / 8.0f));
			glVertex3f(this.x + BlockPos + (1.0f / 8.0f), 	this.y - 1, 	this.z + BlockPos);
			glVertex3f(this.x + BlockPos, 					this.y - 1, 	this.z + BlockPos);
		glEnd();
	}
}
