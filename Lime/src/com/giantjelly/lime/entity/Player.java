package com.giantjelly.lime.entity;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.giantjelly.lime.Lime;
import com.giantjelly.lime.Time;
import com.giantjelly.lime.world.Block;
import com.giantjelly.lime.LimeMath;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;

public class Player {
	public float x = 0;
	public float y = 1.5f;
	public float z = -1;
	public float ys = 0;
	//public float xs = 0;
	//public float zs = 0;
	public boolean OnGround = false;
	
	public float Yaw = -90;
	public float Pitch = 0;
	
	public void Forward(int Speed){
		this.x -= ((float)Math.sin(Math.toRadians(this.Yaw)) / Speed) * Time.Delta;
		this.z -= ((float)Math.cos(Math.toRadians(this.Yaw)) / Speed) * Time.Delta;
	}
	
	public void Backwards(int Speed){
		this.x += ((float)Math.sin(Math.toRadians(this.Yaw)) / Speed) * Time.Delta;
		this.z += ((float)Math.cos(Math.toRadians(this.Yaw)) / Speed) * Time.Delta;
	}
	
	public void StrafeRight(int Speed){
		this.x += ((float)Math.sin(Math.toRadians(this.Yaw + 90)) / Speed) * Time.Delta;
		this.z += ((float)Math.cos(Math.toRadians(this.Yaw + 90)) / Speed) * Time.Delta;
	}
	
	public void StrafeLeft(int Speed){
		this.x -= ((float)Math.sin(Math.toRadians(this.Yaw + 90)) / Speed) * Time.Delta;
		this.z -= ((float)Math.cos(Math.toRadians(this.Yaw + 90)) / Speed) * Time.Delta;
	}
	
	public void Update(){
		if(Keyboard.isKeyDown(Keyboard.KEY_W) && !Keyboard.isKeyDown(Keyboard.KEY_S) && !Keyboard.isKeyDown(Keyboard.KEY_D) && !Keyboard.isKeyDown(Keyboard.KEY_A)){
			this.Forward(10);
		}
		if(!Keyboard.isKeyDown(Keyboard.KEY_W) && Keyboard.isKeyDown(Keyboard.KEY_S) && !Keyboard.isKeyDown(Keyboard.KEY_D) && !Keyboard.isKeyDown(Keyboard.KEY_A)){
			this.Backwards(10);
		}
		if(!Keyboard.isKeyDown(Keyboard.KEY_W) && !Keyboard.isKeyDown(Keyboard.KEY_S) && Keyboard.isKeyDown(Keyboard.KEY_D) && !Keyboard.isKeyDown(Keyboard.KEY_A)){
			this.StrafeRight(10);
		}
		if(!Keyboard.isKeyDown(Keyboard.KEY_W) && !Keyboard.isKeyDown(Keyboard.KEY_S) && !Keyboard.isKeyDown(Keyboard.KEY_D) && Keyboard.isKeyDown(Keyboard.KEY_A)){
			this.StrafeLeft(10);
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_W) && !Keyboard.isKeyDown(Keyboard.KEY_S) && Keyboard.isKeyDown(Keyboard.KEY_D) && !Keyboard.isKeyDown(Keyboard.KEY_A)){
			this.Forward(15);
			this.StrafeRight(15);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_W) && !Keyboard.isKeyDown(Keyboard.KEY_S) && !Keyboard.isKeyDown(Keyboard.KEY_D) && Keyboard.isKeyDown(Keyboard.KEY_A)){
			this.Forward(15);
			this.StrafeLeft(15);
		}
		
		if(!Keyboard.isKeyDown(Keyboard.KEY_W) && Keyboard.isKeyDown(Keyboard.KEY_S) && Keyboard.isKeyDown(Keyboard.KEY_D) && !Keyboard.isKeyDown(Keyboard.KEY_A)){
			this.Backwards(15);
			this.StrafeRight(15);
		}
		if(!Keyboard.isKeyDown(Keyboard.KEY_W) && Keyboard.isKeyDown(Keyboard.KEY_S) && !Keyboard.isKeyDown(Keyboard.KEY_D) && Keyboard.isKeyDown(Keyboard.KEY_A)){
			this.Backwards(15);
			this.StrafeLeft(15);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE) && this.OnGround)
			this.ys = -0.14f;
		
		if(!Lime.GameMenu){
			this.Yaw -= (float)(Mouse.getX() - (1440 / 2)) / 10;
			this.Pitch += (float)(Mouse.getY() - (900 / 2)) / 10;
			
			if(this.Pitch > 90) this.Pitch -= (float)(Mouse.getY() - (900 / 2)) / 10;
			if(this.Pitch < -90) this.Pitch -= (float)(Mouse.getY() - (900 / 2)) / 10;
		}
		
		glRotatef(-this.Pitch, 1.0f, 0.0f, 0.0f);
		glRotatef(-this.Yaw, 0.0f, 1.0f, 0.0f);
		glTranslatef(-this.x, -this.y, -this.z);
		
		//gluLookAt(
		//		this.x + (float)Math.sin(Math.toRadians(this.Yaw)), this.y - (float)Math.tan(Math.toRadians(this.Pitch)), this.z + (float)Math.cos(Math.toRadians(this.Yaw)),
		//		this.x, this.y, this.z,
		//		0.0f, 1.0f, 0.0f
		//		);
		
		this.y -= this.ys * Time.Delta;
		
		this.BlockCollisions();
	}
	
	private void BlockCollisions(){
		this.OnGround = false;
		
		float Right = this.x + 0.25f;
		float Left = this.x - 0.25f;
		float Front = this.z - 0.25f;
		float Back = this.z + 0.25f;
		float Top = this.y + 0.25f;
		float Bottom = this.y - 1.5f;
		
		
			
		for(Block Block : Lime.Blocks){
			if(
					Right > Block.x && Left < Block.x + 1 &&
					Front < Block.z + 1 && Back > Block.z &&
					Top > Block.y - 1 && Bottom < Block.y
			)
			{
				float DisRight = Right - Block.x;
				float DisLeft = (Block.x + 1) - Left;
				
				float DisFront = (Block.z + 1) - Front;
				float DisBack = Back - Block.z;
				
				float DisTop = Top - (Block.y - 1);
				float DisBottom = Block.y - Bottom;
				
				float xDis = LimeMath.Smallest2(DisRight, DisLeft);
				float zDis = LimeMath.Smallest2(DisFront, DisBack);
				float yDis = LimeMath.Smallest2(DisTop, DisBottom);
				
				float Side = LimeMath.Smallest3(xDis, zDis, yDis);
				
				if(Side == xDis){
					if(xDis == DisRight)
						this.x = Block.x - 0.25f;
					else
						this.x = Block.x + 1.25f;
					
					System.out.println("X Collision");
				}
				if(Side == zDis){
					if(zDis == DisFront)
						this.z = Block.z + 1.25f;
					else
						this.z = Block.z - 0.25f;
					
					System.out.println("Z Collision");
				}
				if(Side == yDis){
					if(yDis == DisTop){
						this.y = Block.y - 1.25f;
						this.ys = 0;
					}
					else{
						if(this.ys >= 0){
							this.y = Block.y + 1.5f;
							this.OnGround = true;
							this.ys = 0;
						}
					}
					
					//System.out.println("Y Collision | " + yDis + " | " + DisTop + " | " + DisBottom);
				}
			}
		}
		
		if(OnGround);
			
		else
			this.ys += 0.0075f * Time.Delta;
	}
}