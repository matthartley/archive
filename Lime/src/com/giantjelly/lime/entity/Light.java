package com.giantjelly.lime.entity;

public class Light {
	public float x;
	public float y;
	public float z;
	public float intensity;
	
	public Light(float xx, float yy, float zz, float i){
		x = xx;
		y = yy;
		z = zz;
		intensity = i;
	}
	
	public void draw(){
		
	}
}
