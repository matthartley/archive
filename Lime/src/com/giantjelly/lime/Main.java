package com.giantjelly.lime;

import java.awt.BorderLayout;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.atomic.AtomicReference;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import com.giantjelly.lime.world.Data;
import static org.lwjgl.opengl.GL11.*;

import java.awt.*;

import javax.swing.JFrame;

public class Main extends Canvas{
	public static int WIDTH = 1440;
	public static int HEIGHT = 900;
	public boolean running = true;
	
	/*public void start(){	
		new Thread(this).start();
	}
	
	public void stop(){
		running = false;
	}
	
	public void run(){
		while(running){
			Lime.Update();
			Display.update();
			Display.sync(60);
		}
	}*/
	
	public static void main(String[] args){
		Main game = new Main();
		
		game.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		//game.setMinimumSize(new Dimension(WIDTH / 2, HEIGHT / 2));
		//game.setMaximumSize(new Dimension(WIDTH * 2, HEIGHT * 2));
		
		JFrame frame = new JFrame("New JFrame");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(game);
		frame.pack();
		frame.setResizable(true);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setLocation(0, 0);
		frame.setMinimumSize(new Dimension(WIDTH / 2, HEIGHT / 2));
		frame.setMaximumSize(new Dimension(1920, 1200));
		
		try{
			Display.setParent(game);
			Display.create();
			//Display.setVSyncEnabled(true);
			//Display.setDisplayMode(Display.getDesktopDisplayMode());
			//Display.setFullscreen(true);
		}catch(LWJGLException e){
			
		}
		
		Lime.Init();
		
		game.requestFocus();
		while(game.running){
			Lime.Update();
			
			if(Display.wasResized()){
				glViewport(0, 0, Display.getWidth(), Display.getHeight());
			}
			
			Display.update();
			//Display.sync(60);
		}
		
		Data.Save();
		Display.destroy();
		frame.dispose();
	}
}
