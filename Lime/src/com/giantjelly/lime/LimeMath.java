package com.giantjelly.lime;

import java.util.Random;

public class LimeMath{
	public static Random Rand = new Random();
	
	public static float Positive(float num){
		if(num >= 0)
			return num;
		else
			return num * -1;
	}
	
	public static float Smallest3(float num1, float num2, float num3){
		if(num1 <= num2 && num1 <= num3){
			return num1;
		}
		else if(num2 <= num1 && num2 <= num3){
			return num2;
		}
		else /*if(num3 <= num1 && num3 <= num2)*/{
			return num3;
		}
	}
	
	public static float Smallest2(float num1, float num2){
		if(num1 <= num2)
			return num1;
		else
			return num2;
	}
}
