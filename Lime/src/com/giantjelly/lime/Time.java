package com.giantjelly.lime;
import org.lwjgl.Sys;

public class Time {
	public static long fps;
	public static long Tempfps;
	public static long Lastfps = GetTime();
	public static float Delta = 1;
	
	public static long GetTime(){
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	public static void Update(){
		if(GetTime() - Lastfps > 1000){
			fps = Tempfps;
			Delta = 60.0f / (float)fps;
			Tempfps = 0;
			Lastfps += 1000;
		}
		
		Tempfps++;
	}
}
