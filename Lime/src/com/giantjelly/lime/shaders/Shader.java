package com.giantjelly.lime.shaders;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL11.*;
import java.io.*;

import com.giantjelly.lime.Error;
import com.giantjelly.lime.Lime;

public class Shader {
	/*public static int ShaderProgram = glCreateProgram();
	
	public static void Init(){
		
		
		int VertexShader = glCreateShader(GL_VERTEX_SHADER);
		int FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		
		StringBuilder VertexShaderSource = new StringBuilder();
		StringBuilder FragmentShaderSource = new StringBuilder();
		
		try{
			BufferedReader reader = new BufferedReader(new FileReader("src/com/giantjelly/lime/shaders/shader.vert"));
			String line;
			while((line = reader.readLine()) != null){
				VertexShaderSource.append(line);
			}
			reader.close();
			
			BufferedReader reader2 = new BufferedReader(new FileReader("src/com/giantjelly/lime/shaders/shader.frag"));
			String line2;
			while((line2 = reader2.readLine()) != null){
				FragmentShaderSource.append(line2);
			}
			reader2.close();
			
		}catch(IOException e){
			Lime.ErrorLog.add(new Error("Failed to load Shaders!", e));
		}
		
		System.out.println(VertexShaderSource);
		System.out.println(FragmentShaderSource);
		
		glShaderSource(VertexShader, VertexShaderSource);
		glCompileShader(VertexShader);
		if(glGetShader(VertexShader, GL_COMPILE_STATUS) == GL_FALSE){
			System.out.println("VertexShader didn't compile!");
		}
		
		glShaderSource(FragmentShader, FragmentShaderSource);
		glCompileShader(FragmentShader);
		if(glGetShader(FragmentShader, GL_COMPILE_STATUS) == GL_FALSE){
			System.out.println("FragmentShader didn't compile!");
		}
		
		glAttachShader(ShaderProgram, VertexShader);
		glAttachShader(ShaderProgram, FragmentShader);
		glLinkProgram(ShaderProgram);
		glValidateProgram(ShaderProgram);
	}
	
	public static void Start(){
		glUseProgram(ShaderProgram);
	}
	
	public static void End(){
		glUseProgram(0);
	}*/
	
	public int Program = glCreateProgram();
	
	int VertShader = glCreateShader(GL_VERTEX_SHADER);
	int FragShader = glCreateShader(GL_FRAGMENT_SHADER);
	
	String VertShaderSource = new String();
	String FragShaderSource = new String();
	
	public Shader(String VertFile, String FragFile){
		try{
			BufferedReader VReader = new BufferedReader(new FileReader("src/com/giantjelly/lime/shaders/" + VertFile));
			BufferedReader FReader = new BufferedReader(new FileReader("src/com/giantjelly/lime/shaders/" + FragFile));
			
			String line;
			while((line = VReader.readLine()) != null){
				this.VertShaderSource += line + '\n';
			}
			while((line = FReader.readLine()) != null){
				this.FragShaderSource += line + '\n';
			}
			
			VReader.close();
			FReader.close();
		}catch(IOException e){
			Lime.ErrorLog.add(new Error("Failed to load Shaders!", e));
		}
		
		//System.out.println(this.VertShaderSource);
		//System.out.println(this.FragShaderSource);
		
		glShaderSource(this.VertShader, this.VertShaderSource);
		glCompileShader(this.VertShader);
		glShaderSource(this.FragShader, this.FragShaderSource);
		glCompileShader(this.FragShader);
		
		if(glGetShader(this.VertShader, GL_COMPILE_STATUS) == GL_FALSE){
			System.out.println("VertShader compile error!" + this.VertShader);
			System.out.println(glGetShaderInfoLog(this.VertShader, glGetShader(this.VertShader, GL_INFO_LOG_LENGTH)));
		}
		if(glGetShader(this.FragShader, GL_COMPILE_STATUS) == GL_FALSE){
			System.out.println("FragShader compile error!" + this.FragShader);
			System.out.println(glGetShaderInfoLog(this.FragShader, glGetShader(this.FragShader, GL_INFO_LOG_LENGTH)));
		}
		
		glAttachShader(this.Program, this.VertShader);
		glAttachShader(this.Program, this.FragShader);
		glLinkProgram(this.Program);
		glValidateProgram(this.Program);
	}
	
	public void Begin(){
		glUseProgram(this.Program);
	}
	
	public void End(){
		glUseProgram(0);
	}
}
