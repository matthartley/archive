varying vec3 color;
varying float Light;

void main(){
	gl_FragColor = vec4(color, 1);
}