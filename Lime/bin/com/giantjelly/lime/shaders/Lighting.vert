varying vec3 color;
varying float Light;

void main(){
	vec3 dis = gl_Vertex.xyz - gl_LightSource[0].position.xyz + gl_LightSource[1].position.xyz;
	
	dis.y -= 1;
	
	float light = (1 / (
	
	sqrt(((dis.x * dis.x) + (dis.z * dis.z) + (dis.y * dis.y)) - 3)
	
	));
	
	
	
	gl_Position = ftransform();
	
	color = gl_Color.rgb * light;
	//color.a = gl_Color.a;
}