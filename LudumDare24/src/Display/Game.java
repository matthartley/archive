package Display;

import static org.lwjgl.opengl.GL11.*;

import org.newdawn.slick.Color;

public class Game {
	public static void Update(){
		
		if(Main.GameOver)
			glColor4f(1, 0, 0, 1);
		
		for(int i = 0; i < 16; i++){
			for(int v = 0; v < 16; v++){
				if(Main.Grass[i][v] == 0){
					glEnable(GL_TEXTURE_2D);
					Textures.Tiles.bind();
					glBegin(GL_QUADS);
						glTexCoord2f(0.125f, 0);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.25f, 0);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.25f, 0.125f);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.125f, 0.125f);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
					glEnd();
				}else{
					glEnable(GL_TEXTURE_2D);
					Textures.Tiles.bind();
					glBegin(GL_QUADS);
						glTexCoord2f(0, 0);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.125f, 0);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.125f, 0.125f);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
						
						glTexCoord2f(0, 0.125f);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
					glEnd();
				}
				
				if(v == 0){
					glEnable(GL_TEXTURE_2D);
					Textures.Tiles.bind();
					glBegin(GL_QUADS);
						glTexCoord2f(0.25f, 0);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.25f + (0.125f / 2), 0);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.25f + (0.125f / 2), 0.125f);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.25f, 0.125f);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
					glEnd();
				}
				if(i == 0){
					glEnable(GL_TEXTURE_2D);
					Textures.Tiles.bind();
					glBegin(GL_QUADS);
						glTexCoord2f(0.25f + 0.125f, 0);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.5f, 0);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.5f, 0.125f);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.25f + 0.125f, 0.125f);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
					glEnd();
				}
				if(v == 15){
					glEnable(GL_TEXTURE_2D);
					Textures.Tiles.bind();
					glBegin(GL_QUADS);
						glTexCoord2f(0.5f, 0);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.5f + 0.125f, 0);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.5f + 0.125f, 0.125f);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.5f, 0.125f);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
					glEnd();
				}
				if(i == 15){
					glEnable(GL_TEXTURE_2D);
					Textures.Tiles.bind();
					glBegin(GL_QUADS);
						glTexCoord2f(0.5f + 0.125f, 0);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.5f + 0.25f, 0);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.5f + 0.25f, 0.125f);
						glVertex3f((128 * v)+ 128 + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
						
						glTexCoord2f(0.5f + 0.125f, 0.125f);
						glVertex3f((128 * v) + Main.Mapx - 400, (128 * i) + 128 + Main.Mapy - 400, 0);
					glEnd();
				}
			}
		}
		
		if(Main.You.x - Main.Mapx + 16 < -400){
			Main.You.x += 4;
		}
		if(Main.You.x - Main.Mapx + 96 > (16 * 128) - 400){
			Main.You.x -= 4;
		}
		
		if(Main.You.y - Main.Mapy + 16 < -400){
			Main.You.y += 4;
		}
		if(Main.You.y - Main.Mapy + 96 > (16 * 128) - 400){
			Main.You.y -= 4;
		}
	}
}
