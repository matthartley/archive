package Display;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import java.util.Random;

import Players.Input;
import Players.Player;
import Players.Bullet;
import Players.Enemy;

import java.applet.Applet;
import java.awt.Canvas;

public class Main extends Applet{
	public static boolean ShutDown = false;
	public static boolean Playing = false;
	public static Player You = new Player();
	public static Random Rand = new Random();
	public static Enemy[] Enemies = new Enemy[32];
	public static float Mapx = 0;
	public static float Mapy = 0;
	public static boolean GameOver;
	public static int Wave = 1;
	public static int NextWaveCoolDown = 0;
	
	public static Canvas DisplayParent;
	
	public static int[][] Grass = new int[32][32];
	
	public static int SmoothScrolling = 10;
	
	public static boolean ClickDown = true;
	
	public static void main(String[] argv){
		
		try{
			Display.setDisplayMode(Display.getDesktopDisplayMode());
			Display.setFullscreen(false);
			Display.setResizable(false);
			Display.setVSyncEnabled(true);
			Display.setLocation(-1920, -300);
			Display.create();
		}catch(LWJGLException e){
			e.printStackTrace();
			System.exit(0);
		}
		
		Fonts.Init();
		Textures.LoadTextures();
		
		for(int i = 0; i < 16; i++){
			for(int v = 0; v < 16; v++){
				Grass[i][v] = Main.Rand.nextInt(10);
			}
		}
		
		for(int i = 0; i < 32; i++){
			You.Bullets[i] = new Bullet();
			You.Bullets[i].Active = false;
		}
		
		//for(int i = 0; i < 32; i++){
		//	Enemies[i] = new Enemy();
		//	Enemies[i].Active = true;
		//	Enemies[i].x = Rand.nextInt(6000) - 3000;
		//	Enemies[i].y = Rand.nextInt(6000) - 3000;
		//}
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, Display.getDesktopDisplayMode().getWidth(), Display.getDesktopDisplayMode().getHeight(), 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		while(!Display.isCloseRequested() && !ShutDown){
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glDisable(GL_DEPTH_TEST);
			
			if(Playing){
				Game.Update();
				int a = 0;
				for(int i = 0; i < 32; i++){
					if(You.Bullets[i].Active){
						a++;
					}
				}
				
				if(!GameOver)
					Input.Update();
				
				You.Update();
				for(int i = 0; i < 32; i++){
					if(You.Bullets[i].Active){
						You.Bullets[i].Update();
					}
				}
				
				for(int i = 0; i < 32; i++){
					if(Enemies[i].Active)
						Enemies[i].Update();
				}
				
				if(!GameOver){
					glEnable(GL_TEXTURE_2D);
					Fonts.font.drawString(10, 5, "The Wrong Kind of Evolution", Color.white);
					Fonts.font.drawString(Display.getDesktopDisplayMode().getWidth() - 400, 100, "Life - "+You.Life, Color.white);
					
					int EnemyCount = 0;
					for(int i = 0; i < 32; i++){
						if(Enemies[i].Active)
							EnemyCount++;
					}
					
					Fonts.font.drawString(Display.getDesktopDisplayMode().getWidth() - 400, 10, "Enemies remaining in wave - "+EnemyCount, Color.white);
					Fonts.font.drawString(Display.getDesktopDisplayMode().getWidth() - 400, 40, "Wave - "+Wave, Color.white);
					
					if(EnemyCount == 0){
						Wave++;
						NextWaveCoolDown = 100;
						
						for(int i = 0; i < 32; i++){
							Main.Enemies[i] = new Enemy();
							Main.Enemies[i].Active = true;
							Main.Enemies[i].x = Main.Rand.nextInt(6000) - 3000;
							Main.Enemies[i].y = Main.Rand.nextInt(6000) - 3000;
						}
					}
					
					if(NextWaveCoolDown > 0){
						Fonts.font4.drawString((Display.getDesktopDisplayMode().getWidth() / 2) - 100, 300, "Next Wave!", Color.orange);
						NextWaveCoolDown--;
					}
						
					Fonts.font.drawString(10, Display.getDesktopDisplayMode().getHeight() - 50, "Back to menu", Color.white);
					if(Mouse.getX() > 10 && Mouse.getX() < 200 && Display.getDesktopDisplayMode().getHeight() - Mouse.getY() > Display.getDesktopDisplayMode().getHeight() - 50 && Display.getDesktopDisplayMode().getHeight() - Mouse.getY() < Display.getDesktopDisplayMode().getHeight() - 10){
						glDisable(GL_TEXTURE_2D);
						glColor4f(1, 1, 1, 0.3f);
						glBegin(GL_QUADS);
						glVertex3f(10, Display.getDesktopDisplayMode().getHeight() - 50, 0.9f);
						glVertex3f(200, Display.getDesktopDisplayMode().getHeight() - 50, 0.9f);
						glVertex3f(200, Display.getDesktopDisplayMode().getHeight() - 10, 0.9f);
						glVertex3f(10, Display.getDesktopDisplayMode().getHeight() - 10, 0.9f);
						glEnd();
						
						if(Mouse.isButtonDown(0)){
							Playing = false;
							GameOver = false;
						}
					}
				}
				
				if(GameOver)
					Menu.Update();
			}else
				Menu.Update();
			
			Display.update();
			Display.sync(60);
		}
		
		Display.destroy();
		
	}
}
