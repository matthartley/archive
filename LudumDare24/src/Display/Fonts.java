package Display;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import java.awt.Font;

public class Fonts {
	public static TrueTypeFont font;
	public static TrueTypeFont font2;
	public static TrueTypeFont font3;
	public static TrueTypeFont font4;
	
	public static void Init(){
		font = new TrueTypeFont(new Font("Times", Font.BOLD, 24), false);
		font2 = new TrueTypeFont(new Font("Verdana", Font.PLAIN, 32), false);
		font3 = new TrueTypeFont(new Font("Verdana", Font.BOLD, 48), false);
		font4 = new TrueTypeFont(new Font("Verdana", Font.BOLD, 42), false);
	}
}
