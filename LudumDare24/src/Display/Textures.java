package Display;

import java.io.IOException;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Textures {
	public static Texture Player;
	public static Texture Enemy;
	public static Texture Bullet;
	public static Texture Tiles;
	
	public static void LoadTextures(){
		try{
			Player = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/Textures.png"));
			Enemy = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/EnemySpriteSheet.png"));
			Bullet = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/Bullets.png"));
			Tiles = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/Tiles.png"));
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
