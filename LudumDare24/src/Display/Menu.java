package Display;

import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import Players.Enemy;

public class Menu {
	public static void Update(){
		if(!Mouse.isButtonDown(0)){
			Main.ClickDown = false;
		}
		
		glEnable(GL_TEXTURE_2D);
		Fonts.font2.drawString((Display.getDesktopDisplayMode().getWidth() / 2) - 250, 50, "The Wrong Kind of Evolution", Color.orange);
		
		Fonts.font2.drawString((Display.getDesktopDisplayMode().getWidth() / 2) - 50, 200, "Play");
		Fonts.font2.drawString((Display.getDesktopDisplayMode().getWidth() / 2) - 50, 300, "Exit");
		
		if(Mouse.getX() > (Display.getDesktopDisplayMode().getWidth() / 2) - 50 && Mouse.getX() < (Display.getDesktopDisplayMode().getWidth() / 2) + 50 && Display.getDesktopDisplayMode().getHeight() - Mouse.getY() > 200 && Display.getDesktopDisplayMode().getHeight() - Mouse.getY() < 250){
			glDisable(GL_TEXTURE_2D);
			glColor4f(1, 1, 1, 0.5f);
			glBegin(GL_QUADS);
				glVertex3f((Display.getDesktopDisplayMode().getWidth() / 2) - 50, 200, 0.9f);
				glVertex3f((Display.getDesktopDisplayMode().getWidth() / 2) + 50, 200, 0.9f);
				glVertex3f((Display.getDesktopDisplayMode().getWidth() / 2) + 50, 250, 0.9f);
				glVertex3f((Display.getDesktopDisplayMode().getWidth() / 2) - 50, 250, 0.9f);
			glEnd();
			
			if(Mouse.isButtonDown(0)){
				Main.Playing = true;
				Main.GameOver = false;
				Main.You.Life = 300;
				Main.Mapx = 0;
				Main.Mapy = 0;
				
				for(int i = 0; i < 32; i++){
					Main.Enemies[i] = new Enemy();
					Main.Enemies[i].Active = true;
					Main.Enemies[i].x = Main.Rand.nextInt(6000) - 3000;
					Main.Enemies[i].y = Main.Rand.nextInt(6000) - 3000;
				}
			}
		}
		if(Mouse.getX() > (Display.getDesktopDisplayMode().getWidth() / 2) - 50 && Mouse.getX() < (Display.getDesktopDisplayMode().getWidth() / 2) + 50 && Display.getDesktopDisplayMode().getHeight() - Mouse.getY() > 300 && Display.getDesktopDisplayMode().getHeight() - Mouse.getY() < 350){
			glDisable(GL_TEXTURE_2D);
			glColor4f(1, 1, 1, 0.5f);
			glBegin(GL_QUADS);
			glVertex3f((Display.getDesktopDisplayMode().getWidth() / 2) - 50, 300, 0.9f);
			glVertex3f((Display.getDesktopDisplayMode().getWidth() / 2) + 50, 300, 0.9f);
			glVertex3f((Display.getDesktopDisplayMode().getWidth() / 2) + 50, 350, 0.9f);
			glVertex3f((Display.getDesktopDisplayMode().getWidth() / 2) - 50, 350, 0.9f);
			glEnd();
			
			if(Mouse.isButtonDown(0)){
				Main.ShutDown = true;
			}
		}
		
		Fonts.font.drawString(10, 10, "Made by Matt Hartley", Color.white);
		Fonts.font.drawString(10, 40, "for Ludum Dare 24", Color.white);
		
		if(Main.GameOver){
			Fonts.font3.drawString((Display.getDesktopDisplayMode().getWidth() / 2) - 180, Display.getDesktopDisplayMode().getHeight() - 300, "GAME OVER", Color.red);
			Fonts.font.drawString((Display.getDesktopDisplayMode().getWidth() / 2) - 180, Display.getDesktopDisplayMode().getHeight() - 200, "You reached wave "+Main.Wave, Color.white);
		}
		
		if(!Main.GameOver){
			Fonts.font.drawString(10, Display.getDesktopDisplayMode().getHeight() - 130, "A long time ago some primates went under ground and", Color.white);
			Fonts.font.drawString(10, Display.getDesktopDisplayMode().getHeight() - 100, "evolved in a different way to normal humans", Color.white);
			Fonts.font.drawString(10, Display.getDesktopDisplayMode().getHeight() - 70, "In the present day they come out at night", Color.white);
			Fonts.font.drawString(10, Display.getDesktopDisplayMode().getHeight() - 40, "Use the arrow keys to move around and space to shoot", Color.white);
			
			
		}
		
		Fonts.font.drawString(Display.getDesktopDisplayMode().getWidth() - 400, Display.getDesktopDisplayMode().getHeight() - 100, "If the edges of the floor tiles flash ", Color.white);
		Fonts.font.drawString(Display.getDesktopDisplayMode().getWidth() - 400, Display.getDesktopDisplayMode().getHeight() - 70, "and look bad click this to toggle", Color.white);
		Fonts.font.drawString(Display.getDesktopDisplayMode().getWidth() - 400, Display.getDesktopDisplayMode().getHeight() - 40, "SmoothScrolling", Color.white);
		Fonts.font.drawString(Display.getDesktopDisplayMode().getWidth() - 27, Display.getDesktopDisplayMode().getHeight() - 130, "|", Color.white);
		//Fonts.font.drawString(Display.getDesktopDisplayMode().getWidth() - 30, 60, "^", Color.white);
		
		if(Main.SmoothScrolling == 1){
			Fonts.font.drawString(Display.getDesktopDisplayMode().getWidth() - 250, Display.getDesktopDisplayMode().getHeight() - 160, "SmoothScrolling Off", Color.white);
		}else{
			Fonts.font.drawString(Display.getDesktopDisplayMode().getWidth() - 250, Display.getDesktopDisplayMode().getHeight() - 160, "SmoothScrolling On", Color.white);
		}
		
		if(Mouse.getX() > Display.getDesktopDisplayMode().getWidth() - 50 && Mouse.getX() < Display.getDesktopDisplayMode().getWidth() - 10 && Display.getDesktopDisplayMode().getHeight() - Mouse.getY() > Display.getDesktopDisplayMode().getHeight() - 160 && Display.getDesktopDisplayMode().getHeight() - Mouse.getY() < Display.getDesktopDisplayMode().getHeight() - 130){
			glDisable(GL_TEXTURE_2D);
			glColor4f(1, 1, 1, 0.5f);
			glBegin(GL_QUADS);
			glVertex3f(Display.getDesktopDisplayMode().getWidth() - 50, Display.getDesktopDisplayMode().getHeight() - 160, 0.9f);
			glVertex3f(Display.getDesktopDisplayMode().getWidth() - 10, Display.getDesktopDisplayMode().getHeight() - 160, 0.9f);
			glVertex3f(Display.getDesktopDisplayMode().getWidth() - 10, Display.getDesktopDisplayMode().getHeight() - 130, 0.9f);
			glVertex3f(Display.getDesktopDisplayMode().getWidth() - 50, Display.getDesktopDisplayMode().getHeight() - 130, 0.9f);
			glEnd();
			
			if(Mouse.isButtonDown(0) && !Main.ClickDown){
				Main.ClickDown = true;
				
				if(Main.SmoothScrolling == 1){
					Main.SmoothScrolling = 10;
				}else{
					Main.SmoothScrolling = 1;
				}
			}
		}
	}
}
