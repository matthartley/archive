package Players;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.Display;
import Display.Textures;
import static Display.Main.You;
import static Display.Main.Enemies;
import Display.Main;

public class Enemy {
	public float x = Display.getDesktopDisplayMode().getWidth() / 2;
	public float y = Display.getDesktopDisplayMode().getHeight() / 2;
	
	int AnimationFrame = 0;
	int AnimationCount = 0;
	
	float TexY = 0;
	
	public boolean Active = true;
	
	public int Life = 3;
	
	public void Update(){
		this.AnimationCount++;
		if(this.AnimationCount == 7){
			this.AnimationCount = 0;
			this.AnimationFrame ++;
		}
		if(this.AnimationFrame == 8)
			this.AnimationFrame = 0;
		
		glColor4f(1, 1, 1, 1);
		
		if(Main.GameOver)
			glColor4f(1, 0, 0, 1);
		
		if(this.x + 16 < -400 || this.x + 96 > (16 * 128) - 400 || this.y + 16 < -400 || this.y + 96 > (16 * 128) - 400){
			glColor4f(0, 0, 0, 0.5f);
		}
		
		for(int i = 0; i < 32; i++){
			if(
					You.Bullets[i].x < this.x + 96 + Main.Mapx &&
					You.Bullets[i].x > this.x + Main.Mapx &&
					You.Bullets[i].y < this.y + 96 + Main.Mapy &&
					You.Bullets[i].y > this.y + Main.Mapy
				)
			{
				if(You.Bullets[i].Active){
					//this.Active = false;
					this.Life--;
					You.Bullets[i].Active = false;
					glColor4f(1, 0, 0, 1);
				}
			}
		}
		
		if(Life == 0)
			this.Active = false;
		
		glEnable(GL_TEXTURE_2D);
		Textures.Enemy.bind();
		glBegin(GL_QUADS);
			glTexCoord2f(this.AnimationFrame * 0.125f, this.TexY);
			glVertex3f(this.x + Main.Mapx, this.y + Main.Mapy, 0);
			
			glTexCoord2f((this.AnimationFrame * 0.125f) + 0.125f, this.TexY);
			glVertex3f(this.x + 128 + Main.Mapx, this.y + Main.Mapy, 0);
			
			glTexCoord2f((this.AnimationFrame * 0.125f) + 0.125f, this.TexY + 0.125f);
			glVertex3f(this.x + 128 + Main.Mapx, this.y + 128 + Main.Mapy, 0);
			
			glTexCoord2f(this.AnimationFrame * 0.125f, this.TexY + 0.125f);
			glVertex3f(this.x + Main.Mapx, this.y + 128 + Main.Mapy, 0);
		glEnd();
		
		float XDis;
		float YDis;
		XDis = You.x - this.x; if(XDis < 0) XDis *= -1;
		YDis = You.y - this.y; if(YDis < 0) YDis *= -1;
		
		//if(XDis < 500 && YDis < 500){
			if(this.x + Main.Mapx < You.x){
				if(You.x - (this.x + Main.Mapx) > 10){
					this.x += 2;
					this.TexY = 0.125f;
				}
			}else{
				if((this.x + Main.Mapx) - You.x > 10){
					this.x -= 2;
					this.TexY = 0.375f;
				}
			}
			
			if(this.y + Main.Mapy < You.y){
				if(You.y - (this.y + Main.Mapy) > 10){
					this.y += 2;
					this.TexY = 0.25f;
				}
			}else{
				if((this.y + Main.Mapy) - You.y > 10){
					this.y -= 2;
					this.TexY = 0;
				}
			}
		//}
		
		for(int i = 0; i < 32; i++){
			if((this.x != Enemies[i].x || this.y != Enemies[i].y) && Enemies[i].Active){
				if(
						this.x < Enemies[i].x + 96 &&
						this.x + 96 > Enemies[i].x &&
						this.y < Enemies[i].y + 96 &&
						this.y + 96 > Enemies[i].y
					)
				{
					if(this.x < Enemies[i].x){
						if(this.y < Enemies[i].y){
							if((this.x + 96) - (Enemies[i].x) < (this.y + 96) - (Enemies[i].y)){
								this.x -= 4;
							}
							if((this.x + 96) - (Enemies[i].x) > (this.y + 96) - (Enemies[i].y)){
								this.y -= 4;
							}
						}else{
							if((this.x + 96) - (Enemies[i].x) < (Enemies[i].y + 96) - this.y){
								this.x -= 4;
							}
							if((this.x + 96) - (Enemies[i].x) > (Enemies[i].y + 96) - this.y){
								this.y += 4;
							}
						}
					}else{
						if(this.y < Enemies[i].y){
							if((Enemies[i].x + 96) - this.x < (this.y + 96) - (Enemies[i].y)){
								this.x += 4;
							}
							if((Enemies[i].x + 96) - this.x > (this.y + 96) - (Enemies[i].y)){
								this.y -= 4;
							}
						}else{
							if((Enemies[i].x + 96) - this.x < (Enemies[i].y + 96) - this.y){
								this.x += 4;
							}
							if((Enemies[i].x + 96) - this.x > (Enemies[i].y + 96) - this.y){
								this.y += 4;
							}
						}
					}
				}
			}
		}
	}
}
