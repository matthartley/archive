package Players;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex3f;
import Display.Textures;

public class Bullet {
	public float x;
	public float y;
	public float speedX;
	public float speedY;
	
	public int TexX;
	
	public boolean Active = false;
	
	public int CoolDown = 100;
	
	public void Update(){
		this.x += this.speedX;
		this.y += this.speedY;
		
		glEnable(GL_TEXTURE_2D);
		Textures.Bullet.bind();
		glBegin(GL_QUADS);
			glTexCoord2f(this.TexX * 0.25f, 0);
			glVertex3f(this.x, this.y, 0);
			
			glTexCoord2f((this.TexX * 0.25f) + 0.20f, 0);
			glVertex3f(this.x + 32, this.y, 0);
			
			glTexCoord2f((this.TexX * 0.25f) + 0.20f, 1);
			glVertex3f(this.x + 32, this.y + 32, 0);
			
			glTexCoord2f(this.TexX * 0.25f, 1);
			glVertex3f(this.x, this.y + 32, 0);
		glEnd();
		
		this.CoolDown--;
		if(this.CoolDown < 0){
			this.Active = false;
		}
	}
}
