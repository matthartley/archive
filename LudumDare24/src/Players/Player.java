package Players;

import static Display.Main.Enemies;
import Display.Main;
import static org.lwjgl.opengl.GL11.*;
import Display.Textures;

import org.lwjgl.opengl.Display;

public class Player {
	public float x = Display.getDesktopDisplayMode().getWidth() / 2;
	public float y = Display.getDesktopDisplayMode().getHeight() / 2;
	public float XSpeed = 0;
	public float YSpeed = 0;
	
	int AnimationFrame = 0;
	int AnimationCount = 0;
	
	float TexY = 0;
	
	public boolean SpaceDown = false;
	
	public Bullet[] Bullets = new Bullet[32];
	
	public int Life = 300;
	
	public int BulletSpeedX;
	public int BulletSpeedY;
	public int BulletDir = 0;
	
	public Player(){
		this.BulletSpeedX = 0;
		this.BulletSpeedY = -16;
	}
	
	public void Update(){
		if(this.AnimationCount == 7){
			this.AnimationCount = 0;
			this.AnimationFrame ++;
		}
		if(this.AnimationFrame == 8)
			this.AnimationFrame = 0;
		
		
		for(int i = 0; i < 32; i++){
			if(Enemies[i].Active){
				if(
						this.x + 16 < Enemies[i].x + 96 + Main.Mapx &&
						this.x + 96 > Enemies[i].x + 16 + Main.Mapx &&
						this.y + 16 < Enemies[i].y + 96 + Main.Mapy &&
						this.y + 96 > Enemies[i].y + 16 + Main.Mapy
					)
				{
					glColor4f(1, 0, 0, 1);
					this.Life--;
				}
			}
		}
		
		glEnable(GL_TEXTURE_2D);
		Textures.Player.bind();
		glBegin(GL_QUADS);
			glTexCoord2f(this.AnimationFrame * 0.125f, this.TexY);
			glVertex3f(this.x, this.y, 0);
			
			glTexCoord2f((this.AnimationFrame * 0.125f) + 0.125f, this.TexY);
			glVertex3f(this.x + 128, this.y, 0);
			
			glTexCoord2f((this.AnimationFrame * 0.125f) + 0.125f, this.TexY + 0.125f);
			glVertex3f(this.x + 128, this.y + 128, 0);
			
			glTexCoord2f(this.AnimationFrame * 0.125f, this.TexY + 0.125f);
			glVertex3f(this.x, this.y + 128, 0);
		glEnd();
		
		Main.Mapx -= (this.x - Display.getDesktopDisplayMode().getWidth() / 2) / Main.SmoothScrolling;
		Main.Mapy -= (this.y - Display.getDesktopDisplayMode().getHeight() / 2) / Main.SmoothScrolling;
		this.x -= (this.x - Display.getDesktopDisplayMode().getWidth() / 2) / Main.SmoothScrolling;
		this.y -= (this.y - Display.getDesktopDisplayMode().getHeight() / 2) / Main.SmoothScrolling;
		
		if(this.Life < 0){
			Main.GameOver = true;
		}
	}
}
