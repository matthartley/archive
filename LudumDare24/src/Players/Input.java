package Players;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import static Display.Main.You;
import Display.Main;

public class Input {
	public static void Update(){
		if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT) || Keyboard.isKeyDown(Keyboard.KEY_LEFT) || Keyboard.isKeyDown(Keyboard.KEY_DOWN) || Keyboard.isKeyDown(Keyboard.KEY_UP))
			You.AnimationCount++;
		
		if(!Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
			You.SpaceDown = false;
		}
		
		
		
		if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)){
			You.x += 4;
			You.TexY = 0.125f;
			
			You.BulletSpeedX = 16;
			You.BulletSpeedY = 0;
			You.BulletDir = 1;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
			You.x -= 4;
			You.TexY = 0.125f + 0.25f;
			
			You.BulletSpeedX = -16;
			You.BulletSpeedY = 0;
			You.BulletDir = 3;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)){
			You.y += 4;
			You.TexY = 0.25f;
			
			You.BulletSpeedX = 0;
			You.BulletSpeedY = 16;
			You.BulletDir = 2;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_UP)){
			You.y -= 4;
			You.TexY = 0;
			
			You.BulletSpeedX = 0;
			You.BulletSpeedY = -16;
			You.BulletDir = 0;
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_UP) && Keyboard.isKeyDown(Keyboard.KEY_RIGHT)){
			You.TexY = 0.5f;
			
			You.BulletSpeedX = 8;
			You.BulletSpeedY = -8;
			You.BulletDir = 4;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_DOWN) && Keyboard.isKeyDown(Keyboard.KEY_RIGHT)){
			You.TexY = 0.5f + 0.125f;
			
			You.BulletSpeedX = 8;
			You.BulletSpeedY = 8;
			You.BulletDir = 5;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_UP) && Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
			You.TexY = 0.75f;
			
			You.BulletSpeedX = -8;
			You.BulletSpeedY = -8;
			You.BulletDir = 6;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_DOWN) && Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
			You.TexY = 0.75f + 0.125f;
			
			You.BulletSpeedX = -8;
			You.BulletSpeedY = 8;
			You.BulletDir = 7;
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE) && !You.SpaceDown){
			You.SpaceDown = true;
			for(int i = 0; i < 32; i++){
				if(!You.Bullets[i].Active){
					You.Bullets[i].Active = true;
					You.Bullets[i].CoolDown = 100;
					
					if(You.BulletDir == 0){
						You.Bullets[i].x = You.x + 90;
						You.Bullets[i].y = You.y;
						You.Bullets[i].speedX = You.BulletSpeedX;
						You.Bullets[i].speedY = You.BulletSpeedY;
						You.Bullets[i].TexX = 1;
					}
					if(You.BulletDir == 1){
						You.Bullets[i].x = You.x + 96;
						You.Bullets[i].y = You.y + 96;
						You.Bullets[i].speedX = You.BulletSpeedX;
						You.Bullets[i].speedY = You.BulletSpeedY;
						You.Bullets[i].TexX = 0;
					}
					if(You.BulletDir == 2){
						You.Bullets[i].x = You.x + 32;
						You.Bullets[i].y = You.y + 96;
						You.Bullets[i].speedX = You.BulletSpeedX;
						You.Bullets[i].speedY = You.BulletSpeedY;
						You.Bullets[i].TexX = 1;
					}
					if(You.BulletDir == 3){
						You.Bullets[i].x = You.x;
						You.Bullets[i].y = You.y + 32;
						You.Bullets[i].speedX = You.BulletSpeedX;
						You.Bullets[i].speedY = You.BulletSpeedY;
						You.Bullets[i].TexX = 0;
					}
					
					if(You.BulletDir == 4){
						You.Bullets[i].x = You.x + 120;
						You.Bullets[i].y = You.y + 32;
						You.Bullets[i].speedX = You.BulletSpeedX;
						You.Bullets[i].speedY = You.BulletSpeedY;
						You.Bullets[i].TexX = 3;
					}
					if(You.BulletDir == 5){
						You.Bullets[i].x = You.x + 74;
						You.Bullets[i].y = You.y + 120;
						You.Bullets[i].speedX = You.BulletSpeedX;
						You.Bullets[i].speedY = You.BulletSpeedY;
						You.Bullets[i].TexX = 2;
					}
					if(You.BulletDir == 6){
						You.Bullets[i].x = You.x + 36;
						You.Bullets[i].y = You.y;
						You.Bullets[i].speedX = You.BulletSpeedX;
						You.Bullets[i].speedY = You.BulletSpeedY;
						You.Bullets[i].TexX = 2;
					}
					if(You.BulletDir == 7){
						You.Bullets[i].x = You.x;
						You.Bullets[i].y = You.y + 64;
						You.Bullets[i].speedX = You.BulletSpeedX;
						You.Bullets[i].speedY = You.BulletSpeedY;
						You.Bullets[i].TexX = 3;
					}
					
					
					break;
				}
			}
		}
	}
}
