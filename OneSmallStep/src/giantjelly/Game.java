package giantjelly;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import static java.awt.event.KeyEvent.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import giantjelly.gfx.Bitmap;

public class Game extends Canvas implements Runnable {
	
	public static int HEIGHT = 480/2;
	public static int WIDTH = 720/2;
	public static int SCREENHEIGHT = 480;
	public static int SCREENWIDTH = 720;
	
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
	private boolean running = true;
	
	Screen screen = new Screen(WIDTH, HEIGHT);
	public static InputHandler input = new InputHandler();
	
	public static Bitmap sheet = load("/sheet.png");
	public static Bitmap test = load("/test.png");
	public static Bitmap tile = load("/Tiles.png");
	
	public static int counter = 0;
	
	public void render() {
		//screen.drawBitmap(sheet/*, new int[]{ 0, 0, 4*8, 7*8 }*/, 200, 100, 2);
		/*screen.draw(0xffffff, 0, 0, 1, 1, 1);*/
		//screen.draw(0xffffff, -1, -1, 0.5);
		screen.render();
	}
	
	public void run () {
		int frames = 0;
		long time = System.currentTimeMillis();
		long nanotime = System.nanoTime();
		long per = 1000000000/60;
		
		while(running){
			frames++;
			if(System.currentTimeMillis() - time > 1000){
				System.out.println("Frames " + frames
						/*+ ", x0 " + screen.testx0
						 + ", z0 " + screen.testz0
						 + ", x1 " + screen.testx1
						 + ", z1 " + screen.testz1
						 + ", rot " + Math.sin(Math.toRadians(screen.rot))*/);
				frames = 0;
				time = System.currentTimeMillis();
			}
			
			if(System.nanoTime() - nanotime > per){
				counter++;
				if(counter >= 360) counter = 0;
				
				//if(input.keys[VK_LEFT]) screen.rot--;
				//if(input.keys[VK_RIGHT]) screen.rot++;
				//if(screen.rot >= 360) screen.rot = 0;
				
				if(input.keys[VK_LEFT]) screen.rot -= 2;
				if(input.keys[VK_RIGHT]) screen.rot += 2;
				if(input.keys[VK_UP]){
					screen.xCam += Math.sin(Math.toRadians(screen.rot)) / 15;
					screen.zCam += Math.cos(Math.toRadians(screen.rot)) / 15;
				}
				if(input.keys[VK_DOWN]){
					screen.xCam -= Math.sin(Math.toRadians(screen.rot)) / 15;
					screen.zCam -= Math.cos(Math.toRadians(screen.rot)) / 15;
				}
				
				nanotime = System.nanoTime();
			}
			
			BufferStrategy bs = getBufferStrategy();
			render();
			for(int i = 0; i <pixels.length; i++){
				pixels[i] = screen.pixels[i] /*( ((screen.pixels[i] & 0xf) >> 2) &0xfcfcfc ) >> 2*/;
			}
			Graphics gfx = bs.getDrawGraphics();
			gfx.drawImage(image, 0, 0, getWidth(), getHeight(), null);
			gfx.dispose();
			bs.show();
			screen.clear();
			screen.clearZBuffer();
		}
	}
	
	public void start() {
		createBufferStrategy(3);
		requestFocus();
		new Thread(this).start();
	}
	
	public void stop() {
		running = false;
		System.exit(0);
	}
	
	public static void main(String[] args) {
		Game game = new Game();
		game.setPreferredSize(new Dimension(SCREENWIDTH, SCREENHEIGHT));
		
		JFrame frame = new JFrame("Game");
		//frame.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(game);
		frame.pack();
		frame.setResizable(true);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		game.addKeyListener(input);
		game.addFocusListener(input);
		
		game.start();
	}
	
	public static Bitmap load(String file) {
		BufferedImage res = null;
		
		try{
			res = ImageIO.read(Game.class.getResourceAsStream(file));
		}catch(IOException e) {
			System.out.println(e);
		}
		
		Bitmap bitmap = new Bitmap(res.getWidth(), res.getHeight());
		bitmap.pixels = res.getRGB(0, 0, res.getWidth(), res.getHeight(), null, 0, res.getWidth());
		
		return bitmap;
	}
	
}
