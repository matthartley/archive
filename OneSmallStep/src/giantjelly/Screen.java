package giantjelly;

import giantjelly.gfx.Bitmap;
import giantjelly.gfx.Bitmap3D;

public class Screen extends Bitmap3D {

	public Screen(int w, int h) {
		super(w, h);
	}
	
	/*public void drawimage(Image image, int[] coords, int x, int y, double size, int rotation) {
		
		int xstart = coords[0];
		int ystart = coords[1];
		int width = coords[2];
		int height = coords[3];
		
		for(int ys = 0; ys < height*size; ys++){
			for(int xs = 0; xs < width*size; xs++){
				
				int halfx = (int)(width*size)/2;
				int halfy = (int)(height*size)/2;
				
				int yy = (int) (Math.cos(Math.toRadians(rotation)) * (int)(ys));
				int xx = (int) (Math.sin(Math.toRadians(rotation)) * (int)(xs));
				int yy = (int) ( ((ys-halfy) * Math.cos(Math.toRadians(rotation))) + ((xs-halfx) * Math.sin(Math.toRadians(rotation))) );
				int xx = (int) ( ((xs-halfx) * Math.cos(Math.toRadians(rotation))) - ((ys-halfy) * Math.sin(Math.toRadians(rotation))) );
				
				int index = ((y+yy)*Game.WIDTH)+x+xx;
				int imageindex = ((ystart+(int)(ys/size))*image.width)+xstart+(int)(xs/size);
				
				if(index < pixels.length && imageindex < image.pixels.length && index >= 0 && imageindex >= 0
						&& x+xs < Game.WIDTH && y+ys < Game.HEIGHT){
					pixels[index] = image.pixels[imageindex];
				}
				
			}
		}
		
	}*/
	
	private void renderWall(double x0, double y0, double x1, double y1){
		double rSin = Math.sin(Math.toRadians(rot));
		double rCos = Math.cos(Math.toRadians(rot));
		double fov = height;
		double xCam = 0;
		double yCam = -0.2;
		double zCam = 0;
		
		
		
		double xc0 = ((x0 - 0.5) - xCam) * 2;
		double yc0 = ((y0 - 0.5) - zCam) * 2;
		
		double xx0 = xc0 * rCos - yc0 * rSin;
		double u0 = ((-0.5) - yCam) * 2;
		double l0 = ((+0.5) - yCam) * 2;
		double zz0 = yc0 * rCos + xc0 * rSin;
		
		double xc1 = ((x1 - 0.5) - xCam) * 2;
		double yc1 = ((y1 - 0.5) - zCam) * 2;
		
		double xx1 = xc1 * rCos - yc1 * rSin;
		double u1 = ((-0.5) - yCam) * 2;
		double l1 = ((+0.5) - yCam) * 2;
		double zz1 = yc1 * rCos + xc1 * rSin;
		
		//System.out.println();
		
		double xt0 = 0;
		double xt1 = 16;
		
		double zClip = 0.2;
		
		if(zz0<zClip && zz1<zClip) return;
		
		if(zz0<zClip){
			double p = (zClip-zz0)/(zz1-zz0);
			zz0=zz0+(zz1-zz0)*p;
			xx0=xx0+(xx1-xx0)*p;
			xt0=xt0+(xt1-xt0)*p;
		}
		
		if(zz1<zClip){
			double p = (zClip-zz0)/(zz1-zz0);
			zz1=zz0+(zz1-zz0)*p;
			xx1=xx0+(xx1-xx0)*p;
			xt1=xt0+(xt1-xt0)*p;
		}
		
		double xPixel0 = (xx0 / zz0 * fov + width / 2.0);
		double xPixel1 = (xx1 / zz1 * fov + width / 2.0);
		
		if(xPixel0 >= xPixel1) return;
		int xp0 = (int)Math.floor(xPixel0);
		int xp1 = (int)Math.floor(xPixel1);
		if(xp0<0)
			xp0=0;
		if(xp1 >= width)
			xp1 = width-1;
		
		
		
		double yPixel00 = (u0 / zz0 * fov + height / 2.0);
		double yPixel01 = (l0 / zz0 * fov + height / 2.0);
		double yPixel10 = (u1 / zz1 * fov + height / 2.0);
		double yPixel11 = (l1 / zz1 * fov + height / 2.0);
		
		double iz0 = 1/zz0;
		double iz1 = 1/zz1;
		
		double ixt0 = xt0 * iz0;
		double ixta = xt1 * iz1 - ixt0;
		
		for(int x=xp0; x<=xp1; x++){
			double pr = (x-xPixel0)/(xPixel1-xPixel0);
			
			double iz = iz0 + (iz1 - iz0) * pr;
			int xTex = (int) ((ixt0+ixta*pr)/iz);
 			
			double yPixel0 = yPixel00+(yPixel10-yPixel00)*pr;
			double yPixel1 = yPixel01+(yPixel11-yPixel01)*pr;
			
			int yp0 = (int)Math.floor(yPixel0);
			int yp1 = (int)Math.floor(yPixel1);
			if(yp0<0)
				yp0=0;
			if(yp1 >= height)
				yp1 = height-1;
			
			for(int y=yp0; y<=yp1; y++){
				double pry = (y - yPixel0) / (yPixel1 - yPixel0);
				int yTex = (int)(16*pry);
				/*pixels[x+y*width] = xTex * 100+yTex*100*256;*/
				pixels[x+y*width] = Game.tile.pixels[((xTex & 15) + 0) + ((yTex & 15) + 0) * 16];
				//zBuffer[x + y * width] = 1 / iz*4;
			}
		}
		
		/*for(int i = 0; i < 1000; i++){
			
			
			int xPixel = (int) (xx / zz * fov + width / 2);
			int yPixel = (int) (yy / zz * fov + height / 2);
			if(xPixel>0 && xPixel<width && yPixel>0 && yPixel<height){
				pixels[xPixel + yPixel * width] = 0xff00ff;
			}
		}*/
	}
	
}
