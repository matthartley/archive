package giantjelly.gfx;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

import giantjelly.Game;

public class Bitmap {
	
	public int[] pixels;
	public int width;
	public int height;
	
	public Bitmap(int w, int h) {
		pixels = new int[w*h];
		width = w;
		height = h;
	}
	
	public void draw(Bitmap bitmap) {
		for(int yp = 0; yp < bitmap.height; yp++){
			if(yp > height || yp < 0) continue;
			
			for(int xp = 0; xp < bitmap.width; xp++){
				if(xp > width || xp < 0) continue;
				
				pixels[xp+yp*width] = bitmap.pixels[xp+yp*bitmap.width];
			}
		}
	}
	
	public void draw(int color, int x, int y, int xs, int ys) {
		for(int yp = y; yp < y+ys; yp++){
			if(yp > height || yp < 0) continue;
			
			for(int xp = x; xp < x+xs; xp++){
				if(xp > width || xp < 0) continue;
				
				int index = xp + yp * width;
				pixels[index] = color;
			}
		}
	}
	
	public void drawBitmap(Bitmap bitmap, int x, int y, int size) {
		for(int yp = 0; yp < bitmap.height*size; yp++){
			if(yp > height || yp < 0) continue;
			
			for(int xp = 0; xp < bitmap.width*size; xp++){
				if(xp > width || xp < 0) continue;
				
				int p1 = (x+xp)+(y+yp)*width;
				int p2 = (xp/size)+(yp/size)*bitmap.width;
				
				pixels[p1] = bitmap.pixels[p2];
			}
		}
	}
	
	public void clear() {
		for(int i = 0; i < pixels.length; i++){
			pixels[i] = 0;
		}
	}
	
	public Bitmap cut(int x, int y, int w, int h) {
		Bitmap bitmap = new Bitmap(w, h);
		
		for(int yp = 0; yp < bitmap.height; yp++){
			if(yp > height || yp < 0) continue;
			
			for(int xp = 0; xp < bitmap.width; xp++){
				if(xp > width || xp < 0) continue;
				
				bitmap.pixels[xp+yp*w] = pixels[(x+xp)+(y+yp)*width];
			}
		}
		
		return bitmap;
	}
	
}
