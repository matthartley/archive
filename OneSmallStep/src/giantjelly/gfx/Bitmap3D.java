package giantjelly.gfx;

import java.util.Random;

import giantjelly.Game;

public class Bitmap3D extends Bitmap {
	
	public double[] zBuffer;
	
	public double rot = 0;
	
	public double testx0 = 0;
	public double testz0 = 0;
	public double testx1 = 0;
	public double testz1 = 0;

	public Bitmap3D(int w, int h) {
		super(w, h);
		zBuffer = new double[w*h];
		for(int i = 0; i < w*h; i++) zBuffer[i] = 0;
	}
	
	public double xCam, yCam, zCam = 0;

	public void render() {
		
		//xCam = 0;
		yCam = -0.2;
		//zCam = 0;
		
		double rSin = Math.sin(Math.toRadians(rot));
		double rCos = Math.cos(Math.toRadians(rot));
		double fov = height;
		
		//rot = 45 * Math.sin(Math.toRadians(Game.counter));
		
		for(int y = 0; y < height; y++){
			double yd = y - height / 2;
			
			double z = 100.0 / yd;
			
			for(int x = 0; x < width; x++){
				double xd = x - width / 2;
				xd *= z;
				
				int xx = (int) (xd) & 15;
				int yy = (int) (z) & 15;
				
				pixels[x + y * width] = xx * 128;
			}
		}
		
		//renderWall(0, 1, 0, 2);
		//renderWall(0, 0.2, 0, 1);
		
		//renderWall(1, 2, 1, 1);
		//renderWall(1, 1, 1, 0);
		
		
		
		renderWall(1, 2, 1, 1);
		renderWall(1, 1, 1, 0);
		renderWall(1, 0, 1, -1);
		renderWall(1, -1, 1, -2);
		
		renderWall(0, 1, 0, 2);
		renderWall(0, 0, 0, 1);
		renderWall(0, -1, 0, 0);
		renderWall(0, -2, 0, -1);
		
		renderWall(0, 2, 1, 2);
		renderWall(1, 2, 0, 2);
		//renderWall(1, 2, 2, 2);
	}
	
	private void renderWall(double x0, double z0, double x1, double z1){
		double rSin = Math.sin(Math.toRadians(rot));
		double rCos = Math.cos(Math.toRadians(rot));
		double fov = height;
		
		
		//Corner relative positions
		double xc0 = ((x0 - 0.5) - xCam) * 2;
		double zc0 = ((z0 - 0.5) - zCam) * 2;
		double xc1 = ((x1 - 0.5) - xCam) * 2;
		double zc1 = ((z1 - 0.5) - zCam) * 2;
		
		//Corner relative positions with rotation
		double xx0 = xc0 * rCos - zc0 * rSin;
		double zz0 = zc0 * rCos + xc0 * rSin;
		double u0 = ((-0.5) - yCam) * 2;
		double l0 = ((+0.5) - yCam) * 2;
		double xx1 = xc1 * rCos - zc1 * rSin;
		double zz1 = zc1 * rCos + xc1 * rSin;
		double u1 = ((-0.5) - yCam) * 2;
		double l1 = ((+0.5) - yCam) * 2;
		
		double xt0 = 0;
		double xt1 = 16;
		
		double zClip = 0.2;
		if(zz0<zClip && zz1<zClip) return;
		if(zz0<zClip){
			double p = (zClip-zz0)/(zz1-zz0);
			zz0=zz0+(zz1-zz0)*p;
			xx0=xx0+(xx1-xx0)*p;
			xt0=xt0+(xt1-xt0)*p;
		}
		if(zz1<zClip){
			double p = (zClip-zz0)/(zz1-zz0);
			zz1=zz0+(zz1-zz0)*p;
			xx1=xx0+(xx1-xx0)*p;
			xt1=xt0+(xt1-xt0)*p;
		}
		
		double xPixel0 = xx0 / zz0 * fov + width / 2.0;
		double xPixel1 = xx1 / zz1 * fov + width / 2.0;
		
		double yPixel00 = u0 / zz0 * fov + height / 2.0;
		double yPixel01 = l0 / zz0 * fov + height / 2.0;
		double yPixel10 = u1 / zz1 * fov + height / 2.0;
		double yPixel11 = l1 / zz1 * fov + height / 2.0;
		
		if(xPixel0 > xPixel1) return;
		int xp0 = (int) Math.floor(xPixel0);
		int xp1 = (int) Math.floor(xPixel1);
		if(xp0<0) xp0 = 0;
		if(xp1>=width) xp1 = width - 0;
		
		double iz0 = 1/zz0;
		double iz1 = 1/zz1;
		
		for(int xp = xp0; xp < xp1; xp++){
			//if(xp <= 0 || xp >= width) continue;
			
			double xPosPer = (xp-xPixel0)/(xPixel1-xPixel0);
			//double xPosPer0 = (double)(xp-(int)xPixel0)/(double)((int)xPixel1-(int)xPixel0);
			
			double yPixel0 = yPixel00+(yPixel10-yPixel00)*xPosPer;
			double yPixel1 = yPixel01+(yPixel11-yPixel01)*xPosPer;
			
			int yp0 = (int) Math.floor(yPixel0);
			int yp1 = (int) Math.floor(yPixel1);
			if(yp0<0) yp0 = 0;
			if(yp1>=height) yp1 = height - 0;
			
			double iz = iz0 + (iz1 - iz0) * xPosPer;
			int xpt = (int)( ((xt0*iz0)+(xt1*iz1-xt0*iz0)*xPosPer) / iz );
			
			
			
			for(int yp = yp0; yp < yp1; yp++){
				double yPosPer = (yp - yPixel0) / (yPixel1 - yPixel0);
				int ypt = (int)(16*yPosPer);
				
				if(zBuffer[xp+yp*width] > iz) continue;
				zBuffer[xp+yp*width] = iz;
				
				//if(1/iz*4 < zBuffer[xp+yp*width]){
					pixels[xp+yp*width] = Game.tile.pixels[(xpt & 15) + (ypt & 15) * Game.tile.width];
					//zBuffer[xp+yp*width] = 1/iz*4;
				//}
			}
		}
		
		/*double pr = xPixel0/(xPixel1-xPixel0);
		
		double yPixel0 = yPixel00+(yPixel10-yPixel00)*pr;
		double yPixel1 = yPixel01+(yPixel11-yPixel01)*pr;*/
		
		/*if(xPixel0>0&&xPixel0<width&yPixel00>0&&yPixel00<height) pixels[(int)Math.floor(xPixel0) + (int)Math.floor(yPixel00) * width] = 0xff00ff;
		if(xPixel1>0&&xPixel1<width&yPixel01>0&&yPixel01<height) pixels[(int)Math.floor(xPixel1) + (int)Math.floor(yPixel01) * width] = 0xff00ff;
		if(xPixel0>0&&xPixel0<width&yPixel10>0&&yPixel10<height) pixels[(int)Math.floor(xPixel0) + (int)Math.floor(yPixel10) * width] = 0xff00ff;
		if(xPixel1>0&&xPixel1<width&yPixel11>0&&yPixel11<height) pixels[(int)Math.floor(xPixel1) + (int)Math.floor(yPixel11) * width] = 0xff00ff;*/
	}
	
	public void clearZBuffer() {
		for(int i = 0; i < width*height; i++){
			zBuffer[i] = 0;
		}
	}
	
	/*public void draw(int color, int x, int y, double z, int xs, int ys) {
		
		//double rot = Math.sin(Math.toRadians(Game.counter));
		
		double y1 = y-(ys/2);
		double x1 = -(z * Math.sin(Math.toRadians(rot))) - (xs/2 * Math.cos(Math.toRadians(rot)));
		double z1 = -(z * Math.cos(Math.toRadians(rot))) - (xs/2 * Math.sin(Math.toRadians(rot)));
		
		double y2 = y+(ys/2);
		double x2 = -(z * Math.sin(Math.toRadians(rot))) - (xs/2 * Math.cos(Math.toRadians(rot)));
		double z2 = -(z * Math.cos(Math.toRadians(rot))) - (xs/2 * Math.sin(Math.toRadians(rot)));
		
		double y3 = y-(ys/2);
		double x3 = -(z * Math.sin(Math.toRadians(rot))) + (xs/2 * Math.cos(Math.toRadians(rot)));
		double z3 = -(z * Math.cos(Math.toRadians(rot))) + (xs/2 * Math.sin(Math.toRadians(rot)));
		
		double y4 = y+(ys/2);
		double x4 = -(z * Math.sin(Math.toRadians(rot))) + (xs/2 * Math.cos(Math.toRadians(rot)));
		double z4 = -(z * Math.cos(Math.toRadians(rot))) + (xs/2 * Math.sin(Math.toRadians(rot)));
		
		//ys = 200*ys;
		//xs = 200*xs;
		//z *= 0.75;
		
		int xc = Game.WIDTH/2;
		int yc = Game.HEIGHT/2;
		
		//int ystart = yc-(int)((ys/2)/z);
		//int xstart = xc-(int)((xs/2)/z);
		//ystart += rot*yc;
		//xstart += (rot/45)*xc;
		
		double distance = distance(x1, y1, z1);
		int yp1 = yc + (int)(y1 / distance * 200);
		int xp1 = xc + (int)(x1 / distance * 200);
		
		distance = distance(x2, y2, z2);
		int yp2 = yc - (int)(y2 / distance * 200);
		int xp2 = xc + (int)(x2 / distance * 200);
		
		distance = distance(x3, y3, z3);
		int yp3 = yc + (int)(y3 / distance * 200);
		int xp3 = xc + (int)(x3 / distance * 200);
		
		distance = distance(x4, y4, z4);
		int yp4 = yc - (int)(y4 / distance * 200);
		int xp4 = xc + (int)(x4 / distance * 200);
		
		int yTop = (yp1 <= yp3) ? yp1 : yp3;
		int yBottom = (yp2 >= yp4) ? yp2 : yp4;
		int yTopOther = (yp1 <= yp3) ? yp3 : yp1;
		int yBottomOther = (yp2 >= yp4) ? yp4 : yp2;
		
		for(int yp = 0; yp < yBottom-yTop; yp++){
			for(int xp = 0; xp < xp3-xp1; xp++){
				
				
				double asd = ((double)yp/(yBottom-yTop)) * (yBottomOther-yTopOther);
				pixels[(xp1+xp)+(yTopOther+(int)asd)*width] = color;
			}
		}
		
	}
	
	public static double distance(double x, double y, double z) {
		return Math.sqrt( (x*x) + (y*y) + (z*z) );
	}*/
	
	/*public void draw(int color, double x, double y, double z) {
		double rSin = Math.sin(Math.toRadians(rot));
		double rCos = Math.cos(Math.toRadians(rot));
		
		int xc = Game.WIDTH/2;
		int yc = Game.HEIGHT/2;
		
		double x0 = -(z * rSin) - (1 * rCos);
		double z0 = ((z * rCos) - (1 * rSin));
		double x1 = -(z * rSin) + (1 * rCos);
		double z1 = ((z * rCos) + (1 * rSin));
		
		testx0 = x0;
		testz0 = z0;
		testx1 = x1;
		testz1 = z1;
		
		//int xx = (int)(x*100) - (int)((width/2)*rSin);
		//int yy = (int)(y*100);
		
		int xp0 = xc + (int)((x0 / z0) * width)+100;
		int yp0 = (int)(yc - ((double)(1.0 / distance(x0, -1, z0)))*100);
		
		int xp1 = xc + (int)((x0 / z0) * width)+100;
		int yp1 = (int)(yc + ((double)(1.0 / distance(x0, 1, z0)))*100);
		
		int xp2 = xc + (int)((x1 / z1) * width)-100;
		int yp2 = (int)(yc - ((double)(1.0 / distance(x0, -1, z0)))*100);
		
		int xp3 = xc + (int)((x1 / z1) * width)-100;
		int yp3 = (int)(yc + ((double)(1.0 / distance(x0, 1, z0)))*100);
		
		int yTop = (yp0 <= yp2) ? yp0 : yp2;
		int yBottom = (yp1 >= yp3) ? yp1 : yp3;
		int yTopOther = (yp0 <= yp2) ? yp2 : yp0;
		int yBottomOther = (yp1 >= yp3) ? yp3 : yp1;
		
		for(int yp = 0; yp < yBottom-yTop; yp++){
			for(int xp = 0; xp < xp2-xp0; xp++){
				pixels[(xp0+xp)+(yp0+yp)*width] = color;
			}
		}
		
		pixels[xp0 + yp0 * width] = color;
		pixels[xp1 + yp1 * width] = color;
		pixels[xp2 + yp2 * width] = color;
		pixels[xp3 + yp3 * width] = color;
	}
	
	public static double distance(double x, double y, double z) {
		return Math.sqrt( (x*x) + (y*y) + (z*z) );
	}*/

}
