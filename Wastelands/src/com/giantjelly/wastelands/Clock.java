package com.giantjelly.wastelands;

public class Clock {
	
	public static int minutes = 6*60+40;
	
	public static int getminutes() {
		return minutes%60;
	}
	
	public static int gethours() {
		return minutes/60;
	}
	
	public static String gettime() {
		String hour;
		String min;
		
		if(minutes/60 < 10) hour = "0"+minutes/60; else hour = ""+(minutes/60);
		if(minutes%60 < 10) min = "0"+minutes%60; else min = ""+(minutes%60);
		
		return hour+":"+min;
	}
	
	public static void update() {
		minutes++;
		if(minutes >= 1440) minutes = 0;
	}
	
}
