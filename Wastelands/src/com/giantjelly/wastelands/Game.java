package com.giantjelly.wastelands;

import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.input.Mouse;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.LWJGLException;

import static org.lwjgl.opengl.GL11.*;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.AL.*;

import com.giantjelly.wastelands.gfx.Font;
import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.gui.Message;
import com.giantjelly.wastelands.gui.menu.*;
import com.giantjelly.wastelands.sound.Listener;
import com.giantjelly.wastelands.sound.Sound;
import com.giantjelly.wastelands.sound.Source;
import com.giantjelly.wastelands.world.Block;
import com.giantjelly.wastelands.world.Triangle;
import com.giantjelly.wastelands.entity.Animation;
import com.giantjelly.wastelands.entity.Gun;
import com.giantjelly.wastelands.entity.Player;
import com.giantjelly.wastelands.entity.Turret;
import com.giantjelly.wastelands.networking.GameClient;
import com.giantjelly.wastelands.networking.GameServer;
import com.giantjelly.wastelands.networking.NetPlayer;
import com.giantjelly.wastelands.input.Input;

public class Game {
	
	public static String VERSION = "Pre-Alpha v2";
	
	public static long MAXPING = 5000;
	static int RESPAWNTIME = 3; //10;
	public static int scoreLimit = 3; //20;
	
	public static final int WIDTH = 1024;
	public static final int HEIGHT = 720;
	public static Game game = new Game();
	
	public boolean running = false;
	public static boolean matchRunning = false;
	public static int nextMatchCountDown = 0;
	public static String matchWinner = "";
	
	public static int packetsReceived = 0;
	
	public void start(){
		glClearColor(0.1f, 0.1f, 0.1f, 1);
		MainMenu.rand.setSeed(100);
		
		running = true;
		run();
	}
	
	public static ArrayList<Block> Blocks = new ArrayList<Block>();
	public static ArrayList<Triangle> Triangles = new ArrayList<Triangle>();
	public static ArrayList<Turret> turrets = new ArrayList<Turret>();
	public static ArrayList<Gun> Guns = new ArrayList<Gun>();
	public static String username = "";
	
	public enum Mode {
		LOGIN, SPLASHSCREEN, MENU, EDITORMENU, MULTIPLAYERJOIN, EDITOR, PLAY, PLAYMENU;
	}
	public static Mode mode = Mode.LOGIN;
	
	public static Listener listener = new Listener();
	public static Source blaster = new Source(new Sound("res/sounds/weapons/Blaster.wav"), 0.3f);
	public static Source plasma = new Source(new Sound("res/sounds/weapons/Plasma.wav"), 0.2f);
	public static Source rocketlauncher = new Source(new Sound("res/sounds/weapons/Rocket.wav"), 0.1f);
	public static Source click = new Source(new Sound("res/sounds/Click.wav"), 0.1f);
	public static Source bullet = new Source(new Sound("res/sounds/Bullet.wav"), 0.1f);
	public static Source rocket = new Source(new Sound("res/sounds/Rocket.wav"), 0.3f);
	public static Source footstep = new Source(new Sound("res/sounds/Footstep.wav"), 0.1f);
	
	public static String info = "";
	
	public static ArrayList<Animation> animations = new ArrayList<Animation>();
	
	public static String sfx = "";
	public static String action = "";
	
	public static boolean networkReset = false;
	
	public static Random random = new Random();
	public static int randomNumber = 100;
	
	/*public static void resetServerGame() {
		Play.health = 10;
		Play.kills = 0;
		for(NetPlayer p : Play.players){
			p.health = 10;
			p.kills = 0;
		}
		Play.serverRestart = true;
		Play.player = new Player(128, -64);
		
		Play.player.gunindex = 0;
	}
	
	public static void resetClientGame() {
		Play.player = new Player(128, -64);
		Play.player.gunindex = 0;
	}*/
	
	public void run() {
		int frames = 0;
		long lastframe = System.nanoTime();
		
		int ticks = 0;
		double unprocessed = 1;
		long lasttick = System.nanoTime();
		double nspertick = 1000000000/60;
		
		while(running && !Display.isCloseRequested()){
			long time = System.nanoTime();
			unprocessed += (time - lasttick)/nspertick;
			lasttick = time;
			
			while(unprocessed >= 1){
				ticks++;
				tick();
				unprocessed--;
			}
			
			render();
			frames++;
			
			if(System.nanoTime() - lastframe > 1000000000){
				info = "frames " + frames + ", ticks " + ticks + ", packets " + packetsReceived;
				//System.out.println(info);
				frames = 0;
				ticks = 0;
				packetsReceived = 0;
				lastframe = System.nanoTime();
				
				for(int i = 0; i < Play.players.size(); i++){
					Play.players.get(i).packets = Play.players.get(i).tmppackets;
					Play.players.get(i).tmppackets = 0;
				}
				
				if(Play.respawntime > 0) Play.respawntime--;
				
				//if(GameClient.running) Client.stillhere();
				
				randomNumber = random.nextInt(100);
				
				for(int i = 0; i < Guns.size(); i++){
					Guns.get(i).coolDown--;
				}
				
				if(GameServer.running && !matchRunning){
					if(nextMatchCountDown > 0) nextMatchCountDown--;
					if(nextMatchCountDown <= 0) GameServer.restart();
				}
			}
		}
	}
	
	private static ArrayList<Message> log = new ArrayList<Message>();////////////////////////////////////
	
	public static void log(String text) {
		log.add(new Message(text));
	}
	
	public static Message[] getLog() {
		Message[] m = new Message[log.size()];
		for(int i = 0; i < m.length; i++){
			m[i] = log.get(i);
		}
		return m;
	}
	
	private void render() {
		if(Display.wasResized()){
			glViewport(0, 0, Display.getWidth(), Display.getHeight());
			
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 10, -10);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
		}
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		if(mode == Mode.LOGIN)
			Login.render();
		if(mode == Mode.SPLASHSCREEN)
			SplashScreen.draw();
		if(mode == Mode.MENU)
			MainMenu.render();
		if(mode == Mode.EDITORMENU)
			EditorMenu.render();
		if(mode == Mode.MULTIPLAYERJOIN)
			MultiplayerJoinMenu.render();
		if(mode == Mode.PLAYMENU)
			PlayMenu.render();
		
		if(mode == Mode.EDITOR)
			Editor.render();
		if(mode == Mode.PLAY)
			Play.render();
		else
			Font.small.Draw(10, Display.getHeight()-25, "press escape to go to parent menu");
		
		glEnable(GL_TEXTURE_2D);
		Font.font.Draw(10, 0, "> " + VERSION);
		Font.font.Draw(10, 30, "Packets " + packetsReceived);
		Font.font.Draw(10, 60, info);
		Font.font.Draw(10, 90, "bullets " + Play.Bullets.size() + ", animations " + animations.size());
		Font.font.Draw(10, 120, "clients " + (GameServer.server!=null ? GameServer.server.getConnections().length : "none"));
		Font.font.Draw(10, 150, "server " + (GameClient.client!=null ? GameClient.client.isConnected() : "null"));
		Font.font.Draw(10, 180, matchRunning + " - " + nextMatchCountDown);
		Font.font.Draw(10, 210, "health " + Play.health);
		for(int i = 0; i < Play.players.size(); i++){
			Font.font.Draw(10, 240+(i*30), Play.players.get(i).packet.username+"'s health " + Play.players.get(i).health + " - "+Play.players.get(i).dead);
		}
		//if(GameServer.server.getConnections().length > 0) Font.font.Draw(10, 90, ""+GameServer.server.getConnections()[0].getID());
		//if(GameClient.client.get.length > 0) Font.font.Draw(10, 90, GameServer.server.getConnections()[0].getID());
		if(!Display.isActive()){
			Font.font.Draw((Display.getWidth() / 2) - 100, Display.getHeight() / 2, "CLICK TO FOCUS");
		}
		
		if(!GameClient.running){
			for(int i = log.size()-1; i > -1; i--){
				if(log.get(i).active){
					Font.font.Draw(Display.getWidth()-Font.font.getWidth(log.get(i).text)-10, 10+(((log.size()-1)-i)*30), log.get(i).text);
				}
			}
		}
		if(GameClient.running){
			for(int i = 0; i < Play.serverMessages.length; i++){
				if(Play.serverMessages[i].active){
					Font.font.Draw(Display.getWidth()-Font.font.getWidth(Play.serverMessages[i].text)-10, 10+(((Play.serverMessages.length-1)-i)*30), Play.serverMessages[i].text);
				}
			}
		}
		
		if(Play.deadmsgcooldown>0){
			glColor4f(1, 1, 1, (float)Play.deadmsgcooldown/200);
			String dead = "You Died!";
			Font.font.Draw(
					(Display.getWidth()/2)-Font.font.getWidth(dead),
					(Display.getHeight()/2)-Font.font.getHeight(dead),
					dead
			);
			glColor4f(1, 1, 1, 1);
		}
		
		Display.update();
		//Display.sync(60);
	}
	
	public static boolean cursor = false;
	public static int cd = 20;
	
	private void curs() {
		cd--;
		if(cd <= 0){
			cd = 20;
			if(cursor)
				cursor = false;
			else
				cursor = true;
		}
	}
	
	private void tick() {
		if(Input.f1.downEvent()) Sprite.Load();
		curs();
		
		if(mode == Mode.PLAY){
			Play.tick();
		}
		if(mode == Mode.LOGIN)
			Login.tick();
		if(mode == Mode.SPLASHSCREEN)
			SplashScreen.update();
		if(mode == Mode.MENU)
			MainMenu.tick();
		if(mode == Mode.EDITORMENU)
			EditorMenu.tick();
		if(mode == Mode.MULTIPLAYERJOIN)
			MultiplayerJoinMenu.tick();
		if(mode == Mode.PLAYMENU)
			PlayMenu.tick();
		if(mode == Mode.EDITOR)
			Editor.tick();
		
		for(Message m : log){
			m.update();
		}
		
		if(GameServer.running){
			for(int i = 0; i < Play.players.size(); i++){
				if(System.currentTimeMillis()-Play.players.get(i).lasttime > MAXPING){
					log(Play.players.get(i).packet.username+" has disconnected!");
					Play.players.remove(i);
				}
			}
		}
		
		if(GameClient.running){
			if(System.currentTimeMillis() - GameClient.ping > Game.MAXPING){
				mode = Mode.MULTIPLAYERJOIN;
				GameClient.running = false;
				System.out.println("Connection timed out, returning to menu...");
			}
		}
		
		
		Input.update();
		Input.keyUpdate();
	}
	
	public static void main(String[] args){
		try{
			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
			Display.setResizable(true);
			Display.setTitle("Wastelands by GiantJelly");
			Mouse.setGrabbed(false);
			Display.create();
		}catch(LWJGLException e){
			System.out.println(e);
		}
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 10, -10);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);
		
		glAlphaFunc(GL_GREATER, 0.1f);
		glEnable(GL_ALPHA_TEST);
		
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		
		Sprite.Load();
		
		game.start();
		Game.click.play();
		AL.destroy();
		System.exit(0);
	}
	
	/*public static void die() {
		Play.dead = true;
		Play.health = 10;
		Play.player.x = 128;
		Play.player.y = -64;
		//Play.player.xscroll = 0;
		//Play.player.yscroll = 0;
		Play.deadmsgcooldown = 200;
		Play.respawntime = RESPAWNTIME;
		Play.player.turretIndex = -1;
		Play.player.gunindex = 0;
	}
	
	public static void resetgame() {
		Play.health = 10;
		Play.kills = 0;
		Play.player = new Player(128, -64);
		Play.paused = false;
		Play.player.gunindex = 0;
	}*/
	
	public static void die() {
		Play.dead = true;
		//Play.health = 10;
		//Play.player = new Player(128, -64);
		//Play.paused = false;
		Play.player.turretIndex = -1;
		Play.deadmsgcooldown = 200;
		Play.respawntime = RESPAWNTIME;
	}
	
	public static void reset() {
		Play.dead = false;
		Play.health = 10;
		Play.player = new Player(128, -64);
		Play.paused = false;
		Play.player.turretIndex = -1;
		
		Play.deadmsgcooldown = 0;
		Play.respawntime = 0;
	}
	
	public static void serverReset() {
		Play.health = 10;
		Play.kills = 0;
		Play.dead = false;
		for(NetPlayer p : Play.players){
			p.health = 10;
			p.kills = 0;
			p.dead = false;
		}
	}
	
	public static void soundrunner(String sound) {
		if(sound.equals("blaster")) blaster.play();
		if(sound.equals("plasma")) plasma.play();
		if(sound.equals("bullet")) bullet.play();
		if(sound.equals("rocketlauncher")) rocketlauncher.play();
		if(sound.equals("rocket")) rocket.play();
		
		if(sound.equals("footstep")) footstep.play();
	}
	
	public static void drawsquare(int x, int y, int w, int h) {
		glDisable(GL_TEXTURE_2D);
		
		glBegin(GL_QUADS);
		glVertex3d(x, y, 0);
		glVertex3d(x+w, y, 0);
		glVertex3d(x+w, y+h, 0);
		glVertex3d(x, y+h, 0);
		glEnd();
	}
}
