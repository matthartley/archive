package com.giantjelly.wastelands;

import org.lwjgl.opengl.Display;
import static org.lwjgl.opengl.GL11.*;

import com.giantjelly.wastelands.gfx.Sprite;
import static com.giantjelly.wastelands.Game.mode;

public class SplashScreen {
	
	public static boolean done = false;
	public static float alpha = 0.00f;
	public static int count = 100;
	
	public static void update(){	
		if(alpha < 1 && count == 100)
			alpha += 0.01f;
		
		if(alpha >= 1)
			count--;
		
		if(count <= 0)
			alpha -= 0.01f;
		
		if(alpha <= 0)
			mode = Game.Mode.MENU;
	}
	
	public static void draw() {
		glColor4f(1, 1, 1, alpha);
		
		glEnable(GL_TEXTURE_2D);
		Sprite.SplashScreen.bind();
		glBegin(GL_QUADS);
			glTexCoord2f(0, 0);
			glVertex3f(0, 0, 0);
			
			glTexCoord2f(1, 0);
			glVertex3f(Display.getWidth(), 0, 0);
			
			glTexCoord2f(1, 1);
			glVertex3f(Display.getWidth(), Display.getHeight(), 0);
			
			glTexCoord2f(0, 1);
			glVertex3f(0, Display.getHeight(), 0);
		glEnd();
	}
	
}