package com.giantjelly.wastelands;

//import com.giantjelly.wastelands.networking.Client;
//import com.giantjelly.wastelands.networking.ClientCommand;

import static com.giantjelly.wastelands.Play.*;

import java.io.IOException;

import com.giantjelly.wastelands.networking.GameClient;

public class ClientTick {
	
	public static void tick() {
		for(int i = 0; i < serverTurrets.length; i++){
			serverTurrets[i].update(i);
		}
		
		for(int i = 0; i < serverGuns.length; i++){
				if(i<serverGuns.length) serverGuns[i].update(i);
		}
		
		GameClient.sendUpdate();
		
		/*for(int i = 0; i < serverPlayers.length; i++){
			if(serverPlayers[i].packet.username.equals(Game.username)){
				if(serverPlayers[i].health <= 0){
					Game.die();
					GameClient.dead();
				}
			}
		}*/
	}

}
