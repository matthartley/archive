package com.giantjelly.wastelands.input;

import java.util.Random;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.giantjelly.wastelands.sound.Sound;
import com.giantjelly.wastelands.entity.Bullet;
import com.giantjelly.wastelands.entity.Turret;
import com.giantjelly.wastelands.gfx.Font;
import com.giantjelly.wastelands.gui.Message;
import com.giantjelly.wastelands.gui.TextInput;
import com.giantjelly.wastelands.gui.menu.EditorMenu;
import com.giantjelly.wastelands.gui.menu.Login;
import com.giantjelly.wastelands.gui.menu.MultiplayerJoinMenu;
import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Play;

import java.lang.Math;

public class Input {
	
	public static void update(){
		if(Game.mode == Game.Mode.PLAY){
			while(Keyboard.next()){
				try{
					/*if(Keyboard.getEventKey() == Keyboard.KEY_1 && Keyboard.getEventKeyState()){
						setgun(0);
					}
					if(Keyboard.getEventKey() == Keyboard.KEY_2 && Keyboard.getEventKeyState()){
						setgun(1);
					}
					if(Keyboard.getEventKey() == Keyboard.KEY_3 && Keyboard.getEventKeyState()){
						setgun(2);
					}
					if(Keyboard.getEventKey() == Keyboard.KEY_4 && Keyboard.getEventKeyState()){
						setgun(3);
					}
					if(Keyboard.getEventKey() == Keyboard.KEY_5 && Keyboard.getEventKeyState()){
						setgun(4);
					}
					if(Keyboard.getEventKey() == Keyboard.KEY_6 && Keyboard.getEventKeyState()){
						setgun(5);
					}
					if(Keyboard.getEventKey() == Keyboard.KEY_7 && Keyboard.getEventKeyState()){
						setgun(6);
					}
					if(Keyboard.getEventKey() == Keyboard.KEY_8 && Keyboard.getEventKeyState()){
						setgun(7);
					}*/
				}catch(Exception e){
					System.out.println(e + " || No gun in that slot!");
				}
				
				if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState() && Game.mode == Game.Mode.PLAY){
					if(Play.paused)
						Play.paused = false;
					else
						Play.paused = true;
				}
			}
		}
		
		if(Mouse.isButtonDown(0)){
			if(Play.player.firstWeapon!=null && 
					Play.player.turretIndex==-1 && 
					Game.mode == Game.Mode.PLAY && 
					Play.player.firstWeapon.
					shootcount<=0){
				
				Play.player.firstWeapon.shootcount = 15;
				Play.player.firstWeapon.ammo--;
				float yspeed = (new Random().nextFloat()*5) - 2;
				
				float r = (float)Math.toDegrees(Math.atan2(Mouse.getX() - (Play.player.x+58), Mouse.getY() - (Play.player.y-74)));
				
				int sd;
				int pos;
				boolean gogo = false;
				
				double x=0;
				double y=0;
				
				if(!Play.player.flip){
					pos = 48;
					sd = 40;
					if(r > 0)
						gogo = true;
					
					x = (Math.sin(Math.toRadians(r))*30);
					y = -(Math.cos(Math.toRadians(r))*30);
				}else{
					pos = 68;
					sd = -40;
					if(r < 0)
						gogo = true;
					
					x = (Math.sin(Math.toRadians(r))*30);
					y = -(Math.cos(Math.toRadians(r))*30);
				}
					
				if(Play.player.gunindex!=0){
					Play.player.firstWeapon.fire(new int[]{ ((Play.player.x + pos) - Play.player.xscroll) + (int)x, ((Play.player.y + 64) - Play.player.yscroll) + (int)y }, new float[]{ (int)x*1.3f, (int)y*1.3f });
					Play.player.muzzleflash = 10;
				}else{
					
					Play.player.firstWeapon.fire(new int[]{ ((Play.player.x + pos) - Play.player.xscroll), ((Play.player.y + 78) - Play.player.yscroll) }, new float[]{ sd, yspeed });
				}
				
				Play.player.firstWeapon.bang();
					
			}
		}
		
		typing();
	}
	
	public static Key click = new Key();
	public static Key menuClick = new Key();
	public static Key escape = new Key();
	public static Key x = new Key();
	public static Key e = new Key();
	public static Key rightClick = new Key();
	public static Key f1 = new Key();
	public static Key enter = new Key();
	
	public static void keyUpdate() {
		click.set(Mouse.isButtonDown(0));
		rightClick.set(Mouse.isButtonDown(1));
		menuClick.set(Mouse.isButtonDown(0));
		escape.set(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE));
		x.set(Keyboard.isKeyDown(Keyboard.KEY_X));
		e.set(Keyboard.isKeyDown(Keyboard.KEY_E));
		f1.set(Keyboard.isKeyDown(Keyboard.KEY_F1));
		enter.set(Keyboard.isKeyDown(Keyboard.KEY_RETURN));
	}
	
	private static String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345678900-_.";
	
	private static void textInput(TextInput ti) {
		if(ti.active){
			while(Keyboard.next()){
				if(Keyboard.getEventKeyState()){
					if(Keyboard.getEventKey() == Keyboard.KEY_BACK){
						if(ti.text.length() > 0)
							ti.text = ti.text.substring(0, ti.text.length()-1);
					}else{
						char c = Keyboard.getEventCharacter();
						if(chars.contains(""+c)) ti.text += c;
					}
				}
			}
		}
	}
	
	public static void typing() {
		if(Game.mode==Game.Mode.LOGIN){
			textInput(Login.username);
			textInput(Login.password);
		}
		if(Game.mode==Game.Mode.MULTIPLAYERJOIN){
			textInput(MultiplayerJoinMenu.ip);
		}
		if(Game.mode==Game.Mode.EDITORMENU){
			textInput(EditorMenu.NEW);
		}
		
		while(Keyboard.next());
	}
	
}
