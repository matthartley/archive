package com.giantjelly.wastelands.input;

public class Key {
	
	private boolean down = false;
	private boolean last = false;
	
	public void set(boolean bool) {
		last = down;
		down = bool;
	}
	
	public boolean downEvent() {
		boolean rtn = (down && !last) ? true : false;
		//if(rtn) last = true;
		return rtn;
	}
	
	public boolean down() {
		return down;
	}
	
	public void setState(boolean state) {
		down = state;
		last = state;
	}
	
}
