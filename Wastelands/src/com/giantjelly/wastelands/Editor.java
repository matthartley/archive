package com.giantjelly.wastelands;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import static org.lwjgl.opengl.GL11.*;

import com.giantjelly.wastelands.entity.Turret;
import com.giantjelly.wastelands.entity.Gun;
import com.giantjelly.wastelands.entity.BlasterPistol;
import com.giantjelly.wastelands.entity.PlasmaPistol;
import com.giantjelly.wastelands.entity.RocketLauncher;
import com.giantjelly.wastelands.gfx.Font;
import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.world.Block;
import com.giantjelly.wastelands.world.IO;
import com.giantjelly.wastelands.world.Triangle;
//import com.giantjelly.wastelands.gui.BlankButton;
import com.giantjelly.wastelands.gui.Button;
import static com.giantjelly.wastelands.Game.mode;
import com.giantjelly.wastelands.input.Input;

public class Editor {
	public static int[] current = Sprite.tiles[0];
	public static boolean TileMenu = false;
	public static int layer = 4;
	public static String Mode = "Block";
	public static boolean ModeSelection = false;
	public static int xscroll = 0;
	public static int yscroll = 0;
	public static String sheet = "Blocks";
	
	private static int flash = 0;
	private static int flashSpeed = 1;
	private static int flashLimit = 60;
	
	public static boolean mouseIn(int x, int y, int w, int h) {
		return (Mouse.getX() > x && Mouse.getX() < x+w && Display.getHeight()-Mouse.getY() > y && Display.getHeight()-Mouse.getY() < y+h) ? true : false;
	}
	
	public static Button tiles = new Button(256, 64, "Blocks");
	public static Button usables = new Button(256, 64, "Usables");
	public static Button pickups = new Button(256, 64, "Pickups");
	
	public static void tick() {
		if(Input.escape.downEvent()){
			Input.escape.setState(true);
			Game.mode = Game.Mode.EDITORMENU;
			IO.LoadMapNames();
		}
		
		flashLimit = 20;
		flash += flashSpeed;
		if(flash > flashLimit) flashSpeed = -1;
		if(flash < flashLimit/10) flashSpeed = 1;
		
		if(Keyboard.isKeyDown(Keyboard.KEY_W))
			yscroll += 32;
		if(Keyboard.isKeyDown(Keyboard.KEY_S))
			yscroll -= 32;
		if(Keyboard.isKeyDown(Keyboard.KEY_A))
			xscroll += 32;
		if(Keyboard.isKeyDown(Keyboard.KEY_D))
			xscroll -= 32;
		
		if(Keyboard.isKeyDown(Keyboard.KEY_1))
			layer = 1;
		if(Keyboard.isKeyDown(Keyboard.KEY_2))
			layer = 2;
		if(Keyboard.isKeyDown(Keyboard.KEY_3))
			layer = 3;
		if(Keyboard.isKeyDown(Keyboard.KEY_4))
			layer = 4;
		if(Keyboard.isKeyDown(Keyboard.KEY_5))
			layer = 5;
		if(Keyboard.isKeyDown(Keyboard.KEY_6))
			layer = 6;
		if(Keyboard.isKeyDown(Keyboard.KEY_7))
			layer = 7;
		
		if(Input.rightClick.downEvent()){
			if(!ModeSelection)
				ModeSelection = true;
			else
				ModeSelection = false;
		}
		if(Input.e.downEvent()){
			if(TileMenu == false)
				TileMenu = true;
			else
				TileMenu = false;
		}
		
		
		if(TileMenu){
			if(sheet == "Blocks"){
				for(int i = 0; i < Sprite.tiles.length; i++){
					if(mouseIn( ((Display.getWidth() / 2) - 512)+Sprite.tiles[i][0], 200+Sprite.tiles[i][1], 64/*Sprite.tiles[i][2]*/, 64/*Sprite.tiles[i][3]*/ )){
						if(Input.click.downEvent()){
							current = Sprite.tiles[i];
							TileMenu = false;
							if(Mode != "Block" && Mode != "Triangle0" && Mode != "Triangle1") Mode = "Block";
						}
					}
				}
			}
			else if(sheet == "Usables"){
				if(mouseIn( ((Display.getWidth() / 2) - 512), 200, 128, 64 )){
					if(Input.click.downEvent()){
						Mode = "Turret";
						TileMenu = false;
					}
				}
			}
			else if(sheet == "Pickups"){
				for(int i = 0; i < Sprite.pickups.length; i++){
					if(mouseIn( ((Display.getWidth() / 2) - 512)+Sprite.pickups[i][0], 200+Sprite.pickups[i][1]*4, Sprite.pickups[i][2]*4, Sprite.pickups[i][3]*4 )){
						if(Input.click.downEvent()){
							current = Sprite.pickups[i];
							Mode = "Pickup";
							TileMenu = false;
						}
					}
				}
			}
			
			if(tiles.isclicked()){
				sheet = "Blocks";
			}
			if(usables.isclicked()){
				sheet = "Usables";
			}
			/*if(pickups.isclicked()){
				sheet = "Pickups";
			}*/
		}
		else{
			if(Mode != "Delete" && !ModeSelection){
				if(Input.click.downEvent()){
					int x = Mouse.getX() - current[2] * 2;
					int y = (Display.getHeight() - Mouse.getY()) - current[3] * 2;
					x /= 32;
					y /= 32;
					x *= 32;
					y *= 32;
					
					Game.click.play();
					int a = 1;
					if(layer == 6) a = 2;
					if(layer == 7) a = 5;
					
					if(Mode == "Block")
						Game.Blocks.add(new Block(x - xscroll/a, y - yscroll/a, layer, current));
					
					if(Mode == "Triangle0")
						Game.Triangles.add(new Triangle(x - xscroll/a, y - yscroll/a, layer, 0, current));
					if(Mode == "Triangle1")
						Game.Triangles.add(new Triangle(x - xscroll/a, y - yscroll/a, layer, 1, current));
					if(Mode == "Turret")
						Game.turrets.add(new Turret((x + current[2] * 2) - 64 - xscroll, (y + current[3] * 2) - 16 - yscroll));
					if(Mode == "Pickup"){
						if(current[1]==0){
							Game.Guns.add(new BlasterPistol(x-xscroll, y-yscroll));
						}
						if(current[1]==8){
							Game.Guns.add(new PlasmaPistol(x-xscroll, y-yscroll));
						}
						if(current[1]==16){
							Game.Guns.add(new RocketLauncher(x-xscroll, y-yscroll));
						}
					}
					
					IO.save();
				}
			}
			
			if(Mode == "Delete" && !ModeSelection){
				for(int i = Game.Blocks.size() - 1; i > -1; i--){
					int a = 1;
					int l = Game.Blocks.get(i).layer;
					if(l==6) a=2; else if(l==7) a=5;
					
					if(Game.Blocks.get(i).layer==layer && mouseIn(Game.Blocks.get(i).x + xscroll/a, Game.Blocks.get(i).y + yscroll/a, Game.Blocks.get(i).coords[2]*4, Game.Blocks.get(i).coords[3]*4)){
						if(Input.click.downEvent()){
							Game.Blocks.remove(i);
							IO.save();
						}
					}
				}
				for(int i = Game.Triangles.size() -1; i > -1; i--){
					int a = 1;
					int l = Game.Triangles.get(i).layer;
					if(l==6) a=2; else if(l==7) a=5;
					
					if(Game.Triangles.get(i).layer==layer && mouseIn(Game.Triangles.get(i).x + xscroll/a, Game.Triangles.get(i).y + yscroll/a, Game.Triangles.get(i).coords[2]*4, Game.Triangles.get(i).coords[3]*4)){
						if(Input.click.downEvent()){
							Game.Triangles.remove(i);
							IO.save();
						}
					}
				}
				for(int i = Game.turrets.size() -1; i > -1; i--){
					if(mouseIn(Game.turrets.get(i).x + xscroll, Game.turrets.get(i).y + yscroll, 256, 128)){
						if(Input.click.downEvent()){
							Game.turrets.remove(i);
							IO.save();
						}
					}
				}
				for(int i = Game.Guns.size() -1; i > -1; i--){
					if(mouseIn(Game.Guns.get(i).x + xscroll, Game.Guns.get(i).y + yscroll, 64, 32)){
						if(Input.click.downEvent()){
							Game.Guns.remove(i);
							IO.save();
						}
					}
				}
			}
			
			
			if(ModeSelection){ //RADIAL SELECT MENU
				if(Keyboard.isKeyDown(Keyboard.KEY_UP)){
					Mode = "Block";
					ModeSelection = false;
				}
				if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
					Mode = "Triangle0";
					ModeSelection = false;
				}
				if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)){
					Mode = "Triangle1";
					ModeSelection = false;
				}
				if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)){
					Mode = "Delete";
					ModeSelection = false;
				}
			}
		}
	}
	
	public static void render(){
		String TheLayer = "Collsion";
		if(layer == 1)
			TheLayer = "CloseFG";
		if(layer == 2)
			TheLayer = "FG";
		if(layer == 3)
			TheLayer = "FarFG";
		if(layer == 4)
			TheLayer = "Collision";
		if(layer == 5)
			TheLayer = "CloseBG";
		if(layer == 6)
			TheLayer = "BG";
		if(layer == 7)
			TheLayer = "FarBG";
		Font.font.Draw(10, 50, "Layer: " + layer + " - " + TheLayer);
		
		glEnable(GL_DEPTH_TEST);
			for(int i = Game.Blocks.size() - 1; i > -1; i--){
				int a = 1;
				if(Game.Blocks.get(i).layer == 6) a = 2;
				if(Game.Blocks.get(i).layer == 7) a = 5;
				
				if(layer == Game.Blocks.get(i).layer)
					glColor4f(1, 1, 1, 1);
				else
					glColor4f(1, 1, 1, 0.5f);
				
				Game.Blocks.get(i).draw(xscroll/a, yscroll/a);
			}
			for(int i = Game.Triangles.size() - 1; i > -1; i--){
				int a = 1;
				if(Game.Triangles.get(i).layer == 6) a = 2;
				if(Game.Triangles.get(i).layer == 7) a = 5;
				
				if(layer == Game.Triangles.get(i).layer)
					glColor4f(1, 1, 1, 1);
				else
					glColor4f(1, 1, 1, 0.5f);
				
				Game.Triangles.get(i).draw(xscroll/a, yscroll/a);
			}
			
			for(Turret u : Game.turrets){
				Sprite.draw(Sprite.Usables, new int[]{ 0, 0, 128, 64 }, new float[]{ u.x +xscroll, u.y+yscroll, 0, 2 });
			}
			for(Gun p : Game.Guns){
				Sprite.draw(Sprite.Pickups, p.coords, new float[]{ p.x + xscroll, p.y + yscroll, 4, 4 });
			}
		glDisable(GL_DEPTH_TEST);
		
		Font.fancy.Draw(Display.getWidth()/2 + xscroll, Display.getHeight()/2 + yscroll, "|Spawn|");
		
		if(TileMenu){
			glDisable(GL_TEXTURE_2D);
			glColor4d(172.0/255, 150.0/255, 136.0/255, 1);
			glBegin(GL_QUADS);
			glVertex3f(0, 0, 1);
			glVertex3f(Display.getWidth(), 0, 1);
			glVertex3f(Display.getWidth(), Display.getHeight(), 1);
			glVertex3f(0, Display.getHeight(), 1);
			glEnd();
			glEnable(GL_TEXTURE_2D);
			
			glColor4d(1, 1, 1, 1);
			Sprite.draw(Sprite.random, new int[]{ 0, 0, 64, 64 }, new float[]{ 0, 0, 0, 4 });
			Sprite.draw(Sprite.random, new int[]{ 64, 0, 64, 64 }, new float[]{ Display.getWidth()-256, 0, 0, 4 });
			Sprite.draw(Sprite.random, new int[]{ 0, 64, 64, 64 }, new float[]{ 0, Display.getHeight()-256, 0, 4 });
			Sprite.draw(Sprite.random, new int[]{ 64, 64, 64, 64 }, new float[]{ Display.getWidth()-256, Display.getHeight()-256, 0, 4 });
			
			if(sheet == "Blocks"){
				for(int i = 0; i < Sprite.tiles.length; i++){
					glColor4f(1, 1, 1, 1);
					Sprite.draw(Sprite.TileSheet, Sprite.tiles[i], new float[]{ ((Display.getWidth() / 2) - 512)+Sprite.tiles[i][0], 200+Sprite.tiles[i][1], 1, 1 });
					if(mouseIn( ((Display.getWidth() / 2) - 512)+Sprite.tiles[i][0], 200+Sprite.tiles[i][1], 64/*Sprite.tiles[i][2]*/, 64/*Sprite.tiles[i][3]*/ )){
						glColor4d(1, 1, 1, 0.5);
						Sprite.draw(Sprite.highlights, new int[]{ 0, 64, 64, 64 }, new float[]{ ((Display.getWidth() / 2) - 512)+Sprite.tiles[i][0], 200+Sprite.tiles[i][1], 0, 1 });
					}
				}
			}else if(sheet == "Usables"){
				glColor4f(1, 1, 1, 1);
				Sprite.draw(Sprite.Usables, new int[]{ 0, 0, 128, 64 }, new float[]{ (Display.getWidth() / 2) - 512, 200, 1, 1 });
				if(mouseIn( (Display.getWidth() / 2) - 512, 200, 128, 64 )){
					glColor4d(1, 1, 1, 0.5);
					Sprite.draw(Sprite.highlights, new int[]{ 0, 64, 128, 64 }, new float[]{ ((Display.getWidth() / 2) - 512), 200, 0, 1 });
				}
			}else
				if(sheet == "Pickups"){
					for(int i = 0; i < Sprite.pickups.length; i++){
						glColor4f(1, 1, 1, 1);
						Sprite.draw(Sprite.Pickups, Sprite.pickups[i], new float[]{ ((Display.getWidth() / 2) - 512)+Sprite.pickups[i][0], 200+Sprite.pickups[i][1]*4, 1, 4 });
						if(mouseIn( ((Display.getWidth() / 2) - 512)+Sprite.pickups[i][0], 200+Sprite.pickups[i][1]*4, 64, 32 )){
							glColor4d(1, 1, 1, 0.5);
							Sprite.draw(Sprite.highlights, new int[]{ 0, 64, 64, 32 }, new float[]{ ((Display.getWidth() / 2) - 512)+Sprite.pickups[i][0], 200+Sprite.pickups[i][1]*4, 0, 1 });
						}
					}
				}
			
			tiles.render((Display.getWidth() / 2) - 512, 100);
			usables.render((Display.getWidth() / 2) - 128, 100);
			pickups.render((Display.getWidth() / 2) + 256, 100);
			
		}else{
			if(Mode != "Delete" && !ModeSelection){
				int x = Mouse.getX() - current[2] * 2;
				int y = (Display.getHeight() - Mouse.getY()) - current[3] * 2;
				x /= 32;
				y /= 32;
				x *= 32;
				y *= 32;
				
				if(Mode == "Block")
					Sprite.draw(Sprite.TileSheet, current, new float[]{ x, y, 0, 4 });
				
				if(Mode == "Triangle0")
					Sprite.draw(Sprite.TileSheet, current, new float[]{ x, y, 0, 4 }, 0);
				
				if(Mode == "Triangle1")
					Sprite.draw(Sprite.TileSheet, current, new float[]{ x, y, 0, 4 }, 1);
				
				if(Mode == "Turret"){
					Sprite.draw(Sprite.Usables, new int[]{ 0, 0, 128, 64 }, new float[]{ (x + current[2] * 2) - 64, (y + current[3] * 2) - 16, 0, 2 });
				}
				if(Mode == "Pickup")
					Sprite.draw(Sprite.Pickups, current, new float[]{ x, y, 0, 4 });
			}
			
			if(Mode == "Delete" && !ModeSelection){
				for(int i = Game.Blocks.size() - 1; i > -1; i--){
					int a = 1;
					if(Game.Blocks.get(i).layer == 6) a = 2;
					if(Game.Blocks.get(i).layer == 7) a = 5;
					
					if(Game.Blocks.get(i).layer == layer && mouseIn( Game.Blocks.get(i).x + xscroll/a, Game.Blocks.get(i).y + yscroll/a,
							Game.Blocks.get(i).coords[2]*4, Game.Blocks.get(i).coords[3]*4 )){
						glColor4d(1, 1, 1, ((double)flash/(double)flashLimit)*0.5);
						Sprite.draw(Sprite.highlights,
						new int[]{ 0, 0, Game.Blocks.get(i).coords[2], Game.Blocks.get(i).coords[3] },
						new float[]{ Game.Blocks.get(i).x + xscroll/a, Game.Blocks.get(i).y + yscroll/a, 1, 4 });
					}
				}
				for(int i = Game.Triangles.size() -1; i > -1; i--){
					int a = 1;
					if(Game.Triangles.get(i).layer == 6) a = 2;
					if(Game.Triangles.get(i).layer == 7) a = 5;
					
					if(Game.Triangles.get(i).layer==layer && mouseIn( Game.Triangles.get(i).x + (xscroll/a), Game.Triangles.get(i).y + (yscroll/a),
					Game.Triangles.get(i).coords[2]*4, Game.Triangles.get(i).coords[3]*4 )){
						glColor4d(1, 1, 1, ((double)flash/(double)flashLimit)*0.5);
						Sprite.draw(Sprite.highlights,
						new int[]{ 0, 0, Game.Triangles.get(i).coords[2], Game.Triangles.get(i).coords[3] },
						new float[]{ Game.Triangles.get(i).x + xscroll/a, Game.Triangles.get(i).y + yscroll/a, 1, 4 }, Game.Triangles.get(i).side);
					}
				}
				for(int i = Game.turrets.size() -1; i > -1; i--){
					if(mouseIn( Game.turrets.get(i).x + xscroll, Game.turrets.get(i).y + yscroll, 256, 128 )){
						glColor4d(1, 1, 1, ((double)flash/(double)flashLimit)*0.5);
						Sprite.draw(Sprite.highlights,
						new int[]{ 0, 0, 64, 32 },
						new float[]{ Game.turrets.get(i).x + xscroll, Game.turrets.get(i).y + yscroll, 1, 4 });
					}
				}
				for(int i = Game.Guns.size() -1; i > -1; i--){
					if(mouseIn( Game.Guns.get(i).x + xscroll, Game.Guns.get(i).y + yscroll, 64, 32 )){
						glColor4d(1, 1, 1, ((double)flash/(double)flashLimit)*0.5);
						Sprite.draw(Sprite.highlights,
						new int[]{ 0, 0, 64, 32 },
						new float[]{ Game.Guns.get(i).x + xscroll, Game.Guns.get(i).y + yscroll, 1, 1 });
					}
				}
			}
			
			if(ModeSelection && !TileMenu){
				glColor4d(1, 1, 1, 0.5);
				Sprite.draw(Sprite.random, new int[]{ 0, 128, 512, 512 }, new float[]{ Display.getWidth() / 2 - 256, Display.getHeight() / 2 - 256, 0, 1 });
				
				int x = (Display.getWidth() / 2 -64);
				int y = (Display.getHeight() / 2 -64);
				
				glColor4d(1, 1, 1, 0.75);
				Sprite.draw(Sprite.TileSheet, new int[]{ 0, 0, 64, 64 }, new float[]{ x, 		y-128-16, 0, 2 });
				Sprite.draw(Sprite.TileSheet, new int[]{ 0, 0, 64, 64 }, new float[]{ x-128-16, y, 0, 2 }, 0);
				Sprite.draw(Sprite.TileSheet, new int[]{ 0, 0, 64, 64 }, new float[]{ x+128+16, y, 0, 2 }, 1);
				Sprite.draw(Sprite.random, new int[]{ 256, 0, 128, 128 }, new float[]{ x, 		y+128+16, 0, 1 });
				
				Font.font.Draw(Display.getWidth() / 2 - 64, Display.getHeight() / 2 - 256, "up arrow");
				Font.font.Draw(Display.getWidth() / 2 - (256+128), Display.getHeight() / 2 - 0, "left arrow");
				Font.font.Draw(Display.getWidth() / 2 + 256, Display.getHeight() / 2 - 0, "right arrow");
				Font.font.Draw(Display.getWidth() / 2 - 64, Display.getHeight() / 2 + 256, "down arrow");
			}
		}
			
	}
}
