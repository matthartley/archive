package com.giantjelly.wastelands.sound;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.AL.*;

public class Source {
	
	public IntBuffer source = BufferUtils.createIntBuffer(1);
	public FloatBuffer position = BufferUtils.createFloatBuffer(3).put(new float[]{ 0, 0, 0 });
	public FloatBuffer velocity = BufferUtils.createFloatBuffer(3).put(new float[]{ 0, 0, 0 });
	public float volume;
	
	public Source(Sound sound, float v) {
		position.flip();
		velocity.flip();
		
		alGenSources(source);
		
		if(alGetError() != AL_NO_ERROR)
			System.out.println("Error generating source");
		
		volume = v;
		bindsound(sound.buffer);
	}
	
	public void bindsound(IntBuffer buffer) {
		alSourcei(source.get(0), AL_BUFFER, buffer.get(0));
		alSourcef(source.get(0), AL_PITCH, 1);
		alSourcef(source.get(0), AL_GAIN, volume);
		alSource(source.get(0), AL_POSITION, position);
		alSource(source.get(0), AL_VELOCITY, velocity);
		
		if(alGetError() != AL_NO_ERROR)
			System.out.println("Error binding buffer");
	}
	
	public void play() {
		alSourcePlay(source.get(0));
	}
	
}
