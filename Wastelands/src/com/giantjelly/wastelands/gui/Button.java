package com.giantjelly.wastelands.gui;

import org.lwjgl.input.Mouse;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.opengl.Display;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.input.Input;
import com.giantjelly.wastelands.gfx.Font;
import com.giantjelly.wastelands.gfx.Sprite;

public class Button {
	
	public int lastx=0;
	public int lasty=0;
	protected int w;
	protected int h;
	public String name = "";
	public boolean sound = true;
	
	public Button(int w, int h, String name) {
		this.w = w;
		this.h = h;
		this.name = name;
	}
	
	public Button(int w, int h) {
		this.w = w;
		this.h = h;
	}
	
	public void highlight(int x, int y) {
		Sprite.draw(Sprite.Buttons, new int[]{ 64, 16, 64, 16 }, new float[]{ x, y, 0, 4 });
	}
	
	public void render(int x, int y) {
		lastx=x;
		lasty=y;
		draw(x, y);
		
		if(
				Mouse.getX() > x && Mouse.getX() < x + this.w &&
				Display.getHeight() - Mouse.getY() > y && Display.getHeight() - Mouse.getY() < y + this.h
		){
			highlight(x, y);
		}
		
		Font.fancy.Draw(x+8, y+8, this.name);
	}
	
	public boolean isclicked(){
		if(
				Mouse.getX() > lastx && Mouse.getX() < lastx + this.w &&
				Display.getHeight() - Mouse.getY() > lasty && Display.getHeight() - Mouse.getY() < lasty + this.h &&
				Input.menuClick.downEvent()
		){
			Game.click.play();
			//Input.click.setState(true);
			return true;
		}else
			return false;
	}
	
	public boolean ishover() {
		if(
				Mouse.getX() > lastx && Mouse.getX() < lastx + this.w &&
				Display.getHeight() - Mouse.getY() > lasty && Display.getHeight() - Mouse.getY() < lasty + this.h
		){
			return true;
		}else
			return false;
	}
	
	private void draw(int x, int y) {
		glEnable(GL_TEXTURE_2D);
		glColor4f(1, 1, 1, 1);
		Sprite.draw(Sprite.Buttons, new int[]{ 0, 16, 64, 16 }, new float[]{ x, y, 0, 4 });
	}
}
