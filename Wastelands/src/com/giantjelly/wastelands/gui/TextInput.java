package com.giantjelly.wastelands.gui;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glEnable;

import org.lwjgl.input.Mouse;
import org.lwjgl.input.Keyboard;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.input.Input;
import com.giantjelly.wastelands.gfx.Font;
import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.gui.menu.Login;

public class TextInput extends Button {
	public boolean active = false;
	public String text = "";
	private boolean password = false;
	
	public TextInput(int w, int h, boolean pass) {
		super(w, h);
		password = pass;
	}
	
	public TextInput(int w, int h, boolean pass, String t) {
		super(w, h);
		password = pass;
		text = t;
	}
	
	public void render(int x, int y) {
		super.render(x, y);
		
		String t;
		if(password)
			t = hide(this.text);
		else
			t = this.text;
		
		Font.fancy.Draw(x+8, y+8, t);
		
		if(active && Game.cursor)
			Font.fancy.Draw(x+8, y+8, t+"|");
	}
	
	public void tick() {
		if(isclicked()){
			active = true;
		}
		if(!isclicked()){
			if(Input.menuClick.downEvent()){
				active = false;
			}
		}
	}
	
	private void draw(int x, int y) {
		glEnable(GL_TEXTURE_2D);
		glColor4f(1, 1, 1, 1);
		Sprite.draw(Sprite.Buttons, new int[]{ 64, 0, 64, 16 }, new float[]{ x, y, 0, 4 });
	}
	
	public static String hide(String text) {
		String NEW = "";
		
		for(int i = 0; i < text.length(); i++){
			NEW += "*";
		}
		
		return NEW;
	}

}
