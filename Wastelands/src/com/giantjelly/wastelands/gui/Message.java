package com.giantjelly.wastelands.gui;

import java.io.Serializable;

public class Message implements Serializable {
	
	public String text;
	public int cooldown = 240;
	public boolean active = true;
	
	public Message() {
	}
	
	public Message(String s) {
		text = s;
	}
	
	public void update() {
		if(active){
			cooldown--;
			if(cooldown <= 0) active = false;
		}
	}
	
}
