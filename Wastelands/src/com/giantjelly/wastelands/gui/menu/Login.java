package com.giantjelly.wastelands.gui.menu;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glEnable;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.gfx.Font;
import com.giantjelly.wastelands.gui.Button;
import com.giantjelly.wastelands.gui.TextInput;
import com.giantjelly.wastelands.input.Input;
import com.giantjelly.wastelands.networking.Sql;
import static com.giantjelly.wastelands.Game.mode;

public class Login extends Menu {
	
	public static TextInput username = new TextInput(256, 64, false);
	public static TextInput password = new TextInput(256, 64, true);
	static Button LOGIN = new Button(256, 64, "Login");
	static String usernametype = "";
	static String typemode = "";
	public static ArrayList<String> messages = new ArrayList<String>();
	public static boolean tabbing = false;
	
	public static void render() {
		if(Keyboard.isKeyDown(Keyboard.KEY_TAB) && tabbing==false){
			tabbing = true;
			
			if(!username.active && !password.active){
				username.active = true;
				password.active = false;
			}else
			if(username.active && !password.active){
				username.active = false;
				password.active = true;
			}else
			if(!username.active && password.active){
				username.active = true;
				password.active = false;
			}
		}
		if(!Keyboard.isKeyDown(Keyboard.KEY_TAB)) tabbing = false;
		if(Input.enter.downEvent()){
			login();
		}
		
		Font.fancy.Draw(10, 150, "Username:");
		Font.fancy.Draw(10, 275, "Password:");
		username.render(10, 200);
		password.render(10, 320);
		LOGIN.render(110, 450);
		
		if(messages.size() > 0){
			glEnable(GL_TEXTURE_2D);
			Font.fancy.Draw(300, 250, messages.get(messages.size()-1));
		}
	}
	
	public static void tick() {
		username.tick();
		password.tick();
		
		if(LOGIN.isclicked()){
			login();
		}
	}
	
	public static void login() {
		messages.add("Logging in...");
		
		new Thread(){
			public void run(){
				if(Sql.login(username.text, password.text)){
					mode = Game.Mode.MENU;
				}
			}
		}.start();
	}
	
}
