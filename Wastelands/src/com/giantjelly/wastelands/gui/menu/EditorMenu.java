package com.giantjelly.wastelands.gui.menu;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import com.giantjelly.wastelands.gui.Button;
import com.giantjelly.wastelands.gui.TextInput;
import com.giantjelly.wastelands.world.IO;
import static com.giantjelly.wastelands.Game.mode;
import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.input.Input;

public class EditorMenu extends Menu {
	
	private static Button newmap = new Button(256, 64, "Create new map");
	public static TextInput NEW = new TextInput(256, 64, false);
	
	public static void tick(){
		for(int i = 0; i < IO.maps.length; i++){
			if(IO.maps[i].isclicked()){
				IO.CurrentMap = IO.maps[i].name;
				IO.clear();
				IO.load();
				mode = Game.Mode.EDITOR;
			}
		}
		
		if(newmap.isclicked() && !NEW.text.equals("")){
			IO.CurrentMap = NEW.text + ".dat";
			mode = Game.Mode.EDITOR;
			IO.clear();
		}
		
		NEW.tick();
		
		if(Input.escape.downEvent()){
			mode = Game.Mode.MENU;
		}
	}
	
	public static void render() {
		for(int i = 0; i < IO.maps.length; i++){
			IO.maps[i].render(Display.getWidth()/2-(256+32), 100 + (i*64));
		}
		
		newmap.render(Display.getWidth()/2+32, 200);
		NEW.render(Display.getWidth()/2+32, 100);
	}
	
}
