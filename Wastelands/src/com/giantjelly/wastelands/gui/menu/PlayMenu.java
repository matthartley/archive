package com.giantjelly.wastelands.gui.menu;

import static com.giantjelly.wastelands.Game.mode;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.gui.Message;
import com.giantjelly.wastelands.input.Input;
import com.giantjelly.wastelands.networking.GameServer;
import com.giantjelly.wastelands.world.IO;

public class PlayMenu extends Menu {

	public static void tick() {
		for(int i = 0; i < IO.maps.length; i++){
			if(IO.maps[i].isclicked()){
				if(GameServer.start()){
					mode = Game.Mode.PLAY;
					IO.CurrentMap = IO.maps[i].name;
					IO.clear();
					IO.load();
				}else{
					Game.log("The server failed to start");
				}
			}
		}
		
		if(Input.escape.downEvent()){
			mode = Game.Mode.MENU;
		}
	}

	public static void render() {
		for(int i = 0; i < IO.maps.length; i++){
			IO.maps[i].render(Display.getWidth()/2 - 128, 100 + (i*64));
		}
		
		while(Keyboard.next()){
		}
	}

}
