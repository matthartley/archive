package com.giantjelly.wastelands.gui.menu;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.input.Input;
import com.giantjelly.wastelands.networking.GameClient;
import com.giantjelly.wastelands.gfx.Font;
import com.giantjelly.wastelands.gui.Button;
import com.giantjelly.wastelands.gui.TextInput;

import static com.giantjelly.wastelands.Game.mode;

public class MultiplayerJoinMenu extends Menu {
	
	public static TextInput ip = new TextInput(256, 64, false);
	static Button go = new Button(256, 64, "Go");
	public static String msg = "";
	
	public static void render() {
		ip.render(Display.getWidth()/2-(256+32), 100);
		go.render(Display.getWidth()/2-(256+32), 200);
		
		Font.error.Draw(Display.getWidth()/2+32, 200, msg);
		Font.fancy.Draw(Display.getWidth()/2+32, 50, "Recent Server");
		Font.fancy.Draw(Display.getWidth()/2-(256+32), 50, "ip address");
		
		lastserver.render(Display.getWidth()/2+32, 100);
	}
	
	public static void tick() {
		if(go.isclicked()){
			login(ip.text);
		}
		
		if(lastserver.isclicked()){
			login(lastserver.name);
		}
		
		ip.tick();
		
		if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)){
			login(ip.text);
		}
		
		if(Input.escape.downEvent()){
			mode = Game.Mode.MENU;
		}
	}
	
	public static void login(String ip) {
		msg = "Connecting...";
		addserver(ip);
		try {
			GameClient.login(InetAddress.getByName(ip));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	public static Button lastserver = new Button(256, 64, "");
	
	public static void loadservers() {
		try{
			
			DataInputStream dis = new DataInputStream(new FileInputStream(new File("res/server.dat")));
			lastserver = new Button(256, 64, dis.readUTF());
			dis.close();
			
		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	public static void addserver(String server) {
		try {
			
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(new File("res/server.dat")));
			dos.writeUTF(server);
			lastserver = new Button(256, 64, server);
			dos.close();
			
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
	}
	
}
