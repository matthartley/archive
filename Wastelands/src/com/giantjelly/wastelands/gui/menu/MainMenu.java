package com.giantjelly.wastelands.gui.menu;

import java.util.Random;

import org.lwjgl.opengl.Display;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.gui.Button;
import com.giantjelly.wastelands.world.IO;
import static com.giantjelly.wastelands.Game.mode;

import static org.lwjgl.opengl.GL11.*;

public class MainMenu extends Menu {

	static Button host = new Button(256, 64, "Host Game");
	static Button editor = new Button(256, 64, "Map Editor");
	static Button exit = new Button(256, 64, "Exit");
	static Button join = new Button(256, 64, "Join Game");
	
	public static Random rand = new Random();
	
	public static void render() {
		glColor4d(1, 1, 1, 1);
		rand.setSeed(Game.randomNumber);
		
		int blockSize = 64;
		for(int y = 0; y < Display.getHeight(); y+=blockSize){
			for(int x = 0; x < Display.getWidth(); x+=blockSize){
				if(rand.nextInt(10)==0){
					glColor4d(1, 1, 1, 1);
					Game.drawsquare(x, y, 64, 64);
					
					glColor4d(rand.nextDouble(), rand.nextDouble(), rand.nextDouble(), 0.5);
				}
				else glColor4d(1, 1, 1, 1);
				
				Sprite.draw(Sprite.TileSheet, new int[]{ 0, 0, 64, 64 }, new float[]{ x, y, 0, (float)blockSize/64f });
			}
		}
		
		glColor4d(1, 1, 1, 1);
		Sprite.draw(Sprite.terminalTitle, new int[]{0, 0, 112, 16}, new float[]{ (Display.getWidth()/2)-(4*112), 50, 0, 8 });
		host.			render(Display.getWidth()/2 - 128, 200);
		join.			render(Display.getWidth()/2 - 128, 300);
		editor.			render(Display.getWidth()/2 - 128, 400);
		exit.			render(Display.getWidth()/2 - 128, 500);
	}
	
	public static void tick(){		
		if(host.isclicked()){
			mode = Game.Mode.PLAYMENU;
			IO.LoadMapNames();
		}
		
		if(editor.isclicked()){
			mode = Game.Mode.EDITORMENU;
			IO.LoadMapNames();
		}
		
		if(exit.isclicked())
			Game.game.running = false;
		
		if(join.isclicked()){
			mode = Game.Mode.MULTIPLAYERJOIN;
			MultiplayerJoinMenu.msg = "";
			MultiplayerJoinMenu.loadservers();
		}
	}

}
