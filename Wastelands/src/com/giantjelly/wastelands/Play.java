package com.giantjelly.wastelands;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import java.util.ArrayList;

import com.giantjelly.wastelands.entity.Animation;
import com.giantjelly.wastelands.entity.Gun;
import com.giantjelly.wastelands.entity.Player;
import com.giantjelly.wastelands.entity.Turret;
import com.giantjelly.wastelands.gfx.Font;
import com.giantjelly.wastelands.gui.Button;
import com.giantjelly.wastelands.gui.Message;
import com.giantjelly.wastelands.gui.menu.MultiplayerJoinMenu;
import com.giantjelly.wastelands.input.Input;
import com.giantjelly.wastelands.networking.GameClient;
import com.giantjelly.wastelands.networking.GameServer;
//import com.giantjelly.wastelands.networking.Client;
//import com.giantjelly.wastelands.networking.ClientCommand;
//import com.giantjelly.wastelands.networking.Net;
import com.giantjelly.wastelands.networking.NetPlayer;
//import com.giantjelly.wastelands.networking.Server;
import com.giantjelly.wastelands.entity.Bullet;

public class Play {
	public static Player player = new Player(128, -64);
	public static ArrayList<Bullet> Bullets = new ArrayList<Bullet>();
	
	public static boolean paused = false;
	
	public static ArrayList<NetPlayer> players = new ArrayList<NetPlayer>();
	
	public static String gunPickedUp = null;
	public static int health = 10;
	public static int kills = 0;
	public static int deadmsgcooldown = 0;
	public static boolean dead = false;
	public static int respawntime = 0;
	
	public static NetPlayer[] serverPlayers = new NetPlayer[0];
	public static Gun[] serverGuns = new Gun[0];
	public static Turret[] serverTurrets = new Turret[0];
	public static Bullet[] serverBullets = new Bullet[0];
	public static Animation[] serverAnimations = new Animation[0];
	public static Message[] serverMessages = new Message[0];
	//public static boolean serverRestart = false;
	
	public static void render(){
		glEnable(GL_DEPTH_TEST);
		Sky.draw();
		
		glColor4f(1, 1, 1, 1);
		for(int i = Game.Blocks.size() - 1; i > -1; i--){
			int a = 1;
			if(Game.Blocks.get(i).layer == 6) a = 2;
			if(Game.Blocks.get(i).layer == 7) a = 5;
			Game.Blocks.get(i).draw(player.xscroll/a, player.yscroll/a);
		}
		for(int i = Game.Triangles.size() - 1; i > -1; i--){
			int a = 1;
			if(Game.Triangles.get(i).layer == 6) a = 2;
			if(Game.Triangles.get(i).layer == 7) a = 5;
			Game.Triangles.get(i).draw(player.xscroll/a, player.yscroll/a);
		}
			
		if(GameServer.running){
			for(Turret t : Game.turrets){
				t.draw();
			}
			for(int i = 0; i < Game.Guns.size(); i++){
				Game.Guns.get(i).draw();
			}
			
			for(int i = 0; i < players.size(); i++){
				players.get(i).draw();
				Font.font.Draw(players.get(i).packet.x+Play.player.xscroll+16, players.get(i).packet.y+Play.player.yscroll-32, players.get(i).packet.username);
			}
			
			for(int i = 0; i < Bullets.size(); i++){
				Bullets.get(i).render();
			}
			for(Animation a : Game.animations){
				a.draw();
			}
		}
			
		if(GameClient.running){
			for(int i = 0; i < serverGuns.length; i++){
				serverGuns[i].draw();
			}
			for(int i = 0; i < serverTurrets.length; i++){
				serverTurrets[i].draw();
			}
			
			for(int i = 0; i < serverPlayers.length; i++){
				if(!serverPlayers[i].packet.username.equals(Game.username)){
					serverPlayers[i].draw();
					Font.font.Draw(serverPlayers[i].packet.x+Play.player.xscroll+16, serverPlayers[i].packet.y+Play.player.yscroll-32, serverPlayers[i].packet.username);
				}
			}
			
			for(int i = 0; i < serverBullets.length; i++){
				serverBullets[i].render();
			}
			for(int i = 0; i < serverAnimations.length; i++){
				serverAnimations[i].draw();
			}
		}
		
		
		if(Play.player.firstWeapon != null && Play.player.firstWeapon.shootcount > 15)
			Font.font.Draw(Play.player.x+16, Play.player.y-32, "Reloading...");
		
		if(!dead && Game.matchRunning) player.draw();
			
		
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_2D);
		
		/*glColor4f(1, 1, 1, 1);
		Sprite.draw(Sprite.Gui, new int[]{ 0, 0, 8, 64 }, new float[]{ 0, 100, 0, 8 });
		for(int i = 0; i < 8; i++){
			glColor4f(1, 1, 1, 1);
			if(player.gunindex==i){
				Sprite.draw(Sprite.Gui, new int[]{ 8, 0, 8, 8 }, new float[]{ 64, 100+i*64, 0, 8 });
			}
			if(Inventory[i] != null){
				//Inventory[i].s.draw(0, 100+i*64, 0, 4);
				glColor4f(1, 1, 1, 1);
				
				Sprite.draw(Sprite.Pickups, Inventory[i].coords, new float[]{ 8, 108+i*64, 0, 4 });
				for(int v = 0; v < Inventory[i].ammo; v++){
					drawbullet(8+(3*v), (164-16)+i*64);
				}
			}
		}*/
		
		if(dead){
			Font.font.Draw(Display.getWidth()/2, Display.getHeight()/2, "Respawning in... " + respawntime);
		}
		
		boolean endScreen = (GameServer.running || GameClient.running) && !Game.matchRunning;
		
		if(Keyboard.isKeyDown(Keyboard.KEY_TAB) && !endScreen){
			glDisable(GL_TEXTURE_2D);
			glColor4d(0, 0, 0, 0.8);
			glBegin(GL_QUADS);
			{
				glVertex3d(0, 0, 0);
				glVertex3d(Display.getWidth(), 0, 0);
				glVertex3d(Display.getWidth(), Display.getHeight(), 0);
				glVertex3d(0, Display.getHeight(), 0);
			}
			glEnd();
			
			
			glColor4d(0.7, 0.7, 0.7, 0.3);
			int menux = (Display.getWidth()/2)-128;
			int menuy = (Display.getHeight()/2)-256;
			glBegin(GL_QUADS);
			{
				glVertex3d(menux-16, menuy-16, 0);
				glVertex3d(menux-16+(256+32), menuy-16, 0);
				glVertex3d(menux-16+(256+32), menuy+512+32, 0);
				glVertex3d(menux-16, menuy+512+32, 0);
			}
			glEnd();
			
			Font.font.Draw(menux-8, menuy-0, "Scores");
			if(GameServer.running){
				Font.font.Draw(menux-8, menuy+32, Game.username + " - " + Play.kills);
				for(int i = 0; i < players.size(); i++){
					Font.font.Draw(menux-8, menuy+64 + (32*i), players.get(i).packet.username + " - " + players.get(i).kills);
				}
			}
			if(GameClient.running){
				for(int i = 0; i < serverPlayers.length; i++){
					Font.font.Draw(menux-8, menuy+32 + (32*i), serverPlayers[i].packet.username + " - " + serverPlayers[i].kills);
				}
			}
		}
		
		if(endScreen){
			glDisable(GL_TEXTURE_2D);
			glColor4d(0, 0, 0, 0.8);
			glBegin(GL_QUADS);
			{
				glVertex3d(0, 0, 0);
				glVertex3d(Display.getWidth(), 0, 0);
				glVertex3d(Display.getWidth(), Display.getHeight(), 0);
				glVertex3d(0, Display.getHeight(), 0);
			}
			glEnd();
			
			int menux = (Display.getWidth()/2)-128;
			int menuy = (Display.getHeight()/2)-256;
			
			//String winner = GameServer.running ? GameServer.matchWinner : (GameClient.running ? GameClient.serverMatchWinner : "No winner found");
			String winner = Game.matchWinner;
			Font.font.Draw(menux-8, menuy-0, winner + " won!");
		}
		
		if(paused){
			glDisable(GL_TEXTURE_2D);
			glColor4d(0, 0, 0, 0.8);
			glBegin(GL_QUADS);
			{
				glVertex3d(0, 0, 0);
				glVertex3d(Display.getWidth(), 0, 0);
				glVertex3d(Display.getWidth(), Display.getHeight(), 0);
				glVertex3d(0, Display.getHeight(), 0);
			}
			glEnd();
			
			glColor4d(0.7, 0.7, 0.7, 0.3);
			int menux = (Display.getWidth()/2)-128;
			int menuy = (Display.getHeight()/2)-256;
			glBegin(GL_QUADS);
			{
				glVertex3d(menux-16, menuy-16, 0);
				glVertex3d(menux-16+(256+32), menuy-16, 0);
				glVertex3d(menux-16+(256+32), menuy+512+32, 0);
				glVertex3d(menux-16, menuy+512+32, 0);
			}
			glEnd();
			
			resume.render(menux, menuy);	
		
			if(GameServer.running){
				mainmenu.name = "End Game";
			}else if(GameClient.running){
				mainmenu.name = "Disconnect";
			}
			
			mainmenu.render(menux, menuy + 96);
		}
	}
	
	public static void tick() {
		if(Play.paused){
			if(Play.resume.isclicked())
				Play.paused = false;
			
			if(Play.mainmenu.isclicked()){
				Game.reset();
				
				/*if( !Server.running && !Client.running ){
					Game.mode = Game.Mode.MENU;
				}*/
				if(GameServer.running){
					Game.mode = Game.Mode.MENU;
					GameServer.stop();
				}
				if(GameClient.running){
					Game.mode = Game.Mode.MULTIPLAYERJOIN;
					GameClient.stop();
					MultiplayerJoinMenu.msg = "";
				}
			}
		}
		
		Sky.update();
		if(Game.matchRunning) Play.player.update();
		
		if(GameServer.running) ServerTick.tick();
		if(GameClient.running) ClientTick.tick();
		
		if(Play.deadmsgcooldown>0){
			Play.deadmsgcooldown--;
		}
		
		if(Play.respawntime <= 0 && Play.dead){
			Game.reset();
			if(GameClient.running) GameClient.respawn();
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_BACK)){
			Play.player.turretIndex = -1;
		}
		
		if(Input.x.downEvent() && Play.player.turretIndex != -1){
			Play.player.turretIndex = -1;
		}
	}
	
	public static void drawbullet(int x, int y) {
		glDisable(GL_TEXTURE_2D);
		glColor4d(1, 1, 0, 1);
		
		glPushMatrix();
		glTranslatef(x, y, 0);
		
		glBegin(GL_QUADS);
		
		glVertex3d(0, 0, 0);
		glVertex3d(2, 0, 0);
		glVertex3d(2, 8, 0);
		glVertex3d(0, 8, 0);
		
		glEnd();
		glPopMatrix();
	}
	
	public static Button resume = new Button(256, 64, "Resume");
	public static Button mainmenu = new Button(256, 64, "Main Menu");
	
	public static void ownerKill(String owner) {
		if(owner == Game.username) Play.kills++;
		else{
			for(int i = 0; i < players.size(); i++){
				if(players.get(i).packet.username.equals(owner)) players.get(i).kills++;
			}
		}
	}
	
}
