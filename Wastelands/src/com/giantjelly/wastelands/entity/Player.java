 package com.giantjelly.wastelands.entity;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.networking.GameClient;
import com.giantjelly.wastelands.networking.GameServer;
import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.world.Block;

public class Player extends Actor {
	public Player(int x, int y) {
		super(x, y);
	}
	
	public int sightx = 0;
	public int sighty = 0;
	
	public void run(int speed){
		boolean sound = false;
		
		if(this.AniFrame >= 16){
			this.AniFrame = 0;
			Game.footstep.play();
			sound = true;
		}
		
		if(this.AniCount >= speed){
			this.AniCount = 0;
			this.AniFrame++;
			if(AniFrame==8){
				Game.footstep.play();
				sound = true;
			}
		}
		
		this.AniCount++;
		
		if(sound){
			if(GameServer.running) GameServer.stepSound();
			if(GameClient.running) GameClient.stepSound();
		}
	}
	
	public void update(){
		if(turretIndex == -1 && !Play.dead){
			if(Keyboard.isKeyDown(Keyboard.KEY_D) && !Keyboard.isKeyDown(Keyboard.KEY_A) && !Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)){
				if(this.xs < 8)
					this.xs++;
				
				this.run(3);
			}
			if(!Keyboard.isKeyDown(Keyboard.KEY_D) && Keyboard.isKeyDown(Keyboard.KEY_A) && !Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)){
				if(this.xs > -8)
					this.xs--;
				
				this.run(3);
			}
			
			if(Keyboard.isKeyDown(Keyboard.KEY_D) && !Keyboard.isKeyDown(Keyboard.KEY_A) && Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)){
				if(!flip){
					if(this.xs < 12)
						this.xs++;
					this.run(2);
				}else{
					if(this.xs < 8)
						this.xs++;
					
					this.run(3);
					
					if(xs>8) xs--;
				}
			}
			if(!Keyboard.isKeyDown(Keyboard.KEY_D) && Keyboard.isKeyDown(Keyboard.KEY_A) && Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)){
				if(flip){
					if(this.xs > -12)
						this.xs--;
					this.run(2);
				}else{
					if(this.xs > -8)
						this.xs--;
					
					this.run(3);
					
					if(xs<-8) xs++;
				}
			}
		}
		
		if(
				(!Keyboard.isKeyDown(Keyboard.KEY_D) && !Keyboard.isKeyDown(Keyboard.KEY_A))
				||
				(Keyboard.isKeyDown(Keyboard.KEY_D) && Keyboard.isKeyDown(Keyboard.KEY_A))
		){
			this.xs /= 2;
			this.AniFrame = 0;
		}
		if(this.xs > 8 && !Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || this.xs < -8 && !Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)){
			this.xs /= 2;
		}
		
		this.x += this.xs;
		this.y += this.ys;
		
		this.xscroll -= (this.x - Display.getWidth()/2)/10;
		this.x -= (this.x - Display.getWidth()/2)/10;
		
		this.yscroll -= (this.y - Display.getHeight()/2)/10;
		this.y -= (this.y - Display.getHeight()/2)/10;
		
		
		
		boolean onladder = false;
		for(Block block : Game.Blocks){
			if(block.layer != 4 && block.coords[1] == 256){
				if(
						this.x + left < block.x + (block.coords[2]*4) +this.xscroll &&
						this.x + right > block.x + this.xscroll&&
						this.y + top < block.y + (block.coords[3]*4) + this.yscroll &&
						this.y + bottom > block.y + this.yscroll
				)
				{
					onladder = true;
				}
			}
		}
		
		if(this.collision(left, right, top, bottom) || onladder){
			if(Keyboard.isKeyDown(Keyboard.KEY_W) || Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
				this.ys = -10;
			}
		}
		if(onladder && this.ys > 10)
			this.ys--;
			
			
		if(turretIndex==-1) this.ys++;
		
		if(muzzleflash > 0) muzzleflash--;
		
		sightx += (Mouse.getX()-sightx-16)/10;
		sighty += ((Display.getHeight()-Mouse.getY())-sighty-16)/5;
		
		flip = (sightx>x) ? false : true;
		
		if(shieldi>0) shieldi--;
		if(shieldi<40) shield = false;
		
		if(Mouse.isButtonDown(1) && shieldi<=0){
			shieldi = 60;
			shield = true;
		}
		
		if(firstWeapon != null){
			if(firstWeapon.shootcount > 0)
				firstWeapon.shootcount--;
			if(firstWeapon.ammo <= 0){
				firstWeapon.ammo = firstWeapon.capacity;
				firstWeapon.shootcount = 120;
			}
		}
	}
	
	public void draw(){
		super.draw();
		Sprite.draw(Sprite.Gui, new int[]{ 8, 8, 8, 8 }, new float[]{ sightx, sighty, 0, 4 });
	}
	
}
