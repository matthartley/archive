package com.giantjelly.wastelands.entity;

import static org.lwjgl.opengl.GL11.*;

import java.io.Serializable;
import java.util.Random;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.entity.PhysicsCollidable;
import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.entity.Particle;

public class Bullet extends PhysicsCollidable implements Serializable {
	public int[] coords;
	public int damage;
	
	public String owner = Game.username;
	
	public boolean active = true;
	
	public Bullet() {
		super(0, 0, 16, 12, 0, 0);
	}
	
	public Bullet(int[] ints, float[] floats) {
		super(ints[0], ints[1], ints[2], ints[3], floats[0], floats[1]);
	}
	
	public void speed() {
		
	}
	
	public void tick(){
		speed();
		this.update();
		this.cooldown--;
		if(a){
			spark();
		}
	}
	
	public void render(){
		glDisable(GL_TEXTURE_2D);
		glColor4f(1, 1, 1, 1);
		
		glPushMatrix();
			glTranslatef(this.x + Play.player.xscroll, this.y + Play.player.yscroll, 0);
			glRotatef(this.rotation, 0, 0, 1);
			
			Sprite.draw(Sprite.Particles, coords, new float[]{ 0, 0, 4, 3 });
		glPopMatrix();
	}
	
	public void spark(){
	}
	
	public boolean testcollision(float xx, float yy) {
		if(
				x < xx+Player.right &&
				x > xx+Player.left &&
				y < yy+Player.bottom &&
				y > yy+Player.top
		){
			return true;
		}else{
			return false;
		}
	}
	
}
