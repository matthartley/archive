package com.giantjelly.wastelands.entity;

import static org.lwjgl.opengl.GL11.*;

import java.util.Random;

import org.lwjgl.input.Mouse;

import com.giantjelly.wastelands.networking.GameServer;
import com.giantjelly.wastelands.networking.NetPlayer;
import com.giantjelly.wastelands.networking.packet.SoundPacket;
import com.giantjelly.wastelands.gfx.Font;
import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.input.Input;
import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.Math;

public class Turret {
	
	public int x;
	public int y;
	public int aniframe = 0;
	public int anicount = 0;
	
	public Turret(int xx, int yy) {
		x = xx;
		y = yy;
	}
	
	public Turret() {
	}
	
	public void update(int index) {
		if(Input.x.downEvent()){
				if(Play.player.turretIndex == -1){
					if(Math.Positive(Play.player.x - (this.x + Play.player.xscroll)) < 100 && Math.Positive(Play.player.y - (y + Play.player.yscroll)) < 100){
						Play.player.turretIndex = index;
						Input.x.setState(true);
					}
				}
		}
		
		NetPlayer p = null;
		for(NetPlayer np : Play.players){
			if(np.packet.turretIndex==index) p = np;
		}
		
		if(GameServer.running){
			boolean a = false;
			if(Play.player.turretIndex == index){
				if(Mouse.isButtonDown(0)){
					animate(Game.username);
					a = true;
				}
			}
			if(p != null && p.packet.click){
				animate(p.packet.username);
				a = true;
			}
			if(!a){
				anicount = 0;
				aniframe = 0;
			}
			
			/*if(Play.player.turretIndex == index || p!=null){
				if(Mouse.isButtonDown(0)){
					this.animate(null);
				}else if(p!=null && p.packet.click){
					this.animate(p.packet.username);
				}else{
					this.anicount = 0;
					this.aniframe = 0;
				}
			}else{
				this.anicount = 0;
				this.aniframe = 0;
			}*/
		}
		
	}
	
	public void fire(String owner) {
		float yspeed = (new Random().nextFloat()*9) - 4;
		
		BlasterBullet b = new BlasterBullet(this.x + 128, this.y + 64, 40, yspeed);
		b.owner = owner;
		Play.Bullets.add(b);
		Game.blaster.play();
		GameServer.server.sendToAllTCP(new SoundPacket("blaster"));
	}
	
	public void animate(String owner) {
		this.anicount++;
		
		if(this.anicount >= 3){
			this.aniframe++;
			this.anicount = 0;
			
			if(this.aniframe == 1 && this.anicount == 0) this.fire(owner);
		}
		
		if(this.aniframe >= 3)
			this.aniframe = 0;
	}
	
	public void draw() {
		if(Math.Positive(Play.player.x - (this.x + Play.player.xscroll)) < 100){
			Font.font.Draw(this.x + Play.player.xscroll, this.y + Play.player.yscroll - 30, "(Press X)");
		}
		
		glColor4f(1, 1, 1, 1);
		Sprite.draw(Sprite.Usables, new int[]{ this.aniframe*128, 0, 128, 64 }, new float[]{ this.x+Play.player.xscroll, this.y+Play.player.yscroll, 4, 2 });
	}
}
