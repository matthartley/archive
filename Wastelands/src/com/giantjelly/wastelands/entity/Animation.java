package com.giantjelly.wastelands.entity;

import java.io.Serializable;

public class Animation implements Serializable {
	
	public Animation() {
	}
	
	//public String owner = null;
	
	public int ticks = 0;
	
	public boolean active = true;
	
	int x;
	int y;
	
	int frames;
	int frame = 0;
	
	int count = 0;
	
	public void tick() {
		ticks++;
		
		count++;
		if(count>=3){
			count=0;
			frame++;
		}
		if(frame>=frames){
			active =  false;
		}
	}
	
	public void draw() {
		
	}
	
	/*public boolean distance(float playerx, float playery) {
		return false;
	}*/
	
}
