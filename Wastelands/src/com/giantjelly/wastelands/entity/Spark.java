package com.giantjelly.wastelands.entity;

import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.gfx.Sprite;

public class Spark extends Animation {
	
	public Spark() {
	}
	
	public Spark(int xx, int yy) {
		x=xx;
		y=yy;
		frames = 4;
	}
	
	public void draw() {
		if(active) Sprite.draw(Sprite.animations, new int[]{ 16*frame, 0, 16, 16 }, new float[]{ x+Play.player.xscroll, y+Play.player.yscroll, 0, 2 });
	}
	
}
