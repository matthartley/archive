package com.giantjelly.wastelands.entity;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Math;
import com.giantjelly.wastelands.networking.GameServer;
import com.giantjelly.wastelands.networking.packet.SoundPacket;

public class BlasterBullet extends Bullet {
	
	public BlasterBullet() {
	}
	
	public BlasterBullet(int x, int y, float xsd, float ysd) {
		super(new int[]{ x, y, 16, 12 }, new float[]{ xsd, ysd });
		coords = new int[]{ 0, 0, 8, 8 };
		damage = 1;
	}
	
	public void speed() {
		this.ysd += (0.01f) * (40 - Math.Positive((int)this.xsd));
		if(this.xsd > 0)	
			this.xsd -= 0.01f;
		else if(this.xsd < 0)
			this.xsd += 0.01f;
	}
	
	public void spark(){
		Game.bullet.play();
		GameServer.server.sendToAllTCP(new SoundPacket("bullet"));
		Game.sfx = "bullet";
		
		Game.animations.add(new Spark((int)x+coords[2], (int)y+coords[3]));
	}
	
}
