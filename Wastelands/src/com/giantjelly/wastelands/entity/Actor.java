package com.giantjelly.wastelands.entity;

import static org.lwjgl.opengl.GL11.glColor4d;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glTranslatef;

import org.lwjgl.input.Mouse;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.gfx.Sprite;

public class Actor extends Collidable {

	public Actor(int x, int y) {
		super(x, y, 1);
	}
	
	public static int left = 36;
	public static int right = 96;
	public static int top = 14;
	public static int bottom = 124;
	
	public int AniCount = 0;
	public int AniFrame = 0;
	public boolean flip = false;
	
	public int turretIndex = -1;
	
	public int gunindex = 0;
	public int muzzleflash = 0;
	public int armrotation = 0;
	
	public boolean shield = false;
	public int shieldi = 0;
	
	public Gun firstWeapon = null;
	
	public Gun getGun() {
		return firstWeapon;
	}
	
	public Turret mgGet() {
		if(turretIndex != -1)
			return Game.turrets.get(turretIndex);
		else
			return null;
	}
	
	public void draw(){
		if(turretIndex==-1){
			if(firstWeapon!=null){
				if(firstWeapon!=null){
					glPushMatrix();
					
					int x = 0;
					int y = 0;
					float r;
					if(!flip){
						r = (float)Math.toDegrees(Math.atan2(Mouse.getX() - (this.x+58), Mouse.getY() - (this.y-74))) - 90;
						x = 54;
						y = 74;
					}else{
						r = (float)Math.toDegrees(Math.atan2(Mouse.getX() - (this.x+58), Mouse.getY() - (this.y-74))) + 90;
						x = 74;
						y = 74;
					}
					
					armrotation = (int)r;
					int offset = 0;
					if(muzzleflash > 0) offset = 128;
					
					glTranslatef(this.x, this.y, 0);
					glTranslatef(x, y, 0);
					glRotatef(r, 0.0f, 0.0f, 1.0f);
					glTranslatef(-x, -y, 0);
					
					Sprite.draw(Sprite.WeaponSheet, new int[]{ 64*AniFrame, (firstWeapon.coords[1]*32)+offset, 64, 64 }, new float[]{ 0, 0, 4, 2 }, flip);
					glPopMatrix();
				}else{
					armrotation = 0;
					Sprite.draw(Sprite.WeaponSheet, new int[]{ 64*AniFrame, 768+firstWeapon.coords[1]*16, 64, 64 }, new float[]{ x, y, 4, 2 }, flip);
				}
				
				glPushMatrix();
				glTranslatef(this.x, this.y, 0);
				
				glTranslatef(64, 64, 0);
				glRotatef(this.angle, 0, 0, 1);
				glTranslatef(-64, -64, 0);
					Sprite.draw(Sprite.PlayerSheet, new int[]{ 64*AniFrame, 128, 64, 64 }, new float[]{ 0, 0, 4, 2 }, flip);
				glPopMatrix();
			}else{
				glPushMatrix();
				glTranslatef(this.x, this.y, 0);
				glTranslatef(64, 64, 0);
				glRotatef(this.angle, 0, 0, 1);
				glTranslatef(-64, -64, 0);
				
				Sprite.draw(Sprite.PlayerSheet, new int[]{ 64*AniFrame, 0, 64, 64 }, new float[]{ 0, 0, 4, 2 }, flip);
				glPopMatrix();
			}
		}
		else //MOUNTED GUN!
		{
			x = mgGet().x+xscroll;
			y = mgGet().y+yscroll;
			
			Sprite.draw(Sprite.PlayerSheet, new int[]{ 0, 384, 64, 64 }, new float[]{ 
					(x) - 36, (y) - 14, 4, 2
					});
		}
		
		glColor4d(1, 1, 1, 0.5);
		if(shield){
			glPushMatrix();
			
			glTranslatef(x, y, 0);
			
			glTranslatef(64, 64, 0);
			glRotatef(this.angle, 0, 0, 1);
			glTranslatef(-64, -64, 0);
			
			Sprite.draw(Sprite.shield, new int[]{ 0, 0, 64, 64 }, new float[]{ 0, 0, 3.9f, 2 }, flip);
			glPopMatrix();
		}
		glColor4d(1, 1, 1, 1);
	}

}
