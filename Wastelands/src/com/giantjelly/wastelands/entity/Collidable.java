package com.giantjelly.wastelands.entity;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.world.Block;
import com.giantjelly.wastelands.world.Triangle;
import com.giantjelly.wastelands.Math;

public class Collidable {
	public int x;
	public int y;
	public int xs;
	public int ys;
	public int xscroll;
	public int yscroll;
	
	public int mass;
	
	public static int ypos = 0;
	
	public int angle = 0;
	public float slopedownest = 0;
	
	public Collidable(int x, int y, int mass){
		this.x = x;
		this.y = y;
		this.xs = 0;
		this.ys = 0;
		this.xscroll = 0;
		this.yscroll = 0;
		this.mass = mass;
	}
	
	public boolean collision(int left, int right, int top, int bottom){
		boolean a = false;
		boolean tilt = true;
		
		for(Triangle Tri : Game.Triangles){
			if(
					this.x + left < Tri.x + (Tri.coords[2]*4) +this.xscroll &&
					this.x + right > Tri.x + this.xscroll&&
					this.y + top < Tri.y + (Tri.coords[3]*4) + this.yscroll &&
					this.y + bottom > Tri.y + this.yscroll + ypos &&
					Tri.layer == 4
			)
			{
				tilt = true;
				
				int disleft = (this.x + right) - (Tri.x + this.xscroll);
				int disright = (Tri.x + Tri.coords[2]*4 + this.xscroll) - (this.x + left);
				int distop = (this.y + bottom) - (Tri.y + this.yscroll + ypos);
				int disbottom = (Tri.y + Tri.coords[3]*4 + this.yscroll) - (this.y + top);
				
				float xdis = Math.Smallest(disleft, disright);
				float ydis = Math.Smallest(distop, disbottom);
				
				boolean sloped = false;
				float r = (float)Math.degrees(Math.postoangle(Tri.coords[3], Tri.coords[2]));
				
				int triypos = Tri.y + Tri.coords[3]*4 + this.yscroll + ypos;
				
				if(Tri.side == 0){
					
					
					if(xdis < ydis){
						if(disleft < disright && disleft < distop - 8){
							this.x = (Tri.x + this.xscroll) - right;
						}else if(disright < disleft){
							sloped = true;
						}
					}else{
						if(distop < disbottom){
							sloped = true;
						}else if(disbottom < distop - 8){
							this.y = (Tri.y + Tri.coords[3]*4 + this.yscroll) - top;
							this.ys = 0;
						}
					}
					
					if(sloped){
						
						
						int xpos = (Tri.x + Tri.coords[2]*4 + this.xscroll) - (this.x + left);
						float ratio = (float)Tri.coords[3] / (float)Tri.coords[2];
						
						if(this.y + (bottom) > triypos - (xpos * ratio)){
							this.angle = (int)r;
							
							float y = ((triypos - (xpos * ratio)) - (bottom));
							this.y = (int)y;
							a = true;
							this.ys = 0;
							
							if(this.x + left < Tri.x + this.xscroll){
								this.y = ((Tri.y + this.yscroll) - bottom) + ypos;
								this.angle = 0;
							}else{
								float slide = 4/this.mass *ratio;
								this.x += slide;
								this.y += slide *ratio;
								this.slopedownest = ratio;
							}
						}
					}
					
					
				}
				
				if(Tri.side == 1){
					
					
					if(xdis < ydis){
						if(disleft < disright){
							sloped = true;
						}else if(disright < disleft && disright < distop - 8){
							this.x = (Tri.x + Tri.coords[2]*4 + this.xscroll) - left;
						}
					}else{
						if(distop < disbottom){
							sloped = true;
						}else if(disbottom < distop - 8){
							this.y = (Tri.y + Tri.coords[3]*4 + this.yscroll) - top;
							this.ys = 0;
						}
					}
					
					if(sloped){
						
						
						int xpos = (this.x + right) - (Tri.x + this.xscroll);
						float ratio = (float)Tri.coords[3] / (float)Tri.coords[2];
						
						if(this.y + (bottom) > triypos - (xpos * ratio)){
							this.angle = (int)-r;
							
							float y = ((triypos - (xpos * ratio)) - (bottom));
							this.y = (int)y;
							a = true;
							this.ys = 0;
							
							if(this.x + right > Tri.x + Tri.coords[2]*4 + this.xscroll){
								this.y = ((Tri.y + this.yscroll) - bottom) + ypos;
								this.angle = 0;
							}else{
								float slide = 4/this.mass *ratio;
								this.x -= slide;
								this.y += slide *ratio;
								this.slopedownest = ratio;
							}
						}
					}
					
					
				}
				
			}
		}
		
		if(!tilt)
			this.angle = 0;
		
		for(Block Block : Game.Blocks){
			if(
					this.x + left < Block.x + (Block.coords[2]*4) +this.xscroll &&
					this.x + right > Block.x + this.xscroll&&
					this.y + top < Block.y + (Block.coords[3]*4) + this.yscroll &&
					this.y + bottom > Block.y + this.yscroll &&
					Block.layer == 4
			)
			{
				int disleft = (this.x + right) - (Block.x + this.xscroll);
				int disright = (Block.x + Block.coords[2]*4 + this.xscroll) - (this.x + left);
				int distop = (this.y + bottom) - (Block.y + this.yscroll);
				int disbottom = (Block.y + Block.coords[3]*4 + this.yscroll) - (this.y + top);
				
				float xdis = Math.Smallest(disleft, disright);
				float ydis = Math.Smallest(distop, disbottom);
				
				if(xdis < ydis){
					if(xdis == disleft){
						this.x = (Block.x + this.xscroll) - right;
					}else{
						this.x = (Block.x + Block.coords[2]*4 + this.xscroll) - left;
					}
				}else{
					if(ydis == distop){
						this.y = (Block.y + this.yscroll) - bottom;
						this.ys = 0;
						this.angle = 0;
						a = true;
					}else{
						this.y = (Block.y + Block.coords[3]*4 + this.yscroll) - top;
						this.ys = 0;
						this.angle = 0;
					}
				}
			}
		}
		
		return a;
	}
}
