package com.giantjelly.wastelands.entity;

import com.giantjelly.wastelands.Math;
import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.world.Block;
import com.giantjelly.wastelands.world.Triangle;

import java.io.Serializable;

public class PhysicsCollidable implements Serializable{
	public float x;
	public float y;
	protected int xs;
	protected int ys;
	public float xsd;
	public float ysd;
	
	public int cooldown = 120;
	
	public float rotation = 0;
	protected float rotationspeed = 0;
	
	public boolean a = false;
	
	public PhysicsCollidable(int x, int y, int xs, int ys, float xsd, float ysd){
		this.x = x;
		this.y = y;
		this.xs = xs;
		this.ys = ys;
		this.xsd = xsd;
		this.ysd = ysd;
	}
	
	public void update(){
		this.rotation += this.rotationspeed;
		
		for(int i = 0; i < 100; i++){
			this.x += this.xsd/100;
			this.y += this.ysd/100;
			
			if(this.collision())
				i = 100;
		}
	}
	
	public boolean collision(){
		a = false;
		for(Block Block : Game.Blocks){
			if(
					this.x < Block.x + (Block.coords[2]*4) &&
					this.x + this.xs > Block.x &&
					this.y < Block.y + (Block.coords[3]*4) &&
					this.y + this.ys > Block.y &&
					Block.layer == 4
			)
			{
				float disleft = (this.x + this.xs) - (Block.x);
				float disright = (Block.x + Block.coords[2]*4) - (this.x);
				float distop = (this.y + this.ys) - (Block.y);
				float disbottom = (Block.y + Block.coords[2]*4) - (this.y);
				
				float xdis = Math.Smallest(disleft, disright);
				float ydis = Math.Smallest(distop, disbottom);
				
				if(xdis < ydis){
					if(xdis == disleft){
						this.x -= this.xsd;
						this.xsd /= 8;
						this.xsd *= -1;
						
						this.rotationspeed += this.ysd * 10;
						
					}else{
						this.x -= this.xsd;
						this.xsd /= 8;
						this.xsd *= -1;
						
						this.rotationspeed += this.ysd * 10;
					}
				}else{
					if(ydis == distop){
						this.ysd = 0;
						this.y = Block.y - this.ys;
						
						this.rotationspeed = 0;
						this.xsd = 0;
						
					}else{
						this.y -= this.ysd;
						this.ysd /= 8;
						this.ysd *= -1;
					}
				}
				
				this.cooldown = 0;
				a = true;
			}
		}
		
		for(Triangle tri : Game.Triangles){
			if(
					this.x < tri.x + (tri.coords[2]*4) &&
					this.x  > tri.x &&
					this.y < tri.y + (tri.coords[3]*4) &&
					this.y  > tri.y &&
					tri.layer == 4
			)
			{
				float X = x-tri.x;
				float Y = y-tri.y;
				
				if(tri.side==0){
					if(Y*tri.coords[2]>X*tri.coords[3]){
						a = true;
						cooldown = 0;
					}
				}
				if(tri.side==1){
					if(Y*tri.coords[2]>(tri.coords[2]*4-X)*tri.coords[3]){
						a = true;
						cooldown = 0;
					}
				}
			}
		}
		
		return a;
	}
}
