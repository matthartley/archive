package com.giantjelly.wastelands.entity;

import static org.lwjgl.opengl.GL11.*;

import java.util.Random;

import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.entity.PhysicsCollidable;
import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.Math;

public class Particle extends PhysicsCollidable{

	public Particle(int x, int y, int xs, int ys, float xsd, float ysd) {
		super(x, y, xs, ys, xsd, ysd);
	}
	
	public int cooldown2 = 60;
	
	public void speed() {
		ysd += (0.01f) * (40 - Math.Positive((int)this.xsd));
	}
	
	public void render(){
		speed();
		this.update();
		this.cooldown2--;
	}
	
	public void draw() {
		glColor4f(1, 1, 1, (float)cooldown2 / 60.0f);
		Sprite.draw(Sprite.Particles, new int[]{ 0, 24, 8, 8 }, new float[]{ this.x+Play.player.xscroll, this.y+Play.player.yscroll, 0, 2 });
	}

}
