package com.giantjelly.wastelands.entity;

import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.gfx.Sprite;

public class Explosion extends Animation {
	
	public Explosion() {
	}
	
	public Explosion(int xx, int yy) {
		x=xx;
		y=yy;
		frames = 8;
	}
	
	public void draw() {
		if(active) Sprite.draw(Sprite.animations, new int[]{ 128*frame, 32+128, 128, 128 }, new float[]{ x+Play.player.xscroll, y+Play.player.yscroll, 0, 2 });
	}
	
	/*public boolean distance(float playerx, float playery) {
		int xPos = x+128;
		int yPos = y+128;
		float xx = (playerx < xPos) ? xPos - playerx : playerx - xPos;
		float yy = (playery < yPos) ? yPos - playery : playery - yPos;
		
		if( Math.sqrt((xx*xx)+(yy*yy))  < 200){
			System.out.println(Math.sqrt((xx*xx)+(yy*yy)));
			return true;
		}else{
			System.out.println(Math.sqrt((xx*xx)+(yy*yy)));
			return false;
		}
	}*/
	
}
