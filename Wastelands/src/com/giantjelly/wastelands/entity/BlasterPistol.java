package com.giantjelly.wastelands.entity;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.networking.GameClient;
import com.giantjelly.wastelands.networking.GameServer;
import com.giantjelly.wastelands.networking.packet.SoundPacket;

public class BlasterPistol extends Gun {
	
	public BlasterPistol(int x, int y) {
		super(x, y);
		coords = new int[]{ 0, 0, 16, 8 };
	}
	
	public void fire(int[] ints, float[] floats) {
		Bullet b = new BlasterBullet(ints[0], ints[1], floats[0], floats[1]);
		b.rotation = (!Play.player.flip) ? Play.player.armrotation : Play.player.armrotation+180;
		
		if(GameServer.running){
			Play.Bullets.add(b);
			GameServer.server.sendToAllTCP(new SoundPacket("blaster"));
		}
		if(GameClient.running){
			GameClient.sendBullet(b);
		}
	}
	
	public void bang() {
		Game.blaster.play();
		Game.sfx = "blaster";
	}
	
}
