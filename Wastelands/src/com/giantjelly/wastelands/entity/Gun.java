package com.giantjelly.wastelands.entity;

import java.io.Serializable;

import com.giantjelly.wastelands.Math;
import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.gfx.Font;
import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.input.Input;

public class Gun implements Serializable {
	public final int capacity = 16;
	public int x;
	public int y;
	public int[] coords;
	
	public static int startCoolDown = 10;
	public static int pickedCoolDown = 120;
	public int coolDown = startCoolDown;
	
	public int shootcount = 0;
	public int ammo = capacity;
	
	public String tostring() {
		return x+"-"+y+"-"+coords[0]+"-"+coords[1]+"-"+coords[2]+"-"+coords[3];
	}
	
	public Gun() {
	}
	
	public Gun(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public void update(int index){
		if(coolDown <= 0){
			if(Math.Positive((Play.player.x+64) - (this.x + Play.player.xscroll)) + Math.Positive((Play.player.y+64) - (this.y + Play.player.yscroll)) < 100){
				if(Input.x.downEvent() && coolDown <= 0){
					Input.x.setState(true);
					//pickup(index);
					//Client.pick = this.tostring();
				}
			}
		}
	}
	
	public void draw(){
		if(coolDown <= 0){
			Sprite.draw(Sprite.Pickups, coords, new float[]{ x + Play.player.xscroll, y + Play.player.yscroll, 4, 4 });
			
			if(Math.Positive((Play.player.x+64) - (this.x + Play.player.xscroll)) + Math.Positive((Play.player.y+64) - (this.y + Play.player.yscroll)) < 100){
				Font.font.Draw((this.x-32) + Play.player.xscroll, (this.y-32) + Play.player.yscroll, "(Press X)");
			}
		}
	}
	
	public void fire(int[] ints, float[] floats) {
		
	}
	
	public void bang() {
		
	}
	
}
