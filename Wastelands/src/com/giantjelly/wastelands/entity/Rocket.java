package com.giantjelly.wastelands.entity;

import static com.giantjelly.wastelands.Play.ownerKill;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.networking.GameServer;
import com.giantjelly.wastelands.networking.packet.SoundPacket;

public class Rocket extends Bullet {
	
	public int anicount = 0;
	
	public Rocket() {
	}
	
	public Rocket(int x, int y, float xsd, float ysd) {
		super(new int[]{ x, y, 40, 12 }, new float[]{ xsd, ysd });
		coords = new int[]{ 0, 16, 16, 8 };
		
		this.xsd /= 1.5;
		this.ysd /= 1.5;
		
		damage = 5;
	}
	
	public void speed() {
		if(this.xsd > 0)	
			this.xsd -= 0.01f;
		else if(this.xsd < 0)
			this.xsd += 0.01f;
		
		anicount++;
		if(anicount>=3){
			coords[0] = (coords[0]==0) ? 16 : 0;
			anicount=0;
		}
	}
	
	public void spark(){
		Game.rocket.play();
		GameServer.server.sendToAllTCP(new SoundPacket("explosion"));
		Game.sfx = "rocket";
		
		Animation a = new Explosion((int)x-128, (int)y-128);
		//a.owner = owner;
		Game.animations.add(a);
		
		for(int i = 0; i < Play.players.size(); i++){
			if(distance(Play.players.get(i).packet.x, Play.players.get(i).packet.y)){
				Play.players.get(i).health -= 5;
				if(Play.players.get(i).health <= 0) ownerKill(owner);
			}
		}
		
		if(distance(Play.player.x, Play.player.y)){
			Play.health -= 5;
			if(Play.health <= 0) ownerKill(owner);
		}
	}
	
	public void impact(int player) {
		if(!Play.players.get(player).packet.shield){
			Play.kills++;
		}
		
		spark();
		active = false;
	}
	
	public boolean distance(float playerx, float playery) {
		float xPos = x;
		float yPos = y;
		float xx = (playerx < xPos) ? xPos - playerx : playerx - xPos;
		float yy = (playery < yPos) ? yPos - playery : playery - yPos;
		
		if( Math.sqrt( (xx*xx)+(yy*yy) )  < 200){
			System.out.println(Math.sqrt((xx*xx)+(yy*yy)));
			return true;
		}else{
			System.out.println(Math.sqrt((xx*xx)+(yy*yy)));
			return false;
		}
	}
	
}
