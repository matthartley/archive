package com.giantjelly.wastelands.networking;

import static org.lwjgl.opengl.GL11.*;

import java.io.Serializable;

import org.lwjgl.input.Mouse;

import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.entity.Player;
import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.networking.packet.ClientUpdatePacket;

public class NetPlayer implements Serializable {
	
	public ClientUpdatePacket packet = new ClientUpdatePacket();
	public long ping = 0;
	public int tmppackets = 0;
	public int packets = 0;
	public long lasttime = System.currentTimeMillis();
	public String ip;
	public int health = 10;
	public int kills;
	public boolean dead = false;
	
	public NetPlayer() {
	}
	
	public NetPlayer(String i, String n) {
		ip = i;
		packet.username = n;
	}
	
	public NetPlayer(String u) {
		packet.username = u;
	}
	
	public void draw(){
		if(!dead){
			if(packet.turretIndex==-1){
				if(packet.gunIndex!=0){
					if(packet.gunIndex!=0){
						int x = 0;
						int y = 0;
						if(!packet.flip){
							x = 54;
							y = 74;
						}else{
							x = 74;
							y = 74;
						}
						
						glPushMatrix();
						{
							glTranslatef(packet.x+Play.player.xscroll, packet.y+Play.player.yscroll, 0);
							glTranslatef(x, y, 0);
							glRotatef(packet.armAngle, 0.0f, 0.0f, 1.0f);
							glTranslatef(-x, -y, 0);
							
							Sprite.draw(Sprite.WeaponSheet, new int[]{ 64*packet.aniFrame, packet.yGunStart*256, 64, 64 }, new float[]{ 0, 0, 4, 2 }, packet.flip);
						}
						glPopMatrix();
					}else{
						Sprite.draw(Sprite.WeaponSheet, new int[]{ 64*packet.aniFrame, 768+packet.yGunStart*128, 64, 64 }, new float[]{ packet.x+Play.player.xscroll, packet.y+Play.player.yscroll, 4, 2 }, packet.flip);
					}
					
					glPushMatrix();
					{
						glTranslatef(packet.x+Play.player.xscroll, packet.y+Play.player.yscroll, 0);
						
						glTranslatef(64, 64, 0);
						glRotatef(packet.angle, 0, 0, 1);
						glTranslatef(-64, -64, 0);
						Sprite.draw(Sprite.PlayerSheet, new int[]{ 64*packet.aniFrame, 128, 64, 64 }, new float[]{ 0, 0, 4, 2 }, packet.flip);
					}
					glPopMatrix();
				}else{
					glPushMatrix();
					{
						glTranslatef(packet.x+Play.player.xscroll, packet.y+Play.player.yscroll, 0);
						
						glTranslatef(64, 64, 0);
						glRotatef(packet.angle, 0, 0, 1);
						glTranslatef(-64, -64, 0);
						
						Sprite.draw(Sprite.PlayerSheet, new int[]{ 64*packet.aniFrame, 0, 64, 64 }, new float[]{ 0, 0, 4, 2 }, packet.flip);
					}
					glPopMatrix();
				}
			}
			else //MOUNTED GUN!
			{
				Sprite.draw(Sprite.PlayerSheet, new int[]{ 0, 384, 64, 64 }, new float[]{ 
						(packet.x+Play.player.xscroll) - 36, (packet.y+Play.player.yscroll) - 2, 4, 2
				});
			}
		}
	}
	
}
