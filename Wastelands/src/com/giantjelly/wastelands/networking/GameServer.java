package com.giantjelly.wastelands.networking;

import java.io.IOException;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.EndPoint;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.entity.Animation;
import com.giantjelly.wastelands.entity.BlasterBullet;
import com.giantjelly.wastelands.entity.BlasterPistol;
import com.giantjelly.wastelands.entity.Bullet;
import com.giantjelly.wastelands.entity.Explosion;
import com.giantjelly.wastelands.entity.Gun;
import com.giantjelly.wastelands.entity.Turret;
import com.giantjelly.wastelands.entity.PlasmaBullet;
import com.giantjelly.wastelands.entity.PlasmaPistol;
import com.giantjelly.wastelands.entity.Rocket;
import com.giantjelly.wastelands.entity.RocketLauncher;
import com.giantjelly.wastelands.entity.Spark;
import com.giantjelly.wastelands.entity.Splat;
import com.giantjelly.wastelands.gui.Message;
import com.giantjelly.wastelands.networking.NetPlayer;
import com.giantjelly.wastelands.networking.packet.BulletPacket;
import com.giantjelly.wastelands.networking.packet.ClientUpdatePacket;
import com.giantjelly.wastelands.networking.packet.DeadPacket;
import com.giantjelly.wastelands.networking.packet.EndPacket;
import com.giantjelly.wastelands.networking.packet.LoginDeniedPacket;
import com.giantjelly.wastelands.networking.packet.LoginRequestPacket;
import com.giantjelly.wastelands.networking.packet.MapPacket;
import com.giantjelly.wastelands.networking.packet.RespawnPacket;
import com.giantjelly.wastelands.networking.packet.ServerUpdatePacket;
import com.giantjelly.wastelands.networking.packet.SoundPacket;
import com.giantjelly.wastelands.networking.packet.StartPacket;
import com.giantjelly.wastelands.world.Block;
import com.giantjelly.wastelands.world.Triangle;

public class GameServer {
	
	public static boolean running = false;
	public static int port = 44142;
	
	public static Server server;
	
	public static boolean footstep = false;
	
	/*public static String matchWinner = "";
	public static int nextMatchCountDown = 0;
	public static boolean matchRunning = false;*/
	
	public static boolean start() {
		running = true;
		Game.matchRunning = true;
		
		server = new Server();
		registerClasses(server);
		server.start();
		try {
			server.bind(port, port);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		server.addListener(new Listener() {
			public void received(Connection connection, Object object) {
				
				if(object instanceof LoginRequestPacket){
					LoginRequestPacket loginRequest = (LoginRequestPacket)object;
					
					boolean here = playerFound(loginRequest.username);
					boolean rightVersion = (loginRequest.version.equals(Game.VERSION)) ? true : false;
					
					if(!here && rightVersion){
						connection.sendTCP(new MapPacket());
						Play.players.add(new NetPlayer(loginRequest.username));
						System.out.println(loginRequest.username + " logged in successfully!");
					}else{
						LoginDeniedPacket denied = null;
						if(here) denied = new LoginDeniedPacket("This user is already on this server");
						if(!rightVersion) denied = new LoginDeniedPacket("Incompatable client version");
						connection.sendTCP(denied);
					}
				}
				
				if(object instanceof ClientUpdatePacket){
					ClientUpdatePacket packet = (ClientUpdatePacket) object;
					
					//connection.sendTCP(new ServerUpdatePacket());
					
					for(int i = 0; i < Play.players.size(); i++){
						if(Play.players.get(i).packet.username.equals(packet.username)){
							Play.players.get(i).tmppackets++;
							Play.players.get(i).ping = System.currentTimeMillis()-Play.players.get(i).lasttime;
							Play.players.get(i).lasttime = System.currentTimeMillis();
							Play.players.get(i).packet = packet;
							//if(packet.footstep) Game.footstep.play();
							Game.packetsReceived++;
						}
					}
				}
				
				if(object instanceof BulletPacket){
					BulletPacket packet = (BulletPacket) object;
					
					Play.Bullets.add(packet.bullet);
					
					if(packet.bullet instanceof BlasterBullet){
						//BlasterBullet bullet = (BlasterBullet) object;
						//Play.Bullets.add(bullet);
						Game.blaster.play();
						server.sendToAllExceptTCP(connection.getID(), new SoundPacket("blaster"));
					}
					
					if(packet.bullet instanceof PlasmaBullet){
						//PlasmaBullet bullet = (PlasmaBullet) object;
						//Play.Bullets.add(bullet);
						Game.plasma.play();
						server.sendToAllExceptTCP(connection.getID(), new SoundPacket("plasma"));
					}
					
					if(packet.bullet instanceof Rocket){
						//Rocket bullet = (Rocket) object;
						//Play.Bullets.add(bullet);
						Game.rocket.play();
						server.sendToAllExceptTCP(connection.getID(), new SoundPacket("rocket"));
					}
				}
				
				if(object instanceof SoundPacket){
					SoundPacket packet = (SoundPacket) object;
					
					if(packet.type.equals("step")){
						Game.footstep.play();
						server.sendToAllExceptTCP(connection.getID(), new SoundPacket("step"));
					}
				}
				
				if(object instanceof DeadPacket){
					/*DeadPacket packet = (DeadPacket) object;
					
					for(int i = 0; i < Play.players.size(); i++){
						if(packet.username.equals(Play.players.get(i).packet.username)){
							Play.players.get(i).health = 10;
						}
					}*/
					
					System.out.println("dead packet");
				}
				
				if(object instanceof RespawnPacket){
					RespawnPacket packet = (RespawnPacket) object;
					for(int i = 0; i < Play.players.size(); i++){
						if(Play.players.get(i).packet.username.equals(packet.username)){
							Play.players.get(i).dead = false;
							Play.players.get(i).health = 10;
						}
					}
				}
				
			}
			
			public void connected(Connection connection) {
				Game.log(connection.getID() + " has connected");
			}
			
			public void disconnected(Connection connection) {
				Game.log(connection.getID() + " has disconnected");
			}
		});
		
		return true;
	}
	
	public static void sendUpdate() {
		server.sendToAllUDP(new ServerUpdatePacket());	
	}
	
	public static void stepSound() {
		server.sendToAllTCP(new SoundPacket("step"));
	}
	
	public static void end() {
		server.sendToAllTCP(new EndPacket());
		Game.reset();
		Game.nextMatchCountDown = 20;
		Game.matchRunning = false;
	}
	
	public static void restart() {
		server.sendToAllTCP(new StartPacket());
		Game.serverReset();
		Game.matchRunning = true;
	}
	
	public static void stop() {
		running = false;
		Game.matchRunning = false;
		Game.matchWinner = "";
		server.close();
		server = null;
	}
	
	public static void playerDead(String username) {
		server.sendToAllTCP(new DeadPacket(username));
	}
	
	public static boolean playerFound(String username) {
		boolean here = false;
		for(int i = 0; i < Play.players.size(); i++){
			if(Play.players.get(i).packet.username.equals(username)) here = true;
		}
		
		return here;
	}
	
	public static void registerClasses(EndPoint endPoint) {
		Kryo kryo = endPoint.getKryo();
		
		kryo.register(LoginRequestPacket.class);
		kryo.register(LoginDeniedPacket.class);
		kryo.register(MapPacket.class);
		kryo.register(ClientUpdatePacket.class);
		kryo.register(ServerUpdatePacket.class);
		kryo.register(SoundPacket.class);
		kryo.register(BulletPacket.class);
		kryo.register(DeadPacket.class);
		kryo.register(EndPacket.class);
		kryo.register(StartPacket.class);
		kryo.register(RespawnPacket.class);
		
		kryo.register(NetPlayer.class);
		kryo.register(NetPlayer[].class);
		kryo.register(Block.class);
		kryo.register(Block[].class);
		kryo.register(Triangle.class);
		kryo.register(Triangle[].class);
		kryo.register(Turret.class);
		kryo.register(Turret[].class);
		kryo.register(Message.class);
		kryo.register(Message[].class);
		kryo.register(Gun.class);
		kryo.register(Gun[].class);
		kryo.register(BlasterPistol.class);
		kryo.register(PlasmaPistol.class);
		kryo.register(RocketLauncher.class);
		kryo.register(Bullet.class);
		kryo.register(Bullet[].class);
		kryo.register(BlasterBullet.class);
		kryo.register(PlasmaBullet.class);
		kryo.register(Rocket.class);
		kryo.register(Animation.class);
		kryo.register(Animation[].class);
		kryo.register(Spark.class);
		kryo.register(Splat.class);
		kryo.register(Explosion.class);
		
		kryo.register(int[].class);
	}
	
}
