package com.giantjelly.wastelands.networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.*;
import java.sql.*;
import java.util.ArrayList;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.gui.menu.Login;

public class Sql {
	
	//0cc175b9c0f1b6a831c399e269772661
	public static boolean login(String username, String password) {
		try {
			
			URL url = new URL("http://giantjelly.co.uk/pages/wastelands/login.php?username="+username+"&password="+md5(password));
			URLConnection connection = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			
			String text = "";
			while( (text = in.readLine()) != null ){
				if(!text.equals("")){
					System.out.println(text);
					if(text.equals("ok")){
						Game.username = username;
						return true;
					}else{
						Login.messages.add(text);
					}
				}
			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
			Login.messages.add("Connection error");
		} catch (IOException e) {
			e.printStackTrace();
			Login.messages.add("Connection error");
		}
		
		return false;
	}
	
	/*public static String driver = "com.mysql.jdbc.Driver";
	public static String url = "jdbc:mysql://1one7.com/giantjelly";
	public static String user = "matt";
	public static String pass = "matt123";
	
	public static boolean login(String username, String password) {
		boolean success = false;
		String error = "Login failed - Couldn't connect";
		
		try{
			Class.forName(driver);
			Connection con = DriverManager.getConnection (url, user, pass);
			Statement stmt = con.createStatement();
			ResultSet results = stmt.executeQuery("Select * FROM users WHERE username='" + username + "'");
			
			String thisusername = username;
			String thispassword = md5(password);
			
			boolean founduser = false;
			boolean rightpassword = false;
			
			while (results.next()) {
				founduser = true;
				String dbpassword = results.getString("password");
				boolean dbwastelands = results.getBoolean("wastelands");
				
				if(thispassword.equals(dbpassword)){
					
					rightpassword = true;
					
					if(dbwastelands){
						success = true;
						Game.username = username;
					}
				}
			}
			
			if(thisusername.length() == 0)
				error = "Login failed - You left something blank";
			else
			if(!founduser)
				error = "Login failed - User not found";
			else
			if(!rightpassword)
				error = "Login failed - Incorrect password";
			else
				error = "Login failed - You have not purchased Wastelands";
			
			con.close();
		}catch(SQLException e){
			e.printStackTrace();
			//error += e;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		if(!success)
			Login.messages.add(error);
		return success;
	}*/
	
	public static String md5(String input) {
		try{
		    String result = input;
		    if(input != null) {
		        MessageDigest md = MessageDigest.getInstance("MD5");
		        md.update(input.getBytes());
		        BigInteger hash = new BigInteger(1, md.digest());
		        result = hash.toString(16);
		        while(result.length() < 32) {
		            result = "0" + result;
		        }
		    }
		    return result;
		}catch(NoSuchAlgorithmException nsae){
		    return input;        
		}
	}
	
}
