package com.giantjelly.wastelands.networking;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.entity.Bullet;
import com.giantjelly.wastelands.entity.Turret;
import com.giantjelly.wastelands.networking.packet.BulletPacket;
import com.giantjelly.wastelands.networking.packet.ClientUpdatePacket;
import com.giantjelly.wastelands.networking.packet.DeadPacket;
import com.giantjelly.wastelands.networking.packet.EndPacket;
import com.giantjelly.wastelands.networking.packet.LoginRequestPacket;
import com.giantjelly.wastelands.networking.packet.MapPacket;
import com.giantjelly.wastelands.networking.packet.RespawnPacket;
import com.giantjelly.wastelands.networking.packet.ServerUpdatePacket;
import com.giantjelly.wastelands.networking.packet.SoundPacket;
import com.giantjelly.wastelands.networking.packet.StartPacket;
import com.giantjelly.wastelands.world.Block;
import com.giantjelly.wastelands.world.Triangle;

public class GameClient {

	public static boolean running = false;
	public static int port = 44142;
	public static InetAddress  serverAddress;
	public static long ping = 0;
	
	public static Client client;
	
	//public static boolean serverMatchRunning = false;
	//public static String serverMatchWinner = "";
	
	public static void login(InetAddress address) {
		serverAddress = address;
		client = new Client();
		GameServer.registerClasses(client);
		client.start();
		try {
			client.connect(1000, address, port, port);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		client.addListener(loginListener);
		
		client.sendTCP(new LoginRequestPacket());
	}
	
	public static void stop() {
		running = false;
		Game.matchRunning = false;
		Game.matchWinner = "";
		client.close();
		client = null;
	}
	
	public static Listener loginListener = new Listener() {
		public void received(Connection connection, Object object) {
			
			if(object instanceof MapPacket){
				MapPacket map = (MapPacket) object;
				Game.Blocks = new ArrayList<Block>(Arrays.asList(map.blocks));
				Game.Triangles = new ArrayList<Triangle>(Arrays.asList(map.triangles));
				Game.turrets = new ArrayList<Turret>(Arrays.asList(map.turrets));
				System.out.println("Map received successfully!");
				
				Game.mode = Game.Mode.PLAY;
				running = true;
				ping = System.currentTimeMillis();
				//connection.close();
			}
			
			if(object instanceof ServerUpdatePacket){
				ServerUpdatePacket packet = (ServerUpdatePacket) object;
				
				Play.serverPlayers = packet.players;
				//for(int i = 0; i < packet.players.length; i++) if(packet.players[i].packet.footstep) Game.footstep.play();
				Play.serverGuns = packet.guns;
				Play.serverTurrets = packet.turrets;
				Play.serverBullets = packet.bullets;
				Play.serverAnimations = packet.animations;
				Play.serverMessages = packet.messages;
				//if(packet.restart) Game.resetClientGame();
				Game.matchWinner = packet.matchWinner;
				Game.matchRunning = packet.matchRunning;
				
				ping = System.currentTimeMillis();
				Game.packetsReceived++;
				//connection.close();
			}
			
			if(object instanceof SoundPacket){
				SoundPacket sound = (SoundPacket) object;
				if(sound.type.equals("blaster")) Game.blaster.play();
				if(sound.type.equals("plasma")) Game.plasma.play();
				if(sound.type.equals("rocket")) Game.rocketlauncher.play();
				if(sound.type.equals("step")) Game.footstep.play();
				if(sound.type.equals("bullet")) Game.bullet.play();
				if(sound.type.equals("explosion")) Game.rocket.play();
			}
			
			if(object instanceof EndPacket){
				Game.reset();
			}
			
			if(object instanceof StartPacket){
			}
			
			if(object instanceof DeadPacket){
				DeadPacket packet = (DeadPacket) object;
				if(packet.username.equals(Game.username)){
					Game.die();
				}
			}
			
		}
		
		public void connected(Connection connection) {
			Game.log("Connected to server");
		}
		
		public void disconnected(Connection connection) {
			Game.log("Disconnected from server");
		}
	};
	
	/*public static Listener updateListener = new Listener() {
		public void received(Connection connection, Object object) {
			if(object instanceof ServerUpdatePacket){
				ServerUpdatePacket packet = (ServerUpdatePacket) object;
				
				Play.serverPlayers = packet.players;
				Play.serverGuns = packet.guns;
				Play.serverMountedGuns = packet.mountedGuns;
				Play.serverBullets = packet.bullets;
				Play.serverAnimations = packet.animations;
				Play.serverMessages = packet.messages;
				if(packet.restart) Game.resetClientGame();
				
				ping = System.currentTimeMillis();
				connection.close();
			}
		}
	};*/
	
	public static void sendUpdate() {
		/*Client client = new Client();
		GameServer.registerClasses(client);
		client.start();
		try {
			client.connect(5000, serverAddress, port);
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		//client.addListener(updateListener);
		
		client.sendUDP(new ClientUpdatePacket());
	}
	
	public static void sendBullet(Bullet bullet) {
		client.sendTCP(new BulletPacket(bullet));
	}
	
	public static void stepSound() {
		client.sendTCP(new SoundPacket("step"));
	}
	
	/*public static void dead() {
		client.sendTCP(new DeadPacket());
	}*/
	
	public static void respawn() {
		client.sendTCP(new RespawnPacket());
	}
	
}