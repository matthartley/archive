package com.giantjelly.wastelands.networking.packet;

public class SoundPacket {
	
	public String type;
	
	public SoundPacket() {
	}
	
	public SoundPacket(String t) {
		type = t;
	}
	
}
