package com.giantjelly.wastelands.networking.packet;

import com.giantjelly.wastelands.entity.Bullet;

public class BulletPacket {
	
	public Bullet bullet;
	
	public BulletPacket(Bullet b) {
		bullet = b;
	}
	
}
