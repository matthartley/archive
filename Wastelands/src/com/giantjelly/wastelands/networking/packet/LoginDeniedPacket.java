package com.giantjelly.wastelands.networking.packet;

public class LoginDeniedPacket {
	
	public String message;
	
	public LoginDeniedPacket(String m) {
		message = m;
	}
	
}
