package com.giantjelly.wastelands.networking.packet;

import com.giantjelly.wastelands.Game;

public class LoginRequestPacket {
	
	public String username;
	public String version;
	
	public LoginRequestPacket() {
		username = Game.username;
		version = Game.VERSION;
	}
	
}
