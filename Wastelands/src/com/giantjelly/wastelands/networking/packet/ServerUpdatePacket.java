package com.giantjelly.wastelands.networking.packet;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.entity.Animation;
import com.giantjelly.wastelands.entity.Bullet;
import com.giantjelly.wastelands.entity.Gun;
import com.giantjelly.wastelands.entity.Turret;
import com.giantjelly.wastelands.gui.Message;
import com.giantjelly.wastelands.networking.GameServer;
import com.giantjelly.wastelands.networking.NetPlayer;

public class ServerUpdatePacket {
	
	public NetPlayer[] players;
	
	public Gun[] guns = Game.Guns.toArray(new Gun[Game.Guns.size()]);
	public Turret[] turrets = Game.turrets.toArray(new Turret[Game.turrets.size()]);
	public Bullet[] bullets = Play.Bullets.toArray(new Bullet[Play.Bullets.size()]);
	public Animation[] animations = Game.animations.toArray(new Animation[Game.animations.size()]);
	public Message[] messages = Game.getLog();
	
	public String matchWinner = Game.matchWinner;
	public boolean matchRunning = Game.matchRunning;
	
	public ServerUpdatePacket() {
		players = new NetPlayer[Play.players.size()+1];
		
		for(int i = 0; i < Play.players.size(); i++){
			players[i] = Play.players.get(i);
		}
		
		int yGunStart = (Play.player.firstWeapon!=null) ? Play.player.firstWeapon.coords[1]/8 : 0;
		
		players[players.length-1] = new NetPlayer(Game.username);
		players[players.length-1].packet = new ClientUpdatePacket();
		players[players.length-1].kills = Play.kills;
		players[players.length-1].dead = Play.dead;
	}
	
}
