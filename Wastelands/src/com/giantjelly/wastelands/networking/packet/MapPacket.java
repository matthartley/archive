package com.giantjelly.wastelands.networking.packet;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.entity.Turret;
import com.giantjelly.wastelands.world.Block;
import com.giantjelly.wastelands.world.Triangle;

public class MapPacket {
	
	public Block[] blocks = Game.Blocks.toArray(new Block[Game.Blocks.size()]);
	public Triangle[] triangles = Game.Triangles.toArray(new Triangle[Game.Triangles.size()]);
	public Turret[] turrets = Game.turrets.toArray(new Turret[Game.turrets.size()]);
	
	public MapPacket() {
	}
	
}
