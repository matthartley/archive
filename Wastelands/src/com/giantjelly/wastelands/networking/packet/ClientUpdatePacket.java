package com.giantjelly.wastelands.networking.packet;

import org.lwjgl.input.Mouse;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Play;
import com.giantjelly.wastelands.networking.GameServer;

public class ClientUpdatePacket {
	
	public String username = Game.username;
	public int x = -Play.player.xscroll + Play.player.x;
	public int y = -Play.player.yscroll + Play.player.y;
	public int angle = Play.player.angle;
	public int aniFrame = Play.player.AniFrame;
	public int aniCount = Play.player.AniCount;
	public boolean flip = Play.player.flip;
	public int yGunStart = (Play.player.firstWeapon!=null) ? Play.player.firstWeapon.coords[1]/8 : 0;
	public int gunIndex = Play.player.gunindex;
	public int turretIndex = Play.player.turretIndex;
	public float armAngle = Play.player.armrotation;
	//public boolean dead = Play.dead;
	public boolean shield = Play.player.shield;
	public boolean click = Mouse.isButtonDown(0);
	public boolean footstep;
	
	public ClientUpdatePacket() {
		footstep = GameServer.footstep ? true : false;
		GameServer.footstep = false;
	}
	
}
