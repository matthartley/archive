package com.giantjelly.wastelands.networking.packet;

public class DeadPacket {
	
	public String username;
	
	public DeadPacket() {
	}
	
	public DeadPacket(String u) {
		username = u;
	}
	
}
