package com.giantjelly.wastelands;

import static java.lang.Math.atan2;
import static java.lang.Math.toDegrees;

public class Math {
	public static float Smallest(float num, float num2){
		if(num <= num2)
			return num;
		else
			return num2;
	}
	
	public static int Positive(int num){
		if(num >= 0)
			return num;
		else
			return num * -1;
	}
	
	public static double postoangle(double x, double y){
		return atan2(x, y);
	}
	
	public static double degrees(double rads){
		return toDegrees(rads);
	}
}
