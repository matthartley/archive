package com.giantjelly.wastelands;

import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.opengl.Display;
import com.giantjelly.wastelands.gfx.Sprite;

public class Sky {
	
	public static int ySky = 0;
	public static byte[] pixels;
	public static int r = 0, g = 0, b = 0;
	
	public static void update(){
		Clock.update();
		ySky = (int) -( ((float)Clock.minutes/1440f) * ((4096f*8f) - (float)Display.getHeight()) );
	}
	
	public static void draw() {
		glEnable(GL_TEXTURE_2D);
		Sprite.Cycle.bind();
		
		int x = 0;
		int xs = Display.getWidth();
		int ys = 4096*8;
		
		glBegin(GL_QUADS);
			glTexCoord2f(0, 0);		glVertex3f(x, 			ySky, 				8.0f/10.0f);
			glTexCoord2f(1, 0);		glVertex3f(x + xs, ySky, 				8.0f/10.0f);
			glTexCoord2f(1, 1);		glVertex3f(x + xs, ySky + ys, 			8.0f/10.0f);
			glTexCoord2f(0, 1);		glVertex3f(x, 			ySky + ys, 	8.0f/10.0f);
		glEnd();
	}
	
}
