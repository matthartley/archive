package com.giantjelly.wastelands.world;

import java.util.ArrayList;
import java.io.*;

import com.giantjelly.wastelands.world.Block;
import com.giantjelly.wastelands.world.Triangle;
import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.entity.Turret;
import com.giantjelly.wastelands.entity.Gun;
import com.giantjelly.wastelands.entity.PlasmaPistol;
import com.giantjelly.wastelands.entity.BlasterPistol;
import com.giantjelly.wastelands.entity.RocketLauncher;
import com.giantjelly.wastelands.gfx.Sprite;
import com.giantjelly.wastelands.gui.Button;

public class IO {
	public static ArrayList<String> Maps = new ArrayList<String>();
	public static Button[] maps;
	public static String CurrentMap = "";
	
	public static void LoadMapNames(){
		Maps.removeAll(Maps);
		
		File[] Files = new File("res/maps").listFiles();
		
		for(File File : Files){
			if(File.isFile()){
				if(File.getName().endsWith(".dat")){
					Maps.add(File.getName());
				}
			}
		}
		
		maps = new Button[Maps.size()];
		for(int i = 0; i < maps.length; i++){
			maps[i] = new Button(256, 64, Maps.get(i));
		}
	}
	
	public static void save(){
		try{
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(new File("res/maps/" + CurrentMap)));
			
			for(Block Block : Game.Blocks){
				dos.writeUTF("block");
				
				dos.writeInt(Block.x);
				dos.writeInt(Block.y);
				dos.writeInt(Block.layer);
				
				dos.writeInt(Block.coords[0]);
				dos.writeInt(Block.coords[1]);
				dos.writeInt(Block.coords[2]);
				dos.writeInt(Block.coords[3]);
			}
			
			for(Triangle Triangle : Game.Triangles){
				dos.writeUTF("triangle");
				
				dos.writeInt(Triangle.x);
				dos.writeInt(Triangle.y);
				dos.writeInt(Triangle.layer);
				dos.writeInt(Triangle.side);
				
				dos.writeInt(Triangle.coords[0]);
				dos.writeInt(Triangle.coords[1]);
				dos.writeInt(Triangle.coords[2]);
				dos.writeInt(Triangle.coords[3]);
			}
			
			for(Turret usable : Game.turrets){
				dos.writeUTF("usable");
				
				dos.writeInt(usable.x);
				dos.writeInt(usable.y);
			}
			
			/*for(Gun pickup : Game.Guns){
				dos.writeUTF("pickup");
				
				dos.writeInt(pickup.x);
				dos.writeInt(pickup.y);
				
				dos.writeInt(pickup.coords[0]);
				dos.writeInt(pickup.coords[1]);
				dos.writeInt(pickup.coords[2]);
				dos.writeInt(pickup.coords[3]);
			}*/
			
			dos.close();
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	public static void load(){
		try{
			DataInputStream dis = new DataInputStream(new FileInputStream(new File("res/maps/" + CurrentMap)));
			
			while(dis.available() != 0){
				String a = dis.readUTF();
				
				if(a.equals("block"))
					Game.Blocks.add(new Block(dis.readInt(), dis.readInt(), dis.readInt(), /*new Sprite(Sprite.TileSheet,*/ new int[]{ dis.readInt(), dis.readInt(), dis.readInt(), dis.readInt() }));
				if(a.equals("triangle"))
					Game.Triangles.add(new Triangle(dis.readInt(), dis.readInt(), dis.readInt(), dis.readInt(), /*new Sprite(Sprite.TileSheet,*/ new int[]{ dis.readInt(), dis.readInt(), dis.readInt(), dis.readInt() }));
				if(a.equals("usable"))
					Game.turrets.add(new Turret(dis.readInt(), dis.readInt()));
				/*if(a.equals("pickup")){
					int[] ints = new int[]{ dis.readInt(), dis.readInt(), dis.readInt(), dis.readInt(), dis.readInt(), dis.readInt() };
					if(ints[3]==0){
						Game.Guns.add(new BlasterPistol(ints[0], ints[1]));
					}
					if(ints[3]==8){
						Game.Guns.add(new PlasmaPistol(ints[0], ints[1]));
					}
					if(ints[3]==16){
						Game.Guns.add(new RocketLauncher(ints[0], ints[1]));
					}
				}*/
			}
			
			dis.close();
		}catch(EOFException e){
			Game.log("File loaded");
		}
		catch(IOException e){
			System.out.println(e+" - Loading error!");
		}
	}
	
	public static void clear(){
		while(Game.Blocks.size() > 0)
			Game.Blocks.remove(0);
		
		while(Game.Triangles.size() > 0)
			Game.Triangles.remove(0);
		
		/*while(Game.Guns.size() > 0)
			Game.Guns.remove(0);*/
		
		while(Game.turrets.size() > 0)
			Game.turrets.remove(0);
	}
}
