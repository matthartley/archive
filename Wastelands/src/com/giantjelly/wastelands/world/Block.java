package com.giantjelly.wastelands.world;

import com.giantjelly.wastelands.gfx.Sprite;

import java.io.Serializable;

import com.giantjelly.wastelands.Math;
import com.giantjelly.wastelands.Game;

import static org.lwjgl.opengl.GL11.*;

public class Block implements Serializable {
	public int x;
	public int y;
	public int layer;
	public int[] coords;
	
	public Block() {
	}
	
	public Block(int x, int y, int layer, int[] ints){
		this.x = x;
		this.y = y;
		this.layer = layer;
		this.coords = ints;
	}
	
	public void draw(int x, int y){
		Sprite.draw(Sprite.TileSheet, coords, new float[]{ this.x + x, this.y + y, this.layer, 4 });
	}
}
