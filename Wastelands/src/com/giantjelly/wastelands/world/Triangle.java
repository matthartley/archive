package com.giantjelly.wastelands.world;

import com.giantjelly.wastelands.gfx.Sprite;

public class Triangle extends Block{
	public int side;
	
	public Triangle() {
	}
	
	public Triangle(int x, int y, int layer, int side, int[] ints) {
		super(x, y, layer, ints);
		this.side = side;
	}

	public void draw(int x, int y){
		Sprite.draw(Sprite.TileSheet, coords, new float[]{ this.x + x, this.y + y, this.layer, 4 }, side);
	}
}
