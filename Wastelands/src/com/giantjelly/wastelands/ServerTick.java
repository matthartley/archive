package com.giantjelly.wastelands;

import java.io.IOException;

import com.giantjelly.wastelands.entity.Animation;
import com.giantjelly.wastelands.networking.GameServer;
import com.giantjelly.wastelands.networking.packet.SoundPacket;

import static com.giantjelly.wastelands.Play.*;

public class ServerTick {
	
	public static void tick() {
		for(int i = 0; i < Game.turrets.size(); i++){
			Game.turrets.get(i).update(i);
		}
		for(int i = 0; i < Game.Guns.size(); i++){
			if(i<Game.Guns.size()) Game.Guns.get(i).update(i);
		}
		for(int i = 0; i < Play.Bullets.size(); i++){
			Play.Bullets.get(i).tick();
			if(Play.Bullets.get(i).cooldown < 0){
				Play.Bullets.remove(i);
				i--;
			}
		}
		for(int i = 0; i < Game.animations.size(); i++){
			Game.animations.get(i).tick();
			if(!Game.animations.get(i).active){
				Game.animations.remove(i);
				i--;
			}
		}
		
		GameServer.sendUpdate();
		
		//if(serverRestart) serverRestart = false;
		
		for(int j = 0; j < Play.players.size(); j++){
			if(!Play.players.get(j).dead){
				/*for(int i = 0; i < Game.animations.size(); i++){
					if(Game.animations.get(i).ticks == 1){
						if(Game.animations.get(i).distance(Play.players.get(j).packet.x, Play.players.get(j).packet.y)){
							Play.players.get(j).health -= 5;
							if(Play.players.get(j).health <= 0) ownerKill(Game.animations.get(i).owner);
						}
					}
				}*/
				
				for(int i = 0; i < Bullets.size(); i++){
					if( Bullets.get(i).testcollision(Play.players.get(j).packet.x, Play.players.get(j).packet.y) ){
						if(!Play.players.get(j).packet.shield){
							Bullets.get(i).spark();
							Play.players.get(j).health -= Bullets.get(i).damage;
							if(Play.players.get(j).health <= 0) if(!Bullets.get(i).owner.equals(Play.players.get(j).packet.username)) ownerKill(Bullets.get(i).owner);
							Bullets.remove(i);
						}else{
							Bullets.get(i).xsd *= -1;
							Bullets.get(i).ysd *= -1;
							Bullets.get(i).owner = players.get(j).packet.username;
						}
					}
				}
			}
			
			/*int af = Play.players.get(j).packet.aniFrame;
			int ac = Play.players.get(j).packet.aniCount;
			if( ac == 1 && ( af == 8 || af == 15 ) ){
				Game.footstep.play();
				//GameServer.server.sendToAllTCP(new SoundPacket("footstep"));
			}*/
		}
			
			
		if(!Play.dead){
			/*for(int i = 0; i < Game.animations.size(); i++){
				if(Game.animations.get(i).ticks == 1){
					if(Game.animations.get(i).distance(player.x-player.xscroll, player.y-player.yscroll)){
						Play.health -= 5;
						if(Play.health <= 0) ownerKill(Game.animations.get(i).owner);
					}
				}
			}*/
			
			for(int i = 0; i < Bullets.size(); i++){
				if( Bullets.get(i).testcollision(Play.player.x-player.xscroll, Play.player.y-player.yscroll) ){
					if(!Play.player.shield){
						Bullets.get(i).spark();
						Play.health -= Bullets.get(i).damage;
						if(Play.health <= 0) if(!Bullets.get(i).owner.equals(Game.username)) ownerKill(Bullets.get(i).owner);
						Bullets.remove(i);
					}else{
						Bullets.get(i).xsd *= -1;
						Bullets.get(i).ysd *= -1;
						Bullets.get(i).owner = Game.username;
					}
				}
			}
		}
		
		if(Play.health <= 0 && !Play.dead){
			Game.die();
		}
		for(int i = 0; i < players.size(); i++){
			if(players.get(i).health <= 0 && !players.get(i).dead){
				GameServer.playerDead(players.get(i).packet.username);
				players.get(i).dead = true;
			}
		}
		
		if(Game.matchRunning){
			if(kills >= Game.scoreLimit){
				//Game.log(Game.username + " has won! restarting game...");
				//Game.resetServerGame();
				Game.matchWinner = Game.username;
				GameServer.end();
			}
			for(int i = 0; i < players.size(); i++){
				if(players.get(i).kills >= Game.scoreLimit){
					//Game.log(players.get(i).packet.username + " has won! restarting game...");
					//Game.resetServerGame();
					Game.matchWinner = players.get(i).packet.username;
					GameServer.end();
				}
			}
		}
	}
	
}
