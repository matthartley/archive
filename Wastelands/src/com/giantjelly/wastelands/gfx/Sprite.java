package com.giantjelly.wastelands.gfx;

import static org.lwjgl.opengl.GL11.*;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;
import java.io.IOException;

import com.giantjelly.wastelands.Game;
import com.giantjelly.wastelands.Sky;

public class Sprite {
	public static Texture TileSheet;
	public static Texture PlayerSheet;
	public static Texture WeaponSheet;
	public static Texture Cycle;
	public static Texture Tank;
	public static Texture Particles;
	public static Texture Usables;
	public static Texture Pickups;
	public static Texture Gui;
	public static Texture SplashScreen;
	public static Texture Buttons;
	public static Texture deadmessage;
	public static Texture animations;
	public static Texture shield;
	public static Texture cursor;
	public static Texture highlights;
	public static Texture random;
	public static Texture wastelands;
	public static Texture terminalTitle;
	
	public static int[][] tiles;
	public static int[][] pickups;
	
	public static Texture loadTexture(String file) {
		if(!ResourceLoader.resourceExists(file)){
			Game.log(file + " was not found!");
			return null;
		}
		
		try {
			return TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(file), GL_NEAREST);
		} catch (IOException e) {
			Game.log("Unable to load " + file);
		}
		
		return null;
	}
	
	public static void Load(){
		TileSheet = loadTexture("res/textures/Tiles.png");
		PlayerSheet = loadTexture("res/textures/Player.png");
		WeaponSheet = loadTexture("res/textures/WeaponSheet.png");
		Cycle = loadTexture("res/textures/cyclenew2.png");
		Tank = loadTexture("res/textures/Tank.png");
		Particles = loadTexture("res/textures/Particles.png");
		Usables = loadTexture("res/textures/UsablesNew.png");
		Pickups = loadTexture("res/textures/Pickups.png");
		Gui = loadTexture("res/textures/Gui.png");
		SplashScreen = loadTexture("res/textures/SplashScreenPixelated.png");
		Buttons = loadTexture("res/textures/Buttons.png");
		deadmessage = loadTexture("res/textures/deadmessage.png");
		animations = loadTexture("res/textures/Animations.png");
		shield = loadTexture("res/textures/Shield.png");
		cursor = loadTexture("res/textures/Cursor.png");
		highlights = loadTexture("res/textures/Highlights.png");
		random = loadTexture("res/textures/Rand.png");
		wastelands = loadTexture("res/textures/Wastelands.png");
		terminalTitle = loadTexture("res/textures/TerminalTitle.png");
		
		/*try{
			TileSheet = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Tiles.png"), GL_NEAREST);
			PlayerSheet = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Player.png"), GL_NEAREST);
			WeaponSheet = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/WeaponSheet.png"), GL_NEAREST);
			Cycle = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/cyclenew2.png"), GL_NEAREST);
			Tank = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Tank.png"), GL_NEAREST);
			Particles = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Particles.png"), GL_NEAREST);
			Usables = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/UsablesNew.png"), GL_NEAREST);
			Pickups = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Pickups.png"), GL_NEAREST);
			Gui = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Gui.png"), GL_NEAREST);
			SplashScreen = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/SplashScreenPixelated.png"), GL_NEAREST);
			Buttons = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Buttons.png"), GL_NEAREST);
			deadmessage = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/deadmessage.png"), GL_NEAREST);
			
			animations = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Animations.png"), GL_NEAREST);
			shield = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Shield.png"), GL_NEAREST);
			
			cursor = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Cursor.png"), GL_NEAREST);
			
			highlights = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Highlights.png"), GL_NEAREST);
			
			random = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Rand.png"), GL_NEAREST);
			wastelands = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Wastelands.png"), GL_NEAREST);
			
			terminalTitle = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/TerminalTitle.png"), GL_NEAREST);
		}catch(IOException e){
			System.out.println(e);
		}*/
		
		//System.out.println("Loading done!");
		
		Sky.pixels = Cycle.getTextureData();
		
		tiles = new int[][]{
			{0, 0, 64, 64},
			{64, 0, 64, 32},
			{128, 0, 64, 16},
			{192, 0, 64, 8},
			
			{256, 0, 32, 64},
			{320, 0, 32, 32},
			{384, 0, 32, 16},
			{448, 0, 32, 8},
			
			{512, 0, 16, 64},
			{576, 0, 16, 32},
			{640, 0, 16, 16},
			{704, 0, 16, 8},
			
			{768, 0, 8, 64},
			{832, 0, 8, 32},
			{896, 0, 8, 16},
			{960, 0, 8, 8},
			
			
			{0, 64, 64, 64},
			{64, 64, 64, 32},
			{128, 64, 64, 16},
			{192, 64, 64, 8},
			
			{256, 64, 32, 64},
			{320, 64, 32, 32},
			{384, 64, 32, 16},
			{448, 64, 32, 8},
			
			{512, 64, 16, 64},
			{576, 64, 16, 32},
			{640, 64, 16, 16},
			{704, 64, 16, 8},
			
			{768, 64, 8, 64},
			{832, 64, 8, 32},
			{896, 64, 8, 16},
			{960, 64, 8, 8},
			
			
			
			{0, 128, 64, 64},
			{64, 128, 64, 32},
			{128, 128, 64, 16},
			{192, 128, 64, 8},
			
			{256, 128, 32, 64},
			{320, 128, 32, 32},
			{384, 128, 32, 16},
			{448, 128, 32, 8},
			
			{512, 128, 16, 64},
			{576, 128, 16, 32},
			{640, 128, 16, 16},
			{704, 128, 16, 8},
			
			{768, 128, 8, 64},
			{832, 128, 8, 32},
			{896, 128, 8, 16},
			{960, 128, 8, 8},
			
			
			{0, 192, 64, 64},
			{64, 256, 32, 32}
		};
		
		
		pickups = new int[][]{
				{0, 0, 16, 8},
				{0, 8, 16, 8},
				{0, 16, 16, 8}
		};
	}
	
	public static void draw(Texture texture, int[] coords, float[] pos){
		glEnable(GL_TEXTURE_2D);
		texture.bind();
		
		glBegin(GL_QUADS);
			glTexCoord2f((float)coords[0] / texture.getImageWidth(), (float)coords[1] / texture.getImageHeight());
			glVertex3f(pos[0], pos[1], pos[2]/10.0f);
			
			glTexCoord2f((float)(coords[0] + coords[2]) / texture.getImageWidth(), (float)coords[1] / texture.getImageHeight());
			glVertex3f(pos[0] + coords[2]*pos[3], pos[1], pos[2]/10.0f);
			
			glTexCoord2f((float)(coords[0] + coords[2]) / texture.getImageWidth(), (float)(coords[1] + coords[3]) / texture.getImageHeight());
			glVertex3f(pos[0] + coords[2]*pos[3], pos[1] + coords[3]*pos[3], pos[2]/10.0f);
			
			glTexCoord2f((float)coords[0] / texture.getImageWidth(), (float)(coords[1] + coords[3]) / texture.getImageHeight());
			glVertex3f(pos[0], pos[1] + coords[3]*pos[3], pos[2]/10.0f);
		glEnd();
	}
	
	public static void draw(Texture texture, int[] coords, float[] pos, boolean flip){
		int left;
		int right;
		left = (!flip) ? 0 : coords[2];
		right = (!flip) ? coords[2] : 0;
		
		glEnable(GL_TEXTURE_2D);
		texture.bind();
		
		glBegin(GL_QUADS);
			glTexCoord2f((float)(coords[0]+left) / texture.getImageWidth(), (float)coords[1] / texture.getImageHeight());
			glVertex3f(pos[0], pos[1], pos[2]/10.0f);
			
			glTexCoord2f((float)(coords[0]+right) / texture.getImageWidth(), (float)coords[1] / texture.getImageHeight());
			glVertex3f(pos[0] + coords[2]*pos[3], pos[1], pos[2]/10.0f);
			
			glTexCoord2f((float)(coords[0]+right) / texture.getImageWidth(), (float)(coords[1] + coords[3]) / texture.getImageHeight());
			glVertex3f(pos[0] + coords[2]*pos[3], pos[1] + coords[3]*pos[3], pos[2]/10.0f);
			
			glTexCoord2f((float)(coords[0]+left) / texture.getImageWidth(), (float)(coords[1] + coords[3]) / texture.getImageHeight());
			glVertex3f(pos[0], pos[1] + coords[3]*pos[3], pos[2]/10.0f);
		glEnd();
	}
	
	public static void draw(Texture texture, int[] coords, float[] pos, int blar){
		glEnable(GL_TEXTURE_2D);
		texture.bind();
		
		glBegin(GL_TRIANGLES);
			glTexCoord2f((float)(coords[0] + (coords[2])*blar) / texture.getImageWidth(), (float)coords[1] / texture.getImageHeight());
			glVertex3f(pos[0] + (coords[2]*pos[3])*blar, pos[1], pos[2]/10.0f);
			
			glTexCoord2f((float)(coords[0] + coords[2]) / texture.getImageWidth(), (float)(coords[1] + coords[3]) / texture.getImageHeight());
			glVertex3f(pos[0] + coords[2]*pos[3], pos[1] + coords[3]*pos[3], pos[2]/10.0f);
			
			glTexCoord2f((float)coords[0] / texture.getImageWidth(), (float)(coords[1] + coords[3]) / texture.getImageHeight());
			glVertex3f(pos[0], pos[1] + coords[3]*pos[3], pos[2]/10.0f);
		glEnd();
	}
	
	/*public Texture texture;
	public int x;
	public int y;
	public int xs;
	public int ys;
	
	public Sprite(Texture t, int x, int y, int xs, int ys){
		this.texture = t;
		this.x = x;
		this.y = y;
		this.xs = xs;
		this.ys = ys;
	}
	
	public void draw(int x, int y, float z, int s){
		glEnable(GL_TEXTURE_2D);
		this.texture.bind();
		
		glBegin(GL_QUADS);
			glTexCoord2f((float)this.x / this.texture.getImageWidth(), (float)this.y / this.texture.getImageHeight());		glVertex3f(x, 			y, 				z/10.0f);
			glTexCoord2f((float)(this.x + this.xs) / this.texture.getImageWidth(), (float)this.y / this.texture.getImageHeight());		glVertex3f(x + this.xs * s, y, 				z/10.0f);
			glTexCoord2f((float)(this.x + this.xs) / this.texture.getImageWidth(), (float)(this.y + this.ys) / this.texture.getImageHeight());		glVertex3f(x + this.xs * s, y + this.ys * s, 	z/10.0f);
			glTexCoord2f((float)this.x / this.texture.getImageWidth(), (float)(this.y + this.ys) / this.texture.getImageHeight());		glVertex3f(x, 			y + this.ys * s, 	z/10.0f);
		glEnd();
	}
	
	public void draw(int x, int y, float z, float s){
		glEnable(GL_TEXTURE_2D);
		this.texture.bind();
		
		glBegin(GL_QUADS);
			glTexCoord2f((float)this.x / this.texture.getImageWidth(), (float)this.y / this.texture.getImageHeight());		glVertex3f(x, 			y, 				z/10.0f);
			glTexCoord2f((float)(this.x + this.xs) / this.texture.getImageWidth(), (float)this.y / this.texture.getImageHeight());		glVertex3f(x + this.xs * s, y, 				z/10.0f);
			glTexCoord2f((float)(this.x + this.xs) / this.texture.getImageWidth(), (float)(this.y + this.ys) / this.texture.getImageHeight());		glVertex3f(x + this.xs * s, y + this.ys * s, 	z/10.0f);
			glTexCoord2f((float)this.x / this.texture.getImageWidth(), (float)(this.y + this.ys) / this.texture.getImageHeight());		glVertex3f(x, 			y + this.ys * s, 	z/10.0f);
		glEnd();
	}
	
	public void DrawTriangle(int x, int y, float z, int s, int side){
		glEnable(GL_TEXTURE_2D);
		this.texture.bind();
		//glColor4f(1, 1, 1, 1);
		
		glBegin(GL_TRIANGLES);
			if(side == 0){
				glTexCoord2f((float)this.x / this.texture.getImageWidth(), (float)this.y / this.texture.getImageHeight());		glVertex3f(x, 			y, 				z/10.0f);
			}else{
				glTexCoord2f((float)(this.x + this.xs) / this.texture.getImageWidth(), (float)this.y / this.texture.getImageHeight());		glVertex3f(x + this.xs * s, y, 				z/10.0f);
			}
				
			glTexCoord2f((float)(this.x + this.xs) / this.texture.getImageWidth(), (float)(this.y + this.ys) / this.texture.getImageHeight());		glVertex3f(x + this.xs * s, y + this.ys * s, 	z/10.0f);
			glTexCoord2f((float)this.x / this.texture.getImageWidth(), (float)(this.y + this.ys) / this.texture.getImageHeight());		glVertex3f(x, 			y + this.ys * s, 	z/10.0f);
		glEnd();
	}
	
	
	
	public void drawwithlight(int x, int y, float z, int s){
		glEnable(GL_TEXTURE_2D);
		this.texture.bind();
		
		glBegin(GL_QUADS);
			glTexCoord2f((float)this.x / this.texture.getImageWidth(), (float)this.y / this.texture.getImageHeight());
			
			light(x, y); glVertex3f(x, y, z/10.0f);
			
			glTexCoord2f((float)(this.x + this.xs) / this.texture.getImageWidth(), (float)this.y / this.texture.getImageHeight());
			
			light(x + this.xs * s, y); glVertex3f(x + this.xs * s, y, z/10.0f);
			
			glTexCoord2f((float)(this.x + this.xs) / this.texture.getImageWidth(), (float)(this.y + this.ys) / this.texture.getImageHeight());
			
			light(x + this.xs * s, y + this.ys * s); glVertex3f(x + this.xs * s, y + this.ys * s, z/10.0f);
			
			glTexCoord2f((float)this.x / this.texture.getImageWidth(), (float)(this.y + this.ys) / this.texture.getImageHeight());
			
			light(x, y + this.ys * s); glVertex3f(x, y + this.ys * s, z/10.0f);
		glEnd();
	}
	
	public void DrawTrianglewithlight(int x, int y, float z, int s, int side){
		glEnable(GL_TEXTURE_2D);
		this.texture.bind();
		//glColor4f(1, 1, 1, 1);
		
		glBegin(GL_TRIANGLES);
			if(side == 0){
				glTexCoord2f((float)this.x / this.texture.getImageWidth(), (float)this.y / this.texture.getImageHeight());
				
				light(x, 			y); glVertex3f(x, 			y, 				z/10.0f);
			}else{
				glTexCoord2f((float)(this.x + this.xs) / this.texture.getImageWidth(), (float)this.y / this.texture.getImageHeight());
				
				light(x + this.xs * s, y); glVertex3f(x + this.xs * s, y, 				z/10.0f);
			}
				
			glTexCoord2f((float)(this.x + this.xs) / this.texture.getImageWidth(), (float)(this.y + this.ys) / this.texture.getImageHeight());
			
			light(x + this.xs * s, y + this.ys * s); glVertex3f(x + this.xs * s, y + this.ys * s, 	z/10.0f);
			
			glTexCoord2f((float)this.x / this.texture.getImageWidth(), (float)(this.y + this.ys) / this.texture.getImageHeight());
			
			light(x, 			y + this.ys * s); glVertex3f(x, 			y + this.ys * s, 	z/10.0f);
		glEnd();
	}
	
	public static int lightdis(int x, int y, float bx, float by) {
		int dis = Math.Positive(x-((int)bx+Play.player.xscroll)) + Math.Positive(y-((int)by+Play.player.yscroll));
		return dis;
	}
	
	public static void light(int x, int y) {
		float r = (float)( Math.Positive(Game.sky.r) + Math.Positive(Game.sky.g) + Math.Positive(Game.sky.b) ) / 217;
		float g = (float)( Math.Positive(Game.sky.r) + Math.Positive(Game.sky.g) + Math.Positive(Game.sky.b) ) / 217;
		float b = (float)( Math.Positive(Game.sky.r) + Math.Positive(Game.sky.g) + Math.Positive(Game.sky.b) ) / 217;
		
		int dis = 100;
		
		if(Play.Bullets.size() > 0)
			dis = lightdis(x, y, Play.Bullets.get(0).x, Play.Bullets.get(0).y);
		else if(Play.Particles.size() > 0)
			dis = lightdis(x, y, Play.Particles.get(0).x, Play.Particles.get(0).y);
		
		for(int i = 0; i < Play.Bullets.size(); i++){
			if(lightdis(x, y, Play.Bullets.get(i).x, Play.Bullets.get(i).y) < dis)
				dis = lightdis(x, y, Play.Bullets.get(i).x, Play.Bullets.get(i).y);
		}
		
		for(int i = 0; i < Play.Particles.size(); i++){
			if(lightdis(x, y, Play.Particles.get(0).x, Play.Particles.get(0).y) < dis)
				dis = lightdis(x, y, Play.Particles.get(0).x, Play.Particles.get(0).y);
		}
		
		try{
			dis = lightdis(x, y, Play.Bullets.get(0));
		}catch(Exception e){}
		
		int light = 1;
		try{
			light = 200/dis;
		}catch(Exception e){}
		
		if(light*light > r*r && (Play.Bullets.size() > 0 || Play.Particles.size() > 0))
			glColor4f(light*light, light*light, light*light, 1);
		else
			glColor4f( r*r, g*g, b*b, 1);
		
	}*/
}
