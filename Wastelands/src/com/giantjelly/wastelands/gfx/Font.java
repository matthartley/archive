package com.giantjelly.wastelands.gfx;

import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.Color;
import static org.lwjgl.opengl.GL11.*;

public class Font extends TrueTypeFont{
	public Font(java.awt.Font font, boolean antiAlias) {
		super(font, antiAlias);
	}

	public static Font small = new Font(new java.awt.Font("monospaced", java.awt.Font.PLAIN, 16), true);
	public static Font font = new Font(new java.awt.Font("monospaced", java.awt.Font.PLAIN, 24), true);
	public static Font fancy = new Font(new java.awt.Font("arial", java.awt.Font.BOLD, 24), true);
	public static Font error = new Font(new java.awt.Font("monospaced", java.awt.Font.ITALIC, 40), true);
	
	public void Draw(int x, int y, String text){
		glEnable(GL_TEXTURE_2D);
		this.drawString(x, y, text, Color.white);
	}
}
