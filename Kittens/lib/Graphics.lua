Graphics = {
	
	

}

Graphics.TEXTURE = gl:loadtexture(respath .. "Font.png")
--Graphics.font = gl:loadtexture(respath .. "Font.png")

function Graphics.loadtexture(file)
	return gl:loadtexture(respath .. file)
end

function Graphics.draw(coords, color)
	gl:enabletextures(false);
	gl:color(color.r, color.g, color.b, color.a);

	gl:BEGIN();
		gl:vertex(coords.x1, coords.y1, 0);
		gl:vertex(coords.x2, coords.y2, 0);
		gl:color(50/255, 20/255, 0)
		gl:vertex(coords.x3, coords.y3, 0);
		gl:vertex(coords.x4, coords.y4, 0);
	gl:END();
end

function Graphics.drawsprite(coords, pos)
	gl:enabletextures(true);
	--texture:bind();
	gl:bindtexture(Graphics.TEXTURE);
	--gl:color(1, 0, 1, 1);

	local width = Graphics.TEXTURE:getImageWidth();
	local height = Graphics.TEXTURE:getImageHeight();

	local left = coords[1]/width
	local right = (coords[1]+coords[3])/width

	if pos[5] ~= nil then
		if pos[5] then
			left = right
			right = coords[1]/width
		end
	end

	gl:BEGIN()

		gl:texcoord( left, coords[2]/height )
		gl:vertex( pos[1], pos[2], pos[3] )

		gl:texcoord( right, coords[2]/height )
		gl:vertex( pos[1]+coords[3]*pos[4], pos[2], pos[3] )

		gl:texcoord( right, (coords[2]+coords[4])/height )
		gl:vertex( pos[1]+coords[3]*pos[4], pos[2]+coords[4]*pos[4], pos[3] )

		gl:texcoord( left, (coords[2]+coords[4])/height )
		gl:vertex( pos[1], pos[2]+coords[4]*pos[4], pos[3] )

	gl:END()

	--gl:BEGIN();
	--gl:END();
end

function Graphics.drawtile(size, tile, pos)
	local tiley = math.floor((tile*size)/Graphics.TEXTURE:getImageWidth())*size
	local tilex = (tile*size)%Graphics.TEXTURE:getImageWidth()

	Graphics.drawsprite({  tilex,  tiley,  size,  size }, pos)
end

function Graphics.color(color)
	gl:color(color[1], color[2], color[3], color[4])
end