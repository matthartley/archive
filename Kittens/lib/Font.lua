Font = {}

Font.SMALL = 1;
Font.MEDIUM = 2;
Font.LARGE = 4;

Font.characters = "abcdefghijklmnopqrstuvwxyz 1234567890-"

function Font.draw(size, text, xx, yy)
	for i=0,Text:stringlength(text)-1 do
		Graphics.drawtile(8, Text:characterindex(Font.characters, string.lower(text), i), { (xx+(8*size)*i), yy, -10, size })
		--print(Text:characterindex(Font.characters, text, i))
	end
end