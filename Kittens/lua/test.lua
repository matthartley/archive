table1 = {
	name = "one",
	number = 1,
}

table2 = {
	name = "two"
}

--print(#table)

function printtable(table)
	-- for i=1,#table do
	-- 	print(table[i])
	-- end

	for i,v in ipairs(table) do
		print(i .. " - " .. v)
	end
end

printtable(table1)