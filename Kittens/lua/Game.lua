dofile(libpath .. "Main.lua")

xoffset = 0
yoffset = 0

score = 150

entities = {}
foods = {}

dofile(srcpath .. "Sprites.lua")
dofile(srcpath .. "Floor.lua")
dofile(srcpath .. "Entity.lua")
dofile(srcpath .. "Kitten.lua")
dofile(srcpath .. "Food.lua")
dofile(srcpath .. "Input.lua")


--frame = 0
time = math.floor(os.time()/1000)
frames = 0
framerate = 0

--kitten1 = Kitten.new(50, 50)
--kitten2 = Kitten.new(200, 200)
--food = Food.new(100, 100)


for i=1,2 do
	-- table.insert(entities, Kitten.new((math.random()*1000)+250, (math.random()*1000)+250))
	table.insert(entities, Kitten.new( 400, 200 ))
end

-- for i=1,8 do
-- 	table.insert(foods, Food.new((math.random()*1000)+250, (math.random()*1000)+250))
-- end

function render()
		
	Input.render()

	Floor.render()

	--food.render()

	--kitten1.render()
	--kitten2.render()

	for i,kitten in ipairs(entities) do
		kitten.render()
	end

	for i,food in ipairs(foods) do
		food.render()
	end

	frames = frames + 1

	if math.floor(os.time()/1000) - time >= 1 then
		time = math.floor(os.time()/1000)
		--print("frames " .. frames)
		framerate = frames
		frames = 0
	end
	
	--frame = frame + 0.3
	--if frame >= 8 then frame = 0 end

	--Graphics.TEXTURE = Sprites.kitten
	--Graphics.drawsprite({ x=math.floor(frame)*32, y=0, width=32, height=32 }, { x=100, y=100, z=0, size=8 })
	
	Graphics.TEXTURE = Sprites.font
	Font.draw(Font.LARGE, "Kittens - The Game", 10, 10)
	Font.draw(Font.LARGE, "Score - " .. score, 10, 10+48)
	Font.draw(Font.LARGE, "Kittens - " .. #entities, 10, 10+96)

end

Main.start(render)