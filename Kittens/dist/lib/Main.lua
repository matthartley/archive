Display = luajava.newInstance("com.giantjelly.jellymoon.Display");
dm = luajava.newInstance("org.lwjgl.opengl.DisplayMode", 1280, 720);
gl = luajava.newInstance("com.giantjelly.jellymoon.gl");
Text = luajava.newInstance("com.giantjelly.jellymoon.text");
INPUT = luajava.newInstance("com.giantjelly.jellymoon.Input");

dofile("config.lua")

Display:createdisplay(dm, fullscreen);

dofile(libpath .. "Graphics.lua");
dofile(libpath .. "Font.lua");
dofile(libpath .. "Input.lua")

function clear()
	gl:clear()
end

function update()
	Display:update()
	Display:sync(fpstarget)
end

function running()
	if Display:iscloserequested() then
		return false
	else
		return true
	end
end

Main = {
	
}

function Main.start(func)
	while running()==true do
		clear()

		func()

		update()
	end
end