-- class Entity

Entity = {}

function Entity.new(x, y)
	local self = {}

	self.x = x
	self.y = y

	self.move = function(xd, yd)
		self.x = self.x + xd
		self.y = self.y + yd
	end

	self.getx = function()
		return self.x + xoffset
	end

	self.gety = function()
		return self.y + yoffset
	end

	return self
end