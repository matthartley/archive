-- class Kitten extends Entity

Kitten = {}

ID = 1

function Kitten.new(x, y)
	local self = Entity.new(x, y)

	self.id = "id " .. ID
	ID = ID + 1

	self.frame = 0
	self.framecount = 0
	self.movewait = 120
	self.moving = true
	self.hori = 1
	self.vert = 1

	self.hunger = 50
	self.ready = false

	self.animate = function()
		self.framecount = self.framecount + 1
		if self.framecount >= 3 then
			self.framecount = 0
			self.frame = self.frame + 1
		end
		if self.frame >= 8 then
			self.frame = 0
		end
	end

	self.movement = function()
		--if self.stillwait > 0 then self.stillwait = self.stillwait - 1 end
		if self.movewait > 0 then self.movewait = self.movewait - 1 end

		-- if self.movewait <= 0 then
		-- 	self.stillwait = 120
		-- end

		if self.movewait <= 0 then
			if self.moving then
				self.moving = false
			else
				self.moving = true
			end

			if self.moving then
				rand = math.random()
				rand2 = math.random()

				if rand > 0.5 then self.hori = 1 else self.hori = -1 end
				if rand2 > 0.5 then self.vert = 1 else self.vert = -1 end
			end

			local rand = math.random()*120
			print(rand)
			self.movewait = rand
		end

		if self.movewait > 0 and self.moving then
			self.move(self.hori*2, self.vert*2)
		end
	end

	self.collision = function()
		local floorsize = 1536

		if self.x < 0 then self.x = 0 end
		if self.x + 128 > floorsize then self.x = floorsize - 128 end
		if self.y < 0 then self.y = 0 end
		if self.y + 128 > floorsize then self.y = floorsize - 128 end
	end

	self.render = function()
		local getfood = false

		if self.hunger > 0 then
			for i,food in ipairs(foods) do
				if food.food > 0 then
					--self.moving = false
					getfood = true
					if self.x > food.x +62 then self.move(-2, 0) self.hori = -1 elseif self.x < food.x -62 then self.move(2, 0) self.hori = 1 end
					if self.y > food.y +62 then self.move(0, -2) self.vert = -1 elseif self.y < food.y -62 then self.move(0, 2) self.vert = 1 end

					if self.x > food.x -64 and self.x < food.x +64 and self.y > food.y -64 and self.y < food.y +64 and self.hunger > 0 then
						--Graphics.TEXTURE = Sprites.kitten
						--Graphics.drawsprite({ 96, 64, 32, 32 }, { self.getx()+48, self.gety() -32, -(self.y/1000), 4 })
						self.hunger = self.hunger - 1
						food.food = food.food - 1
					end

					break
				end
			end
		end

		if self.hunger <= 0 then
			local asd = math.random()
			if asd > 0 and asd < 0.2 then self.ready = true end

			if self.ready then
				for i,kitty in ipairs(entities) do
					if kitty.hunger <= 0 and kitty.id ~= self.id then
						getfood = true

						if self.x > kitty.x +62 then self.move(-2, 0) self.hori = -1 elseif self.x < kitty.x -62 then self.move(2, 0) self.hori = 1 end
						if self.y > kitty.y +62 then self.move(0, -2) self.vert = -1 elseif self.y < kitty.y -62 then self.move(0, 2) self.vert = 1 end

						if self.x > kitty.x -64 and self.x < kitty.x +64 and self.y > kitty.y -64 and self.y < kitty.y +64 then
							self.hunger = 50
							self.ready = false
							kitty.hunger = 50
							kitty.ready = false
							table.insert(entities, Kitten.new( self.x, self.y ))
							score = score + 150
						end

						break
					end
				end
			end
		end

		if self.moving or getfood then self.animate() else self.frame = 5 end
		if getfood == false then self.movement() end

		self.collision()

		Graphics.TEXTURE = Sprites.kitten
		local flip
		if self.hori > 0 then flip = false else flip = true end
		Graphics.drawsprite({ math.floor(self.frame)*32, 0, 32, 32 }, { self.getx(), self.gety(), -(self.y/1000), 4, flip })

		if self.hunger <= 0 and self.ready then
			Graphics.drawsprite({ 64, 64, 32, 32 }, { self.getx()+48, self.gety() -32, -(self.y/1000), 4 })
		end
	end

	print(self.x)
	print(self.y)
	print(self.frame)
	print(self.framecount)

	return self
end