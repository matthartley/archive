-- class Food extends Entity

Food = {}

function Food.new(x, y)
	local self = Entity.new(x, y)

	self.food = 100

	self.render = function()
		local empty = 0
		if self.food <= 0 then empty = 32 end

		Graphics.TEXTURE = Sprites.kitten
		Graphics.drawsprite({ empty, 64, 32, 32 }, { self.getx(), self.gety(), -(self.y/1000), 4 })
	end

	return self
end