package com.giantjelly.spaceshapes;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import static com.giantjelly.spaceshapes.Game.WIDTH;
import static com.giantjelly.spaceshapes.Game.HEIGHT;

public class Font {
	public static int TILESIZE = 8;
	public static int XTILES = 8;
	
	private BufferedImage sheet;
	private int width, height;
	private int[] pixels;
	
	public Font(String name) {
		try {
			sheet = ImageIO.read(Game.class.getResourceAsStream(name));
		} catch (IOException e) {
			System.out.println(e);
		}
		
		width = sheet.getWidth();
		height = sheet.getHeight();
		pixels = sheet.getRGB(0, 0, width, height, null, 0, width);
	}
	
	public void draw(Screen screen, int x, int y, int scale, int tile) {
		int hh = (tile/XTILES);
		int ww = (tile%XTILES);
		//System.out.println(hh+ " , "+ww);
		int Y = x/1024;
		int X = x%1024;
		
		for(int yy = 0; yy < TILESIZE*scale; yy++) {
			for(int xx = 0; xx < TILESIZE*scale; xx++){
				screen.pixels[((y+Y*((int)(TILESIZE*1.5)*scale))+yy)*WIDTH +(xx+(X))] = pixels[((hh*TILESIZE)+yy/scale)*width + ((ww*TILESIZE)+xx/scale)];
			}
		}
	}
	
	String font = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890[]().,/?<>{}:;";
	
	public void draw(Screen screen, String msg, int scale) {
		String text = msg.toUpperCase();
		
		for(int i = 0; i < text.length(); i++){
			int index = font.indexOf(text.charAt(i));
			
			if(index >= 0 && index < width*height){
				draw(screen, i*TILESIZE*scale, 0, scale, index);
			}
		}
	}
}
