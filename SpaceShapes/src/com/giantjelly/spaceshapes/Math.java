package com.giantjelly.spaceshapes;

public class Math {
	public static double positive(double num) {
		if(num >= 0)
			return num;
		else
			return num * -1;
	}
	
	public static double negative(double num) {
		if(num <= 0)
			return num;
		else
			return num * -1;
	}
	
	public static double smallest(double num1, double num2) {
		if(num1 <= num2)
			return num1;
		else
			return num2;
	}
}
