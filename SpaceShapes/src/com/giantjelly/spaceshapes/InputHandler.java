package com.giantjelly.spaceshapes;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.giantjelly.spaceshapes.Game;

public class InputHandler implements KeyListener {
	public String typed = "";
	
	public boolean up = false;
	public boolean down = false;
	public boolean left = false;
	public boolean right = false;
	
	public int qued = 0;
	public int scaleout = 0;
	public int scalein = 0;
	
	public InputHandler(Game game){
		game.addKeyListener(this);
	}

	public void keyPressed(KeyEvent key) {
		if(key.getKeyCode() == KeyEvent.VK_UP){
			up = true;
		}
		if(key.getKeyCode() == KeyEvent.VK_DOWN){
			down = true;
		}
		if(key.getKeyCode() == KeyEvent.VK_LEFT){
			left = true;
		}
		if(key.getKeyCode() == KeyEvent.VK_RIGHT){
			right = true;
		}
	}

	public void keyReleased(KeyEvent key) {
		if(key.getKeyCode() == KeyEvent.VK_UP){
			up = false;
		}
		if(key.getKeyCode() == KeyEvent.VK_DOWN){
			down = false;
		}
		if(key.getKeyCode() == KeyEvent.VK_LEFT){
			left = false;
		}
		if(key.getKeyCode() == KeyEvent.VK_RIGHT){
			right = false;
		}
	}

	public void keyTyped(KeyEvent key) {
		if(key.getKeyChar() != ''){
			typed = typed + key.getKeyChar();
			System.out.println(typed);
		}
		else{
			typed = typed.substring(0, typed.length() - 1);
		}
		
		if(key.getKeyChar() == 'x'){
			qued++;
		}
		
		if(key.getKeyChar() == '-'){
			scaleout++;
		}
		if(key.getKeyChar() == '='){
			scalein++;
		}
	}

}
