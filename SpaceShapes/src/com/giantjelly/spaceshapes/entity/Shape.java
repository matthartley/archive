package com.giantjelly.spaceshapes.entity;

import java.util.ArrayList;

import com.giantjelly.spaceshapes.Game;
import com.giantjelly.spaceshapes.Screen;
import com.giantjelly.spaceshapes.Math;

import static com.giantjelly.spaceshapes.Game.WIDTH;
import static com.giantjelly.spaceshapes.Game.HEIGHT;
import static com.giantjelly.spaceshapes.Game.MAPWIDTH;
import static com.giantjelly.spaceshapes.Game.MAPHEIGHT;
import static com.giantjelly.spaceshapes.Game.XSCROLL;
import static com.giantjelly.spaceshapes.Game.YSCROLL;
import static com.giantjelly.spaceshapes.Game.WORLDSCALE;

public class Shape {
	public double x = 0;
	public double y = 0;
	public double xs = 16;
	public double ys = 16;
	public double xss = 0;
	public double yss = 0;
	public int color;
	
	public Shape(int xx, int yy, int xxs, int yys, double xxss, double yyss) {
		x = xx;
		y = yy;
		xs = xxs;
		ys = yys;
		xss = xxss;
		yss = yyss;
		color = Game.rand.nextInt(1000000000);
	}
	
	public Shape(int xx, int yy, int xxs, int yys, double xxss, double yyss, int cc) {
		x = xx;
		y = yy;
		xs = xxs;
		ys = yys;
		xss = xxss;
		yss = yyss;
		color = cc;
	}
	
	public void update(ArrayList<Shape> shapes) {
		x += xss;
		y += yss;
		
		if(x+xs > MAPWIDTH)
			xss = Math.negative(xss);
		if(y+ys > MAPHEIGHT)
			yss = Math.negative(yss);
		if(x < 0)
			xss = Math.positive(xss);
		if(y < 0)
			yss = Math.positive(yss);
		
		for(Shape s : shapes){
			if(
					x < s.x + s.xs && x + xs > s.x &&
					y < s.y + s.ys && y + ys > s.y &&
					s != this
			){
				double left = (x + xs) - s.x;
				double right = (s.x + s.xs) - x;
				double top = (y + ys) - s.y;
				double bottom = (s.y + s.ys) - y;
				
				double horiz = Math.smallest(left, right);
				double verti = Math.smallest(top, bottom);
				
				double thisxspeed = xss;
				double thisyspeed = yss;
				double otherxspeed = s.xss;
				double otheryspeed = s.yss;
				
				if(Math.smallest(horiz, verti) == horiz){
					if(Math.smallest(left, right) == left){
						x = s.x - s.xs;
						//xss = Math.negative(xss);
					}else{
						x = s.x + s.xs;
						//xss = Math.positive(xss);
					}
					xss *= (otherxspeed/thisxspeed);
					s.xss *= (thisxspeed/otherxspeed);
				}else{
					if(Math.smallest(top, bottom) == top){
						y = s.y - s.ys;
						//yss = Math.negative(yss);
					}else{
						y = s.y + s.ys;
						//yss = Math.positive(yss);
					}
					yss *= (otheryspeed/thisyspeed);
					s.yss *= (thisyspeed/otheryspeed);
				}
			}
		}
	}
	
	public void draw(Screen screen) {
		screen.draw((int)(x*WORLDSCALE)+(int)XSCROLL, (int)(y*WORLDSCALE)+(int)YSCROLL, (int)(xs*WORLDSCALE), (int)(ys*WORLDSCALE), color);
	}
}

/*if(xss > 0 && s.xss > 0){
	xss -= xss-s.xss;
}else
if(xss < 0 && s.xss < 0){
	xss -= s.xss-xss;
}*/
