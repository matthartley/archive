package com.giantjelly.spaceshapes.entity;

import com.giantjelly.spaceshapes.Game;
import static com.giantjelly.spaceshapes.Game.XSCROLL;
import static com.giantjelly.spaceshapes.Game.YSCROLL;
import static com.giantjelly.spaceshapes.Game.WORLDSCALE;
import com.giantjelly.spaceshapes.Screen;

public class Star {
	private int x;
	private int y;
	private int color;
	
	private static int[] colors = {0x222222, 0x555555, 0x777777, 0x999999, 0xaaaaaa, 0xcccccc, 0xeeeeee, 0xffffff};//2, 5, 7, 9, a, c, e, f
	private static int[] colors2 = {0xff0000, 0x00ff00, 0x0000ff};
	
	public Star(int xx, int yy) {
		x=xx;
		y=yy;
		color = colors[Game.rand.nextInt(8)] /** colors2[Game.rand.nextInt(3)]*/;
	}
	
	public void draw(Screen screen) {
		screen.draw((int)(x*WORLDSCALE)+(int)XSCROLL, (int)(y*WORLDSCALE)+(int)YSCROLL, (int)(1), (int)(1), (int)(color));
	}
}
