package com.giantjelly.spaceshapes;

import java.awt.image.*;

import static com.giantjelly.spaceshapes.Game.WIDTH;
import static com.giantjelly.spaceshapes.Game.HEIGHT;

public class Screen {
	/*public static int HEIGHT = Game.HEIGHT;
	public static int WIDTH = Game.WIDTH;*/
	
	public BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	public int[] pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
	
	public void draw(int x, int y, int width, int height, int color) {
		
		for(int yy = 0; yy < height; yy++) {
			for(int xx = 0; xx < width; xx++){
				if(
						x+xx >= 0 && y+yy >= 0 && x+xx < WIDTH && y+yy < HEIGHT
				){
					pixels[(y+yy)*WIDTH + x+xx] = color;
				}
			}
		}
		
	}
}
