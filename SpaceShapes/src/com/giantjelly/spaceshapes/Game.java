package com.giantjelly.spaceshapes;

import java.awt.*;
import java.awt.image.*;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;

import com.giantjelly.spaceshapes.InputHandler;
import com.giantjelly.spaceshapes.Screen;
import com.giantjelly.spaceshapes.entity.Shape;
import com.giantjelly.spaceshapes.entity.Star;
//import com.giantjelly.spaceshapes.networking.Sql;

public class Game extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;
	public static int HEIGHT = 480;
	public static int WIDTH = 720;
	public static int MAPHEIGHT = HEIGHT*8;
	public static int MAPWIDTH = WIDTH*8;
	public static double XSCROLL = 0;
	public static double YSCROLL = 0;
	public static double WORLDSCALE = 1;
	
	public static Random rand = new Random();
	
	private Screen screen = new Screen();
	private Font font = new Font("/Font.png");
	private boolean running = true;
	private InputHandler input = new InputHandler(this);
	
	public ArrayList<Shape> shapes = new ArrayList<Shape>();
	public ArrayList<Star> stars = new ArrayList<Star>();
	
	public void start() {
		//System.out.println(Sql.login("testing", "test"));
		
		shapes.add(new Shape(WIDTH/2, HEIGHT/2, 32, 32, 2, 2, 0xff8330));
		for(int i = 0; i < 100; i++){
			shapes.add(new Shape(rand.nextInt(1024-32), rand.nextInt(720-32), 32, 32, (rand.nextFloat()*8)-4, (rand.nextFloat()*8)-4));
		}
		
		for(int i = 0; i < MAPHEIGHT*MAPWIDTH; i++){
			if(rand.nextInt(1000) == 0){
				stars.add(new Star(i%MAPWIDTH, i/MAPWIDTH));
			}
		}
		
		requestFocus();
		createBufferStrategy(3);
		new Thread(this).start();
	}
	
	public void stop() {
		
	}

	public void run() {
		long lasttick = System.nanoTime();
		long lastframe = System.nanoTime();
		double nspertick = 1000000000.0/60.0;
		double ts = 0;
		int frames = 0;
		int ticks = 0;
		
		while(running){
			
			
			ts += (System.nanoTime() - lasttick)/nspertick;
			lasttick = System.nanoTime();
			while(ts >= 1){
				ticks++;
				tick();
				ts--;
			}
			
			
			
			frames++;
			draw();
			render();
			
			if(System.nanoTime() - lastframe > 1000000000.0){
				System.out.println("frames " + frames + " , ticks " + ticks  + " , count " + count + " , xscroll " + XSCROLL + " , yscroll " + YSCROLL);
				FRAMES = frames;
				TICKS = ticks;
				frames = 0;
				ticks = 0;
				lastframe = System.nanoTime();
			}
		}
	}
	
	private int count = 0;
	private int FRAMES = 0;
	private int TICKS = 0;
	
	private void draw() {
		/*int time = (int)System.currentTimeMillis();
		for(int i = 0; i < screen.pixels.length; i++){
			screen.pixels[i] = 0xff00ff;
		}*/
		
		for(int i = 0; i < screen.pixels.length; i++){
			screen.pixels[i] = 0x000000;
		}
		
		for(int i = 0; i < stars.size(); i++){
			stars.get(i).draw(screen);
		}
		
		for(Shape shape : shapes){
			shape.draw(screen);
		}
		
		font.draw(screen, "frames "+ FRAMES + " , ticks "+TICKS + " , shapes "+shapes.size(), 2);
	}
	
	private void tick() {
		if(input.up && shapes.get(0).yss > -8)
			shapes.get(0).yss -= 0.2f;
		
		if(input.down && shapes.get(0).yss < 8)
			shapes.get(0).yss += 0.2f;
		
		if(input.left && shapes.get(0).xss > -8)
			shapes.get(0).xss -= 0.2f;
		
		if(input.right && shapes.get(0).xss < 8)
			shapes.get(0).xss += 0.2f;
		
		for(Shape shape : shapes){
			shape.update(shapes);
		}
		
		//double xx = shapes.get(0).x-512;
		//double yy = shapes.get(0).y-360;
		//shapes.get(0).x -= xx;
		//shapes.get(0).y -= yy;
		//XSCROLL = -(shapes.get(0).x-512);
		//YSCROLL = -(shapes.get(0).y-360);
		XSCROLL += ((-(XSCROLL)+512)-shapes.get(0).x)/10;
		YSCROLL += ((-(YSCROLL)+360)-shapes.get(0).y)/10;
		//shapes.get(0).x = 512;
		//shapes.get(0).y = 360;
		
		while(input.qued > 0){
			shapes.add(new Shape(512-(int)XSCROLL, 360-(int)YSCROLL, 32, 32, 4, 4/*rand.nextInt(8)-4, rand.nextInt(8)-4*/));
			input.qued--;
		}
		
		while(input.scaleout>0){
			WORLDSCALE-=0.01;
			//XSCROLL *= 0.99;
			//YSCROLL *= 0.99;
			input.scaleout--;
		}
		while(input.scalein>0){
			WORLDSCALE+=0.01;
			//XSCROLL /= 0.99;
			//YSCROLL /= 0.99;
			input.scalein--;
		}
		
		//WORLDSCALE -= 0.001;
	}
	
	public static void main(String args[]) {
		Game game = new Game();
		game.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		frame.setMinimumSize(new Dimension(WIDTH/2, HEIGHT/2));
		frame.setLayout(new BorderLayout());
		frame.add(game, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
		
		game.start();
	}
	
	private void render() {
		BufferStrategy bs = getBufferStrategy();
		Graphics g = bs.getDrawGraphics();
		g.drawImage(screen.image, 0, 0, getWidth(), getHeight(), null);
		g.dispose();
		bs.show();
	}

}
