package com.giantjelly.ld25.entity;

import static org.lwjgl.opengl.GL11.glColor4f;

import com.giantjelly.ld25.Game;
import com.giantjelly.ld25.Sprite;

public class Tower {
	
	public int level = 1;
	
	public void update() {
		glColor4f(1, 1, 1, 1);
		new Sprite((level-1)*32, 0, 32, 84, Sprite.tower).draw(-32+Game.XSCROLL, -128+Game.YSCROLL, 4,  -((float)(2016-(-128+(84*4)))/2000) );
	}
	
}
