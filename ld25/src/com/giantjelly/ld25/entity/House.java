package com.giantjelly.ld25.entity;

import java.util.ArrayList;

import com.giantjelly.ld25.Audio;
import com.giantjelly.ld25.Fonty;
import com.giantjelly.ld25.Game;
import com.giantjelly.ld25.Sprite;

import static org.lwjgl.opengl.GL11.*;

public class House extends Building {
	
	public int health = 100;
	public int spritex = 0;
	public int spritey = 0;
	public int height;
	
	public Dude[] dudes = new Dude[5];
	
	public House(int x, int y, int sx, int sy, int h) {
		super(x,y);
		spritex = sx;
		spritey = sy;
		height = h;
		
		if(spritex==29&&spritey==0){
			for(int i = 0; i < dudes.length; i++){
				dudes[i] = new Dude(x+Game.rand.nextInt(32), y+Game.rand.nextInt(32), 4);   //4-5
			}
		}
		if(spritex==0&&spritey==0){
			for(int i = 0; i < dudes.length; i++){
				dudes[i] = new Dude(x+Game.rand.nextInt(32), y+Game.rand.nextInt(32), 5);   //4-5
			}
		}
	}
	
	public void update() {
		if((spritex==0&&spritey==0) || (spritex==29&&spritey==0)){
			for(int i = 0; i < dudes.length; i++){
				if(!dudes[i].dead){
				
					Dude asd = clostestdude(dudes[i]);
					if(asd != null){
						dudes[i].tx = asd.x;
						dudes[i].ty = asd.y;
					}
					
					dudes[i].update();
					
					int extra = 0;
					if(dudes[i].type == 5) extra = 0;
					
					for(Dude dude : Game.evilknights){
						if(
								dudes[i].x-extra < dude.x+20+extra
								&&
								dudes[i].x+20+extra > dude.x-extra
								&&
								dudes[i].y-extra < dude.y+30+extra
								&&
								dudes[i].y+30+extra > dude.y-extra
						){
							dude.health--;
							if(dudes[i].type == 4) dude.health--;
							dudes[i].health-=2;
						}
					}
					
					for(Dude dude : Game.evilarchers){
						if(
								dudes[i].x-extra < dude.x+20+extra
								&&
								dudes[i].x+20+extra > dude.x-extra
								&&
								dudes[i].y-extra < dude.y+30+extra
								&&
								dudes[i].y+30+extra > dude.y-extra
						){
							dude.health--;
							if(dudes[i].type == 4) dude.health--;
							dudes[i].health-=2;
						}
					}
					
					if(dudes[i].health <= 0)
						Audio.deadenemy.play();
				}
			}
		}
		
		glColor4f(1, 1, 1, 1);
		new Sprite(spritex, spritey, 29, 29, Sprite.buildings).draw(x+Game.XSCROLL, y+Game.YSCROLL, 4,  -((float)(2016-(y+(height*4)))/2000) );
		Fonty.font.draw(x+Game.XSCROLL, (y+Game.YSCROLL)-32, "-"+health+"-");
	}
	
	public void damage(ArrayList<Dude> dudes){
		for(Dude dude : dudes){
			int extra = 0;
			if(dude.type == 2) extra = 64;
			
			if(
				dude.x-extra <	x+116
				&&
				dude.x+20+extra > x
				&&
				dude.y-extra < y+116
				&&
				dude.y+30+extra > y
			){
				health--;
				if(dude.type == 3) health--;
			}
		}
	}
	
	public Dude clostestdude(Dude d) {
		Dude dude = null;
		
		for(int i = 0; i < Game.evilknights.size(); i++){
			if(i==0)
				dude = Game.evilknights.get(i);
			
			if(Game.positive(Game.evilknights.get(i).x - d.x) + Game.positive(Game.evilknights.get(i).y - d.y) < 
					Game.positive(dude.x - d.x) + Game.positive(dude.y - d.y)){
				dude = Game.evilknights.get(i);
			}
		}
		
		for(int i = 0; i < Game.evilarchers.size(); i++){
			if(i==0 && dude == null)
				dude = Game.evilarchers.get(i);
			
			if(Game.positive(Game.evilarchers.get(i).x - d.x) + Game.positive(Game.evilarchers.get(i).y - d.y) < 
					Game.positive(dude.x - d.x) + Game.positive(dude.y - d.y)){
				dude = Game.evilarchers.get(i);
			}
		}
		
		return dude;
	}
	
}
