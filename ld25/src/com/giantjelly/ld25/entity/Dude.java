package com.giantjelly.ld25.entity;

import static org.lwjgl.opengl.GL11.glColor4f;

import java.util.ArrayList;

import com.giantjelly.ld25.Game;
import com.giantjelly.ld25.Sprite;

public class Dude {
	
	public int x;
	public int y;
	public int type;
	//public int tile;
	public int spritey = 0;
	
	public int strollx = 0;
	public int strolly = 0;
	
	public int health = 30;
	
	public int tx = 0;
	public int ty = 0;
	
	public boolean dead = false;
	
	public Dude(int xx, int yy, int t) {
		x=xx;
		y=yy;
		type=t;
		
	}
	
	public void update() {
		if(health <= 0)
			dead = true;
		
		//animate();
		//collision(Game.evilknights);
		//collision(Game.evilarchers);
		collision();
		
		
		//tile = 64*32+32;
		
		glColor4f(1, 1, 1, 1);
		new Sprite(aniframe*4, (type*24)+(spritey*6), 4, 6, Sprite.dudes).draw(x+Game.XSCROLL, y+Game.YSCROLL, 5,  -((float)(2016-(y+30))/2000) );
		
		int tilex = tx + 24;
		int tiley = ty - 12;
		
		if(y+64 < tiley){
			y++;
			spritey = 1;
			strolly = setstroll();
		}else
		if(y-64 > tiley){
			y--;
			spritey = 2;
			strolly = setstroll();
		}else{
			y += strolly;
		}
		
		if(x+64 < tilex){
			x++;
			spritey = 0;
			strollx = setstroll();
		}else
		if(x-64 > tilex){
			x--;
			spritey = 3;
			strollx = setstroll();
		}else{
			x += strollx;
		}
		
		if(x == tilex && y == tiley)
			aniframe = 0;
		else
			animate();
	}
	
	int aniframe = 0;
	int anicount = 0;
	
	public void animate() {
		anicount++;
		
		if(anicount >= 24){
			anicount = 0;
			aniframe++;
		}
		
		if(aniframe >= 3){
			aniframe = 1;
		}
	}
	
	public void collision(/*ArrayList<Dude> dudes*/) {
		
		for(int i = 0; i < Game.evilknights.size(); i++){
			if(Game.evilknights.get(i) != this){
				if(
						x < Game.evilknights.get(i).x+20
						&&
						x+20 > Game.evilknights.get(i).x
						&&
						y < Game.evilknights.get(i).y+30
						&&
						y+30 > Game.evilknights.get(i).y
						){
					
					if(x <= Game.evilknights.get(i).x)
						x--;
					if(x > Game.evilknights.get(i).x)
						x++;
					if(y <= Game.evilknights.get(i).y)
						y--;
					if(y > Game.evilknights.get(i).y)
						y++;
					
				}
			}
		}
		
		for(int i = 0; i < Game.evilarchers.size(); i++){
			if(Game.evilarchers.get(i) != this){
				if(
						x < Game.evilarchers.get(i).x+20
						&&
						x+20 > Game.evilarchers.get(i).x
						&&
						y < Game.evilarchers.get(i).y+30
						&&
						y+30 > Game.evilarchers.get(i).y
						){
					
					if(x <= Game.evilarchers.get(i).x)
						x--;
					if(x > Game.evilarchers.get(i).x)
						x++;
					if(y <= Game.evilarchers.get(i).y)
						y--;
					if(y > Game.evilarchers.get(i).y)
						y++;
					
				}
			}
		}
		
		if(type != 2 && type != 3){
			for(House house : Game.houses){
				for(int i = 0; i < house.dudes.length; i++){
					if(house.dudes[i] != this){
						if(
								x+5 < house.dudes[i].x+15
								&&
								x+15 > house.dudes[i].x+5
								&&
								y+5 < house.dudes[i].y+25
								&&
								y+25 > house.dudes[i].y+5
								){
							
							if(x <= house.dudes[i].x)
								x--;
							if(x > house.dudes[i].x)
								x++;
							if(y <= house.dudes[i].y)
								y--;
							if(y > house.dudes[i].y)
								y++;
							
						}
					}
				}
			}
		}
		
	}
	
	public int setstroll() {
		int num = 1;
		if(Game.rand.nextInt(2) == 0)
			num = -1;
		
		return num;
	}
	
}
