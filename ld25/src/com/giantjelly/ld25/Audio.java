package com.giantjelly.ld25;

import com.giantjelly.ld25.Sound.*;

public class Audio {
	
	public static Listener listener = new Listener();
	
	public static Source newdude = new Source(new Sound("res/sounds/newdude.wav"), 1);
	public static Source deaddude = new Source(new Sound("res/sounds/deaddude.wav"), 1);
	public static Source newbuilding = new Source(new Sound("res/sounds/newbuilding.wav"), 1);
	public static Source deadbuilding = new Source(new Sound("res/sounds/deadbuilding.wav"), 1);
	public static Source menu = new Source(new Sound("res/sounds/menu.wav"), 1);
	public static Source deadenemy = new Source(new Sound("res/sounds/deadenemy.wav"), 1);
	public static Source hurtbuilding = new Source(new Sound("res/sounds/hurtbuilding.wav"), 1);
	
}
