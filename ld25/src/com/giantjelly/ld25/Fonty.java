package com.giantjelly.ld25;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Font;

import org.newdawn.slick.TrueTypeFont;

public class Fonty extends TrueTypeFont {
	
	public Fonty(Font font) {
		super(font, true);
	}
	
	public void draw(int x, int y, String text){
		glEnable(GL_TEXTURE_2D);
		this.drawString(x, y, text);
	}
	
	public static Fonty font = new Fonty(new Font("monospaced", Font.PLAIN, 24));
	
}
