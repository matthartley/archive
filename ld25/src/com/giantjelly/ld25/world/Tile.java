package com.giantjelly.ld25.world;

import com.giantjelly.ld25.Game;
import com.giantjelly.ld25.Sprite;

public class Tile {
	
	public int x;
	public int y;
	public int sx;
	public int sy;
	
	public Tile(int xx, int yy, int sxx, int syy) {
		x=xx;
		y=yy;
		sx=sxx;
		sy=syy;
	}
	
	public Tile(int xx, int yy, int r, int g, int b){
		
	}
	
	public void draw() {
		new Sprite(sx, sy, 32, 16, Sprite.tiles).draw(x+Game.XSCROLL, y+Game.YSCROLL, 2, -9);
	}
	
}
