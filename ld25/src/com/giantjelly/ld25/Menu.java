package com.giantjelly.ld25;

import org.lwjgl.input.Keyboard;

public class Menu {
	
	public static String[] buttons = new String[]{ "Play", "Instructions", "Credits", "Exit" };
	public static int selected = 0;
	
	public static void main() {
		
		for(int i = 0; i < 4; i++){
			String text = "";
			if(i==selected) text = " -";
			
			Fonty.font.draw(10, i*30, buttons[i] + text);
		}
		
		while(Keyboard.next()){
			if(Keyboard.getEventKey() == Keyboard.KEY_DOWN && Keyboard.getEventKeyState()){
				selected++;
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_UP && Keyboard.getEventKeyState()){
				selected--;
			}
			
			if(selected < 0)
				selected = 3;
			if(selected > 3)
				selected = 0;
			
			if(Keyboard.getEventKey() == Keyboard.KEY_RETURN && Keyboard.getEventKeyState()){
				Game.screen = selected+1;
				Audio.menu.play();
			}
		}
		
	}
	
	public static void instructions() {
		Fonty.font.draw(10, 0, "INSTRUCTIONS");
		
		Fonty.font.draw(10, 60, "Use the 1, 2 and 3 keys to switch between the targeter and building placers");
		Fonty.font.draw(10, 90, "Click with the Targeter to move your dudes");
		Fonty.font.draw(10, 120, "Place Forts to increase your knight limit");
		Fonty.font.draw(10, 150, "Place Tents to increase your Archer limit");
		Fonty.font.draw(10, 180, "Move your dudes over the human buildings to destroy them and earn energy");
		Fonty.font.draw(10, 210, "Placing buildings and creating dudes uses energy");
		Fonty.font.draw(10, 240, "Archers have longer range and Knights do more damage");
		
		Fonty.font.draw(10, 300, "Play until you have so many dudes the game breaks");
		
		exit();
	}
	
	public static void credits() {
		Fonty.font.draw(10, 0, "CREDITS");
		
		Fonty.font.draw(10, 60, "Programing:");
		Fonty.font.draw(10, 90, "- Matt Hartley");
		
		Fonty.font.draw(10, 150, "Art:");
		Fonty.font.draw(10, 180, "- Nathan Walker");
		
		Fonty.font.draw(10, 240, "Game Design:");
		Fonty.font.draw(10, 270, "- Matt Hartley");
		Fonty.font.draw(10, 300, "- Nathan Walker");
		
		exit();
	}
	
	public static void exit() {
		while(Keyboard.next()){
			if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState()){
				Game.screen = 0;
				Audio.menu.play();
			}
		}
	}
	
}
