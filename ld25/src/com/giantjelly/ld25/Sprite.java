package com.giantjelly.ld25;

import static org.lwjgl.opengl.GL11.*;

import java.io.IOException;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Sprite {
	
	public Texture texture;
	public float x;
	public float y;
	public float width;
	public float height;
	
	public Sprite(float xx, float yy, float w, float h, Texture t) {
		x = xx;
		y = yy;
		width = w;
		height = h;
		texture = t;
	}
	
	public void draw(float xx, float yy, float size, float z) {
		if(
				xx + width*size > 0
				&&
				xx < Game.WIDTH
				&&
				yy + height*size > 0
				&&
				yy < Game.HEIGHT
				){
			glEnable(GL_TEXTURE_2D);
			texture.bind();
			//glColor4f(1, 1, 1, 1);
			
			glBegin(GL_QUADS);
			glTexCoord2f((float)x/(float)texture.getImageWidth(), (float)y/(float)texture.getImageHeight()); 					glVertex3f(xx, yy, z);
			glTexCoord2f((float)(x+width)/(float)texture.getImageWidth(), (float)y/(float)texture.getImageHeight()); 			glVertex3f(xx+(width*size), yy, z);
			glTexCoord2f((float)(x+width)/(float)texture.getImageWidth(), (float)(y+height)/(float)texture.getImageHeight()); 	glVertex3f(xx+(width*size), yy+(height*size), z);
			glTexCoord2f((float)x/(float)texture.getImageWidth(), (float)(y+height)/(float)texture.getImageHeight()); 			glVertex3f(xx, yy+(height*size), z);
			glEnd();
		}
	}
	
	public static Texture tiles;
	public static Texture dudes;
	public static Texture heightmap;
	public static Texture tower;
	public static Texture buildings;
	public static Texture gameover;
	
	public static void load() {
		
		try{
			
			tiles = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Tiles.png"), GL_NEAREST);
			dudes = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Dudes.png"), GL_NEAREST);
			heightmap = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/HeightMap.png"), GL_NEAREST);
			tower = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/DarkTower.png"), GL_NEAREST);
			buildings = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/Buildings.png"), GL_NEAREST);
			gameover = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/textures/gameover.png"), GL_NEAREST);
			System.out.println(tiles.getImageWidth());
			System.out.println(tiles.getImageHeight());
			System.out.println(dudes.getImageWidth());
			System.out.println(dudes.getImageHeight());
			
		}catch(IOException e){
			System.out.println(e);
		}
		
	}
	
}
