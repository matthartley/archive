package com.giantjelly.ld25;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.Random;
import java.awt.Font.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import com.giantjelly.ld25.entity.Dude;
import com.giantjelly.ld25.entity.House;
import com.giantjelly.ld25.entity.Tower;
import com.giantjelly.ld25.world.Tile;

public class Game {
	
	public static int HEIGHT = 768;
	public static int WIDTH = 1280;
	
	static long lasttime = System.currentTimeMillis();
	static int frames = 0;
	static int fps = 0;
	
	public static Random rand = new Random();
	
	public static Tile[] tiles = new Tile[64*64];
	
	public static int XSCROLL = 594;
	public static int YSCROLL = 152;
	
	public static int count = 50;
	public static int selected = 16*64+16;
	public static int mode = 1;
	public static int screen = 0;
	
	public static int energy = 100;
	public static int maxknights = 10;
	public static int maxarchers = 10;
	public static Tower tower = new Tower();
	public static ArrayList<Dude> evilknights = new ArrayList<Dude>();
	public static ArrayList<Dude> evilarchers = new ArrayList<Dude>();
	
	public static ArrayList<House> houses = new ArrayList<House>();
	public static ArrayList<House> tents = new ArrayList<House>();
	public static ArrayList<House> forts = new ArrayList<House>();
	
	public static void load() {
		for(int i = 0; i < tiles.length; i++){
			tiles[i] = new Tile( ( (i%64)*32 ) - ((i/64)*32) , ( (i%64)*16 ) + ((i/64)*16) , 0, 48);
			/*if(rand.nextInt(10) == 0)
				tiles[i].sx = 32;
			else
			if(rand.nextInt(10) == 0)
				tiles[i].sx = 64;
			
			if(i%64 < 16 && i/64 < 16){
				tiles[i].sy = 0;
				tiles[i].sx = 0;
				
				if(rand.nextInt(10) == 0)
					tiles[i].sy = 16;
			}*/
		}
		
		byte[] rgb = Sprite.heightmap.getTextureData();
		
		for(int i = 0; i < rgb.length; i += 3){
			int index = 0;
			if(i > 0) index = i/3;
			
			if(rgb[i] == 0 && rgb[i+1] == 0 && rgb[i+2] == 0){
				tiles[index].sx = 0;
				tiles[index].sy = 0;
			}
			if(rgb[i] == 127 && rgb[i+1] == 51 && rgb[i+2] == 0){
				tiles[index].sx = 32;
				tiles[index].sy = 0;
			}
			if(rgb[i] == -128 && rgb[i+1] == -128 && rgb[i+2] == -128){
				tiles[index].sx = 64;
				tiles[index].sy = 0;
			}
			
			
			if(rgb[i] == -1 && rgb[i+1] == 106 && rgb[i+2] == 0){
				tiles[index].sx = 0;
				tiles[index].sy = 16;
			}
			
			if(rgb[i] == 91 && rgb[i+1] == 127 && rgb[i+2] == 0){
				tiles[index].sx = 64;
				tiles[index].sy = 48;
			}
			if(rgb[i] == -1 && rgb[i+1] == -1 && rgb[i+2] == -1){
				tiles[index].sx = 32;
				tiles[index].sy = 48;
			}
		}
		
		for(int i = 0; i < 16; i++){
			tiles[i*64+16].sx = 32;
			tiles[i*64+16].sy = 32;
		}
		for(int i = 0; i < 16; i++){
			tiles[16*64+i].sx = 0;
			tiles[16*64+i].sy = 32;
		}
		
		/*for(int i = 0; i < tiles.length; i++){
			if(tiles[i].sx == 0 && tiles[i].sy == 16){
				if(tiles[i-64].sx != 0 || tiles[i-64].sy != 16){//above
					tiles[i-64].sx = 32;
					tiles[i-64].sy = 16;
				}
				if(tiles[i+1].sx != 0 || tiles[i+1].sy != 16){//right
					tiles[i+1].sx = 96;
					tiles[i+1].sy = 16;
				}
				if(tiles[i+64].sx != 0 || tiles[i+64].sy != 16){//below
					tiles[i+64].sx = 128;
					tiles[i+64].sy = 16;
				}
				if(tiles[i-1].sx != 0 || tiles[i-1].sy != 16){//left
					tiles[i-1].sx = 64;
					tiles[i-1].sy = 16;
				}
			}
		}*/
		
		System.out.println(rgb[0]+"/"+rgb[0+1]+"/"+rgb[0+2]);
		
		//houses.add(new House(tiles[15].x, tiles[15].y));
	}
	
	public static void main(String args[]) {
		try {
			
			Display.setDisplayMode(new DisplayMode(1280, 768));
			Display.setResizable(false);
			//Display.setInitialBackground(0.9f, 0.9f, 0.9f);
			Display.setTitle("Pillage");
			Display.create();
			
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, Display.getWidth(), Display.getHeight(), 0, -10, 10);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glEnable(GL_DEPTH_TEST);
			
			glAlphaFunc(GL_GREATER, 0.1f);
			glEnable(GL_ALPHA_TEST);
			
			Sprite.load();
			load();
			
			while(!Display.isCloseRequested()){
				
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				
				frames++;
				
				if(System.currentTimeMillis() - lasttime > 1000){
					fps = frames;
					frames = 0;
					lasttime = System.currentTimeMillis();
					
					secondtick();
				}
				
				//new Sprite(0, 0, 512, 256, Sprite.tiles).draw(100, 100, 2);
				run();
			}
			
		} catch (LWJGLException e) {
			System.out.println(e);
		}
	}
	
	public static void secondtick() {
		if(screen == 1){
			if(evilknights.size() < maxknights && energy >= 5){
				evilknights.add(new Dude(20, 200, 3));
				Audio.newdude.play();
				energy -= 5;
			}
			if(evilarchers.size() < maxarchers && energy >= 5){
				evilarchers.add(new Dude(20, 200, 2));
				Audio.newdude.play();
				energy -= 5;
			}
			
			count++;
			
			for(int i = 0; i < houses.size(); i++){
				int h = houses.get(i).health;
				houses.get(i).damage(evilknights);
				houses.get(i).damage(evilarchers);
				
				if(houses.get(i).health < h)
					Audio.hurtbuilding.play();
				
				if(houses.get(i).health <= 0){
					houses.remove(i);
					Audio.deadbuilding.play();
					energy += 30;
					i--;
				}
			}
		}
	}
	
	public static void run() {
		
		if(screen == 1){
		
			//energy = 1000;
			maxknights = (forts.size()*10)+10;
			maxarchers = (tents.size()*10)+10;
			//maxknights = 1;
			//maxarchers = 0;
			
			for(int i = 0; i < tiles.length; i++){
				if(selected==i) glColor4f(1, 0, 1, 1); else glColor4f(1, 1, 1, 1);
				tiles[i].draw();
			}
			
			tower.update();
			
			for(int i = 0 ; i < houses.size(); i++){
				houses.get(i).update();
			}
			for(int i = 0 ; i < tents.size(); i++){
				tents.get(i).update();
			}
			for(int i = 0 ; i < forts.size(); i++){
				forts.get(i).update();
			}
			
			//dude.update();
			for(int i = 0; i < evilknights.size(); i++){
				evilknights.get(i).update();
				evilknights.get(i).tx = tiles[selected].x;
				evilknights.get(i).ty = tiles[selected].y;
				if(evilknights.get(i).health <= 0){
					evilknights.remove(i);
					Audio.deaddude.play();
					i--;
				}
			}
			for(int i = 0; i < evilarchers.size(); i++){
				evilarchers.get(i).update();
				evilarchers.get(i).tx = tiles[selected].x;
				evilarchers.get(i).ty = tiles[selected].y;
				if(evilarchers.get(i).health <= 0){
					evilarchers.remove(i);
					Audio.deaddude.play();
					i--;
				}
			}
			
			if(Keyboard.isKeyDown(Keyboard.KEY_W)){
				YSCROLL += 8;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_S)){
				YSCROLL -= 8;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_A)){
				XSCROLL += 8;
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_D)){
				XSCROLL -= 8;
			}
			
			if(count >= 20){
				count = 0;
				
				int x = 0;
				int y = 0;
				
				while(x < 16 && y < 16){
					x = rand.nextInt(64);
					y = rand.nextInt(64);
				}
				
				int sx=0;
				if(rand.nextInt(2)==0)
					sx = 29;
				
				houses.add(new House(tiles[y*64+x].x, tiles[y*64+x].y, sx, 0, 29));
				Audio.newbuilding.play();
				System.out.println("New house at: " + x+"/"+y+"/"+houses.get(houses.size()-1).spritex+"/"+houses.get(houses.size()-1).spritey+" | "+houses.get(houses.size()-1).dudes[0]);
			}
			
			while(Mouse.next()){
				if(Mouse.getEventButton() == 0 && Mouse.getEventButtonState()){
					if(mode == 1)
						selected = gettile(Mouse.getX(), Display.getHeight()-Mouse.getY());
					if(mode == 2 && energy >= 50){
						forts.add(new House(
								tiles[gettile(Mouse.getX(), Display.getHeight()-Mouse.getY())].x,
								tiles[gettile(Mouse.getX(), Display.getHeight()-Mouse.getY())].y,
								0,
								29,
								15
								));
						Audio.newbuilding.play();
						energy-=50;
					}
					if(mode == 3 && energy >= 50){
						tents.add(new House(
								tiles[gettile(Mouse.getX(), Display.getHeight()-Mouse.getY())].x,
								tiles[gettile(Mouse.getX(), Display.getHeight()-Mouse.getY())].y,
								29,
								29,
								15
								));
						Audio.newbuilding.play();
						energy-=30;
					}
					
					
					//System.out.println(Mouse.getX()+XSCROLL+" / "+(Display.getHeight()-Mouse.getY()+YSCROLL));
				}
			}
			
			while(Keyboard.next()){
				if(Keyboard.getEventKey() == Keyboard.KEY_1 && Keyboard.getEventKeyState()){
					mode = 1;
				}
				if(Keyboard.getEventKey() == Keyboard.KEY_2 && Keyboard.getEventKeyState()){
					mode = 2;
				}
				if(Keyboard.getEventKey() == Keyboard.KEY_3 && Keyboard.getEventKeyState()){
					mode = 3;
				}
				
				if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState()){
					screen = 0;
				}
			}
			
			//selected = gettile(Mouse.getX(), Mouse.getY());
			
			//Fonty.font.draw(10, 0, "Frames " + fps);
			Fonty.font.draw(10, 0, "Energy " + energy);
			Fonty.font.draw(10, 30, "Max Knights " + maxknights);
			Fonty.font.draw(10, 60, "Max Archers " + maxarchers);
			Fonty.font.draw(10, 90, "Knights " + evilknights.size());
			Fonty.font.draw(10, 120, "Archers " + evilarchers.size());
			//Fonty.font.draw(10, 180, "Selected Tile " + selected);
			//Fonty.font.draw(10, 210, "Count " +  count + ", Houses " + houses.size());
			//System.out.println(XSCROLL+"/"+YSCROLL);
			
			if(mode == 1) Fonty.font.draw(WIDTH - 256, 0, "- Targeter" ); else Fonty.font.draw(WIDTH - 256, 0, "# Targeter" );
			if(mode == 2) Fonty.font.draw(WIDTH - 256, 30, "- Place KnightFort" ); else Fonty.font.draw(WIDTH - 256, 30, "# Place KnightFort" );
			if(mode == 3) Fonty.font.draw(WIDTH - 256, 60, "- Place ArcherTent" ); else Fonty.font.draw(WIDTH - 256, 60, "# Place ArcherTent" );
			//Fonty.font.draw(WIDTH - 256, 90, "Press 1, 2 and 3" );
			
			if(evilknights.size() == 0 && evilarchers.size() == 0 && energy < 10){
				new Sprite(0, 0, WIDTH, HEIGHT, Sprite.gameover).draw(0, 0, 1.5f, 0);
			}
			
		}
		
		if(screen == 0){
			Menu.main();
		}
		
		if(screen == 2){
			Menu.instructions();
		}
		
		if(screen == 3){
			Menu.credits();
		}
		
		if(screen == 4){
			System.exit(0);
		}
		
		Display.update();
		Display.sync(60);
	}
	
	public static int gettile(int x, int y) {
		int tile = 0;
		int dis = positive((tiles[0].x+XSCROLL)-x) + positive((tiles[0].y+YSCROLL)-y);
		//System.out.println("tile: " + dis);
		
		for(int i = 0; i < tiles.length; i++){
			if(positive((tiles[i].x+XSCROLL)-x) + positive((tiles[i].y+YSCROLL)-y) < dis){
				dis = positive((tiles[i].x+XSCROLL)-x) + positive((tiles[i].y+YSCROLL)-y);
				tile = i;
			}
		}
		
		return tile;
	}
	
	public static int positive(int num) {
		if(num >= 0)
			return num;
		else
			return -num;
	}
	
}