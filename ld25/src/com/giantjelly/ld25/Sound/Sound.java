package com.giantjelly.ld25.Sound;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.WaveData;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.AL.*;

public class Sound {
	
	public IntBuffer buffer = BufferUtils.createIntBuffer(1);
	
	public Sound(String path) {
		alGenBuffers(buffer);
		
		if(alGetError() != AL_NO_ERROR)
			System.out.println("Error Loading Sound" + this);
		
		try {
			
			WaveData wave = WaveData.create(new BufferedInputStream(new FileInputStream(path)));
			alBufferData(buffer.get(0), wave.format, wave.data, wave.samplerate);
			wave.dispose();
			
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (NullPointerException e){
			System.out.println(e);
		}
	}
	
}
