package com.giantjelly.ld25.Sound;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.AL.*;

public class Listener {

	public FloatBuffer position = BufferUtils.createFloatBuffer(3).put(new float[]{ 0, 0, 0 });
	public FloatBuffer velocity = BufferUtils.createFloatBuffer(3).put(new float[]{ 0, 0, 0 });
	
	public FloatBuffer orientation = BufferUtils.createFloatBuffer(6).put(new float[]{ 0, 0, -1, 0, 1, 0 });
	
	public Listener() {
		try {
			create();
		} catch (LWJGLException e) {
			System.out.println(e);
		}
		
		position.flip();
		velocity.flip();
		orientation.flip();
		
		alListener(AL_POSITION, position);
		alListener(AL_VELOCITY, velocity);
		alListener(AL_ORIENTATION, orientation);
	}
	
}
