part of game;

class Shader {

	var shaderProgram;

	var vertexPositionAttribute;
	var textureCoordAttribute;

	var modelViewLocation;

	Shader (var gl) {

		var vertexShader = '''
			attribute vec3 vertexPosition;
			attribute vec2 textureCoord;
	
			varying lowp vec4 color;
			varying highp vec2 vTextureCoord;
	
			uniform mat4 modelViewMatrix;
			uniform mat4 viewMatrix;
	
			void main (void) {
				
				gl_Position = viewMatrix * modelViewMatrix * vec4(vertexPosition, 1.0);
				color = vec4(0.5, 1.0, 0.5, 1.0);
				vTextureCoord = textureCoord;
			}
		''';

		var fragmentShader = '''
			varying lowp vec4 color;
			varying highp vec2 vTextureCoord;
	
			uniform sampler2D uSampler;
	
			void main (void) {
	
				gl_FragColor = texture2D(uSampler, vTextureCoord);
				//gl_FragColor = color;
				if(gl_FragColor.a < 0.1) discard;
			}
		''';

		var vShader = gl.createShader(webgl.VERTEX_SHADER);
		gl.shaderSource(vShader, vertexShader);
		gl.compileShader(vShader);
		if (!gl.getShaderParameter(vShader, webgl.COMPILE_STATUS)) {
	  		print('vertex shader error: ' + gl.getShaderInfoLog(vShader));
		}

		var fShader = gl.createShader(webgl.FRAGMENT_SHADER);
		gl.shaderSource(fShader, fragmentShader);
		gl.compileShader(fShader);
		if (!gl.getShaderParameter(fShader, webgl.COMPILE_STATUS)) {
	  		print('fragment shader error: ' + gl.getShaderInfoLog(fShader));
		}

		shaderProgram = gl.createProgram();
		gl.attachShader(shaderProgram, vShader);
		gl.attachShader(shaderProgram, fShader);
		gl.linkProgram(shaderProgram);
		if(!gl.getProgramParameter(shaderProgram, webgl.LINK_STATUS)) print('No shaders!');

		gl.useProgram(shaderProgram);

		vertexPositionAttribute = gl.getAttribLocation(shaderProgram, 'vertexPosition');
		gl.enableVertexAttribArray(vertexPositionAttribute);
		textureCoordAttribute = gl.getAttribLocation(shaderProgram, 'textureCoord');
		gl.enableVertexAttribArray(textureCoordAttribute);

		modelViewLocation = gl.getUniformLocation(shaderProgram, 'modelViewMatrix');
	}

}