part of game;

class Buffer {

	var vertexBuffer;
	var indexBuffer;
	var textureBuffer;

	var vertices;
	var indices;
	var colors;
	var texture;

	Buffer (/*var gl,*/ this.vertices, this.indices, this.colors, this.texture) {

		Float32List vertexList = new Float32List(vertices.length);
		for(int i = 0; i < vertices.length/3; i++) {
			vertexList.setAll(i*3, [
				vertices[i*3],
				vertices[i*3+1],
				vertices[i*3+2]
			]);
		}

		Int16List indexList = new Int16List(indices.length);
		indexList.setAll(0, indices);

		Float32List textureCoords = new Float32List(texture.length);
		for(int i = 0; i < texture.length/2; i++) {
			textureCoords.setAll(i*2, [ texture[i*2], texture[i*2+1] ]);
		}

		vertexBuffer = gl.createBuffer();
		gl.bindBuffer(webgl.ARRAY_BUFFER, vertexBuffer);
		gl.bufferDataTyped(webgl.ARRAY_BUFFER, vertexList, webgl.STATIC_DRAW);
		gl.vertexAttribPointer(shader.vertexPositionAttribute, 3, webgl.FLOAT, false, 0, 0);

		indexBuffer = gl.createBuffer();
		gl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, indexBuffer);
		gl.bufferDataTyped(webgl.ELEMENT_ARRAY_BUFFER, indexList, webgl.STATIC_DRAW);

		textureBuffer = gl.createBuffer();
		gl.bindBuffer(webgl.ARRAY_BUFFER, textureBuffer);
		gl.bufferDataTyped(webgl.ARRAY_BUFFER, textureCoords, webgl.STATIC_DRAW);
		gl.vertexAttribPointer(shader.textureCoordAttribute, 2, webgl.FLOAT, false, 0, 0);
	}

	void render (/*var gl, var shader, var modelMatrix, var modelViewUniform*/) {

		gl.bindBuffer(webgl.ARRAY_BUFFER, vertexBuffer);
		gl.vertexAttribPointer(shader.vertexPositionAttribute, 3, webgl.FLOAT, false, 0, 0);
		gl.bindBuffer(webgl.ARRAY_BUFFER, textureBuffer);
		gl.vertexAttribPointer(shader.textureCoordAttribute, 2, webgl.FLOAT, false, 0, 0);

		Matrix4 modelMatrix = new Matrix4.identity();
		modelMatrix.translate(0.0, 0.0, -3.0);
		gl.uniformMatrix4fv(shader.modelViewLocation, false, modelMatrix.storage);
		gl.drawElements(webgl.TRIANGLES, indices.length, webgl.UNSIGNED_SHORT, 0);
	}

}