part of game;

class Textureasd {

	ImageElement image;
	var texture;

	Textureasd (String file) {

		image = new ImageElement();
		texture = gl.createTexture();

		image.onLoad.listen(
			(e) {
				gl.bindTexture(webgl.TEXTURE_2D, texture);
				gl.texImage2DImage(webgl.TEXTURE_2D, 0, webgl.RGBA, webgl.RGBA, webgl.UNSIGNED_BYTE, image);
				gl.texParameteri(webgl.TEXTURE_2D, webgl.TEXTURE_MIN_FILTER, webgl.NEAREST);
	      		gl.texParameteri(webgl.TEXTURE_2D, webgl.TEXTURE_MAG_FILTER, webgl.NEAREST);
				gl.generateMipmap(webgl.TEXTURE_2D);
				gl.bindTexture(webgl.TEXTURE_2D, null);
				print(file + ' has been loaded');
			}
		);

		image.src = file;
	}

	void bind () {

		gl.activeTexture(webgl.TEXTURE0);
		gl.bindTexture(webgl.TEXTURE_2D, texture);
		gl.uniform1i(gl.getUniformLocation(shader.shaderProgram, 'uSampler'), 0);
	}

}