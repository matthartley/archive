library game;

import 'dart:html';
import 'dart:math' as Math;
import 'dart:typed_data';
import 'dart:web_gl' as webgl;
import 'package:vector_math/vector_math.dart';

part 'src/canvas.dart';
part 'src/texture.dart';
part 'src/buffer.dart';
part 'src/shader.dart';

webgl.RenderingContext gl;
var WIDTH;
var HEIGHT;

var xCoord = 6.0/512.0;
var yCoord = 6.0/512.0;

Shader shader;
Buffer test = new Buffer(
	//gl,
	[
		-0.5, -0.5, 0.0,
		-0.5, 0.5, 0.0,
		0.5, 0.5, 0.0,
		0.5, -0.5, 0.0,

		0.5, -0.5, 0.0,
		0.5, 0.5, 0.0,
		1.5, 0.5, 0.0,
		1.5, -0.5, 0.0,

		-0.5, 0.5, 0.0,
		-0.5, 1.5, 0.0,
		0.5, 1.5, 0.0,
		0.5, 0.5, 0.0,
	],
	[
		0, 1, 2, 0, 2, 3,
		4, 5, 6, 4, 6, 7,
		8, 9, 10, 8, 10, 11,
	],
	[
		1.0, 0.5, 0.5, 1.0,
		0.5, 1.0, 0.5, 1.0,
		0.5, 0.5, 1.0, 1.0,
		1.0, 0.5, 1.0, 1.0,
	],
	[
		0.0, yCoord,
		0.0, 0.0,
		xCoord, 0.0,
		xCoord, yCoord,
		0.0, yCoord,
		0.0, 0.0,
		xCoord, 0.0,
		xCoord, yCoord,
		0.0, yCoord,
		0.0, 0.0,
		xCoord, 0.0,
		xCoord, yCoord,
	]
);





var vertexBuffer;
var indexBuffer;
var colorBuffer;
var textureBuffer;

void buffers () {

//	var vertices = [
//		10.0, 10.0, 0.0,
//		-10.0, 10.0, 0.0,
//		10.0, -10.0, 0.0,
//		-10.0, -10.0, 0.0
//	];
//
//	var colors = [
//		1.0, 0.5, 0.5, 1.0,
//		0.5, 1.0, 0.5, 1.0,
//		0.5, 0.5, 1.0, 1.0,
//		1.0, 0.5, 1.0, 1.0
//	];

	Float32List vertices = new Float32List(4*3);
	vertices.setAll(0*3, [	-0.05, 	-0.05, 0.0]);
	vertices.setAll(1*3, [	-0.05, 	0.05, 0.0]);
	vertices.setAll(2*3, [	0.05, 	0.05, 0.0]);
	vertices.setAll(3*3, [	0.05, 	-0.05, 0.0]);

	Int16List indices = new Int16List(6);
	indices.setAll(0, [0, 1, 2, 0, 2, 3]);

	vertexBuffer = gl.createBuffer();
	gl.bindBuffer(webgl.ARRAY_BUFFER, vertexBuffer);
	gl.bufferDataTyped(webgl.ARRAY_BUFFER, vertices, webgl.STATIC_DRAW);

	indexBuffer = gl.createBuffer();
	gl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, indexBuffer);
	gl.bufferDataTyped(webgl.ELEMENT_ARRAY_BUFFER, indices, webgl.STATIC_DRAW);

	colorBuffer = gl.createBuffer();
	gl.bindBuffer(webgl.ARRAY_BUFFER, colorBuffer);
	Float32List colors = new Float32List(4*4);
	colors.setAll(0*4, [1.0, 0.5, 0.5, 1.0]);
	colors.setAll(1*4, [0.5, 1.0, 0.5, 1.0]);
	colors.setAll(2*4, [0.5, 0.5, 1.0, 1.0]);
	colors.setAll(3*4, [1.0, 0.5, 1.0, 1.0]);
	gl.bufferDataTyped(webgl.ARRAY_BUFFER, colors, webgl.STATIC_DRAW);

//	var xCoord = 6.0/512.0;
//	var yCoord = 6.0/512.0;

	textureBuffer = gl.createBuffer();
	gl.bindBuffer(webgl.ARRAY_BUFFER, textureBuffer);
	Float32List textureCoords = new Float32List(4*2);
	textureCoords.setAll(0*2, [0.0, yCoord]);
	textureCoords.setAll(1*2, [0.0, 0.0]);
	textureCoords.setAll(2*2, [xCoord, 0.0]);
	textureCoords.setAll(3*2, [xCoord, yCoord]);
	print('x ' + xCoord.toString() + ', y ' + yCoord.toString());
	gl.bufferDataTyped(webgl.ARRAY_BUFFER, textureCoords, webgl.STATIC_DRAW);

	//56
	//132
}

var textVertexBuffer;
var textIndexBuffer;
var textTextureBuffer;

void textBuffer () {

	Float32List vertices = new Float32List(4*3);
	vertices.setAll(0*3, [	-2.25, 	-0.5, 0.0]);
	vertices.setAll(1*3, [	-2.25, 	0.5, 0.0]);
	vertices.setAll(2*3, [	2.25, 	0.5, 0.0]);
	vertices.setAll(3*3, [	2.25, 	-0.5, 0.0]);

	Int16List indices = new Int16List(6);
	indices.setAll(0, [0, 1, 2, 0, 2, 3]);

	textVertexBuffer = gl.createBuffer();
	gl.bindBuffer(webgl.ARRAY_BUFFER, textVertexBuffer);
	gl.bufferDataTyped(webgl.ARRAY_BUFFER, vertices, webgl.STATIC_DRAW);

	textIndexBuffer = gl.createBuffer();
	gl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, textIndexBuffer);
	gl.bufferDataTyped(webgl.ELEMENT_ARRAY_BUFFER, indices, webgl.STATIC_DRAW);

	var xCoordStart = 0.0;
	var yCoordStart = 8.0/512.0;
	var xCoordEnd = 288.0/512.0;
	var yCoordEnd = 72.0/512.0;

	textTextureBuffer = gl.createBuffer();
	gl.bindBuffer(webgl.ARRAY_BUFFER, textTextureBuffer);
	Float32List textureCoords = new Float32List(4*2);
	textureCoords.setAll(0*2, [xCoordStart, yCoordEnd]);
	textureCoords.setAll(1*2, [xCoordStart, yCoordStart]);
	textureCoords.setAll(2*2, [xCoordEnd, yCoordStart]);
	textureCoords.setAll(3*2, [xCoordEnd, yCoordEnd]);
	//print('x ' + xCoord.toString() + ', y ' + yCoord.toString());
	gl.bufferDataTyped(webgl.ARRAY_BUFFER, textureCoords, webgl.STATIC_DRAW);
}

//var santa;
//var santaImage;
var sheet;
//ImageElement sheetImage;
//
//void textures () {
//
//	sheetImage = new ImageElement();
//	sheet = gl.createTexture();
//	sheetImage.onLoad.listen(
//		(e) {
//			gl.bindTexture(webgl.TEXTURE_2D, sheet);
//			gl.texImage2DImage(webgl.TEXTURE_2D, 0, webgl.RGBA, webgl.RGBA, webgl.UNSIGNED_BYTE, sheetImage);
//			gl.texParameteri(webgl.TEXTURE_2D, webgl.TEXTURE_MIN_FILTER, webgl.NEAREST);
//      		gl.texParameteri(webgl.TEXTURE_2D, webgl.TEXTURE_MAG_FILTER, webgl.NEAREST);
//			gl.generateMipmap(webgl.TEXTURE_2D);
//			gl.bindTexture(webgl.TEXTURE_2D, null);
//			print('IMAGE LOADED!');
//		}
//	);
//
//	sheetImage.src = 'img/sheet.png';
//}

var rand = new Math.Random();

class Snow {

	double x;
	double y;
	double z;

	double xOffset = 0.0;
	int cooldown = 0;

	Snow (xx, yy, zz) {

		x = xx;
		y = yy;
		z = zz;
	}

	void render () {

		Matrix4 modelMatrix = new Matrix4.identity();
		modelMatrix.translate(x, y, z);
		var modelViewUniform = gl.getUniformLocation(shader.shaderProgram, 'modelViewMatrix');
		gl.uniformMatrix4fv(modelViewUniform, false, modelMatrix.storage);
		gl.drawElements(webgl.TRIANGLES, 6, webgl.UNSIGNED_SHORT, 0);
	}

	void tick () {

		if(cooldown <= 0) {
			cooldown = rand.nextInt(100) + 50;
			xOffset = (rand.nextDouble() * 0.01) - 0.005;
		}

		y -= 0.01;
		x += xOffset;
		cooldown--;
	}

}

var rotation = 0.0;
int frames = 0;
var lastTime = new DateTime.now().millisecondsSinceEpoch;
var snows = new List<Snow>();

void tick () {

	for(int i = 0; i < snows.length; i++) {
		snows.elementAt(i).tick();
		if(snows.elementAt(i).y < -10.0) {
			snows.removeAt(i);
			i--;
		}
	}

	snows.add(new Snow( (rand.nextDouble()*20.0)-10.0, 10.0, -(rand.nextDouble()*20.0) ));
}

var nanoTime = new DateTime.now().millisecondsSinceEpoch;
var unprocessed = 0.0;
var msPerTick = 1000.0/60.0;
int ticks = 0;

void render (double time) {

	//print('time ' + time.toString());

	rotation++;
	frames++;

	if(new DateTime.now().millisecondsSinceEpoch - lastTime > 1000) {
		print('frames ' + frames.toString() + ', ticks ' + ticks.toString() + ', snows ' + snows.length.toString());
		lastTime = new DateTime.now().millisecondsSinceEpoch;
		frames = 0;
		ticks = 0;
	}

	unprocessed += (new DateTime.now().millisecondsSinceEpoch - nanoTime) / msPerTick;
	nanoTime = new DateTime.now().millisecondsSinceEpoch;

	while(unprocessed >= 1.0) {
		unprocessed--;
		tick();
		ticks++;
	}

	gl.clear(webgl.COLOR_BUFFER_BIT | webgl.DEPTH_BUFFER_BIT);
	gl.clearColor(0.0, 1.0, 1.0, 1.0);
	Matrix4 viewMatrix = makePerspectiveMatrix(70, WIDTH/HEIGHT, 0.1, 100.0);
	Matrix4 modelMatrix = new Matrix4.identity();

	gl.bindBuffer(webgl.ARRAY_BUFFER, vertexBuffer);
	gl.vertexAttribPointer(shader.vertexPositionAttribute, 3, webgl.FLOAT, false, 0, 0);

	gl.bindBuffer(webgl.ARRAY_BUFFER, textureBuffer);
	gl.vertexAttribPointer(shader.textureCoordAttribute, 2, webgl.FLOAT, false, 0, 0);

	//gl.bindBuffer(webgl.ARRAY_BUFFER, colorBuffer);
	//gl.vertexAttribPointer(vertex);

	//modelMatrix.rotateZ( 45.0 * (Math.PI / 180.0) );
	//modelMatrix.rotateY( -20.0 * (Math.PI / 180.0) );

	modelMatrix.translate(0.0, 0.0, -3.0);
	modelMatrix.rotate(new Vector3(0.0, 1.0, 0.0), rotation * (Math.PI / 180.0));

	var modelViewUniform = gl.getUniformLocation(shader.shaderProgram, 'modelViewMatrix');
	gl.uniformMatrix4fv(modelViewUniform, false, modelMatrix.storage);

	var viewUniform = gl.getUniformLocation(shader.shaderProgram, 'viewMatrix');
	gl.uniformMatrix4fv(viewUniform, false, viewMatrix.storage);

	gl.activeTexture(webgl.TEXTURE0);
	gl.bindTexture(webgl.TEXTURE_2D, sheet);
	gl.uniform1i(gl.getUniformLocation(shader.shaderProgram, 'uSampler'), 0);

	//gl.drawArrays(webgl.TRIANGLE_STRIP, 0, 4);

	//gl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, indexBuffer);
	//gl.drawElements(webgl.TRIANGLES, 6, webgl.UNSIGNED_SHORT, 0);

	for(int i = 0; i < snows.length; i++) {
		snows.elementAt(i).render();
	}


	//TEXT
	gl.bindBuffer(webgl.ARRAY_BUFFER, textVertexBuffer);
	gl.vertexAttribPointer(shader.vertexPositionAttribute, 3, webgl.FLOAT, false, 0, 0);
	gl.bindBuffer(webgl.ARRAY_BUFFER, textTextureBuffer);
	gl.vertexAttribPointer(shader.textureCoordAttribute, 2, webgl.FLOAT, false, 0, 0);

	modelMatrix = new Matrix4.identity();
	modelMatrix.translate(0.0, 0.0, -4.0);
	modelViewUniform = gl.getUniformLocation(shader.shaderProgram, 'modelViewMatrix');
	gl.uniformMatrix4fv(modelViewUniform, false, modelMatrix.storage);
	gl.drawElements(webgl.TRIANGLES, 6, webgl.UNSIGNED_SHORT, 0);

	test.render(/*gl, shader, modelMatrix, modelViewUniform*/);

	window.requestAnimationFrame(render);
}

void main() {

	CanvasElement canvas;
	canvas = querySelector('#canvas');
	gl = canvas.getContext('webgl');
	if(gl == null) gl = canvas.getContext('experimental-webgl');
	if(gl == null) print('Failed to create gl context!');

	if(gl != null) print('gl context created!');

	WIDTH = canvas.width;
	HEIGHT = canvas.height;
	gl.viewport(0, 0, canvas.width, canvas.height);

	gl.enable(webgl.DEPTH_TEST);
	gl.depthFunc(webgl.LESS);
	gl.blendFunc(webgl.SRC_ALPHA, webgl.ONE_MINUS_SRC_ALPHA);
	//gl.blendEquation(webgl.ONE_MINUS_SRC_ALPHA);
	gl.enable(webgl.BLEND);
	//gl.colorMask(true, true, true, true);



	//gl.colorMask(true, true, true, false);
	gl.clear(webgl.COLOR_BUFFER_BIT | webgl.DEPTH_BUFFER_BIT);

	//textures();
	sheet = new Textureasd('img/sheet.png');
	shader = new Shader(gl);
	buffers();
	textBuffer();

	for(int i = 0; i < 1000; i++) {
		snows.add(new Snow( (rand.nextDouble()*20.0)-10.0, (rand.nextDouble()*20.0)-10.0, -(rand.nextDouble()*20.0) ));
	}

	window.requestAnimationFrame(render);
}


